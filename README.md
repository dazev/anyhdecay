# AnyHdecay

A modern C++17 library RIPOFF FROM JONAS that wraps the non-supersymmetric **HDECAY** variants.

The intent of this rep is to extend with new models if necessary

**Grab the latest [release] or take a look at the [documentation].**

## References
This library was written by Jonas Wittbrodt. If you find it useful feel free to
cite this webpage.

**The original HDECAY is currently being developed again. To avoid frequent breaks, the HDECAY version used in AnyHdecay for the SM and 2HDM has been frozen to 6.60 for now.**

If you use this library please always cite the original codes:

1. **[HDECAY][]:**  
    - *HDECAY: A Program for Higgs boson decays in the standard model and its
      supersymmetric extension*                                                 <br/>
      Abdelhak Djouadi, Jan Kalinowski, Michael Spira                           <br/>
      Comput.Phys.Commun. 108 (1998) 56-74, [hep-ph/9704448]  
    - *An Update of the Program HDECAY* in *THE TOOLS AND MONTE CARLO WORKING
      GROUP Summary Report from the Les Houches 2009 Workshop on TeV Colliders* <br/>
      Abdelhak Djouadi, Jan Kalinowski, Margarete Mühlleitner, Michael Spira    <br/>
      [arXiv:1003.1643]
    - *HDECAY: Twenty++ years after*                                            <br/>
      Abdelhak Djouadi, Jan Kalinowski, Margarete Mühlleitner, Michael Spira    <br/>
      Comput.Phys.Commun. 238 (2019) 214-231, [arXiv:1801.09506]
2. **[sHDECAY][]:**
    - *Singlet Extensions of the Standard Model at LHC Run 2: Benchmarks and
      Comparison with the NMSSM*                                                <br/>
      Raul Costa, Margarete Mühlleitner, Marco O. P. Sampaio, Rui Santos        <br/>
      JHEP 1606 (2016) 034, [arXiv:1512.05355]                                  <br/>
3. **[N2HDECAY][]:**
    - *The N2HDM under Theoretical and Experimental Scrutiny*                   <br/>
      Margarete Mühlleitner, Marco O. P. Sampaio, Rui Santos, Jonas Wittbrodt   <br/>
      JHEP 1703 (2017) 094, [arXiv:1612.01309]
    - *N2HDECAY: Higgs Boson Decays in the Different Phases of the N2HDM*       <br/>
      Isabell Engeln, Margarete Mühlleitner, Jonas Wittbrodt                    <br/>
      Comput.Phys.Commun. 234 (2019) 256-262, [arXiv:1805.00966] 
4. **[C2HDM_HDECAY][]:**
    - *The C2HDM revisited*                                                     <br/>
      Duarte Fontes, Margarete Mühlleitner, Jorge C. Romão, Rui Santos, João P. 
      Silva, Jonas Wittbrodt                                                    <br/>
      JHEP 1802 (2018) 073, [arXiv:1711.09419]

## Compilation

### Prerequisites
   - a Fortran95 compliant Fortran compiler (e.g. gfortran)
   - a C++17 compliant compiler (e.g. gcc-7, clang-5 or newer)
   - CMake 3.11+. If your distribution does not provide a sufficiently new
     CMake, grab the newest version from [here][cmake-download].

### Compilation
The library is compiled using CMake, i.e.
```bash
mkdir build && cd build
cmake ..
make
```
In the `cmake` step CMake will automatically download the newest versions of the
codes listed above. These are then preprocessed in a way that allows linking
them into one library (this involves extracting only the needed subroutines from
each code and renaming some of them to avoid name clashes).

This results in a single library `libanyhdecay.a` located in the `lib` subfolder
of the build directory. All of the functionality is provided by the `Hdecay`
class in `include/AnyHdecay.hpp`. 

CMake exports the relevant targets, so any project using cmake can link against
AnyHdecay by:
```cmake
find_package(AnyHdecay)
# ...
target_link_libraries(yourTarget PRIVATE AnyHdecay::anyhdecay)
```
To link AnyHdecay manually, use `-L/path/to/anyhdecay/lib -lanyhdecay` as well
as the Fortran interface libraries required on your platform (typically
`-lgfortran -lquadmath -lm`).

### Tests
A test suite that - among other things - checks that the results of the library
math tabulated results of the separate codes is included and can be executed
through `make check` or (for more detailed results) `ctest --output-on-failure`.
Note that the test suite will take a few minutes to complete.


## Documentation
The header files of the library are fully documented and detailed html
documentation is available [here][documentation].

It can also be generated locally using [Doxygen][]:
```
make doc
```
will create the documentation which can be accessed through `html/index.html`.

[HDECAY]: http://tiger.web.psi.ch/hdecay
[hep-ph/9704448]: https://inspirehep.net/record/442662
[arXiv:1801.09506]: https://inspirehep.net/record/1650944
[arXiv:1003.1643]: https://inspirehep.net/record/848006
[sHdecay]: https://www.itp.kit.edu/~maggie/sHDECAY/
[arXiv:1512.05355]: https://inspirehep.net/record/1410072
[N2HDECAY]: https://gitlab.com/jonaswittbrodt/N2HDECAY
[arXiv:1612.01309]: http://inspirehep.net/record/1501710
[arXiv:1805.00966]: http://inspirehep.net/record/1671515
[C2HDM_HDECAY]: https://www.itp.kit.edu/~maggie/C2HDM/
[arXiv:1711.09419]: https://inspirehep.net/record/1639021
[cmake-download]: https://cmake.org/download/
[Doxygen]: http://www.doxygen.nl/
[documentation]: https://jonaswittbrodt.gitlab.io/anyhdecay
[release]: https://gitlab.com/jonaswittbrodt/anyhdecay/releases
[pipeline status]: https://gitlab.com/jonaswittbrodt/anyhdecay/badges/master/pipeline.svg
