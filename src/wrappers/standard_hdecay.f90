subroutine hdecay_2hdm(type2HDM, tgbet, m12sq, &
                       mA, mhc, mHh, mHl, &
                       alpha, &
                       smparams, &
                       BR) bind(c, name='hdecay_2HDM')
    use, intrinsic :: iso_c_binding

    implicit none
    integer(kind=c_int), intent(in), value :: TYPE2HDM
    real(kind=c_double), intent(in), value :: TGBET, M12SQ, &
                                              mA, mhc, mHh, mHl, &
                                              alpha
    real(kind=c_double), intent(in) ::  smparams(22)
    real(kind=c_double), intent(out) :: BR(63)

! common block variables from read_hdec
    double precision ams, amc, amb, amt
    double precision AMSB
    double precision amcb, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0, IHIGGS, NNLO
    integer IPOLE, IONSH, IONWZ, IOFSUSY, NFGG
    double precision TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM
    integer ITYPE2HDM, I2HDM, IPARAM2HDM
    integer IOELW
    double precision AMTP, AMBP, AMNUP, AMEP
    integer ISM4, IGGELW
    integer IFERMPHOB
    integer IMODEL
    integer INDIDEC
    double precision CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA
    integer ICOUPELW
    double precision AXMPL, AXMGD
    integer IGOLD

! MMM changed 28/2/23
    double precision glteff,glbeff,gllepeff,ghteff,ghbeff, &
        ghlepeff,gateff,gabeff,galepeff,I2HDMEFF
! end MMM changed 28/2/23

    COMMON/MASSES_HDEC/AMS, AMC, AMB, AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/PARAM_HDEC/GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    COMMON/CKMPAR_HDEC/VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    COMMON/WZWDTH_HDEC/GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    COMMON/ALS_HDEC/XLAMBDA, AMC0, AMB0, AMT0, N0
    COMMON/FLAG_HDEC/IHIGGS, NNLO, IPOLE
    COMMON/ONSHELL_HDEC/IONSH, IONWZ, IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/THDM_HDEC/TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM, ITYPE2HDM, I2HDM, IPARAM2HDM
    COMMON/OMIT_ELW_HDEC/IOELW
    COMMON/SM4_HDEC/AMTP, AMBP, AMNUP, AMEP, ISM4, IGGELW
    COMMON/FERMIOPHOBIC_HDEC/IFERMPHOB
    COMMON/MODEL_HDEC/IMODEL
    COMMON/FLAGS_HDEC/INDIDEC
    COMMON/CPSM_HDEC/CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA, ICOUPELW
    COMMON/GOLDST_HDEC/AXMPL, AXMGD, IGOLD

! MMM changed 28/2/23
    common/EFF2HDMCOUP/glteff,glbeff,gllepeff,ghteff,ghbeff, &
        ghlepeff,gateff,gabeff,galepeff,I2HDMEFF
! end MMM changed 28/2/23

! local variables
    double precision alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber

! common block variables from WRITE_HDEC
    double precision ABRB, ABRL, ABRM, ABRS, ABRC, ABRT, ABRG, ABRGA, &
        ABRZGA, ABRZ, AWDTH
    double precision HCBRB, HCBRL, HCBRM, HCBRBU, HCBRS, HCBRC, HCBRT, &
        HCBRW, HCBRA, HCWDTH
    double precision HLBRB, HLBRL, HLBRM, HLBRS, HLBRC, HLBRT, HLBRG, &
        HLBRGA, HLBRZGA, HLBRW, HLBRZ, HLBRA, HLBRAZ, HLBRHW, &
        HLWDTH
    double precision HHBRB, HHBRL, HHBRM, HHBRS, HHBRC, HHBRT, HHBRG, &
        HHBRGA, HHBRZGA, HHBRW, HHBRZ, HHBRH, HHBRA, HHBRAZ, &
        HHBRHW, HHWDTH
    double precision hcbrcd, hcbrts, hcbrtd
    double precision hcbrwhh, hhbrchch, hlbrchch, abrhhaz, abrhawphm
    COMMON/WIDTHA_HDEC/ABRB, ABRL, ABRM, ABRS, ABRC, ABRT, ABRG, ABRGA, &
        ABRZGA, ABRZ, AWDTH
    COMMON/WIDTHHL_HDEC/HLBRB, HLBRL, HLBRM, HLBRS, HLBRC, HLBRT, HLBRG, &
        HLBRGA, HLBRZGA, HLBRW, HLBRZ, HLBRA, HLBRAZ, HLBRHW, &
        HLWDTH
    COMMON/WIDTHHH_HDEC/HHBRB, HHBRL, HHBRM, HHBRS, HHBRC, HHBRT, HHBRG, &
        HHBRGA, HHBRZGA, HHBRW, HHBRZ, HHBRH, HHBRA, HHBRAZ, &
        HHBRHW, HHWDTH
    COMMON/WIDTHHC_HDEC/HCBRB, HCBRL, HCBRM, HCBRBU, HCBRS, HCBRC, HCBRT, &
        HCBRW, HCBRA, HCWDTH
    COMMON/WIDTH_HC_ADD/hcbrcd, hcbrts, hcbrtd
    COMMON/WIDTH_2HDM/hcbrwhh, hhbrchch, hlbrchch, abrhhaz, abrhawphm

    I2HDMEFF = 0
    IHIGGS = 5
    IOELW = 0
    ISM4 = 0
    IGGELW = 0
    IFERMPHOB = 0
    I2HDM = 1
    IMODEL = 0

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    Iparam2hdm = 1
    Itype2hdm = type2HDM
    tgbet2hdm = TGBET
    AM12SQ = m12sq
    ALPH2HDM = alpha
    AMHL2HDM = mHl
    AMHH2HDM = mHh
    AMHA2HDM = mA
    AMHC2HDM = mhc

    NNLO = 1
    IONSH = 0
    IONWZ = 0
    IPOLE = 0
    IOFSUSY = 1
    NFGG = 5
    IGOLD = 0
    INDIDEC = 0
    ICOUPELW = 0

! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)

    CALL HDEC_2HDM(tgbet)

    BR = (/HLWDTH, HLBRB, HLBRL, HLBRM, HLBRS, HLBRC, HLBRT, &
           HLBRG, HLBRGA, HLBRZGA, HLBRW, HLBRZ, &
           HLBRAZ, HLBRHW, &
           HLBRA, HLBRCHCH, &
           HHWDTH, HHBRB, HHBRL, HHBRM, HHBRS, HHBRC, HHBRT, &
           HHBRG, HHBRGA, HHBRZGA, HHBRW, HHBRZ, &
           HHBRAZ, HHBRHW, &
           HHBRH, HHBRA, HHBRCHCH, &
           awdth, abrb, abrl, abrm, abrs, abrc, abrt, abrg, &
           abrga, abrzga, ABRZ, ABRHHAZ, &
           ABRHAWPHM, &
           hcwdth, hcbrb, hcbrl, hcbrm, hcbrs, hcbrc, hcbrt, &
           hcbrcd, hcbrbu, hcbrts, hcbrtd, &
           HCBRW, HCBRWHH, hcbra, &
           gamt1, gamt0/gamt1, (gamt1 - gamt0)/gamt1/)

    return
end

!   ugly hack for the eft version of the 2hdm
subroutine hdecay_2hdm_eft(type2HDM, tgbet, m12sq, &
                            mA, mhc, mHh, mHl, &
                            alpha, &
                            lambdas, yukawas_mod,&
                            smparams, &
                            BR) bind(c, name='hdecay_2HDM_eft')
    use, intrinsic :: iso_c_binding

    implicit none
    integer(kind=c_int), intent(in), value :: TYPE2HDM
    real(kind=c_double), intent(in), value :: TGBET, M12SQ, &
                                              mA, mhc, mHh, mHl, &
                                              alpha
    real(kind=c_double), intent(in) ::  lambdas(5)
    real(kind=c_double), intent(in) ::  yukawas_mod(9)
    real(kind=c_double), intent(in) ::  smparams(22)
    real(kind=c_double), intent(out) :: BR(63)

! common block variables from read_hdec
    double precision ams, amc, amb, amt
    double precision AMSB
    double precision amcb, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0, IHIGGS, NNLO
    integer IPOLE, IONSH, IONWZ, IOFSUSY, NFGG
    double precision TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM
    integer ITYPE2HDM, I2HDM, IPARAM2HDM
    integer IOELW
    double precision AMTP, AMBP, AMNUP, AMEP
    integer ISM4, IGGELW
    integer IFERMPHOB
    integer IMODEL
    integer INDIDEC
    double precision CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA
    integer ICOUPELW
    double precision AXMPL, AXMGD
    integer IGOLD

! MMM changed 28/2/23
    double precision glteff,glbeff,gllepeff,ghteff,ghbeff, &
        ghlepeff,gateff,gabeff,galepeff,I2HDMEFF
! end MMM changed 28/2/23

    COMMON/MASSES_HDEC/AMS, AMC, AMB, AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/PARAM_HDEC/GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    COMMON/CKMPAR_HDEC/VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    COMMON/WZWDTH_HDEC/GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    COMMON/ALS_HDEC/XLAMBDA, AMC0, AMB0, AMT0, N0
    COMMON/FLAG_HDEC/IHIGGS, NNLO, IPOLE
    COMMON/ONSHELL_HDEC/IONSH, IONWZ, IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/THDM_HDEC/TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM, ITYPE2HDM, I2HDM, IPARAM2HDM
    COMMON/OMIT_ELW_HDEC/IOELW
    COMMON/SM4_HDEC/AMTP, AMBP, AMNUP, AMEP, ISM4, IGGELW
    COMMON/FERMIOPHOBIC_HDEC/IFERMPHOB
    COMMON/MODEL_HDEC/IMODEL
    COMMON/FLAGS_HDEC/INDIDEC
    COMMON/CPSM_HDEC/CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA, ICOUPELW
    COMMON/GOLDST_HDEC/AXMPL, AXMGD, IGOLD
! MMM changed 28/2/23
    common/EFF2HDMCOUP/glteff,glbeff,gllepeff,ghteff,ghbeff, &
        ghlepeff,gateff,gabeff,galepeff,I2HDMEFF
! end MMM changed 28/2/23
! local variables
    double precision alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber

! common block variables from WRITE_HDEC
    double precision ABRB, ABRL, ABRM, ABRS, ABRC, ABRT, ABRG, ABRGA, &
        ABRZGA, ABRZ, AWDTH
    double precision HCBRB, HCBRL, HCBRM, HCBRBU, HCBRS, HCBRC, HCBRT, &
        HCBRW, HCBRA, HCWDTH
    double precision HLBRB, HLBRL, HLBRM, HLBRS, HLBRC, HLBRT, HLBRG, &
        HLBRGA, HLBRZGA, HLBRW, HLBRZ, HLBRA, HLBRAZ, HLBRHW, &
        HLWDTH
    double precision HHBRB, HHBRL, HHBRM, HHBRS, HHBRC, HHBRT, HHBRG, &
        HHBRGA, HHBRZGA, HHBRW, HHBRZ, HHBRH, HHBRA, HHBRAZ, &
        HHBRHW, HHWDTH
    double precision hcbrcd, hcbrts, hcbrtd
    double precision hcbrwhh, hhbrchch, hlbrchch, abrhhaz, abrhawphm
    COMMON/WIDTHA_HDEC/ABRB, ABRL, ABRM, ABRS, ABRC, ABRT, ABRG, ABRGA, &
        ABRZGA, ABRZ, AWDTH
    COMMON/WIDTHHL_HDEC/HLBRB, HLBRL, HLBRM, HLBRS, HLBRC, HLBRT, HLBRG, &
        HLBRGA, HLBRZGA, HLBRW, HLBRZ, HLBRA, HLBRAZ, HLBRHW, &
        HLWDTH
    COMMON/WIDTHHH_HDEC/HHBRB, HHBRL, HHBRM, HHBRS, HHBRC, HHBRT, HHBRG, &
        HHBRGA, HHBRZGA, HHBRW, HHBRZ, HHBRH, HHBRA, HHBRAZ, &
        HHBRHW, HHWDTH
    COMMON/WIDTHHC_HDEC/HCBRB, HCBRL, HCBRM, HCBRBU, HCBRS, HCBRC, HCBRT, &
        HCBRW, HCBRA, HCWDTH
    COMMON/WIDTH_HC_ADD/hcbrcd, hcbrts, hcbrtd
    COMMON/WIDTH_2HDM/hcbrwhh, hhbrchch, hlbrchch, abrhhaz, abrhawphm

    I2HDMEFF = 1
    IHIGGS = 5
    IOELW = 0
    ISM4 = 0
    IGGELW = 0
    IFERMPHOB = 0
    I2HDM = 1
    IMODEL = 0

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    A1LAM2HDM = lambdas(1)
    A2LAM2HDM = lambdas(2)
    A3LAM2HDM = lambdas(3)
    A4LAM2HDM = lambdas(4)
    A5LAM2HDM = lambdas(5)

    glteff = yukawas_mod(1)
    glbeff = yukawas_mod(2)
    gllepeff = yukawas_mod(3)
    ghteff = yukawas_mod(4)
    ghbeff = yukawas_mod(5)
    ghlepeff = yukawas_mod(6)
    gateff = yukawas_mod(7)
    gabeff = yukawas_mod(8)
    galepeff = yukawas_mod(9)

    Iparam2hdm = 1
    Itype2hdm = type2HDM
    tgbet2hdm = TGBET
    AM12SQ = m12sq
    ALPH2HDM = alpha
    AMHL2HDM = mHl
    AMHH2HDM = mHh
    AMHA2HDM = mA
    AMHC2HDM = mhc

    NNLO = 1
    IONSH = 0
    IONWZ = 0
    IPOLE = 0
    IOFSUSY = 1
    NFGG = 5
    IGOLD = 0
    INDIDEC = 0
    ICOUPELW = 0

! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)

    CALL HDEC_2HDM_EFT(tgbet)

    BR = (/HLWDTH, HLBRB, HLBRL, HLBRM, HLBRS, HLBRC, HLBRT, &
           HLBRG, HLBRGA, HLBRZGA, HLBRW, HLBRZ, &
           HLBRAZ, HLBRHW, &
           HLBRA, HLBRCHCH, &
           HHWDTH, HHBRB, HHBRL, HHBRM, HHBRS, HHBRC, HHBRT, &
           HHBRG, HHBRGA, HHBRZGA, HHBRW, HHBRZ, &
           HHBRAZ, HHBRHW, &
           HHBRH, HHBRA, HHBRCHCH, &
           awdth, abrb, abrl, abrm, abrs, abrc, abrt, abrg, &
           abrga, abrzga, ABRZ, ABRHHAZ, &
           ABRHAWPHM, &
           hcwdth, hcbrb, hcbrl, hcbrm, hcbrs, hcbrc, hcbrt, &
           hcbrcd, hcbrbu, hcbrts, hcbrtd, &
           HCBRW, HCBRWHH, hcbra, &
           gamt1, gamt0/gamt1, (gamt1 - gamt0)/gamt1/)

    return
end

subroutine hdecay_sm(mH, smparams, BR) bind(C, name='hdecay_SM')
    use, intrinsic :: iso_c_binding

    implicit none
    real(kind=c_double), intent(in), value :: mH
    real(kind=c_double), intent(in) ::  smparams(22)
    real(kind=c_double), intent(out) :: BR(12)

!  common blocks from read_hdec
    double precision AMSM, AMA, AML, AMH, AMCH, AMAR
    integer IOELW
    integer IHIGGS, NNLO, IPOLE
    double precision AMTP, AMBP, AMNUP, AMEP
    integer ISM4, IGGELW
    integer IFERMPHOB
    integer IMODEL
    integer IONSH, IONWZ, IOFSUSY
    integer NFGG
    double precision TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM
    integer ITYPE2HDM, I2HDM, IPARAM2HDM
    double precision AXMPL, AXMGD
    integer IGOLD
    integer INDIDEC
    double precision CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA
    integer ICOUPELW
    double precision AMS, AMC, AMB, AMT
    double precision AMSB
    double precision AMCB, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0
    COMMON/HMASS_HDEC/AMSM, AMA, AML, AMH, AMCH, AMAR
    COMMON/OMIT_ELW_HDEC/IOELW
    COMMON/FLAG_HDEC/IHIGGS, NNLO, IPOLE
    COMMON/SM4_HDEC/AMTP, AMBP, AMNUP, AMEP, ISM4, IGGELW
    COMMON/FERMIOPHOBIC_HDEC/IFERMPHOB
    COMMON/MODEL_HDEC/IMODEL
    COMMON/ONSHELL_HDEC/IONSH, IONWZ, IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/THDM_HDEC/TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM, ITYPE2HDM, I2HDM, IPARAM2HDM
    COMMON/GOLDST_HDEC/AXMPL, AXMGD, IGOLD
    COMMON/FLAGS_HDEC/INDIDEC
    COMMON/CPSM_HDEC/CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA, ICOUPELW
    COMMON/MASSES_HDEC/AMS, AMC, AMB, AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/PARAM_HDEC/GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    COMMON/CKMPAR_HDEC/VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    COMMON/WZWDTH_HDEC/GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    COMMON/ALS_HDEC/XLAMBDA, AMC0, AMB0, AMT0, N0

! local variables
    double precision alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber

! commons from write_hdec
    double precision SMBRB, SMBRL, SMBRM, SMBRS, SMBRC, SMBRT, SMBRG, &
        SMBRGA, SMBRZGA, SMBRW, SMBRZ, SMWDTH
    COMMON/WIDTHSM_HDEC/SMBRB, SMBRL, SMBRM, SMBRS, SMBRC, SMBRT, SMBRG, &
        SMBRGA, SMBRZGA, SMBRW, SMBRZ, SMWDTH

    IHIGGS = 0
    IOELW = 0
    ISM4 = 0
    IGGELW = 0
    IFERMPHOB = 0
    I2HDM = 0
    IMODEL = 0
    NNLO = 1
    IONSH = 0
    IONWZ = 0
    IPOLE = 0
    IOFSUSY = 1
    NFGG = 5
    IGOLD = 0
    INDIDEC = 0
    ICOUPELW = 0
    CPW = 1.
    CPZ = 1.
    CPTAU = 1.
    CPMU = 1.
    CPT = 1.
    CPB = 1.
    CPC = 1.
    CPS = 1.
    CPGAGA = 0.
    CPGG = 0.
    CPZGA = 0.

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    AMSM = mH

! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)

    CALL HDEC_2HDM(1.D0)

    BR = (/SMWDTH, SMBRB, SMBRL, SMBRM, SMBRS, SMBRC, SMBRT, SMBRG, &
           SMBRGA, SMBRZGA, SMBRW, SMBRZ/)

    return
end
