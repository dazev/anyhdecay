#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void hdecay_SM(double mH, const double smparams[22], double BR[12]);

void hdecay_2HDM(int type2HDM, double tbeta, double m12sq, double mA,
                 double mHc, double mHh, double mHl, double alpha,
                 const double smparams[22], double BR[63]);

void hdecay_2HDM_eft(int type2HDM, double tbeta, double m12sq, double mA,
                 double mHc, double mHh, double mHl, double alpha,
                 double lambdas[5], double yukawas_mod[9],
                 const double smparams[22], double BR[63]);

void hdecay_N2HDM_broken(int type2HDM, double tbeta, double m12sq, double mA,
                         double mHc, double mH1, double mH2, double mH3,
                         double a1, double a2, double a3, double vs,
                         const double smparams[22], double BR[84]);

void hdecay_N2HDM_darks(int type2HDM, double tbeta, double m12sq, double mA,
                        double mHc, double mHD, double mH1, double mH2,
                        double alpha, double L7, double L8,
                        const double smparams[22], double BR[65]);

void hdecay_N2HDM_darkd(double mA, double mHc, double mDM, double mHl,
                        double mHh, double alpha, double L7, double m22sq,
                        double vs, const double smparams[22], double BR[40]);

void hdecay_N2HDM_darksd(double mA, double mhc, double mHsm, double mHDD,
                         double mHDS, double m22sq, double mssq,
                         const double smparams[22], double BR[25]);

void hdecay_RxSM_broken(double alpha, double mH1, double mH2, double vs,
                        const double smparams[22], double BR[26]);

void hdecay_RxSM_dark(double mH1, double mD, double m2sq,
                      const double smparams[22], double BR[13]);

void hdecay_CxSM_broken(double alpha1, double alpha2, double alpha3, double m1,
                        double m3, double vs, const double smparams[22],
                        double BR[45]);

void hdecay_CxSM_dark(double alpha1, double mH1, double mH2, double mD,
                      double vs, double a1, const double smparams[22],
                      double BR[30]);

void hdecay_C2HDM(int type2HDM, double tbeta, double re_m12sq, double mHc,
                  double mH1, double mH2, double a1, double a2, double a3,
                  const double smparams[22], double BR[66]);

#ifdef __cplusplus
}
#endif
