!     Extracted from hdecay 6.60 on 12.03.2021
      subroutine twohdmcp_hdec(tgbet)
      implicit double precision (a-h,m,o-z)
      double precision la1,la2,la3,la4,la5,la6,la7,la3t
      complex*16 f0_hdec
      dimension mst(2),gltt(2,2),ghtt(2,2),
     .          msb(2),glbb(2,2),ghbb(2,2)
      dimension itest(30)
! MMM changed 28/2/23
      double precision glteff,glbeff,gllepeff,ghteff,ghbeff,
     .             ghlepeff,gateff,gabeff,galepeff,I2HDMEFF
! end MMM changed 28/2/23
      common/flag_hdec/ihiggs,nnlo,ipole
      common/model_hdec/imodel
      common/masses_hdec/ams,amc,amb,amt
      common/hmass_hdec/amsm,ama,aml,amh,amch,amar
      common/hmassr_hdec/amlr,amhr
      common/chimass_hdec/amchi
      common/hself_hdec/la1,la2,la3,la4,la5,la6,la7
      common/break_hdec/amel,amer,amsq,amur,amdr,al,au,ad,amu,am2
      common/breakscale_hdec/susyscale
      common/breakglu_hdec/amglu
      common/sfer1st_hdec/amql1,amur1,amdr1,amel1,amer1
      common/param_hdec/gf,alph,amtau,ammuon,amz,amw
      common/coup_hdec/gat,gab,glt,glb,ght,ghb,gzah,gzal,
     .            ghhh,glll,ghll,glhh,ghaa,glaa,glvv,ghvv,
     .            glpm,ghpm,b,a
      common/gluino_hdec/amgluino,amsb1,amsb2,sthb,cthb,
     .              xlbb(2,2),xhbb(2,2),xabb(2,2),
     .              amst1,amst2,stht,ctht,
     .              xltt(2,2),xhtt(2,2),xatt(2,2)
      common/als_hdec/xlambda,amc0,amb0,amt0,n0
      common/slha_vals_hdec/islhai,islhao
      common/slha_hmass_hdec/slhaml,slhamh,slhamc,slha_alpha
      common/slha_gaug_hdec/slhaneut(4),slhaxneut(4),slhachar(2),
     .          slhau(2,2),slhav(2,2),slhaz(4,4),slhaxchar(2)
c mmm changed 21/8/13
      common/thdm_hdec/tgbet2hdm,alph2hdm,amhl2hdm,amhh2hdm,
     .     amha2hdm,amhc2hdm,am12sq,a1lam2hdm,a2lam2hdm,a3lam2hdm,
     .     a4lam2hdm,a5lam2hdm,itype2hdm,i2hdm,iparam2hdm
      common/thdm_test/itestcond
      common/thdm_coup_hdec/gllep,ghlep,galep
c end mmm changed 21/8/13
c MMM changed 28/2/23
      common/EFF2HDMCOUP/glteff,glbeff,gllepeff,ghteff,ghbeff,
     .     ghlepeff,gateff,gabeff,galepeff,I2HDMEFF
c end MMM changed 28/2/23
      common/hmssm_hdec/amhl10
      common/feynhiggs0_hdec/ifeynhiggs,itheta
      ftriang(x,y)= (x**2+y**2)/2.d0-x**2*y**2/(x**2-y**2)*
     .     dlog(x**2/y**2)
      fptriang(x,y)=-1.d0/3.d0*(4.d0/3.d0-(x**2*dlog(x**2)
     .     -y**2*dlog(y**2))/(x**2-y**2)-(x**2+y**2)/(x**2-y**2)**2*
     .     ((x**2+y**2)/2.d0-x**2*y**2/(x**2-y**2)*
     .     dlog(x**2/y**2)))
      bb1(p2,am1,am2,xmu)=((am1**2-am2**2)*b02_hdec(0.d0,am1,am2,xmu**2)
     .           -(p2+am1**2-am2**2)*b02_hdec(p2,am1,am2,xmu**2))/2/p2
      pmsq1(qq,am,amg,als) = cf*als/pi*(amg**2*dlog(qq**2/amg**2)
     . + am**2/2*dlog(amg**2/am**2) + am**2/2 + 3*amg**2/2
     . + (amg**2-am**2)**2/2/am**2*dlog(dabs(amg**2-am**2)/amg**2))
      pmsq10(qq,am,als) = cf*als/pi*(am**2*dlog(qq**2/am**2)
     . + am**2/2 + 3*am**2/2)
      als_susy(x,xlb,b0,b1)=12.d0*pi/(b0*dlog(x**2/xlb**2))
     .          *(1.d0-b1*dlog(dlog(x**2/xlb**2))
     .           /dlog(x**2/xlb**2))

      pi=4*datan(1d0)
      v=1.d0/dsqrt(dsqrt(2.d0)*gf)
      bet=datan(tgbet)
      sb = dsin(bet)
      cb = dcos(bet)
      amar = ama
c  ============ transformation of input for subh ==========
      cf = 4/3.d0
      ca = 3
      q0 = dsqrt(2*amsq**2+amur**2+amdr**2)/2
      if(ifeynhiggs.eq.0) q0 = susyscale
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     q0 = susyscale
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      alsp = alphas_hdec(q0,3)/pi
      altp = runm_hdec(q0,6)**2/2/pi/v**2/sb**2 / pi
      albp = runm_hdec(q0,5)**2/2/pi/v**2/cb**2 / pi
      rmt = runm_hdec(amt,6)
      rmb = runm_hdec(amt,5)
      qt = dsqrt(dmax1(amsq**2+rmt**2,amur**2+rmt**2))
      qb = dsqrt(dmax1(amsq**2+rmb**2,amdr**2+rmb**2))
      amh12 = ama**2*sb**2 - amz**2/2*(cb**2-sb**2) - amu**2
      amh22 = ama**2*cb**2 + amz**2/2*(cb**2-sb**2) - amu**2
      xb = amsq**2 + amdr**2 + amh12 + ad**2
      xt = amsq**2 + amur**2 + amh22 + au**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     ad0 = ad + (cf*alsp*amglu + 3*altp/2*au + albp/4*ad)
c    .         * dlog(qb**2/q0**2)
c     amdl0 = dsqrt(amsq**2 + (-cf*alsp*amglu**2 + (altp*xt+albp*xb)/4)
c    .                      * dlog(qb**2/q0**2))
c     amdr0 = dsqrt(amdr**2 + (-cf*alsp*amglu**2 + albp*xb/4)
c    .                      * dlog(qb**2/q0**2))
c     au0 = au + (cf*alsp*amglu + altp/4*au + 3*albp/2*ad)
c    .         * dlog(qt**2/q0**2)
c     amul0 = dsqrt(amsq**2 + (-cf*alsp*amglu**2 + (altp*xt+albp*xb)/4)
c    .                      * dlog(qt**2/q0**2))
c     amur0 = dsqrt(amur**2 + (-cf*alsp*amglu**2 + altp*xt/4)
c    .                      * dlog(qt**2/q0**2))
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     adqcd = cf*alsp*amglu*dlog(qb**2/q0**2)
c     sdqcd = -cf*alsp*amglu**2*dlog(qb**2/q0**2)
c     auqcd = cf*alsp*amglu*dlog(qt**2/q0**2)
c     suqcd = -cf*alsp*amglu**2*dlog(qt**2/q0**2)
c     goto 8765
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      amg = amglu
      qq0 = q0
      als = alphas_hdec(qq0,3)
      sw2=1.d0-amw**2/amz**2
c up squarks:
      amsq0 = dsqrt(amql1**2+(0.5d0-2.d0/3.d0*sw2)*amz**2*dcos(2.d0*b))
      if(dabs(amg).ne.amsq0)then
       amu1l = dsqrt(amsq0**2 + pmsq1(qq0,amsq0,amg,als))
      else
       amu1l = dsqrt(amsq0**2 + pmsq10(qq0,amsq0,als))
      endif
      amsq0 = dsqrt(amur1**2+2.d0/3.d0*sw2*amz**2*dcos(2.d0*b))
      if(dabs(amg).ne.amsq0)then
       amu1r = dsqrt(amsq0**2 + pmsq1(qq0,amsq0,amg,als))
      else
       amu1r = dsqrt(amsq0**2 + pmsq10(qq0,amsq0,als))
      endif
c down squarks
      amsq0 = dsqrt(amql1**2+(-0.5d0+1.d0/3.d0*sw2)*amz**2*dcos(2.d0*b))
      if(dabs(amg).ne.amsq0)then
       amd1l = dsqrt(amsq0**2 + pmsq1(qq0,amsq0,amg,als))
      else
       amd1l = dsqrt(amsq0**2 + pmsq10(qq0,amsq0,als))
      endif
      amsq0 = dsqrt(amdr1**2-1.d0/3.d0*sw2*amz**2*dcos(2.d0*b))
      if(dabs(amg).ne.amsq0)then
       amd1r = dsqrt(amsq0**2 + pmsq1(qq0,amsq0,amg,als))
      else
       amd1r = dsqrt(amsq0**2 + pmsq10(qq0,amsq0,als))
      endif
      eps = 0
      ffb = amb*2*sthb*cthb/amg
      fft = amt*2*stht*ctht/amg
      am3 = amg*(1-alphas_hdec(dabs(amg),3)/4/pi
     .         *(4*ca+3*ca*dlog(qq0**2/amg**2)
     .          + bb1(amg**2,eps,amu1l,qq0)
     .          + bb1(amg**2,eps,amu1r,qq0)
     .          + bb1(amg**2,eps,amd1l,qq0)
     .          + bb1(amg**2,eps,amd1r,qq0)
     .          + bb1(amg**2,ams,amd1l,qq0)
     .          + bb1(amg**2,ams,amd1r,qq0)
     .          + bb1(amg**2,amc,amu1l,qq0)
     .          + bb1(amg**2,amc,amu1r,qq0)
     .          + bb1(amg**2,amb,amsb1,qq0)
     .          + bb1(amg**2,amb,amsb2,qq0)
     .          + bb1(amg**2,amt,amst1,qq0)
     .          + bb1(amg**2,amt,amst2,qq0)
     .          + ffb*(b02_hdec(amg**2,amb,amsb1,qq0**2)
     .                -b02_hdec(amg**2,amb,amsb2,qq0**2))
     .          + fft*(b02_hdec(amg**2,amt,amst1,qq0**2)
     .                -b02_hdec(amg**2,amt,amst2,qq0**2))
     .          ))
      als_susy0= alphas_hdec(qq0,3)*(1+alphas_hdec(qq0,3)/pi
     .         * (dlog(qq0**2/amt**2)/6 + dlog(qq0**2/amg**2)/2
     .         + (2*(dlog(qq0**2/amu1l**2)+dlog(qq0**2/amu1r**2)
     .              +dlog(qq0**2/amd1l**2)+dlog(qq0**2/amd1r**2))
     .           +dlog(qq0**2/amsb1**2)+dlog(qq0**2/amsb2**2)
     .           +dlog(qq0**2/amst1**2)+dlog(qq0**2/amst2**2))/24))
      acc = 1.d-10
      xlb_susy = xitsusy_hdec(qq0,als_susy0,acc)
      b0 = 9
      b1 = 14.d0/9
      als0 = als_susy(qq0,xlb_susy,b0,b1)
      alst = als_susy(qt,xlb_susy,b0,b1)
      alsb = als_susy(qb,xlb_susy,b0,b1)
      auqcd = am3*(-16.d0/9*(alst/als0-1)*(1+als0/6/pi)
     .             -16*als0/27/pi*(alst**2/als0**2-1))
      adqcd = am3*(-16.d0/9*(alsb/als0-1)*(1+als0/6/pi)
     .             -16*als0/27/pi*(alsb**2/als0**2-1))
      suqcd = am3**2*(8.d0/9*(alst**2/als0**2-1)*(1+als0/6/pi)
     .               -4*als0/9/pi*(alst**3/als0**3-1))
      sdqcd = am3**2*(8.d0/9*(alsb**2/als0**2-1)*(1+als0/6/pi)
     .               -4*als0/9/pi*(alsb**3/als0**3-1))
c     write(6,*)'subh1: ',amsq**2,sdqcd,
c    .                   ((altp*xt+albp*xb)/4)*dlog(qb**2/q0**2)
8765  continue
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      ad0 = ad + adqcd + (3*altp/2*au + albp/4*ad)*dlog(qb**2/q0**2)
      amdl0 = dsqrt(amsq**2 + sdqcd
     .           + ((altp*xt+albp*xb)/4)*dlog(qb**2/q0**2))
      amdr0 = dsqrt(amdr**2 + sdqcd
     .           + albp*xb/4*dlog(qb**2/q0**2))
      au0 = au + auqcd + (altp/4*au + 3*albp/2*ad)*dlog(qt**2/q0**2)
      amul0 = dsqrt(amsq**2 + suqcd
     .           + (altp*xt+albp*xb)/4*dlog(qt**2/q0**2))
      amur0 = dsqrt(amur**2 + suqcd + altp*xt/4*dlog(qt**2/q0**2))
c     write(6,*)'delmb: ',q0,amsq**2,sdqcd,
c    .                    ((altp*xt+albp*xb)/4)*dlog(qb**2/q0**2),
c    .             (amsq**2 + sdqcd
c    .           + ((altp*xt+albp*xb)/4)*dlog(qb**2/q0**2))
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)
c     write(6,*)
c     write(6,*)'als param: ',xlambda,amc0,amb0,amt0,n0
c     write(6,*)'q0,qt,qb: ',q0,qt,qb
c     write(6,*)'alsp,altp,albp: ',alsp,altp,albp
c     write(6,*)'stop params:    ',runm_hdec(q0,6),v,sb
c     write(6,*)'sbot params:    ',runm_hdec(q0,5),v,cb
c     write(6,*)'a_t params: ',cf*alsp*amglu,altp/4*au,3*albp/2*ad,
c    .                         dlog(qt**2/q0**2)
c     write(6,*)'stop: ',amsq,amur,au
c     write(6,*)'      ',amul0,amur0,au0
c     write(6,*)'sbot: ',amsq,amdr,ad
c     write(6,*)'      ',amdl0,amdr0,ad0
c     write(6,*)'      ',amdr**2,-cf*alsp*amglu**2 * dlog(qb**2/q0**2),
c    .                   albp*xb/4 * dlog(qb**2/q0**2)
c     write(6,*)'      ',amglu
c     write(6,*)
c     write(6,*)amdr**2,sdqcd,albp*xb/4*dlog(qb**2/q0**2)
c     write(6,*)amg,am3
c     write(6,*)amst1,amst2,amsb1,amsb2
c  ============ heaviest chargino mass needed for subh ==========
      if(islhai.eq.0) then
         amchi2=am2**2+amu**2+2.d0*amw**2+dsqrt((am2**2-amu**2)**2
     .        +4.d0*amw**4*dcos(2.d0*bet)**2+4.d0*amw**2*
     .        (am2**2+amu**2+2.d0*amu*am2*dsin(2.d0*bet) ) )
         amchi=dsqrt(0.5d0*amchi2)
      else
         amchi = slhachar(2)
      endif
c ===============================================================
c ========== running masses
      if(islhai.eq.0) then
      if(imodel.eq.1)then
       call subh1_hdec(ama,tgbet,amul0,amdl0,amur0,amdr0,amt,au0,ad0,
     .            amu,amchi,amlr,amhr,amch,sa,ca,tanba,amglu)
      elseif(imodel.eq.2)then
       call subh2_hdec(ama,tgbet,amul0,amur0,amt,au0,ad0,amu,
     .            amlr,amhr,amch,sa,ca,tanba)
      elseif(imodel.eq.3)then
       call haber(tgbet,sa,ca)
       amlr = aml
       amhr = amh
      elseif(imodel.eq.4)then
c--use carena et al. for everything not included in feynhiggs....
       call subh1_hdec(ama,tgbet,amul0,amdl0,amur0,amdr0,amt,au0,ad0,
     .            amu,amchi,amlr,amhr,amch,sa,ca,tanba,amglu)
       if(ctht.ge.0.d0)then
        xmst1 = amst1
        xmst2 = amst2
        stt = stht
       else
        xmst1 = amst1
        xmst2 = amst2
        stt = ctht
       endif
       if(cthb.ge.0.d0)then
        xmsb1 = amsb1
        xmsb2 = amsb2
        stb = sthb
       else
        xmsb1 = amsb1
        xmsb2 = amsb2
        stb = cthb
       endif
       call feynhiggs(ama,tgbet,amt,xmst1,xmst2,stt,xmsb1,
     .                xmsb2,stb,amu,amglu,am2,amlr,amhr,sa,ca)
      elseif(imodel.eq.10)then
c--hmssm?
       aml = amhl10
       c2b = cb**2-sb**2
       amh = dsqrt(((ama**2+amz**2-aml**2)*(amz**2*cb**2+ama**2*sb**2)
     .       -ama**2*amz**2*c2b**2)/(amz**2*cb**2+ama**2*sb**2-aml**2))
       amch = dsqrt(ama**2+amw**2)
       a = -datan((amz**2+ama**2)*sb*cb
     .            /(amz**2*cb**2+ama**2*sb**2-aml**2))
       amlr = aml
       amhr = amh
       sa = dsin(a)
       ca = dcos(a)
       deps = (aml**2*(ama**2+amz**2-aml**2)-ama**2*amz**2*c2b**2)
     .      / (amz**2*cb**2+ama**2*sb**2-aml**2)
      endif
      else
       call subh1_hdec(ama,tgbet,amul0,amdl0,amur0,amdr0,amt,au0,ad0,
     .            amu,amchi,amlr,amhr,amch,sa,ca,tanba,amglu)
       amlr = slhaml
       amhr = slhamh
       aml  = slhaml
       amh  = slhamh
       amch = slhamc
       sa   = dsin(slha_alpha)
       ca   = dcos(slha_alpha)
      endif
c mmm changed 21/8/13
      if(i2hdm.eq.1) then
         ammh2 = am12sq/sb/cb
         bb = datan(tgbet2hdm)
         cb = dcos(bb)
         sb = dsin(bb)
c MMM changed 28/2/23
         if(i2hdmeff.eq.0) then
c end MMM changed 28/2/23
         if(iparam2hdm.eq.1) then
            sa = dsin(alph2hdm)
            ca = dcos(alph2hdm)
            amar = amha2hdm
            ama = amar
            amlr= amhl2hdm
            amhr= amhh2hdm
            aml = amhl2hdm
            amh = amhh2hdm
            amch= amhc2hdm
            amar= amha2hdm
c from hep-ph/0408364, eqs. (26)-(30)
            la1 = 1.d0/v**2/cb**2*(-sb**2*ammh2+sa**2*amlr**2
     .           +ca**2*amhr**2)
            la2 = 1.d0/v**2/sb**2*(-cb**2*ammh2+ca**2*amlr**2
     .           +sa**2*amhr**2)
            la3 = -ammh2/v**2+2.d0*amch**2/v**2+1.d0/v**2*sa*ca/sb/cb*
     .           (amhr**2-amlr**2)
            la4 = 1.d0/v**2*(ammh2+amar**2-2.d0*amch**2)
            la5 = 1.d0/v**2*(ammh2-amar**2)
            la6 = 0.d0
            la7 = 0.d0
c            print*,'lambdas',la1,la2,la3,la4,la5
         elseif(iparam2hdm.eq.2) then
            la1=a1lam2hdm
            la2=a2lam2hdm
            la3=a3lam2hdm
            la4=a4lam2hdm
            la5=a5lam2hdm
            la6 = 0.d0
            la7 = 0.d0
c            print*,'lambdas',la1,la2,la3,la4,la5

            am11h2 = (la1*cb**4+la2*sb**4+2.d0*(la3+la4+la5)*
     .           cb**2*sb**2)*v**2
            am12h2 = (-la1*cb**2+la2*sb**2+(la3+la4+la5)*
     .           (cb**2-sb**2))*cb*sb*v**2
            am22h2 = ammh2 + 1.d0/8.d0*(la1+la2-2.d0*(la3+la4+la5))*
     .           (1.d0-dcos(4.d0*bb))*v**2

            aminb = 1.d0/2.d0*datan(2.d0*am12h2/(am11h2-am22h2))
            alph2hdm = aminb + bb
            sa = dsin(alph2hdm)
            ca = dcos(alph2hdm)

            amlch2 = (dsin(alph2hdm-bb))**2*am11h2
     .           -dsin(2.d0*(alph2hdm-bb))*am12h2
     .           + (dcos(alph2hdm-bb))**2*am22h2

            amhch2 = (dcos(alph2hdm-bb))**2*am11h2
     .           +dsin(2.d0*(alph2hdm-bb))*am12h2
     .           + (dsin(alph2hdm-bb))**2*am22h2

            amlr = dsqrt(amlch2)
            amhr = dsqrt(amhch2)

            if(amlr.gt.amhr) then
               alph2hdm = alph2hdm - pi/2.d0
               sa = dsin(alph2hdm)
               ca = dcos(alph2hdm)
               tmp1 = amlr
               tmp2 = amhr
               amhr = tmp1
               amlr = tmp2
            endif

            aml  = amlr
            amh  = amhr

            amach2 = ammh2-la5*v**2
            amcch2 = ammh2-1.d0/2.d0*(la4+la5)*v**2

            amar = dsqrt(amach2)
            amch = dsqrt(amcch2)

c            print*,'vals',aml,amh,amar,amch,alph2hdm
         endif
c MMM changed 28/2/23
         elseif(i2hdmeff.eq.1) then
            sa = dsin(alph2hdm)
            ca = dcos(alph2hdm)
            amar = AMHA2HDM
            ama = amar
            amlr= AMHL2HDM
            amhr= AMHH2HDM
            aml = AMHL2HDM
            amh = AMHH2HDM
            amch= AMHC2HDM
            amar= AMHA2HDM

            la1=a1lam2hdm
            la2=a2lam2hdm
            la3=a3lam2hdm
            la4=a4lam2hdm
            la5=a5lam2hdm
            LA6 = 0.D0
            LA7 = 0.D0            
         endif
c end MMM changed 28/2/23
         if(itestcond.eq.1) then
c test of vacuum stability (s.kanemura eal, phys.rev.d70(2004)115002)
            do i=1,30,1
               itest(i) = 0
            end do
            if(la1.gt.0.d0) then
               itest(1)=1
            endif
            if(la1.gt.0.d0) then
               itest(2)=1
            endif
            if(0.d0.le.la4+la5.and.0.d0.le.la4-la5) then
               amin=0.d0
            elseif(la4+la5.le.0.d0.and.la4+la5.le.la4-la5) then
               amin=la4+la5
            elseif(la4-la5.le.0.d0.and.la4-la5.le.la4+la5) then
               amin=la4-la5
            endif
            valtest = dsqrt(la1*la2)+la3+amin
            if(valtest.gt.0.d0) then
               itest(3)=1
            endif
c            print*,'check',amin,itest(1),itest(2),itest(3)
            iteststab=itest(1)*itest(2)*itest(3)
            if(iteststab.eq.0) then
               print*,'attention: vacuum stability is not fulfilled.'
            endif

c test for perturbativity (arxiv:1106.0034, eqs.(3.363)-(3.372))
            pap = 1.5d0*(la1+la2)+dsqrt(9.d0/4.d0*(la1-la2)**2+
     .           (2.d0*la3+la4)**2)
            pam = 1.5d0*(la1+la2)-dsqrt(9.d0/4.d0*(la1-la2)**2+
     .           (2.d0*la3+la4)**2)
            pbp = 0.5d0*(la1+la2)+0.5d0*dsqrt((la1-la2)**2+4.d0*la4**2)
            pbm = 0.5d0*(la1+la2)-0.5d0*dsqrt((la1-la2)**2+4.d0*la4**2)
            pcp = 0.5d0*(la1+la2)+0.5d0*dsqrt((la1-la2)**2+4.d0*la5**2)
            pcm = 0.5d0*(la1+la2)-0.5d0*dsqrt((la1-la2)**2+4.d0*la5**2)
            pe1 = la3+2.d0*la4-3.d0*la5
            pe2 = la3-la5
            pfp = la3+2.d0*la4+3.d0*la5
            pfm = la3+la5
            pf1 = la3+la4
            pp1 = la3-la4

            pertval = 8.d0*pi

            if(dabs(pap).lt.pertval) then
               itest(4) = 1
            endif
            if(dabs(pam).lt.pertval) then
               itest(5) = 1
            endif
            if(dabs(pbp).lt.pertval) then
               itest(6) = 1
            endif
            if(dabs(pbm).lt.pertval) then
               itest(7) = 1
            endif
            if(dabs(pcp).lt.pertval) then
               itest(8) = 1
            endif
            if(dabs(pcm).lt.pertval) then
               itest(9) = 1
            endif
            if(dabs(pe1).lt.pertval) then
               itest(10) = 1
            endif
            if(dabs(pe2).lt.pertval) then
               itest(11) = 1
            endif
            if(dabs(pfp).lt.pertval) then
               itest(12) = 1
            endif
            if(dabs(pfm).lt.pertval) then
               itest(13) = 1
            endif
            if(dabs(pf1).lt.pertval) then
               itest(14) = 1
            endif
            if(dabs(pp1).lt.pertval) then
               itest(15) = 1
            endif
            itestpert = itest(4)*itest(5)*itest(6)*itest(7)*itest(8)*
     .           itest(9)*itest(10)*itest(11)*itest(12)
            if(itestpert.eq.0) then
            print*,'attention: perturbative unitarity is not fulfilled.'
            endif

c the s- and t-parameter in the 2hdm (1108.3297, eqs. (12),(13))
            params2hdm = -1.d0/(4.d0*pi)*(1.d0/3.d0*dlog(amch**2)
     .           -(dsin(bb-alph2hdm))**2*fptriang(amh,amar)
     .           -(dcos(bb-alph2hdm))**2*fptriang(aml,amar))
            paramt2hdm = -dsqrt(2.d0)*gf/16.d0/pi**2/alph*(
     .           -ftriang(amar,amch)+(dsin(bb-alph2hdm))**2*(
     .           ftriang(amh,amar)-ftriang(amh,amch))
     .           +(dcos(bb-alph2hdm))**2*(ftriang(aml,amar)
     .           -ftriang(aml,amch)))
c            print*,'par s,t',params2hdm,paramt2hdm

c the s- and t-parameter limits are taken from 1209.2716, eq.(9)

            tparamlimp = 0.17d0
            tparamlimm = -0.07d0

            sparamlimp = 0.13d0
            sparamlimm = -0.07d0

          if(params2hdm.gt.sparamlimp.or.params2hdm.lt.sparamlimm) then
             print*,'the limits on the s-parameter are not fulfilled.'
          endif
          if(paramt2hdm.gt.tparamlimp.or.paramt2hdm.lt.tparamlimm) then
             print*,'the limits on the t-parameter are not fulfilled.'
          endif

         endif

c -- this is for a check --
c         print*,'alphacheck',alphacheck-2.d0*datan(1.d0)
c         print*,'alpha',datan(dsin(alph2hdm)/dcos(alph2hdm))
c         print*,'alpha2hdm',alph2hdm
c         print*,'check: aml,amh,ama,amch',amlc,amhc,amac,amcc
c         print*,'m12h2',am12sq,am12sq/sb/cb,ammh2
      endif
c end mmm changed 21/8/13
      la3t=la3+la4+la5
      ama2=amar**2
      aml2=amlr**2
      amh2=amhr**2
      amp2=amch**2
c ========== higgs couplings
      sbma = sb*ca-cb*sa
      cbma = cb*ca+sb*sa
      sbpa = sb*ca+cb*sa
      cbpa = cb*ca-sb*sa
      s2a = 2*sa*ca
      c2a = ca**2-sa**2
      s2b = 2*sb*cb
      c2b = cb**2-sb**2
      glzz = 1/v/2*aml2*sbma
      ghzz = 1/v/2*amh2*cbma
      glww = 2*glzz
      ghww = 2*ghzz
      glaz = 1/v*(aml2-ama2)*cbma
      ghaz = -1/v*(amh2-ama2)*sbma
      glpw = -1/v*(amp2-aml2)*cbma
      glmw = glpw
      ghpw = 1/v*(amp2-amh2)*sbma
      ghmw = ghpw
      gapw = 1/v*(amp2-ama2)
      gamw = -gapw
      ghhh = v/2*(la1*ca**3*cb + la2*sa**3*sb + la3t*sa*ca*sbpa
     .     + la6*ca**2*(3*sa*cb+ca*sb) + la7*sa**2*(3*ca*sb+sa*cb))
      glll = -v/2*(la1*sa**3*cb - la2*ca**3*sb + la3t*sa*ca*cbpa
     .     - la6*sa**2*(3*ca*cb-sa*sb) + la7*ca**2*(3*sa*sb-ca*cb))
      glhh = -3*v/2*(la1*ca**2*cb*sa - la2*sa**2*sb*ca
     .     + la3t*(sa**3*cb-ca**3*sb+2*sbma/3)
     .     - la6*ca*(cb*c2a-sa*sbpa) - la7*sa*(c2a*sb+ca*sbpa))
      ghll = 3*v/2*(la1*sa**2*cb*ca + la2*ca**2*sb*sa
     .     + la3t*(sa**3*sb+ca**3*cb-2*cbma/3)
     .     - la6*sa*(cb*c2a+ca*cbpa) + la7*ca*(c2a*sb+sa*cbpa))
      glaa = -v/2*(la1*sb**2*cb*sa - la2*cb**2*sb*ca
     .     - la3t*(sb**3*ca-cb**3*sa) + 2*la5*sbma
     .     - la6*sb*(cb*sbpa+sa*c2b) - la7*cb*(c2b*ca-sb*sbpa))
      ghaa = v/2*(la1*sb**2*cb*ca + la2*cb**2*sb*sa
     .     + la3t*(sb**3*sa+cb**3*ca) - 2*la5*cbma
     .     - la6*sb*(cb*cbpa+ca*c2b) + la7*cb*(sb*cbpa+sa*c2b))
      glpm = 2*glaa + v*(la5 - la4)*sbma
c     write(6,*)'glpm: ',glpm,glaa,v,la5,la4,sbma

c --- this is for a check against hep-ph/0408364 ---

      if(i2hdm.eq.1) then
      aa = alph2hdm

      xlll = -1.d0/4.d0/v/(2.d0*cb*sb)*(
     .     (dcos(3.d0*aa-bb)+3.d0*dcos(aa+bb))*amlr**2
     .     -4.d0*(dcos(aa-bb))**2*dcos(aa+bb)*ammh2)

      xlhh = -dsin(aa-bb)/2.d0/v/dsin(2.d0*bb)*(dsin(2.d0*aa)*
     .     (amlr**2+2.d0*amhr**2)-(3.d0*dsin(2.d0*aa)+dsin(2.d0*bb))*
     .     ammh2)

      xhll = -dcos(aa-bb)/2.d0/v/dsin(2.d0*bb)*(dsin(2.d0*aa)*(2.d0*
     .     amlr**2+amhr**2)-(3.d0*dsin(2.d0*aa)-dsin(2.d0*bb))*ammh2)

      xhhh = 1.d0/4.d0/v/dsin(2.d0*b)*((dsin(3.d0*aa-bb)
     .     -3.d0*dsin(aa+bb))*amhr**2+4.d0*(dsin(aa-bb))**2*
     .     dsin(aa+bb)*ammh2)

      xlaa = -1.d0/4.d0/v/dsin(2.d0*bb)*((dcos(aa-3.d0*bb)+3.d0*
     .     dcos(aa+bb))*
     .     amlr**2-4.d0*dsin(2.d0*bb)*dsin(aa-bb)*amar**2-4.d0*
     .     dcos(aa+bb)*ammh2)

      xhaa = -1.d0/4.d0/v/dsin(2.d0*bb)*((dsin(aa-3.d0*bb)+3.d0*
     .     dsin(aa+bb))*amhr**2+4.d0*dsin(2.d0*bb)*dcos(aa-bb)*amar**2
     .     -4.d0*dsin(aa+bb)*ammh2)

      xlpm = -1.d0/2.d0/v/dsin(2.d0*bb)*((dcos(aa-3.d0*bb)+3.d0*
     .     dcos(aa+bb))*
     .     amlr**2-4.d0*dsin(2.d0*bb)*dsin(aa-bb)*amch**2-4.d0*
     .     dcos(aa+bb)*ammh2)

      xhpm = -1.d0/2.d0/v/dsin(2.d0*bb)*((dsin(aa-3.d0*bb)+3.d0*
     .     dsin(aa+bb))*amhr**2+4.d0*dsin(2.d0*bb)*dcos(aa-bb)*
     .     amch**2-4.d0*dsin(aa+bb)*ammh2)

c      print*,'lll',glll,xlll
c      print*,'lhh',glhh,xlhh
c      print*,'hll',ghll,xhll
c      print*,'hhh',ghhh,xhhh
c      print*,'laa',glaa,xlaa
c      print*,'haa',ghaa,xhaa
c      print*,'lh+h-',glpm,xlpm
c      print*,'ma',amar,amch
c      print*,'mh,mh,ma,mh+',amlr,amhr,amar,amch
c      print*,'alpha,tan(beta)',aa,tgbet2hdm

c ---------------------------------------------------

c      print*,'la1-la5',la1,la2,la3,la4,la5
c      print*,'glaa',glaa
c      print*,'sbma,sb,cb,sa,ca',sbma,sb,cb,sa,ca
      endif
c     write(6,*)'glaa,sb,cb,sa,ca,sbpa,c2b,la1,la2,la3t,la5,la6,la7: ',
c    .           glaa,sb,cb,sa,ca,sbpa,c2b,la1,la2,la3t,la5,la6,la7
c     write(6,*)'glpm,glaa,v,la5,la4,sbma,la5-la4: ',
c    .           glpm,glaa,v,la5,la4,sbma,la5-la4
      ghpm = 2*ghaa + v*(la5 - la4)*cbma
c      print*,'hh+h-',ghpm,xhpm
      glzz = 2*glzz
      ghzz = 2*ghzz
      glll = 6*glll
      ghhh = 6*ghhh
      glhh = 2*glhh
      ghll = 2*ghll
      glaa = 2*glaa
      ghaa = 2*ghaa
      xnorm = amz**2/v
      glll = glll/xnorm
      ghll = ghll/xnorm
      glhh = glhh/xnorm
      ghhh = ghhh/xnorm
      ghaa = ghaa/xnorm
      glaa = glaa/xnorm
      glpm = glpm/xnorm

c--hmssm?
      if(imodel.eq.10)then
        depsb = deps - 16*amt**4/(4*pi)**2/v**2/sb**2
        ghhh = 3*c2a*cbpa            + 3*depsb/amz**2*sa**3/sb
        glll = 3*c2a*sbpa            + 3*depsb/amz**2*ca**3/sb
        ghll = 2*s2a*sbpa - c2a*cbpa + 3*depsb/amz**2*sa*ca**2/sb
        glhh =-2*s2a*cbpa - c2a*sbpa + 3*depsb/amz**2*sa**2*ca/sb
        ghaa =-c2b*cbpa              + depsb/amz**2*sa*cb**2/sb
        glaa = c2b*sbpa              + depsb/amz**2*ca*cb**2/sb
        glpm = 2*amw**2/amz**2*sbma + c2b*sbpa
        ghpm = 2*amw**2/amz**2*cbma - c2b*cbpa
      endif


c      print*,'lll',glll
c      print*,'lhh',glhh
c      print*,'hll',ghll
c      print*,'hhh',ghhh
c      print*,'laa',glaa
c      print*,'haa',ghaa
c      print*,'lh+h-',glpm
c      print*,'hh+h-',ghpm*v/amz**2
c      print*,'norm = v/amz**2',v/amz**2

c      print*,'glpm',glpm

      ghpm = ghpm/xnorm
      gat=1.d0/tgbet
      gab=tgbet
c mmm changed 21/8/13
      if(i2hdm.eq.0) then
c end mmm changed 2178/13
      glt=ca/sb
      glb=-sa/cb
      ght=sa/sb
      ghb=ca/cb
c mmm changed 21/8/13
      elseif(i2hdm.eq.1) then
c MMM changed 28/2/23
         if(i2hdmeff.eq.0) then
c end MMM changed 28/2/23
         if(itype2hdm.eq.1) then
            glt   = ca/sb
            glb   = ca/sb
            gllep = glb
            ght   = sa/sb
            ghb   = sa/sb
            ghlep = ghb
            gat   = 1.d0/tgbet
            gab   =-1.d0/tgbet
            galep = gab
         elseif(itype2hdm.eq.2) then
            glt   = ca/sb
            glb   =-sa/cb
            gllep = glb
            ght   = sa/sb
            ghb   = ca/cb
            ghlep = ghb
            gat   = 1.d0/tgbet
            gab   = tgbet
            galep = gab
         elseif(itype2hdm.eq.3) then
            glt   = ca/sb
            glb   = ca/sb
            gllep =-sa/cb
            ght   = sa/sb
            ghb   = sa/sb
            ghlep = ca/cb
            gat   = 1.d0/tgbet
            gab   =-1.d0/tgbet
            galep = tgbet
         elseif(itype2hdm.eq.4) then
            glt   = ca/sb
            glb   =-sa/cb
            gllep = ca/sb
            ght   = sa/sb
            ghb   = ca/cb
            ghlep = sa/sb
            gat   = 1.d0/tgbet
            gab   = tgbet
            galep =-1.d0/tgbet
         endif
c MMM changed 28/2/23
         elseif(i2hdmeff.eq.1) then
            glt   = glteff
            glb   = glbeff
            gllep = gllepeff
            ght   = ghteff
            ghb   = ghbeff
            ghlep = ghlepeff
            gat   = gateff
            gab   = gabeff
            galep = galepeff
         endif            
c end MMM changed 28/2/23
      endif
c end mmm changed 21/8/13
      gzal=-cbma
      gzah=sbma
      glvv=sbma
      ghvv=cbma
      b=bet
      if(ca.eq.0)then
       a = pi/2
      else
       a=datan(sa/ca)
      endif
      if(ca.lt.0d0)then
       if(sa.lt.0d0)then
        a = a-pi
       else
        a = a+pi
       endif
      endif
c ===============================================================
c ========== pole masses
      if(islhai.eq.0) then
      if(imodel.eq.1)then
      if(ipole.eq.1) then
       mt=runm_hdec(amt,6)
       mb=runm_hdec(amt,5)
       sw2=1.d0-amw**2/amz**2
c===== stop masses
       mstl2=amsq**2+(0.5d0-2.d0/3.d0*sw2)*amz**2*dcos(2.d0*b)
       mstr2=amur**2+2.d0/3.d0*sw2*amz**2*dcos(2.d0*b)
       mlrt=au-amu/tgbet
       delt=(mstl2-mstr2)**2+4*mt**2*mlrt**2
       mst12=mt**2+0.5d0*(mstl2+mstr2-dsqrt(delt))
       mst22=mt**2+0.5d0*(mstl2+mstr2+dsqrt(delt))
        if(mst12.lt.0.d0)goto 111
       mst(1)=dsqrt(mst12)
       mst(2)=dsqrt(mst22)
       if(mstl2.eq.mstr2) then
        thet = pi/4
       else
        thet=0.5d0*datan(2.d0*mt*mlrt / (mstl2-mstr2) )
        if(mstl2.gt.mstr2) thet = thet + pi/2
       endif
       cst= dcos(thet)
       sst= dsin(thet)
c===== sbottom masses
       msbl2=amsq**2+(-0.5d0+1.d0/3.d0*sw2)*amz**2*dcos(2.d0*b)
       msbr2=amdr**2-1.d0/3.d0*sw2*amz**2*dcos(2.d0*b)
       mlrb=ad-amu*tgbet
       delb=(msbl2-msbr2)**2+4*mb**2*mlrb**2
       msb12=mb**2+0.5d0*(msbl2+msbr2-dsqrt(delb))
       msb22=mb**2+0.5d0*(msbl2+msbr2+dsqrt(delb))
        if(msb12.lt.0.d0)goto 111
       msb(1)=dsqrt(msb12)
       msb(2)=dsqrt(msb22)
       if(msbl2.eq.msbr2) then
        theb = pi/4
       else
        theb=0.5d0*datan(2.d0*mb*mlrb / (msbl2-msbr2) )
        if(msbl2.gt.msbr2) theb = theb + pi/2
       endif
       csb= dcos(theb)
       ssb= dsin(theb)
c===== light higgs couplings
       gltt(1,1)=-sbpa*(0.5d0*cst**2-2.d0/3.d0*sw2*dcos(2*thet) )
     .     +mt**2/amz**2*glt + mt*sst*cst/amz**2*(au*glt+amu*ght)
       gltt(2,2)=-sbpa*(0.5d0*sst**2+2.d0/3.d0*sw2*dcos(2*thet) )
     .     +mt**2/amz**2*glt - mt*sst*cst/amz**2*(au*glt+amu*ght)
       gltt(1,2)=-2*sbpa*sst*cst*(2.d0/3.d0*sw2-0.25d0)
     .     + mt*dcos(2*thet)/2.d0/amz**2*(au*glt+amu*ght)
       gltt(2,1)=-2*sbpa*sst*cst*(2.d0/3.d0*sw2-0.25d0)
     .     + mt*dcos(2*thet)/2.d0/amz**2*(au*glt+amu*ght)
       glbb(1,1)=-sbpa*(-0.5d0*csb**2+1.d0/3.d0*sw2*dcos(2*theb))
     .     +mb**2/amz**2*glb + mb*ssb*csb/amz**2*(ad*glb-amu*ghb)
       glbb(2,2)=-sbpa*(-0.5d0*ssb**2-1.d0/3.d0*sw2*dcos(2*theb))
     .     +mb**2/amz**2*glb - mb*ssb*csb/amz**2*(ad*glb-amu*ghb)
       glbb(1,2)=-2*sbpa*ssb*csb*(-1.d0/3.d0*sw2+0.25d0)
     .    + mb*dcos(2*theb)/2.d0/amz**2*(ad*glb-amu*ghb)
       glbb(2,1)=-2*sbpa*ssb*csb*(-1.d0/3.d0*sw2+0.25d0)
     .     + mb*dcos(2*theb)/2.d0/amz**2*(ad*glb-amu*ghb)
c===== heavy higgs couplings
       ghtt(1,1)=cbpa*(0.5d0*cst**2-2.d0/3.d0*sw2*dcos(2*thet))
     .     +mt**2/amz**2*ght + mt*sst*cst/amz**2*(au*ght-amu*glt)
       ghtt(2,2)=cbpa*(0.5d0*sst**2+2.d0/3.d0*sw2*dcos(2*thet))
     .     +mt**2/amz**2*ght - mt*sst*cst/amz**2*(au*ght-amu*glt)
       ghtt(1,2)=2*cbpa*sst*cst*(2.d0/3.d0*sw2-0.25d0)
     .     +mt*dcos(2*thet)/2.d0/amz**2*(au*ght-amu*glt)
       ghtt(2,1)=2*cbpa*sst*cst*(2.d0/3.d0*sw2-0.25d0)
     .     + mt*dcos(2*thet)/2.d0/amz**2*(au*ght-amu*glt)
       ghbb(1,1)=cbpa*(-0.5d0*csb**2+1.d0/3.d0*sw2*dcos(2*theb))
     .     +mb**2/amz**2*ghb + mb*ssb*csb/amz**2*(ad*ghb+amu*glb)
       ghbb(2,2)=cbpa*(-0.5d0*ssb**2-1.d0/3.d0*sw2*dcos(2*theb))
     .     + mb**2/amz**2*ghb - mb*ssb*csb/amz**2*(ad*ghb+amu*glb)
       ghbb(1,2)=2*cbpa*ssb*csb*(-1.d0/3.d0*sw2+0.25d0)
     .     + mb*dcos(2*theb)/2.d0/amz**2*(ad*ghb+amu*glb)
       ghbb(2,1)=2*cbpa*ssb*csb*(-1.d0/3.d0*sw2+0.25d0)
     .     + mb*dcos(2*theb)/2.d0/amz**2*(ad*ghb+amu*glb)
c===== pseudoscalar higgs couplings
       gatt=mt/2.d0/amz**2*(amu+au*gat)
       gabb=mb/2.d0/amz**2*(amu+ad*gab)
c======= loop corrections
       xdlt=gf/(2.d0*dsqrt(2.d0)*pi**2)*glt**2*(-2.d0*mt**2+0.5d0*aml2)
     .     *dreal(f0_hdec(mt,mt,aml2))
     .     *3*mt**2
       xdlb=gf/(2.d0*dsqrt(2.d0)*pi**2)*glb**2*(-2.d0*mb**2+0.5d0*aml2)
     .     *dreal(f0_hdec(mb,mb,aml2))
     .     *3*mb**2
c--bug in carena et al. fixed
     .     +gf/(2.d0*dsqrt(2.d0)*pi**2)*glb**2*(0.5d0*aml2)
     .     *dlog(mb**2/mt**2)
     .     *3*mb**2
       xdht=gf/(2.d0*dsqrt(2.d0)*pi**2)*ght**2*(-2.d0*mt**2+0.5d0*amh2)
     .     *dreal(f0_hdec(mt,mt,amh2))
     .     *3*mt**2
       xdhb=gf/(2.d0*dsqrt(2.d0)*pi**2)*ghb**2*(-2.d0*mb**2+0.5d0*amh2)
     .     *dreal(f0_hdec(mb,mb,amh2))
     .     *3*mb**2
c--bug in carena et al. fixed
     .     +gf/(2.d0*dsqrt(2.d0)*pi**2)*ghb**2*(0.5d0*amh2)
     .     *dlog(mb**2/mt**2)
     .     *3*mb**2
       xdat=gf/(2.d0*dsqrt(2.d0)*pi**2)*gat**2*(-0.5d0*ama2)
     .     *dreal(f0_hdec(mt,mt,ama2))
     .     *3*mt**2
       xdab=gf/(2.d0*dsqrt(2.d0)*pi**2)*gab**2*(-0.5d0*ama2)
     .     *dreal(f0_hdec(mb,mb,ama2))
     .     *3*mb**2
c--bug in carena et al. fixed
     .     +gf/(2.d0*dsqrt(2.d0)*pi**2)*gab**2*(-0.5d0*ama2)
     .     *dlog(mb**2/mt**2)
     .     *3*mb**2
       xdlst=0.d0
       xdlsb=0.d0
       xdhst=0.d0
       xdhsb=0.d0
         do 311 i=1,2
         do 311 j=1,2
       xdlst=xdlst+gf/(2.d0*dsqrt(2.d0)*pi**2)*gltt(i,j)**2*
     .       dreal(f0_hdec(mst(i),mst(j),aml2))
     .     *3*amz**4
       xdlsb=xdlsb+gf/(2.d0*dsqrt(2.d0)*pi**2)*glbb(i,j)**2*
     .       dreal(f0_hdec(msb(i),msb(j),aml2))
     .    *3*amz**4
       xdhst=xdhst+gf/(2.d0*dsqrt(2.d0)*pi**2)*ghtt(i,j)**2*
     .       dreal(f0_hdec(mst(i),mst(j),amh2))
     .     *3*amz**4
       xdhsb=xdhsb+gf/(2.d0*dsqrt(2.d0)*pi**2)*ghbb(i,j)**2*
     .       dreal(f0_hdec(msb(i),msb(j),amh2))
     .     *3*amz**4
311    continue
       xdast=gf/(1.d0*dsqrt(2.d0)*pi**2)*gatt**2*
     .       dreal(f0_hdec(mst(1),mst(2),ama2))
     .     *3*amz**4
       xdasb=gf/(1.d0*dsqrt(2.d0)*pi**2)*gabb**2*
     .       dreal(f0_hdec(msb(1),msb(2),ama2))
     .     *3*amz**4

       aml=dsqrt(aml2+xdlt+xdlb+xdlst+xdlsb)
       amh=dsqrt(amh2+xdht+xdhb+xdhst+xdhsb)
       ama=dsqrt(ama2+xdat+xdab+xdast+xdasb)
      else
       aml=amlr
       amh=amhr
       ama=amar
      endif
      else
       aml=amlr
       amh=amhr
       ama=amar
      endif
      endif
c mmm changed 21/8/13
c     if(i2hdm.eq.1) then
c        ama = amar
c        amlr= amhl2hdm
c        amhr= amhh2hdm
c        aml = amhl2hdm
c        amh = amhh2hdm
c        amch= amhc2hdm
c     endif
c end mmm changed 21/8/13
      return
111   stop
      end

c ===================== the function f0 ===============
