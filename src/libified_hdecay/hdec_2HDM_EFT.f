      SUBROUTINE HDEC_2HDM_EFT(TGBET)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION LAMB_HDEC
      COMPLEX*16 CFACQ_HDEC,CFACSQ_HDEC,CKOFQ_HDEC,CKOFQ,CFACQ,CFACQ0
      integer ivegas(4)
      DIMENSION XX(4),YY(4)
      DIMENSION AMCHAR(2),AMNEUT(4),XMNEUT(4),
     .          AC1(2,2),AC2(2,2),AC3(2,2),
     .          AN1(4,4),AN2(4,4),AN3(4,4),
     .          ACNL(2,4),ACNR(2,4),
     .          AMST(2),AMSB(2),AMSL(2),
     .          AMSU(2),AMSD(2),AMSE(2),AMSN(2),AMSN1(2),
     .          GLTT(2,2),GLBB(2,2),GLEE(2,2),
     .          GHTT(2,2),GHBB(2,2),GHEE(2,2),
     .          GCTB(2,2),GCEN(2,2)
      DIMENSION GMST(2),GMSB(2),GMSL(2),GMSU(2),GMSD(2),GMSE(2),
     .          GMSN(2),GMSN1(2)
      DIMENSION HLBRSC(2,2),HLBRSN(4,4),HHBRSC(2,2),
     .          HHBRSN(4,4),HABRSC(2,2),HABRSN(4,4),HCBRSU(2,4),
     .          HHBRST(2,2),HHBRSB(2,2),HCBRSTB(2,2) 
      DIMENSION WHLCH(2,2),WHLNE(4,4),WHHCH(2,2),WHHNE(4,4),
     .          WHACH(2,2),WHANE(4,4),WHCCN(2,4),
     .          WHHST(2,2),WHHSB(2,2),WHHSTAU(2,2),WHCSTB(2,2), 
     .          WHLST(2,2),WHLSB(2,2),WHLSTAU(2,2)
      DIMENSION WHHST0(2,2),WHHSB0(2,2)
      DIMENSION WHLGD(4),WHCGD(2),WHHGD(4),WHAGD(4)
      DIMENSION AGDL(4),AGDA(4),AGDH(4),AGDC(2)
      DIMENSION slhaneut(4),slhaxneut(4),slhachar(2),slhau(2,2),
     .          slhav(2,2),slhaz(4,4),xmchar(2)
      DIMENSION XGLBB(2,2),XGHBB(2,2),XGCTB(2,2)
      DIMENSION XHGG(3),XHQQ(3)
      COMPLEX*16 CF,CG,CI1,CI2,CA,CB,CTT,CTB,CTC,CTW,CLT,CLB,CLC,CLW,
     .           CAT,CAB,CAC,CAW,CAH,CTH,CLH,CX1,CX2,CAX1,CAX2,CTL,CAL,
     .           CSL,CSQ,CSB1,CSB2,CST1,CST2,CSL1,CSL2,
     .           CXL,CXQ,CXB1,CXB2,CXT1,CXT2,CXL1,CXL2
      COMPLEX*16 CSEL,CSER,CSUL,CSUR,CSDL,CSDR,
     .           CXEL,CXER,CXUL,CXUR,CXDL,CXDR
      COMPLEX*16 CAT0,CAB0,CAC0,CXUL0,CXUR0,CXDL0,CXDR0,CXB10,CXB20,
     .           CXT10,CXT20
      COMPLEX*16 CLE
      COMPLEX*16 CTTP,CTBP,CTEP,CLTP,CLBP,CLEP,CATP,CABP,CAEP
      COMPLEX*16 CCTT0,CTT1,CTT2,CAT1,CAT2,CTRUN,CDCT,CDCTA
      COMPLEX*16 CATP0,CABP0
      COMPLEX*16 CAT00,CAB00,CAC00,CAL00,CAW00
      COMPLEX*16 CATP00,CABP00,CAEP00
      COMPLEX*16 CTOT,CDT,CDB,CTOT0,CDT0,CDB0
! MMM changed 28/2/23
      double precision glteff,glbeff,gllepeff,ghteff,ghbeff,
     .             ghlepeff,gateff,gabeff,galepeff,I2HDMEFF
! end MMM changed 28/2/23
      COMMON/HMASS_HDEC/AMSM,AMA,AML,AMH,AMCH,AMAR
      COMMON/HMASSR_HDEC/AMLR,AMHR
      COMMON/CHIMASS_HDEC/AMCHI
      COMMON/MASSES_HDEC/AMS,AMC,AMB,AMT
      COMMON/MSBAR_HDEC/AMCB,AMBB
      COMMON/ALS_HDEC/XLAMBDA,AMC0,AMB0,AMT0,N0
      COMMON/PARAM_HDEC/GF,ALPH,AMTAU,AMMUON,AMZ,AMW
      COMMON/CKMPAR_HDEC/VTB,VTS,VTD,VCB,VCS,VCD,VUB,VUS,VUD
      COMMON/BREAK_HDEC/AMEL,AMER,AMSQ,AMUR,AMDR,AL,AU,AD,AMU,AM2
      COMMON/BREAKGLU_HDEC/AMGLU
      COMMON/WZWDTH_HDEC/GAMC0,GAMT0,GAMT1,GAMW,GAMZ
      COMMON/MODEL_HDEC/IMODEL
      COMMON/ONSHELL_HDEC/IONSH,IONWZ,IOFSUSY
      COMMON/OLDFASH_HDEC/NFGG
      COMMON/FLAG_HDEC/IHIGGS,NNLO,IPOLE
      COMMON/SM4_HDEC/AMTP,AMBP,AMNUP,AMEP,ISM4,IGGELW
      COMMON/FERMIOPHOBIC_HDEC/IFERMPHOB
      COMMON/WIDTHSM_HDEC/SMBRB,SMBRL,SMBRM,SMBRS,SMBRC,SMBRT,SMBRG,
     .               SMBRGA,SMBRZGA,SMBRW,SMBRZ,SMWDTH
      COMMON/WIDTHSM4_HDEC/SMBRNUP,SMBREP,SMBRBP,SMBRTP
      COMMON/WIDTHA_HDEC/ABRB,ABRL,ABRM,ABRS,ABRC,ABRT,ABRG,ABRGA,
     .              ABRZGA,ABRZ,AWDTH
      COMMON/WIDTHHL_HDEC/HLBRB,HLBRL,HLBRM,HLBRS,HLBRC,HLBRT,HLBRG,
     .               HLBRGA,HLBRZGA,HLBRW,HLBRZ,HLBRA,HLBRAZ,HLBRHW,
     .               HLWDTH
      COMMON/WIDTHHH_HDEC/HHBRB,HHBRL,HHBRM,HHBRS,HHBRC,HHBRT,HHBRG,
     .               HHBRGA,HHBRZGA,HHBRW,HHBRZ,HHBRH,HHBRA,HHBRAZ,
     .               HHBRHW,HHWDTH
      COMMON/WIDTHHC_HDEC/HCBRB,HCBRL,HCBRM,HCBRBU,HCBRS,HCBRC,HCBRT,
     .               HCBRW,HCBRA,HCWDTH
      COMMON/WISUSY_HDEC/HLBRSC,HLBRSN,HHBRSC,HHBRSN,HABRSC,HABRSN,
     .              HCBRSU,HLBRCHT,HHBRCHT,HABRCHT,HLBRNET,HHBRNET,
     .              HABRNET,HCBRCNT,HLBRSL,HHBRSL,HCBRSL,HABRSL,HABRST,
     .              HABRSB,HHBRSQ,HHBRST,HHBRSB,HHBRSQT,HCBRSQ,HCBRSTB,
     .              HCBRSQT,HLBRSQ,HLBRSQT
      COMMON/WISFER_HDEC/BHLSLNL,BHLSLEL,BHLSLER,BHLSQUL,BHLSQUR,
     .              BHLSQDL,BHLSQDR,BHLST(2,2),BHLSB(2,2),BHLSTAU(2,2),
     .              BHHSLNL,BHHSLEL,BHHSLER,BHHSQUL,BHHSQUR,BHHSQDL,
     .              BHHSQDR,BHHST(2,2),BHHSB(2,2),BHHSTAU(2,2),
     .              BHASTAU,BHASB,BHAST,
     .              BHCSL00,BHCSL11,BHCSL21,BHCSQ,BHCSTB(2,2)
      COMMON/SMASS_HDEC/AMNEUT,XMNEUT,AMCHAR,AMST,AMSB,AMSL,
     .              AMSU,AMSD,AMSE,AMSN,AMSN1
      COMMON/COUP_HDEC/GAT,GAB,GLT,GLB,GHT,GHB,GZAH,GZAL,
     .            GHHH,GLLL,GHLL,GLHH,GHAA,GLAA,GLVV,GHVV,
     .            GLPM,GHPM,B,A
      COMMON/GOLDST_HDEC/AXMPL,AXMGD,IGOLD
      COMMON/WIGOLD_HDEC/HLBRGD,HABRGD,HHBRGD,HCBRGD
      COMMON/SLHA_gaug_HDEC/slhaneut,slhaxneut,slhachar,slhau,slhav,
     .                      slhaz,xmchar
      COMMON/SLHA_vals_HDEC/islhai,islhao
      COMMON/BREAKSCALE_HDEC/SUSYSCALE
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      COMMON/DAVID/QSUSY1,QSUSY2,LOOP
      COMMON/SQNLO_HDEC/YMSB(2),STYB,CTYB,YLBB(2,2),YHBB(2,2),YABB,
     .                  YMST(2),STYT,CTYT,YLTT(2,2),YHTT(2,2),YATT
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      COMMON/CPSM_HDEC/CPW,CPZ,CPTAU,CPMU,CPT,CPB,CPC,CPS,
     .                 CPGAGA,CPGG,CPZGA,ICOUPELW
      COMMON/CPSM4_HDEC/CPTP,CPBP,CPNUP,CPEP
c MMM changed 21/8/13
      COMMON/THDM_HDEC/TGBET2HDM,ALPH2HDM,AMHL2HDM,AMHH2HDM,
     .     AMHA2HDM,AMHC2HDM,AM12SQ,A1LAM2HDM,A2LAM2HDM,A3LAM2HDM,
     .     A4LAM2HDM,A5LAM2HDM,ITYPE2HDM,I2HDM,IPARAM2HDM
      COMMON/THDM_COUP_HDEC/gllep,ghlep,galep
      COMMON/WIDTH_HC_ADD/hcbrcd,hcbrts,hcbrtd
      COMMON/WIDTH_2HDM/hcbrwhh,hhbrchch,hlbrchch,abrhhaz,abrhawphm
      common/DECPARAMETERS/amhi,amhj,amhk,gamtotj,gamtotk
      COMMON/OMIT_ELW_HDEC/IOELW
      common/marcelvos_hdec/scalmq,imarcelvos,ioutput
c MMM changed 28/2/23
      common/EFF2HDMCOUP/glteff,glbeff,gllepeff,ghteff,ghbeff,
     .     ghlepeff,gateff,gabeff,galepeff,I2HDMEFF
c end MMM changed 28/2/23
      external hvhinteg
c end MMM changed 21/8/13
      HVV(X,Y)= GF/(4.D0*PI*DSQRT(2.D0))*X**3/2.D0*BETA_HDEC(Y)
     .            *(1.D0-4.D0*Y+12.D0*Y**2)
      AFF(X,Y)= GF/(4*PI*DSQRT(2.D0))*X**3*Y*(BETA_HDEC(Y))
      HFF(X,Y)= GF/(4*PI*DSQRT(2.D0))*X**3*Y*(BETA_HDEC(Y))**3
      CFF(Z,TB,X,Y)= GF/(4*PI*DSQRT(2.D0))*Z**3*LAMB_HDEC(X,Y)
     .              *((1.D0-X-Y)*(X*TB**2+Y/TB**2)-4.D0*X*Y)
      HV(V)=3.D0*(1.D0-8.D0*V+20.D0*V**2)/DSQRT((4.D0*V-1.D0))
     .      *DACOS((3.D0*V-1.D0)/2.D0/DSQRT(V**3))
     .      -(1.D0-V)*(47.D0/2.D0*V-13.D0/2.D0+1.D0/V)
     .      -3.D0/2.D0*(1.D0-6.D0*V+4.D0*V**2)*DLOG(V)
      HVH(X,Y)=0.25D0*( (1-X)*(-2+4*X-2*X**2+9*Y+9*X*Y-6*Y**2)
     .        /(3*Y)-2*(1-X-X**2+X**3-3*Y-2*X*Y-3*X**2*Y+3*Y**2
     .        +3*X*Y**2-Y**3)*(-PI/2- DATAN((1-2*X+X**2-Y-X*Y)/
     .         ((1-X)*DSQRT(-1.D0+2*X+2*Y-(X-Y)**2))))/DSQRT(-1.D0
     .         +2*X-(X-Y)**2+2*Y)-(1+X**2-2*Y-2*X*Y+Y**2)*DLOG(X))

c     HVH(X,Y)=0.25D0*( (1-X)*(5.D0*(1.D0+X)-4.D0*Y-2.D0/Y*
c    .     (-1.D0+2.D0*X+2.D0*Y-(X-Y)**2))/3
c    .        -2*(1-X-X**2+X**3-3*Y-2*X*Y-3*X**2*Y+3*Y**2
c    .        +3*X*Y**2-Y**3)*(-PI/2- DATAN((1-2*X+X**2-Y-X*Y)/
c    .         ((1-X)*DSQRT(-1.D0+2*X+2*Y-(X-Y)**2))))/DSQRT(-1.D0
c    .         +2*X-(X-Y)**2+2*Y)-(1+X**2-2*Y-2*X*Y+Y**2)*DLOG(X))

      qcd0(x) = (1+x**2)*(4*sp_hdec((1-x)/(1+x))+2*sp_hdec((x-1)/(x+1))
     .        - 3*dlog((1+x)/(1-x))*dlog(2/(1+x))
     .        - 2*dlog((1+x)/(1-x))*dlog(x))
     .        - 3*x*dlog(4/(1-x**2)) - 4*x*dlog(x)
      hqcdm(x)=qcd0(x)/x+(3+34*x**2-13*x**4)/16/x**3*dlog((1+x)/(1-x))
     .        + 3.d0/8/x**2*(7*x**2-1)
      aqcdm(x)=qcd0(x)/x + (19+2*x**2+3*x**4)/16/x*dlog((1+x)/(1-x))
     .        + 3.d0/8*(7-x**2)
      hqcd(x)=(4.d0/3*hqcdm(beta_hdec(x))
     .        +2*(4.d0/3-dlog(x))*(1-10*x)/(1-4*x))*ash/pi
c    .       + (29.14671d0 + x*(-93.72459d0+12)
     .       + (29.14671d0
     .         +ratcoup*(1.570d0 - 2*dlog(higtop)/3
     .                                     + dlog(x)**2/9))*(ash/pi)**2
     .       + (164.14d0 - 25.77d0*5 + 0.259d0*5**2)*(ash/pi)**3
     .       +(39.34d0-220.9d0*5+9.685d0*5**2-0.0205d0*5**3)*(ash/pi)**4
      aqcd(x)=(4.d0/3*aqcdm(beta_hdec(x))
     .        +2*(4.d0/3-dlog(x))*(1-6*x)/(1-4*x))*ash/pi
     .       + (29.14671d0 + ratcoup*(23/6.d0 - dlog(higtop)
     .                                     + dlog(x)**2/6))*(ash/pi)**2
     .       + (164.14d0 - 25.77d0*5 + 0.259d0*5**2)*(ash/pi)**3
     .       +(39.34d0-220.9d0*5+9.685d0*5**2-0.0205d0*5**3)*(ash/pi)**4
      qcdh(x)=1.d0+hqcd(x)
      qcda(x)=1.d0+aqcd(x)
      TQCDH(X)=1.D0+4.D0/3*HQCDM(BETA_HDEC(X))*ASH/PI
      TQCDA(X)=1.D0+4.D0/3*AQCDM(BETA_HDEC(X))*ASH/PI
      QCDC(X,Y)=1.D0+4/3.D0*ASH/PI*(9/4.D0 + (3-2*X+2*Y)/4*DLOG(X/Y)
     .         +((1.5D0-X-Y)*LAMB_HDEC(X,Y)**2+5*X*Y)/2/LAMB_HDEC(X,Y)
     .         /(1-X-Y)*DLOG(XI_HDEC(X,Y)*XI_HDEC(Y,X))
     .         + BIJ_HDEC(X,Y))
     .         + ASH/PI*(2*(4/3.D0-DLOG(X))
     .         - (X*2*(4/3.D0-DLOG(X)) + Y*2*(4/3.D0-DLOG(Y)))/(1-X-Y)
     .         - (X*2*(4/3.D0-DLOG(X))*(1-X+Y)
     .           +Y*2*(4/3.D0-DLOG(Y))*(1+X-Y))/LAMB_HDEC(X,Y)**2)
      QCDCI(X,Y)=1.D0+4/3.D0*ASH/PI*(3 + (Y-X)/2*DLOG(X/Y)
     .         +(2*(1-X-Y)+LAMB_HDEC(X,Y)**2)/2/LAMB_HDEC(X,Y)
     .         *DLOG(XI_HDEC(X,Y)*XI_HDEC(Y,X))
     .         + BIJ_HDEC(X,Y))
     .         + ASH/PI*(2*(4/3.D0-DLOG(X)) + 2*(4/3.D0-DLOG(Y))
     .         - (X*2*(4/3.D0-DLOG(X))*(1-X+Y)
     .           +Y*2*(4/3.D0-DLOG(Y))*(1+X-Y))/LAMB_HDEC(X,Y)**2)
      QCDCM(X,Y)=1.D0+4/3.D0*ASH/PI*(9/4.D0 + (3-2*X+2*Y)/4*DLOG(X/Y)
     .         +((1.5D0-X-Y)*LAMB_HDEC(X,Y)**2+5*X*Y)/2/LAMB_HDEC(X,Y)
     .         /(1-X-Y)*DLOG(4*X*Y/(1-X-Y+LAMB_HDEC(X,Y))**2)
     .         + BIJ_HDEC(X,Y))
      QCDCMI(X,Y)=1.D0+4/3.D0*ASH/PI*(3 + (Y-X)/2*DLOG(X/Y)
     .         +(2*(1-X-Y)+LAMB_HDEC(X,Y)**2)/2/LAMB_HDEC(X,Y)
     .         *DLOG(4*X*Y/(1-X-Y+LAMB_HDEC(X,Y))**2)
     .         + BIJ_HDEC(X,Y))
c------------------------------------------------------------------------
c--running x Yukawa coupling
      QCDCM1(X,Y)=1.D0+4/3.D0*ASH/PI*(9/4.D0 + (3-2*X+2*Y)/4*DLOG(X/Y)
     .         +((1.5D0-X-Y)*LAMB_HDEC(X,Y)**2+5*X*Y)/2/LAMB_HDEC(X,Y)
     .         /(1-X-Y)*DLOG(4*X*Y/(1-X-Y+LAMB_HDEC(X,Y))**2)
     .         + BIJ_HDEC(X,Y))
     .         + ASH/PI*(2*(4/3.D0-DLOG(X)))
      QCDCMI1(X,Y)=1.D0+4/3.D0*ASH/PI*(3 + (Y-X)/2*DLOG(X/Y)
     .         +(2*(1-X-Y)+LAMB_HDEC(X,Y)**2)/2/LAMB_HDEC(X,Y)
     .         *DLOG(4*X*Y/(1-X-Y+LAMB_HDEC(X,Y))**2)
     .         + BIJ_HDEC(X,Y))
     .         + ASH/PI*(2*(4/3.D0-DLOG(X)))
c------------------------------------------------------------------------
      CQCD(Z,TB,X,Y,R)= GF/(4*PI*DSQRT(2.D0))*Z**3*LAMB_HDEC(X,Y)
     .              *((1.D0-X-Y)*(X*TB**2*R**2*QCDC(X,Y)
     .                           +Y/TB**2*QCDC(Y,X))
     .               -4.D0*X*Y*R*QCDCI(X,Y))
      CQCDM(Z,TB,X,Y,R)= GF/(4*PI*DSQRT(2.D0))*Z**3*LAMB_HDEC(X,Y)
     .              *((1.D0-X-Y)*(X*TB**2*R**2*QCDCM1(X,Y)
     .                           +Y/TB**2*QCDCM(Y,X))
     .               -4.D0*X*Y*R*QCDCMI1(X,Y))
c MMM changed 21/8/13
      CQCD2HDM(Z,GCPD,GCPU,X,Y,R)= 
     .     GF/(4*PI*DSQRT(2.D0))*Z**3*LAMB_HDEC(X,Y)
     .     *((1.D0-X-Y)*(X*GCPD**2*R**2*QCDC(X,Y)
     .                  +Y*GCPU**2*QCDC(Y,X))
     .     -4.D0*GCPD*GCPU*X*Y*R*QCDCI(X,Y))
      CQCDM2HDM(Z,GCPD,GCPU,X,Y,R)= 
     .     GF/(4*PI*DSQRT(2.D0))*Z**3*LAMB_HDEC(X,Y)
     .     *((1.D0-X-Y)*(X*GCPD**2*R**2*QCDCM1(X,Y)
     .                  +Y*GCPU**2*QCDCM(Y,X))
     .     -4.D0*GCPD*GCPU*X*Y*R*QCDCMI1(X,Y))
c end MMM changed 21/8/13
      ELW(AMH,AMF,AMFP,QF,AI3F)=IOELW*(
     .        ELWFULL_HDEC(AMH,AMF,AMFP,QF,AI3F,AMW)
     .      + ALPH/PI*QF**2*HQCDM(BETA_HDEC(AMF**2/AMH**2))
     .      - GF*AMH**2/16.D0/PI**2/DSQRT(2.D0)*2.117203D0)
      ELW0(AMH,AMF,QF,ACF)=IOELW*(ALPH/PI*3.D0/2*QF**2
     .                              *(3.D0/2-DLOG(AMH**2/AMF**2))
     .      +GF/8/DSQRT(2.D0)/PI**2*(ACF*AMT**2
     .        +AMW**2*(3*DLOG(CS)/SS-5)+AMZ**2*(0.5D0
     .          -3*(1-4*SS*DABS(QF))**2)))
      CF(CA) = -CDLOG(-(1+CDSQRT(1-CA))/(1-CDSQRT(1-CA)))**2/4
      CG(CA) = CDSQRT(1-CA)/2*CDLOG(-(1+CDSQRT(1-CA))/(1-CDSQRT(1-CA)))
      CI1(CA,CB) = CA*CB/2/(CA-CB)
     .           + CA**2*CB**2/2/(CA-CB)**2*(CF(CA)-CF(CB))
     .           + CA**2*CB/(CA-CB)**2*(CG(CA)-CG(CB))
      CI2(CA,CB) = -CA*CB/2/(CA-CB)*(CF(CA)-CF(CB))
      HGGQCD(ASG,NF)=1.D0+ASG/PI*(95.D0/4.D0-NF*7.D0/6.D0)
      HGGQCD2(ASG,NF,AMH,AMT)=1.D0+ASG/PI*(95.D0/4.D0-NF*7.D0/6.D0)
     . +(ASG/PI)**2*(149533/288.D0-363/8.D0*ZETA2-495/8.D0*ZETA3
     .              +19/8.D0*DLOG(AMH**2/AMT**2)
     . +NF*(-4157/72.D0+11/2.D0*ZETA2+5/4.D0*ZETA3
     . +2/3.D0*DLOG(AMH**2/AMT**2))
     . +NF**2*(127/108.D0-1/6.D0*ZETA2))
     . +(ASG/PI)**3*(467.683620788D0+122.440972222D0*DLOG(AMH**2/AMT**2)
     .              +10.9409722222D0*DLOG(AMH**2/AMT**2)**2)
      PHGGQCD(ASG,NF)=1.D0+ASG/PI*(73/4.D0-7/6.D0*NF)
      PHGGQCD2(ASG,NF,AMH,AMT)=1.D0+ASG/PI*(73/4.D0-7/6.D0*NF)
     . +(ASG/PI)**2*(37631/96.D0-363/8.D0*ZETA2-495/8.D0*ZETA3
     . +NF*(-7189/144.D0+11/2.D0*ZETA2+5/4.D0*ZETA3)
     . +NF**2*(127/108.D0-1/6.D0*ZETA2))
     . +(ASG/PI)**3*(-212.447364638D0)
      DHGGQCD(ASG,NF)=1.D0+ASG/PI*(21.D0-NF*7.D0/6.D0)
      DHGGQCD2(ASG,NF,AMH,AMT)=1.D0+ASG/PI*(21.D0-NF*7.D0/6.D0)
     . +(ASG/PI)**2*(32531/72.D0-363/8.D0*ZETA2-495/8.D0*ZETA3
     .              +19/16.D0*DLOG(AMH**2/AMT**2)
     . +NF*(-15503/288.D0+11/2.D0*ZETA2+5/4.D0*ZETA3
     .     +1/3.D0*DLOG(AMH**2/AMT**2))
     . +NF**2*(127/108.D0-1/6.D0*ZETA2))
     . +(ASG/PI)**3*(63.7474683529D0+53.3715277778D0*DLOG(AMH**2/AMT**2)
     .              +5.47048611111D0*DLOG(AMH**2/AMT**2)**2)
      SGGQCD(ASG)=ASG/PI*7.D0/2.D0
      AGGQCD(ASG,NF)=1.D0+ASG/PI*(97.D0/4.D0-NF*7.D0/6.D0)
      AGGQCD2(ASG,NF,AMA,AMT)=1.D0+ASG/PI*(97.D0/4.D0-NF*7.D0/6.D0)
     . +(ASG/PI)**2*(237311/864.D0-529/24.D0*ZETA2-445/8.D0*ZETA3
     . +5*DLOG(AMA**2/AMT**2))
      HFFSELF(AMH)=1.D0+IOELW*(
     .              GF*AMH**2/16.D0/PI**2/DSQRT(2.D0)*2.117203D0
     .            -(GF*AMH**2/16.D0/PI**2/DSQRT(2.D0))**2*32.6567D0)
      HVVSELF(AMH)=1.D0+IOELW*(
     .              GF*AMH**2/16.D0/PI**2/DSQRT(2.D0)*2.800952D0
     .            +(GF*AMH**2/16.D0/PI**2/DSQRT(2.D0))**2*62.0308D0)
c     WTOP0(X,ASG,NF)=ASG/PI*(-2/3.D0*(PI**2+2*SP_HDEC(X)-2*SP_HDEC(1-X)
c    .               +(4*X*(1-X-2*X**2)*DLOG(X)
c    .                +2*(1-X)**2*(5+4*X)*DLOG(1-X)
c    .                -(1-X)*(5+9*X-6*X**2))
c    .               /2/(1-X)**2/(1+2*X)))
      WTOP(X,ASG,NF)=ASG/PI*(-2/3.D0*(PI**2+2*SP_HDEC(X)-2*SP_HDEC(1-X)
     .               +(4*X*(1-X-2*X**2)*DLOG(X)
     .                +2*(1-X)**2*(5+4*X)*DLOG(1-X)
     .                -(1-X)*(5+9*X-6*X**2))
     .               /2/(1-X)**2/(1+2*X)))
     .       + (ASG/2/PI)**2*(-110.4176D0+7.978D0*NF)
c     CHTOP0(X)=-4/3.D0*((5/2.D0-1/X)*DLOG(1-X)+X/(1-X)*DLOG(X)
c    .            +SP_HDEC(X)-SP_HDEC(1-X)+PI**2/2-9/4.D0)
      CHTOP(X)=8/3.D0*(SP_HDEC(1-X)-X/2/(1-X)*DLOG(X)
     .        + DLOG(X)*DLOG(1-X)/2+(1-2.5D0*X)/2/X*DLOG(1-X)-PI**2/3
     .        +9/8.D0)
      CHTOP1(X)=8/3.D0*(SP_HDEC(1-X)-X/2/(1-X)*DLOG(X)
     .        + DLOG(X)*DLOG(1-X)/2+(1-2.5D0*X)/2/X*DLOG(1-X)-PI**2/3
     .        +17/8.D0)

      PI=4D0*DATAN(1D0)
      SS=1.D0-(AMW/AMZ)**2
      CS=1.D0-SS

      ZETA2 = PI**2/6
      ZETA3 = 1.202056903159594D0
      ZETA4 = PI**4/90
      ZETA5 = 1.03692775514337D0

      IF(IHIGGS.NE.0)THEN
       TSC = (AMSQ+AMUR+AMDR)/3
       BSC = (AMSQ+AMUR+AMDR)/3
       CALL SFERMION_HDEC(TSC,BSC,AMSQ,AMUR,AMDR,AMEL,AMER,AL,AU,AD,AMU,
     .                AMST,AMSB,AMSL,AMSU,AMSD,AMSE,AMSN,AMSN1,
     .                GLEE,GLTT,GLBB,GHEE,GHTT,GHBB,
     .                GAEE,GATT,GABB,GCEN,GCTB)
       CALL twohdmcp_hdec(TGBET)
      ENDIF

C--DECOUPLING THE TOP QUARK FROM ALPHAS
      AMT0=3.D8

C--TOP QUARK DECAY WIDTH
c     GAMT00= GF*AMT**3/8/DSQRT(2D0)/PI*(1-AMW**2/AMT**2)**2
c    .                                 *(1+2*AMW**2/AMT**2)
c    .      * (1+WTOP(AMW**2/AMT**2,ALPHAS_HDEC(AMT,3),5))
      GAMT0 = GF*AMT**3/8/DSQRT(2D0)/PI*VTB**2
     .      * LAMB_HDEC(AMB**2/AMT**2,AMW**2/AMT**2)
     .      * ((1-AMW**2/AMT**2)*(1+2*AMW**2/AMT**2)
     .         -AMB**2/AMT**2*(2-AMW**2/AMT**2-AMB**2/AMT**2))
     .      * (1+WTOP(AMW**2/AMT**2,ALPHAS_HDEC(AMT,3),5))
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c--real!!
        LOOP  = 2
        QSUSY = 1
        QSUSY1 = QSUSY
        QSUSY2 = QSUSY
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       DO I = -10,10,1
c        QSUSY = 10.D0**(I/10.D0)
c        QSUSY1 = QSUSY
c        QSUSY2 = QSUSY
c        QSUSY = QSUSY1
c        CALL BOTSUSY_HDEC(GLB,GHB,GAB,XGLB,XGHB,XGAB,QSUSY,LOOP)
c        CALL STRSUSY_HDEC(GLB,GHB,GAB,XGLB,XGHB,XGAB,QSUSY,LOOP)
c       ENDDO
c       CLOSE(51)
c       CLOSE(52)
c       CLOSE(53)
c       CLOSE(54)
c       RETURN
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      IF(IHIGGS.NE.0.AND.AMT.GT.AMCH+AMB)THEN
       IF(I2HDM.EQ.0)THEN
        TSC = (AMSQ+AMUR+AMDR)/3
        BSC = (AMSQ+AMUR+AMDR)/3
       CALL SFERMION_HDEC(TSC,BSC,AMSQ,AMUR,AMDR,AMEL,AMER,AL,AU,AD,AMU,
     .                 AMST,AMSB,AMSL,AMSU,AMSD,AMSE,AMSN,AMSN1,
     .                 GLEE,GLTT,GLBB,GHEE,GHTT,GHBB,
     .                 GAEE,GATT,GABB,GCEN,GCTB)
c       LOOP  = 2
c       QSUSY = 1
c       QSUSY1 = QSUSY
c       QSUSY2 = QSUSY
        QSUSY = QSUSY1
        CALL BOTSUSY_HDEC(GLB,GHB,GAB,XGLB,XGHB,XGAB,QSUSY,LOOP)
       ELSE
        XGLB = GLB
        XGHB = GHB
        XGAB = GAB
       ENDIF
c      GAMT10= GF*AMT**3/8/DSQRT(2D0)/PI*VTB**2*(1-AMCH**2/AMT**2)**2
c    .        *((AMB/AMT)**2*XGAB**2 + GAT**2)
c    .      * (1+ALPHAS_HDEC(AMT,3)/PI*CHTOP0(AMCH**2/AMT**2))
       YMB = RUNM_HDEC(AMT,5)
c      GAMT10= GF*AMT**3/8/DSQRT(2D0)/PI*VTB**2
c    .      * LAMB_HDEC(AMB**2/AMT**2,AMCH**2/AMT**2)
c    .      * (((YMB/AMT)**2*XGAB**2 + GAT**2)
c    .      * (1+AMB**2/AMT**2-AMCH**2/AMT**2)+4*YMB**2/AMT**2*XGAB*GAT)
c    .      * (1+ALPHAS_HDEC(AMT,3)/PI*CHTOP0(AMCH**2/AMT**2))
       GAMT1 = GF*AMT**3/8/DSQRT(2D0)/PI*VTB**2
     .      * LAMB_HDEC(AMB**2/AMT**2,AMCH**2/AMT**2)
     .      * (GAT**2*(1+AMB**2/AMT**2-AMCH**2/AMT**2)
     .      * (1+ALPHAS_HDEC(AMT,3)/PI*CHTOP(AMCH**2/AMT**2))
     .        +(YMB/AMT)**2*XGAB**2*(1+AMB**2/AMT**2-AMCH**2/AMT**2)
     .      * (1+ALPHAS_HDEC(AMT,3)/PI*CHTOP1(AMCH**2/AMT**2))
     .      + 4*YMB**2/AMT**2*XGAB*GAT)
      ELSE
       GAMT1 = 0
      ENDIF
      GAMT1 = GAMT0+GAMT1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'topw: ',GAMT0,WTOP(AMW**2/AMT**2,ALPHAS_HDEC(AMT,3),5),
c    .                   WTOP0(AMW**2/AMT**2,ALPHAS_HDEC(AMT,3),5),
c    .                   ALPHAS_HDEC(AMT,3)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     GAMT10= GAMT00+GAMT10
c     GAMT0 = GAMT00
c     GAMT1 = GAMT10
c     write(6,*)'topw: ',GAMT0,GAMT00,GAMT00/GAMT0
c     write(6,*)'toph: ',GAMT1-GAMT0,GAMT10-GAMT00,
c    .                  (GAMT10-GAMT00)/(GAMT1-GAMT0)
c     write(6,*)'top:  ',gamt0/gamt1,(gamt1-gamt0)/gamt1,gamt1
c     write(6,*)'top0: ',gamt00/gamt10,(gamt10-gamt00)/gamt10,gamt10
c     write(6,*)'toph: ',CHTOP(AMCH**2/AMT**2),CHTOP0(AMCH**2/AMT**2),
c    .                   CHTOP(AMCH**2/AMT**2)/CHTOP0(AMCH**2/AMT**2)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'CKM: ',VUS,VCB,VUB/VCB
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      IF(IHIGGS.EQ.0)THEN

C        =========================================================
C                              SM HIGGS DECAYS
C        =========================================================
      AMXX=AMH
      AMH=AMSM
C     =============  RUNNING MASSES 
      RMS = RUNM_HDEC(AMH,3)
      RMC = RUNM_HDEC(AMH,4)
      RMB = RUNM_HDEC(AMH,5)
      RMT = RUNM_HDEC(AMH,6)
c     write(6,*) 'm_{s,c,b,t} (M_H) = ',AMH,RMS,RMC,RMB,RMT
c     write(6,*) 'm_{s,c,b,t} = ',runm_hdec(2.d0,3,0),runm_hdec(3.d0,4,0),
c    .                            runm_hdec(ambb,5,1),amt
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     XXX = RUNM_HDEC(AMB,5,1)
c     write(6,*) 'mb = ',XXX
c     do i=1,23
c      XXX = RUNM_HDEC(XXX,5,1)
c      write(6,*) 'mb = ',XXX
c     enddo
c     write(6,*)
c     XXX = RUNM_HDEC(AMC,4,0)
c     write(6,*) 'mc = ',XXX
c     do i=1,43
c      XXX = RUNM_HDEC(XXX,4,0)
c      write(6,*) 'mc = ',XXX
c     enddo
c     write(6,*)'mc(3): ',RUNM_HDEC(3.D0,4,0)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)
c     write(6,*) 'alpha_s(MZ) = ',ALPHAS_HDEC(AMZ,3)
c     write(6,*)
c     XXX = RUNM_HDEC(AMBB,5,1)
c     write(6,*) 'Mb = ',AMB
c     write(6,*) 'mb = ',XXX
c     write(6,*) 'mb = ',AMBB
c     write(6,*)
c     XXX = RUNM_HDEC(3.D0,4,0)
c     write(6,*) 'Mc = ',AMC
c     write(6,*) 'mc = ',XXX
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,"(F9.2,3(2X,G12.5))")AMH,RMC,RMB,RMT
      IF(ISM4.NE.0)THEN
       RMBP= RUNM_HDEC(AMH,7)
       RMTP= RUNM_HDEC(AMH,8)
c      write(6,*)AMTP,AMBP
c      write(6,*)RUNM_HDEC(AMTP,8,1),RUNM_HDEC(AMTP,7,1)
c      write(6,*)RMTP,RMBP,AMH
      ENDIF
      RATCOUP = 1
      HIGTOP = AMH**2/AMT**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'strange mass: ',runm_hdec(1.d0,3,0),runm_hdec(2.d0,3,0)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     X1 = RUNM_HDEC(AMB,5,1)
c     X2 = RUNM_HDEC(AMH,5,1)
c     do i=1,100
c      x1 = RUNM_HDEC(x1,5,1)
c     enddo
c     write(6,*)'mb,MH      = ',AMB,AMH
c     write(6,*)'als(MZ)    = ',ALPHAS_HDEC(AMZ,3)
c     write(6,*)'als(mb,MH) = ',ALPHAS_HDEC(AMB,3),ALPHAS_HDEC(AMH,3)
c     write(6,*)'mb(mb,MH)  = ',X1,X2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     TOPFAC = 1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     x2 = runm_hdec(amh/4,6,1)
c     x3 = runm_hdec(amh/2,6,1)
c     x4 = runm_hdec(amh,6,1)
c     x1 = amt
c     do i=1,100
c      x1 = RUNM_HDEC(x1,6,1)
c     enddo
c     write(6,*)'MH           = ',amh
c     write(6,*)'mt(mt,k*MH)  = ',amt,x1,x2,x3,x4
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     XK = 16.11D0
c     XK = XK - 1.0414D0*(1.D0-AMB/AMT)
c     XK = XK - 1.0414D0*(1.D0-AMC/AMT)
c     XK = XK - 1.0414D0*(1.D0-AMS/AMT)
c     XK = XK - 1.0414D0*(2.D0)
c     XK2 = 0.65269D0*(5)**2 - 29.7010D0*(5) + 239.2966D0
c     x0 = x1*(1+4*ALPHAS_HDEC(AMT,3)/3/PI
c    .              +XK*(ALPHAS_HDEC(AMT,3)/PI)**2
c    .              +XK2*(ALPHAS_HDEC(AMT,3)/PI)**3)
c    .   * (1-ALPHAS_HDEC(AMT,3)/PI*DLOG(AMT**2/x1**2))
c     write(6,*)x0,x0/x1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      x1 = runm_hdec(amh,4)
      x2 = runm_hdec(amh,5)
      x3 = runm_hdec(amh,6)
c     write(6,*)amh,x1,x2,x3
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      ASH=ALPHAS_HDEC(AMH,3)
c     write(66,('f4.0,3(g18.10)'))AMH,ASH,RMB,RUNM_HDEC(AMH/2,5,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      ASH=ALPHAS_HDEC(AMH,3)
c     write(66,('f4.0,3(g18.10)'))AMH,ASH,RMB,RUNM_HDEC(AMH/2,5,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      ASH=ALPHAS_HDEC(AMH,3)
c     write(66,('f4.0,3(g18.10)'))AMH,ASH,RMB,RUNM_HDEC(AMH/2,5,1)
      AMC0=1.D8
      AMB0=2.D8
      AS3=ALPHAS_HDEC(AMH,3)
      AMC0=AMC
      AS4=ALPHAS_HDEC(AMH,3)
      AMB0=AMB
C     AMT0=AMT
C     =============== PARTIAL WIDTHS 
C  H ---> G G
C
C  mass dependent NLO QCD corrections
       CALL CORRGG_HDEC(AMH,AMT,AMB,XHGG,XHQQ)
c      write(6,*)amh,amt,amb,xhgg,xhqq
       EPS=1.D-8
       NFEXT = 3
       ASG = AS3
       CTT = 4*AMT**2/AMH**2*DCMPLX(1D0,-EPS)
       CTB = 4*AMB**2/AMH**2*DCMPLX(1D0,-EPS)
       CTC = 4*AMC**2/AMH**2*DCMPLX(1D0,-EPS)
       CAT0= 2*CTT*(1+(1-CTT)*CF(CTT))
       CAB0= 2*CTB*(1+(1-CTB)*CF(CTB))
       CAC0= 2*CTC*(1+(1-CTC)*CF(CTC))
       CAT = CAT0 * CPT
       CAB = CAB0 * CPB
       CAC = CAC0 * CPC
       CATP = 0
       CABP = 0
       CATP0= 0
       CABP0= 0
       IF(ISM4.NE.0)THEN
        CTTP = 4*AMTP**2/AMH**2*DCMPLX(1D0,-EPS)
        CTBP = 4*AMBP**2/AMH**2*DCMPLX(1D0,-EPS)
        CATP0= 2*CTTP*(1+(1-CTTP)*CF(CTTP))
        CABP0= 2*CTBP*(1+(1-CTBP)*CF(CTBP))
        CATP = CATP0 * CPTP
        CABP = CABP0 * CPBP
       ENDIF
       FQCD=HGGQCD(ASG,NFEXT)
       DQCD=DHGGQCD(ASG,NFEXT)
       PQCD=PHGGQCD(ASG,NFEXT)
       XFAC = CDABS(CAT+CAB+CAC+CATP+CABP)**2*FQCD
     .      + 4*DREAL(DCONJG(CAT+CAB+CAC+CATP+CABP)*CPGG)*DQCD
     .      + (2*CPGG)**2*PQCD
C  mass dependent NLO QCD corrections
       XFAC0 =(CDABS(CAT)**2*(XHGG(1)+XHQQ(1)*NFEXT)
     .       + CDABS(CAB)**2*(XHGG(2)+XHQQ(2)*NFEXT)
     .       + 2*DREAL(DCONJG(CAT)*CAB)*(XHGG(3)+XHQQ(3)*NFEXT))*ASG/PI
       XFAC = XFAC + XFAC0
       HGG=HVV(AMH,0.D0)*(ASG/PI)**2*XFAC/8

C  H ---> G G* ---> G CC   TO BE ADDED TO H ---> CC
       NFEXT = 4
       ASG = AS4
       FQCD=HGGQCD(ASG,NFEXT)
       DQCD=DHGGQCD(ASG,NFEXT)
       PQCD=PHGGQCD(ASG,NFEXT)
       XFAC = CDABS(CAT+CAB+CAC+CATP+CABP)**2*FQCD
     .      + 4*DREAL(DCONJG(CAT+CAB+CAC+CATP+CABP)*CPGG)*DQCD
     .      + (2*CPGG)**2*PQCD
C  mass dependent NLO QCD corrections
       XFAC0 =(CDABS(CAT)**2*(XHGG(1)+XHQQ(1)*NFEXT)
     .       + CDABS(CAB)**2*(XHGG(2)+XHQQ(2)*NFEXT)
     .       + 2*DREAL(DCONJG(CAT)*CAB)*(XHGG(3)+XHQQ(3)*NFEXT))*ASG/PI
       XFAC = XFAC + XFAC0
       DCC=HVV(AMH,0.D0)*(ASG/PI)**2*XFAC/8 - HGG
C  H ---> G G* ---> G BB   TO BE ADDED TO H ---> BB
       NFEXT = 5
       ASG = ASH
       FQCD=HGGQCD(ASG,NFEXT)
       DQCD=DHGGQCD(ASG,NFEXT)
       PQCD=PHGGQCD(ASG,NFEXT)
       XFAC = CDABS(CAT+CAB+CAC+CATP+CABP)**2*FQCD
     .      + 4*DREAL(DCONJG(CAT+CAB+CAC+CATP+CABP)*CPGG)*DQCD
     .      + (2*CPGG)**2*PQCD
C  mass dependent NLO QCD corrections
       XFAC0 =(CDABS(CAT)**2*(XHGG(1)+XHQQ(1)*NFEXT)
     .       + CDABS(CAB)**2*(XHGG(2)+XHQQ(2)*NFEXT)
     .       + 2*DREAL(DCONJG(CAT)*CAB)*(XHGG(3)+XHQQ(3)*NFEXT))*ASG/PI
       XFAC = XFAC + XFAC0
       DBB=HVV(AMH,0.D0)*(ASG/PI)**2*XFAC/8 - HGG - DCC

C  H ---> G G: FULL NNNLO CORRECTIONS FOR NF=5
       IF(ISM4.EQ.0)THEN
        FQCD0=HGGQCD(ASG,5)
        FQCD=HGGQCD2(ASG,5,AMH,AMT)
        DQCD=DHGGQCD2(ASG,5,AMH,AMT)
        PQCD=PHGGQCD2(ASG,5,AMH,AMT)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      FQCD=FQCD0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        XFAC = CDABS(CAT+CAB+CAC+CATP+CABP)**2*FQCD
     .      + 4*DREAL(DCONJG(CAT+CAB+CAC+CATP+CABP)*CPGG)*DQCD
     .      + (2*CPGG)**2*PQCD
C  mass dependent NLO QCD corrections
        XFAC0 =(CDABS(CAT)**2*(XHGG(1)+XHQQ(1)*NFEXT)
     .        + CDABS(CAB)**2*(XHGG(2)+XHQQ(2)*NFEXT)
     .        + 2*DREAL(DCONJG(CAT)*CAB)*(XHGG(3)+XHQQ(3)*NFEXT))*ASG/PI
c       write(6,*)'xfac: ',XFAC,XFAC0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      XFAC0=0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        XFAC = XFAC + XFAC0
        HGG=HVV(AMH,0.D0)*(ASG/PI)**2*XFAC/8
C  electroweak corrections
        IF(ICOUPELW.EQ.0)THEN
c        XFAC00 = CDABS(CAT0+CAB0+CAC0+CATP0+CABP0)**2*FQCD
         XFAC00= DREAL(DCONJG(CAT+CAB+CAC+CATP+CABP)
     .                     * (CAT0+CAB0+CAC0+CATP0+CABP0))*FQCD
        ELSE
         XFAC00 = CDABS(CAT+CAB+CAC+CATP+CABP)**2*FQCD
     .          + 4*DREAL(DCONJG(CAT+CAB+CAC+CATP+CABP)*CPGG)*DQCD
     .          + (2*CPGG)**2*PQCD
        ENDIF
        HGG0 =HVV(AMH,0.D0)*(ASG/PI)**2*XFAC00/8*IOELW*GLGL_ELW(AMT,AMH)
c       write(6,*)'hgg: ',HGG,HGG0,XFAC00,GLGL_ELW(AMT,AMH)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      HGG0=0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        HGG=HGG+HGG0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      CDUM0 = CAT0+CAB0
c      CDUM  = CAT +CAB
c      CTOT0= CAT0/CDUM0
c      CDT0 = CAT0/CAT0
c      CDB0 = CAB0/CAT0
c      CTOT = CAT/CDUM
c      CDT  = CAT/CAT
c      CDB  = CAB/CAT
c      write(6,*)'H -> gg ',CTOT,CDT,CDB
c      write(6,*)'H -> gg0',CTOT0,CDT0,CDB0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'alpha_s= ',ASG
c     write(6,*)'H -> gg: ',1.D0,ASG/PI*(95.D0/4.D0-5*7.D0/6.D0),
c    .  (ASG/PI)**2*(149533/288.D0-363/8.D0*ZETA2-495/8.D0*ZETA3
c    .              +19/8.D0*DLOG(AMH**2/AMT**2)
c    . +5*(-4157/72.D0+11/2.D0*ZETA2+5/4.D0*ZETA3
c    . +2/3.D0*DLOG(AMH**2/AMT**2))
c    . +5**2*(127/108.D0-1/6.D0*ZETA2)),
c    .  (ASG/PI)**3*(467.683620788D0+122.440972222D0*DLOG(AMH**2/AMT**2)
c    .  +10.9409722222D0*DLOG(AMH**2/AMT**2)**2),
c    .  XFAC0/CDABS(CAT+CAB+CAC+CATP+CABP)**2,
c    .  XFAC/CDABS(CAT+CAB+CAC+CATP+CABP)**2,GLGL_ELW(AMT,AMH),
c    .  (HGG)/(HVV(AMH,0.D0)*(ASG/PI)**2/8*CDABS(CAT+CAB+CAC)**2),
c    .  (XFAC-XFAC0)/CDABS(CAT+CAB+CAC+CATP+CABP)**2
c    . * (1+GLGL_ELW(AMT,AMH)) + XFAC0/CDABS(CAT+CAB+CAC+CATP+CABP)**2,
c    .  (FQCD+XFAC0/CDABS(CAT+CAB+CAC)**2)*(1+GLGL_ELW(AMT,AMH)),
c    .  (FQCD+XFAC0/CDABS(CAT+CAB+CAC)**2)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       write(6,*)'Hgg:',XFAC/FQCD/(4/3.d0)**2-1
c    .                  ,CDABS(CAT)**2/(4/3.d0)**2-1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       write(6,*)CDABS(CAT)**2,16/9.D0,16/9.D0/CDABS(CAT)**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       write(46,*)AMH,FQCD0,FQCD0+XFAC0/CDABS(CAT+CAB)**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       YY0 = CDABS(CAT+CAC)**2*FQCD
c    .      + CDABS(CAT)**2*(XHGG(1)+XHQQ(1)*NFEXT)*ASG/PI
c       YTT = HVV(AMH,0.D0)*(ASG/PI)**2*YY0/8/HGG*(1+GLGL_ELW(AMT,AMH))
c       YY0 = CDABS(CAB)**2*(FQCD+(XHGG(2)+XHQQ(2)*NFEXT)*ASG/PI)
c       YBB = HVV(AMH,0.D0)*(ASG/PI)**2*YY0/8/HGG*(1+GLGL_ELW(AMT,AMH))
c       YY0 = 2*DREAL(DCONJG(CAT+CAC)*CAB)*FQCD
c    .      + 2*DREAL(DCONJG(CAT)*CAB)*(XHGG(3)+XHQQ(3)*NFEXT)*ASG/PI
c       YTB = HVV(AMH,0.D0)*(ASG/PI)**2*YY0/8/HGG*(1+GLGL_ELW(AMT,AMH))
c       write(51,('f4.0,4(g18.10)'))AMH,HGG,YTT,YBB,YTB
c       write(6,*)'gg: ',AMH,YTT+YBB+YTB
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       XFAC1 = CDABS(CAT)**2*(XHGG(1)+XHQQ(1)*NFEXT)*ASG/PI
c       XFAC2 = CDABS(CAB)**2*(XHGG(2)+XHQQ(2)*NFEXT)*ASG/PI
c       XFAC3 = 2*DREAL(DCONJG(CAT)*CAB)*(XHGG(3)+XHQQ(3)*NFEXT)*ASG/PI
c       write(50,*)AMH,FQCD0,XFAC1/CDABS(CAT)**2/FQCD0
c       write(50,*)AMH,FQCD0,XFAC2/CDABS(CAB)**2/FQCD0
c       write(50,*)AMH,FQCD0,XFAC3/(2*DREAL(DCONJG(CAT)*CAB))/FQCD0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       write(50,*)AMH,FQCD0,FQCD0+XFAC1/CDABS(CAT)**2
c       write(51,*)AMH,FQCD0,FQCD0+XFAC2/CDABS(CAB)**2
c       write(52,*)AMH,FQCD0,FQCD0+XFAC3/(2*DREAL(DCONJG(CAT)*CAB))
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       ELSE
        IM = IGGELW
        FQCD0=HGGQCD(ASG,5)
        DQCD=DHGGQCD2(ASG,5,AMH,AMT)
        PQCD=PHGGQCD2(ASG,5,AMH,AMT)
        FAC4 = -ASG**2/PI**2*(77/288.D0*2 + (2/3.D0*5+19/8.D0)
     .       * (CPBP*DLOG(AMBP**2/AMT**2)+CPTP*DLOG(AMTP**2/AMT**2))
     .         / (CPT+CPBP+CPTP))
        IF(CPBP.EQ.0.D0.AND.CPTP.EQ.0.D0.AND.CPT.EQ.0.D0)
     .                            FAC4 = -ASG**2/PI**2*(77/288.D0*2)
        FQCD=HGGQCD2(ASG,5,AMH,AMT)+FAC4
        DQCD=DQCD+FAC4/2
        XFAC = CDABS(CAT+CAB+CAC+CATP+CABP)**2*FQCD
        HGG=HVV(AMH,0.D0)*(ASG/PI)**2*XFAC/8
        IF(ICOUPELW.EQ.0)THEN
         XFAC00= DREAL(DCONJG(CAT+CAB+CAC+CATP+CABP)
     .                     * (CAT0+CAB0+CAC0+CATP0+CABP0))*FQCD
        ELSE
         XFAC00 = CDABS(CAT+CAB+CAC+CATP+CABP)**2*FQCD
     .          + 4*DREAL(DCONJG(CAT+CAB+CAC+CATP+CABP)*CPGG)*DQCD
     .          + (2*CPGG)**2*PQCD
        ENDIF
        HGG0 = HVV(AMH,0.D0)*(ASG/PI)**2*XFAC00/8 * GLGL_ELW4(IM,AMH)
        HGG=HGG+HGG0
c     write(6,*)'H -> gg: ',AMH,HGG,GLGL_ELW4(IM,AMH)*100,FQCD,
c    .                      FAC4/FQCD*100
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        IF(AMNUP.EQ.AMEP)THEN
         XXL = AMEP**2
        ELSE
         XXL =AMNUP**2*AMEP**2/(AMNUP**2-AMEP**2)*DLOG(AMNUP**2/AMEP**2)
        ENDIF
        IF(AMTP.EQ.AMBP)THEN
         XXQ = AMTP**2
        ELSE
         XXQ = AMTP**2*AMBP**2/(AMTP**2-AMBP**2)*DLOG(AMTP**2/AMBP**2)
        ENDIF
        CELW = GF/8/DSQRT(2.D0)/PI**2*(5*AMT**2/2
     .       + 7*(AMNUP**2+AMEP**2)/6 - XXL
     .       + 3*(AMTP**2+AMBP**2)/2 - 3*XXQ)
c      write(6,*)'H -> gg: ',AMH,2*CELW*100,GLGL_ELW4(IM,AMH)*100
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       ENDIF
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'H -> gg: ',HGG,DBB,DCC
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      IF(NFGG.EQ.3)THEN
       HGG = HGG - DBB - DCC
      ELSEIF(NFGG.EQ.4)THEN
       HGG = HGG - DBB
       DCC = 0
      ELSE
       DCC = 0
       DBB = 0
      ENDIF
      IF(IFERMPHOB.NE.0)THEN
c      write(6,*)'Fermiophobic: H -> gg ---> 0'
       HGG = 0
       DBB = 0
       DCC = 0
      ENDIF
c     write(6,*)'H -> GG: ',FQCD0,FQCD,FQCD-FQCD0
c     write(6,*)'H -> GG: ',(FQCD0-1)/ASG*PI,(FQCD-FQCD0)/ASG**2*PI**2
c     write(6,*)'H -> GG: ',GLGL_ELW(AMT,AMH)
c     write(6,*)'XFAC(',AMH,') = ',xfac
c     write(6,*)'BR(H -> gg) = ',HGG
c     write(6,*)'Ga(H -> gg) = ',HGG,HGG0,HVV(AMH,0.D0),XFAC,XFAC0

      SM4FACF=1
      SM4FACW = 1
      SM4FACZ = 1
      IF(ISM4.NE.0)THEN
       SM4FACF=1+ELW4_HDEC(AMNUP,AMEP,AMTP,AMBP)
       SM4FACW=1+ELW4V_HDEC(1,AMNUP,AMEP,AMTP,AMBP)
       SM4FACZ=1+ELW4V_HDEC(2,AMNUP,AMEP,AMTP,AMBP)
c      write(6,*)'SM4 elw.: ff,WW,ZZ ',(SM4FACF-1)*100,
c    .                 (SM4FACW-1)*100,(SM4FACZ-1)*100
      ENDIF

C  H ---> E E
c     CPEL = CPMU
c     AMELEC = 0.510998910D-3
c     IF(AMH.LE.2*AMELEC) THEN
c      HEE = 0
c     ELSE
c     HEE=HFF(AMH,(AMELEC/AMH)**2)
c    .    *(1+ELW0(AMH,AMELEC,-1.D0,7.D0))
c    .    *(1+ELW(AMH,AMELEC,0.D0,-1.D0,-1/2.D0))
c    .    *HFFSELF(AMH)
c    .    *SM4FACF
c    .    * CPEL**2
c     IF(ICOUPELW.EQ.0)THEN
c      HEE=HFF(AMH,(AMELEC/AMH)**2) * CPMU
c    .    * ((CPEL-1)
c    .       +(1+ELW0(AMH,AMELEC,-1.D0,7.D0))
c    .       +(1+ELW(AMH,AMELEC,0.D0,-1.D0,-1/2.D0))
c    .       *HFFSELF(AMH) * SM4FACF)
c     ENDIF
c     ENDIF
C  H ---> MU MU
      IF(AMH.LE.2*AMMUON) THEN
       HMM = 0
      ELSE
      HMM=HFF(AMH,(AMMUON/AMH)**2)
c    .    *(1+ELW0(AMH,AMMUON,-1.D0,7.D0))
     .    *(1+ELW(AMH,AMMUON,0.D0,-1.D0,-1/2.D0))
     .    *HFFSELF(AMH)
     .    *SM4FACF
     .    * CPMU**2
      IF(ICOUPELW.EQ.0)THEN
       HMM=HFF(AMH,(AMMUON/AMH)**2) * CPMU
     .    * ((CPMU-1)
c    .       +(1+ELW0(AMH,AMMUON,-1.D0,7.D0))
     .       +(1+ELW(AMH,AMMUON,0.D0,-1.D0,-1/2.D0))
     .       *HFFSELF(AMH) * SM4FACF)
      ENDIF
      ENDIF
c     write(6,*)'ee/mumu: ',HEE/HMM,HEE/HMM * AMMUON**2/AMELEC**2
C  H ---> TAU TAU
      IF(AMH.LE.2*AMTAU) THEN
       HLL = 0
      ELSE
      HLL=HFF(AMH,(AMTAU/AMH)**2)
c    .    *(1+ELW0(AMH,AMTAU,-1.D0,7.D0))
     .    *(1+ELW(AMH,AMTAU,0.D0,-1.D0,-1/2.D0))
     .    *HFFSELF(AMH)
     .    *SM4FACF
     .    * CPTAU**2
      IF(ICOUPELW.EQ.0)THEN
       HLL=HFF(AMH,(AMTAU/AMH)**2) * CPTAU
     .    * ((CPTAU-1)
c    .        +(1+ELW0(AMH,AMTAU,-1.D0,7.D0))
     .        +(1+ELW(AMH,AMTAU,0.D0,-1.D0,-1/2.D0))
     .        *HFFSELF(AMH) * SM4FACF)
      ENDIF
      ENDIF

c      write(96,*)AMH,ELW0(AMH,AMTAU,-1.D0,7.D0)
c    .               -ALPH/PI*3.D0/2*(3.D0/2-DLOG(AMH**2/AMTAU**2))
c    .               ,ELW(AMH,AMTAU,0.D0,-1.D0,-1/2.D0)
c    .               -ALPH/PI*HQCDM(BETA_HDEC(AMTAU**2/AMH**2))
c    .               +GF*AMH**2/16.D0/PI**2/DSQRT(2.D0)*2.117203D0

c      write(6,*)'tau tau: ',AMH,ELW0(AMH,AMTAU,-1.D0,7.D0),HFFSELF(AMH)
C  H --> SS
      IRAT = 0
      RATCOUP = 1
      XQCD0 = QCDH(RMS**2/AMH**2)
      IF(CPT.EQ.0.D0)THEN
       RATCOUP = 0
      ELSE
       IF(CPS.EQ.0.D0)THEN
        CPS = 1.D-20
        IRAT = 1
       ENDIF
       RATCOUP = CPT/CPS
      ENDIF
      XQCD1= QCDH(RMS**2/AMH**2)
      XQCD = (XQCD0-XQCD1)/2/XQCD1
      IF(AMH.LE.2*AMS) THEN
       HSS = 0
      ELSE
       HS2=3.D0*HFF(AMH,(RMS/AMH)**2)
     .    *QCDH(RMS**2/AMH**2)
c    .    *(1+ELW0(AMH,RMS,-1.D0/3.D0,7.D0))
     .    *(1+ELW(AMH,AMS,AMC,-1/3.D0,-1/2.D0))
     .    *HFFSELF(AMH) * SM4FACF
     .    * CPS**2
      IF(ICOUPELW.EQ.0)THEN
       HS2=3.D0*HFF(AMH,(RMS/AMH)**2) * CPS
     .    *QCDH(RMS**2/AMH**2)
     .    * ((CPS-1)
c    .         +(1+ELW0(AMH,RMS,-1.D0/3.D0,7.D0)*(1+XQCD))
     .         +(1+ELW(AMH,AMS,AMC,-1/3.D0,-1/2.D0)*(1+XQCD))
     .       *HFFSELF(AMH) * SM4FACF)
      ENDIF
c      IF(HS2.LT.0.D0) HS2 = 0
       HS1=3.D0*HFF(AMH,(AMS/AMH)**2)
     .    *TQCDH(AMS**2/AMH**2)
c    .    *(1+ELW0(AMH,RMS,-1.D0/3.D0,7.D0))
     .    *(1+ELW(AMH,AMS,AMC,-1/3.D0,-1/2.D0))
     .    *HFFSELF(AMH) * SM4FACF
     .    *CPS**2
      IF(ICOUPELW.EQ.0)THEN
       HS1=3.D0*HFF(AMH,(AMS/AMH)**2) * CPS
     .    *TQCDH(AMS**2/AMH**2)
     .    * ((CPS-1)
c    .         +(1+ELW0(AMH,RMS,-1.D0/3.D0,7.D0))
     .         +(1+ELW(AMH,AMS,AMC,-1/3.D0,-1/2.D0))
     .       *HFFSELF(AMH) * SM4FACF)
      ENDIF
       RAT = 2*AMS/AMH
       HSS = QQINT_HDEC(RAT,HS1,HS2)
      ENDIF
c     HSS = HSS * SM4FACF
      IF(IRAT.EQ.1)CPS = 0
C  H --> CC
      IRAT = 0
      RATCOUP = 1
      XQCD0 = QCDH(RMC**2/AMH**2)
      IF(CPT.EQ.0.D0)THEN
       RATCOUP = 0
      ELSE
       IF(CPC.EQ.0.D0)THEN
        CPC = 1.D-20
        IRAT = 1
       ENDIF
       RATCOUP = CPT/CPC
      ENDIF
      XQCD1= QCDH(RMC**2/AMH**2)
      XQCD = (XQCD0-XQCD1)/2/XQCD1
      IF(AMH.LE.2*AMC) THEN
       HCC = 0
      ELSE
       HC2=3.D0*HFF(AMH,(RMC/AMH)**2)
     .    *QCDH(RMC**2/AMH**2)
c    .    *(1+ELW0(AMH,RMC,2.D0/3.D0,7.D0))
     .    *(1+ELW(AMH,AMC,AMS,2/3.D0,1/2.D0))
     .    *HFFSELF(AMH) * SM4FACF
     .    *CPC**2
     .   + DCC
      IF(ICOUPELW.EQ.0)THEN
       HC2=3.D0*HFF(AMH,(RMC/AMH)**2) * CPC
     .    *QCDH(RMC**2/AMH**2)
     .    * ((CPC-1)
c    .       +(1+ELW0(AMH,RMC,2.D0/3.D0,7.D0)*(1+XQCD))
     .       +(1+ELW(AMH,AMC,AMS,2/3.D0,1/2.D0)*(1+XQCD))
     .       *HFFSELF(AMH) * SM4FACF)
     .   + DCC
      ENDIF
c      IF(HC2.LT.0.D0) HC2 = 0
       HC1=3.D0*HFF(AMH,(AMC/AMH)**2)
     .    *TQCDH(AMC**2/AMH**2)
c    .    *(1+ELW0(AMH,AMC,2.D0/3.D0,7.D0))
     .    *(1+ELW(AMH,AMC,AMS,2/3.D0,1/2.D0))
     .    *HFFSELF(AMH) * SM4FACF
     .    *CPC**2
     .   + DCC
      IF(ICOUPELW.EQ.0)THEN
       HC1=3.D0*HFF(AMH,(AMC/AMH)**2) * CPC
     .    *TQCDH(AMC**2/AMH**2)
     .    * ((CPC-1)
c    .       +(1+ELW0(AMH,AMC,2.D0/3.D0,7.D0))
     .       +(1+ELW(AMH,AMC,AMS,2/3.D0,1/2.D0))
     .       *HFFSELF(AMH) * SM4FACF)
     .   + DCC
      ENDIF
       RAT = 2*AMC/AMH
       HCC = QQINT_HDEC(RAT,HC1,HC2)
c      write(6,*)'H -> cc: ',HCC,QCDH(RMC**2/AMH**2,5)
c      write(6,*)'H -> cc: ',
c    .  RMC**2/AMH**2*(-93.72459D0+12)*(ASH/PI)**2/QCDH(RMC**2/AMH**2,5)

c      eps00 = 1.d-15
c      xq1 = amb*(1-eps00)
c      xq2 = amb*(1+eps00)
c      xal1 = alphas_hdec(xq1,3)
c      xal2 = alphas_hdec(xq2,3)
c      xdel1= 7*alphas_hdec(xq1,3)**2/pi**2/24
c      xdel2= 7*alphas_hdec(xq2,3)**2/pi**2/24
c      write(6,*)xal1,xal2
c      write(6,*)(xal2-xal1)/xal1,(xal2-xal1)/xal2
c      write(6,*)xdel2,xdel1

c      ALS0 = ALPHAS_HDEC(AMZ,3)
c      WMC = RUNM_HDEC(AMC,4,0)
c      YMC = RUNM_HDEC(3.D0,4,0)
c      ZMC = 0.996D0-(ALS0-0.1189D0)/2.D0*9
c      do i =125,133
c       sc = i/100.D0
c       XMC = RUNM_HDEC(sc,4,0)
c       write(6,*)
c       write(6,*)sc,XMC,AMC,YMC,ZMC,WMC,ALS0
c      enddo
      ENDIF
c     HCC = HCC * SM4FACF
      IF(IRAT.EQ.1)CPC = 0
c     write(6,*)AMB,AMC
C  H --> BB :
      IRAT = 0
      RATCOUP = 1
      XQCD0 = QCDH(RMB**2/AMH**2)
      IF(CPT.EQ.0.D0)THEN
       RATCOUP = 0
      ELSE
       IF(CPB.EQ.0.D0)THEN
        CPB = 1.D-20
        IRAT = 1
       ENDIF
       RATCOUP = CPT/CPB
      ENDIF
      XQCD1= QCDH(RMB**2/AMH**2)
      XQCD = (XQCD0-XQCD1)/2/XQCD1
      IF(AMH.LE.2*AMB) THEN
       HBB = 0
      ELSE
       HB2=3.D0*HFF(AMH,(RMB/AMH)**2)
     .    *QCDH(RMB**2/AMH**2)
c    .    *(1+ELW0(AMH,RMB,-1.D0/3.D0,1.D0))
     .    *(1+ELW(AMH,AMB,AMT,-1/3.D0,-1/2.D0))
     .    *HFFSELF(AMH) * SM4FACF
     .    * CPB**2
     .   + DBB
       IF(ICOUPELW.EQ.0)THEN
        HB2=3.D0*HFF(AMH,(RMB/AMH)**2) * CPB
     .     *QCDH(RMB**2/AMH**2)
     .     * ((CPB-1)
c    .        +(1+ELW0(AMH,RMB,-1.D0/3.D0,1.D0)*(1+XQCD))
     .        +(1+ELW(AMH,AMB,AMT,-1/3.D0,-1/2.D0)*(1+XQCD))
     .        *HFFSELF(AMH) * SM4FACF)
     .    + DBB
       ENDIF
c      IF(HB2.LT.0.D0) HB2 = 0
       HB1=3.D0*HFF(AMH,(AMB/AMH)**2)
     .    *TQCDH(AMB**2/AMH**2)
c    .    *(1+ELW0(AMH,AMB,-1.D0/3.D0,1.D0))
     .    *(1+ELW(AMH,AMB,AMT,-1/3.D0,-1/2.D0))
     .    *HFFSELF(AMH) * SM4FACF
     .    * CPB**2
     .   + DBB
      IF(ICOUPELW.EQ.0)THEN
       HB1=3.D0*HFF(AMH,(AMB/AMH)**2) * CPB
     .    *TQCDH(AMB**2/AMH**2)
     .    * ((CPB-1)
c    .        +(1+ELW0(AMH,AMB,-1.D0/3.D0,1.D0))
     .        +(1+ELW(AMH,AMB,AMT,-1/3.D0,-1/2.D0))
     .       *HFFSELF(AMH) * SM4FACF)
     .   + DBB
      ENDIF
       RAT = 2*AMB/AMH
       HBB = QQINT_HDEC(RAT,HB1,HB2)

c      write(6,*)'h -> bb = ',AMH,HBB
c      write(6,*)'H -> bb: ',HBB,QCDH(RMB**2/AMH**2,5)
c      write(6,*)'H -> bb: ',QCDH(RMB**2/AMH**2,5)
c      write(6,*)'H -> bb: ',
c    .  RMB**2/AMH**2*(-93.72459D0+12)*(ASH/PI)**2/QCDH(RMB**2/AMH**2,5)

c     write(6,*)AMH,HBB,RMB,RUNM_HDEC(AMB,5,1),AMB,HB1,HB2
c     write(6,*)AMH,ELW0(AMH,AMB,-1.D0/3.D0,1.D0),
c    .              ELW(AMH,AMB,AMT,-1/3.D0,-1/2.D0)
c     write(6,*)(1.570D0-2*DLOG(HIGTOP)/3+ DLOG(RMB**2/AMH**2)**2/9)
c    .          *(ASH/PI)**2,
c    .          QCDH(RMB**2/AMH**2,5)-1
c     write(6,*)'mu,tau,b: ',ELW(AMH,AMMUON,0.D0,-1.D0,-1/2.D0)
c    .                      ,ELW(AMH,AMTAU,0.D0,-1.D0,-1/2.D0)
c    .                      ,ELW(AMH,AMB,AMT,-1/3.D0,-1/2.D0)

c      write(97,*)AMH,ELW0(AMH,AMB,-1/3.D0,1.D0)
c    .               -ALPH/PI*3.D0/2/9*(3.D0/2-DLOG(AMH**2/AMB**2))
c    .               ,ELW(AMH,AMB,AMT,-1/3.D0,-1/2.D0)
c    .               -ALPH/PI/9*HQCDM(BETA_HDEC(AMB**2/AMH**2))
c    .               +GF*AMH**2/16.D0/PI**2/DSQRT(2.D0)*2.117203D0

c      write(6,*)AMH,
c    .        ALPH/PI*AMH**2/32.D0/AMW**2/(1-AMW**2/AMZ**2)*2.117203D0
c      write(6,*)

c      write(6,*)RMC,RUNM_HDEC(AMC,4,0),RMC/RUNM_HDEC(AMC,4,0),
c    .           QCDH(RMC**2/AMH**2,5),
c    .           RMB,RUNM_HDEC(AMB,5,1),RMB/RUNM_HDEC(AMB,5,1),
c    .           QCDH(RMB**2/AMH**2,5)

c      write(6,*)'bb: ',AMH,RATCOUP*(1.570D0 - 2*DLOG(HIGTOP)/3
c    .    + DLOG(AMH**2/RMB**2)**2/9)*(ASH/PI)**2,QCDH(RMB**2/AMH**2,5)-1,
c    .        ELW0(AMH,AMB,-1.D0/3.D0,1.D0),HFFSELF(AMH)

c      write(6,*)AMH,HB1,HB2,HBB

c      write(61,*)AMH,1.D0+GF*AMH**2/16.D0/PI**2/DSQRT(2.D0)*2.117203D0,
c    .               HFFSELF(AMH)

c      ALS0 = ALPHAS_HDEC(AMZ,3)
c      WMB = RUNM_HDEC(AMB,5,1)
c      YMB = 4.163D0-(ALS0-0.1189D0)/2.D0*12
c      XMB = RUNM_HDEC(YMB,5,1)
c      write(6,*)
c      write(6,*)AMB,XMB,YMB,WMB,ALS0
c      do i =1,100
c       sc = i
c       XMS = RUNM_HDEC(sc,3,0)
c       XMC = RUNM_HDEC(sc,4,0)
c       XMB = RUNM_HDEC(sc,5,1)
c       write(66,*)sc,XMS,XMC,XMB,ALS0
c      enddo
      ENDIF
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c--check interpolation massive NLO with massles N4LO
c     IF(AMH.LE.2*AMB) THEN
c      HB1=0
c      HB2=3.D0*HFF(AMH,(RMB/AMH)**2)
c    .    *QCDH(RMB**2/AMH**2,5)
c     ELSE
c      HB1=3.D0*HFF(AMH,(AMB/AMH)**2)
c    .    *TQCDH(AMB**2/AMH**2)
c      HB2=3.D0*HFF(AMH,(RMB/AMH)**2)
c    .    *QCDH(RMB**2/AMH**2,5)
c     ENDIF
c      RAT = 2*AMB/AMH
c      HB3 = QQINT_HDEC(RAT,HB1,HB2)
c     write(96,"(g10.4,3(1x,g12.5))")amh,hb3,hb1,hb2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     HBB = HBB * SM4FACF
      IF(IRAT.EQ.1)CPB = 0

c     HB1X=3.D0*HFF(AMH,(AMB/AMH)**2)
c    .    *TQCDH(AMB**2/AMH**2)
c    .    /(BETA_HDEC(AMB**2/AMH**2))**3
c     HB2X=3.D0*HFF(AMH,(RMB/AMH)**2)
c    .    *QCDH(RMB**2/AMH**2,5)
c    .    /(BETA_HDEC(RMB**2/AMH**2))**3
c     RATCOUP = 0
c     deltaqcd = QCDH(RMB**2/AMH**2,5)
c     RATCOUP = 1
c     deltat = QCDH(RMB**2/AMH**2,5) - deltaqcd
c     write(6,*)'SM: MH     = ',AMH
c     write(6,*)'scale        = ',scalmq
c     write(6,*)'alphas(MZ)   = ',ALPHAS_HDEC(AMZ,3)
c     write(6,*)'alphas(MH/2) = ',ALPHAS_HDEC(AMH/2,3)
c     write(6,*)'alphas(MH)   = ',ALPHAS_HDEC(AMH,3)
c     write(6,*)'alphas(2*MH) = ',ALPHAS_HDEC(2*AMH,3)
c     write(6,*)'alphas(mb) = ',ALPHAS_HDEC(AMB,3)
c     write(6,*)'mb,mb(mb)  = ',AMB,RUNM_HDEC(AMB,5,1)
c     write(6,*)'mb(MH,100) = ',RMB,RUNM_HDEC(100.D0,5,1)
c     write(6,*)'deltaqcd,t = ',deltaqcd,deltat
c     write(6,*)'Gamma(0)   = ',HB2X,HB1X
c     write(6,*)'Gamma(mb)  = ',HB2,HB1
c     write(6,*)'mb         = ',AMB
c     write(6,*)'mb(MH/2)   = ',RUNM_HDEC(AMH/2,5,1)
c     write(6,*)'mc         = ',AMC
c     write(6,*)'mc(MH/2)   = ',RUNM_HDEC(AMH/2,4,0)
c     write(6,*)'mb(mb),Mb  = ',AMBB,AMBB*(1+4*ALPHAS_HDEC(AMBB,3)/3/PI)

c     ALS0 = ALPHAS_HDEC(AMZ,3)
c     WMT = RUNM_HDEC(AMT,6,1)
c     do i =165440,165450
c      sc = i/1000.D0
c      XMT = RUNM_HDEC(sc,6,1)
c      write(6,*)
c      write(6,*)sc,XMT,AMT,WMT,ALS0
c     enddo

C  H ---> TT
      IRAT = 0
      RATCOUP = 0
      IF(IONSH.EQ.0)THEN
       DLD=3D0
       DLU=5D0
       XM1 = 2D0*AMT-DLD
       XM2 = 2D0*AMT+DLU
       IF (AMH.LE.AMT+AMW+AMB) THEN
       HTT=0.D0
       ELSEIF (AMH.LE.XM1) THEN
        FACTT=6.D0*GF**2*AMH**3*AMT**2/2.D0/128.D0/PI**3
        CALL HTOTTS_HDEC(AMH,AMT,AMB,AMW,HTTS)
        HTT=FACTT*HTTS
       ELSEIF (AMH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        FACTT=6.D0*GF**2*XX(1)**3*AMT**2/2.D0/128.D0/PI**3
        CALL HTOTTS_HDEC(XX(1),AMT,AMB,AMW,HTTS)
        YY(1)=FACTT*HTTS
        FACTT=6.D0*GF**2*XX(2)**3*AMT**2/2.D0/128.D0/PI**3
        CALL HTOTTS_HDEC(XX(2),AMT,AMB,AMW,HTTS)
        YY(2)=FACTT*HTTS
        XMT = RUNM_HDEC(XX(3),6)
        XY2=3.D0*HFF(XX(3),(XMT/XX(3))**2)
     .    *QCDH(XMT**2/XX(3)**2)
     .    *(1+ELW(XX(3),AMT,AMB,2/3.D0,1/2.D0))
     .    *HFFSELF(XX(3)) * SM4FACF
     .    * CPT**2
        IF(ICOUPELW.EQ.0)THEN
         XY2=3.D0*HFF(XX(3),(XMT/XX(3))**2) * CPT
     .      *QCDH(XMT**2/XX(3)**2)
     .      * ((CPT-1)
     .        +(1+ELW(XX(3),AMT,AMB,2/3.D0,1/2.D0))
     .         *HFFSELF(XX(3)) * SM4FACF)
        ENDIF
        IF(XY2.LT.0.D0) XY2 = 0
        XY1=3.D0*HFF(XX(3),(AMT/XX(3))**2)
     .    *TQCDH(AMT**2/XX(3)**2)
     .    *(1+ELW(XX(3),AMT,AMB,2/3.D0,1/2.D0))
     .    *HFFSELF(XX(3)) * SM4FACF
     .    * CPT**2
        IF(ICOUPELW.EQ.0)THEN
         XY1=3.D0*HFF(XX(3),(AMT/XX(3))**2) * CPT
     .     *TQCDH(AMT**2/XX(3)**2)
     .      * ((CPT-1)
     .        +(1+ELW(XX(3),AMT,AMB,2/3.D0,1/2.D0))
     .    *HFFSELF(XX(3)) * SM4FACF)
        ENDIF
        RAT = 2*AMT/XX(3)
        YY(3) = QQINT_HDEC(RAT,XY1,XY2)
        XMT = RUNM_HDEC(XX(4),6)
        XY2=3.D0*HFF(XX(4),(XMT/XX(4))**2)
     .    *QCDH(XMT**2/XX(4)**2)
     .    *(1+ELW(XX(4),AMT,AMB,2/3.D0,1/2.D0))
     .    *HFFSELF(XX(4)) * SM4FACF
     .    * CPT**2
        IF(ICOUPELW.EQ.0)THEN
         XY2=3.D0*HFF(XX(4),(XMT/XX(4))**2) * CPT
     .      *QCDH(XMT**2/XX(4)**2)
     .      * ((CPT-1)
     .         +(1+ELW(XX(4),AMT,AMB,2/3.D0,1/2.D0))
     .         *HFFSELF(XX(4)) * SM4FACF)
        ENDIF
        IF(XY2.LT.0.D0) XY2 = 0
        XY1=3.D0*HFF(XX(4),(AMT/XX(4))**2)
     .    *TQCDH(AMT**2/XX(4)**2)
     .    *(1+ELW(XX(4),AMT,AMB,2/3.D0,1/2.D0))
     .    *HFFSELF(XX(4)) * SM4FACF
     .    * CPT**2
        IF(ICOUPELW.EQ.0)THEN
         XY1=3.D0*HFF(XX(4),(AMT/XX(4))**2) * CPT
     .      *TQCDH(AMT**2/XX(4)**2)
     .      * ((CPT-1)
     .         +(1+ELW(XX(4),AMT,AMB,2/3.D0,1/2.D0))
     .         *HFFSELF(XX(4)) * SM4FACF)
        ENDIF
        RAT = 2*AMT/XX(4)
        YY(4) = QQINT_HDEC(RAT,XY1,XY2)
        HTT = FINT_HDEC(AMH,XX,YY)
       ELSE
        HT2=3.D0*HFF(AMH,(RMT/AMH)**2)
     .    *QCDH(RMT**2/AMH**2)
     .    *(1+ELW(AMH,AMT,AMB,2/3.D0,1/2.D0))
     .    *HFFSELF(AMH) * SM4FACF
     .    * CPT**2
        IF(ICOUPELW.EQ.0)THEN
         HT2=3.D0*HFF(AMH,(RMT/AMH)**2) * CPT
     .      *QCDH(RMT**2/AMH**2)
     .      * ((CPT-1)
     .         +(1+ELW(AMH,AMT,AMB,2/3.D0,1/2.D0))
     .         *HFFSELF(AMH) * SM4FACF)
        ENDIF
        IF(HT2.LT.0.D0) HT2 = 0
        HT1=3.D0*HFF(AMH,(AMT/AMH)**2)
     .    *TQCDH(AMT**2/AMH**2)
     .    *(1+ELW(AMH,AMT,AMB,2/3.D0,1/2.D0))
     .    *HFFSELF(AMH) * SM4FACF
     .    * CPT**2
        IF(ICOUPELW.EQ.0)THEN
         HT1=3.D0*HFF(AMH,(AMT/AMH)**2) * CPT
     .      *TQCDH(AMT**2/AMH**2)
     .      * ((CPT-1)
     .         +(1+ELW(AMH,AMT,AMB,2/3.D0,1/2.D0))
     .      *HFFSELF(AMH) * SM4FACF)
        ENDIF
        RAT = 2*AMT/AMH
        HTT = QQINT_HDEC(RAT,HT1,HT2)
c      write(6,*)'H -> tt: ',
c    .  RMT**2/AMH**2*(-93.72459D0+12)*(ASH/PI)**2/QCDH(RMT**2/AMH**2,5)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       HT0=HTT
c       TOPFAC = 0.5D0
c       RMT = RUNM_HDEC(TOPFAC*AMH,6,1)
c       HT2=3.D0*HFF(AMH,(RMT/AMH)**2)
c    .    *QCDH(RMT**2/AMH**2,5)
c    .    *HFFSELF(AMH)
c       HTL = QQINT_HDEC(RAT,HT1,HT2)
c       TOPFAC = 2
c       RMT = RUNM_HDEC(TOPFAC*AMH,6,1)
c       HT2=3.D0*HFF(AMH,(RMT/AMH)**2)
c    .    *QCDH(RMT**2/AMH**2,5)
c    .    *HFFSELF(AMH)
c       HTU = QQINT_HDEC(RAT,HT1,HT2)
c       write(6,*)'H -> TT: ',AMH,HT0,HTL/HT0,HTU/HT0
c       TOPFAC = 1
c       RMT = RUNM_HDEC(AMH,6,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       ENDIF
      ELSE
       IF (AMH.LE.2.D0*AMT) THEN
        HTT=0.D0
       ELSE
        HT2=3.D0*HFF(AMH,(RMT/AMH)**2)
     .    *QCDH(RMT**2/AMH**2)
     .    *(1+ELW(AMH,AMT,AMB,2/3.D0,1/2.D0))
     .    *HFFSELF(AMH) * SM4FACF
     .    * CPT**2
        IF(ICOUPELW.EQ.0)THEN
         HT2=3.D0*HFF(AMH,(RMT/AMH)**2) * CPT
     .      *QCDH(RMT**2/AMH**2)
     .      * ((CPT-1)
     .         +(1+ELW(AMH,AMT,AMB,2/3.D0,1/2.D0))
     .         *HFFSELF(AMH) * SM4FACF)
        ENDIF
        IF(HT2.LT.0.D0) HT2 = 0
        HT1=3.D0*HFF(AMH,(AMT/AMH)**2)
     .    *TQCDH(AMT**2/AMH**2)
     .    *(1+ELW(AMH,AMT,AMB,2/3.D0,1/2.D0))
     .    *HFFSELF(AMH) * SM4FACF
     .    * CPT**2
        IF(ICOUPELW.EQ.0)THEN
         HT1=3.D0*HFF(AMH,(AMT/AMH)**2) * CPT
     .      *TQCDH(AMT**2/AMH**2)
     .      * ((CPT-1)
     .         +(1+ELW(AMH,AMT,AMB,2/3.D0,1/2.D0))
     .         *HFFSELF(AMH) * SM4FACF)
        ENDIF
        RAT = 2*AMT/AMH
        HTT = QQINT_HDEC(RAT,HT1,HT2)
       ENDIF
      ENDIF
      
c     HTT=HTT*SM4FACF
      IF(IFERMPHOB.NE.0)THEN
c      write(6,*)'Fermiophobic: H -> mu mu,tau tau,ss,cc,bb,tt ---> 0'
       HMM = 0
       HLL = 0
       HSS = 0
       HCC = 0
       HBB = 0
       HTT = 0
      ENDIF
      
C  H ---> GAMMA GAMMA
       EPS=1.D-8
       XRMC = RUNM_HDEC(AMH/2,4)*AMC/RUNM_HDEC(AMC,4)
       XRMB = RUNM_HDEC(AMH/2,5)*AMB/RUNM_HDEC(AMB,5)
       XRMT = RUNM_HDEC(AMH/2,6)*AMT/RUNM_HDEC(AMT,6)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      XRMC = AMC
c      XRMB = AMB
c      XRMT = AMT
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CTT = 4*XRMT**2/AMH**2*DCMPLX(1D0,-EPS)
       CTB = 4*XRMB**2/AMH**2*DCMPLX(1D0,-EPS)
       CTC = 4*XRMC**2/AMH**2*DCMPLX(1D0,-EPS)
       CTL = 4*AMTAU**2/AMH**2*DCMPLX(1D0,-EPS)
       CTW = 4*AMW**2/AMH**2*DCMPLX(1D0,-EPS)
       CAW = -(2+3*CTW+3*CTW*(2-CTW)*CF(CTW)) * CPW
       CAW00= -(2+3*CTW+3*CTW*(2-CTW)*CF(CTW))
       CAT00= 4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT))
       CAB00= 1/3D0 * 2*CTB*(1+(1-CTB)*CF(CTB))
       CAC00= 4/3D0 * 2*CTC*(1+(1-CTC)*CF(CTC))
       CAL00 =         2*CTL*(1+(1-CTL)*CF(CTL))
       CAT0= 4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT)) * CPT
       CAB0= 1/3D0 * 2*CTB*(1+(1-CTB)*CF(CTB)) * CPB
       CAC0= 4/3D0 * 2*CTC*(1+(1-CTC)*CF(CTC)) * CPC
       CAT = 4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT)) * CPT
     .     * CFACQ_HDEC(0,AMH,XRMT)
       CAB = 1/3D0 * 2*CTB*(1+(1-CTB)*CF(CTB)) * CPB
     .     * CFACQ_HDEC(0,AMH,XRMB)
       CAC = 4/3D0 * 2*CTC*(1+(1-CTC)*CF(CTC)) * CPC
     .     * CFACQ_HDEC(0,AMH,XRMC)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c--default
c     topscale0 = amh/2
c     xfac = cdabs(cat+cab+cac+cal+caw)**2
c     xxx0 = cdabs(cat00+cab00+cac00+cal+caw)**2
c--pole mass
c     dshift = dlog(amt**2/topscale0**2)
c     ctrun=-cdct(amh,amt)*dshift*alphas_hdec(amh,3)/pi
c     cctt0 = 4*amt**2/amh**2*dcmplx(1D0,-eps)
c     cat1 = 4/3d0 * 2*cctt0*(1+(1-cctt0)*cf(cctt0))
c    .     * (cfacq_hdec(0,amh,amt)+ctrun)
c     xfac0 = cdabs(cat0+cab+cac+cal+caw)**2
c--scale = M_H/4
c     topscale = amh/4
c     dshift = dlog(topscale**2/topscale0**2)
c     xrmt0 = runm_hdec(topscale,6,1)*amt/runm_hdec(amt,6,1)
c     ctrun=-cdct(amh,xrmt0)*dshift*alphas_hdec(amh,3)/pi
c     ctt1 = 4*xrmt0**2/amh**2*dcmplx(1D0,-eps)
c     cat1 = 4/3d0 * 2*ctt1*(1+(1-ctt1)*cf(ctt1))
c    .     * (cfacq_hdec(0,amh,xrmt0)+ctrun)
c     xfac1 = cdabs(cat1+cab+cac+cal+caw)**2
c     dshift = dlog(topscale**2/topscale0**2)+4/3.d0
c     xrmt0 = runm_hdec(topscale,6,1)
c     ctrun=-cdct(amh,xrmt0)*dshift*alphas_hdec(amh,3)/pi
c     ctt1 = 4*xrmt0**2/amh**2*dcmplx(1D0,-eps)
c     cat1 = 4/3d0 * 2*ctt1*(1+(1-ctt1)*cf(ctt1))
c    .     * (cfacq_hdec(0,amh,xrmt0)+ctrun)
c     xfac10= cdabs(cat1+cab+cac+cal+caw)**2
c--scale = M_H
c     topscale = amh
c     dshift = dlog(topscale0**2/topscale**2)
c     xrmt0 = runm_hdec(topscale,6,1)*amt/runm_hdec(amt,6,1)
c     ctrun=-cdct(amh,xrmt0)*dshift*alphas_hdec(amh,3)/pi
c     ctt2 = 4*xrmt0**2/amh**2*dcmplx(1D0,-eps)
c     cat2 = 4/3d0 * 2*ctt2*(1+(1-ctt2)*cf(ctt2))
c    .     * (cfacq_hdec(0,amh,xrmt0)+ctrun)
c     xfac2 = cdabs(cat2+cab+cac+cal+caw)**2
c     dshift = dlog(topscale**2/topscale0**2)+4/3.d0
c     xrmt0 = runm_hdec(topscale,6,1)
c     ctrun=-cdct(amh,xrmt0)*dshift*alphas_hdec(amh,3)/pi
c     ctt2 = 4*xrmt0**2/amh**2*dcmplx(1D0,-eps)
c     cat2 = 4/3d0 * 2*ctt2*(1+(1-ctt2)*cf(ctt2))
c    .     * (cfacq_hdec(0,amh,xrmt0)+ctrun)
c     xfac20= cdabs(cat2+cab+cac+cal+caw)**2
c     write(6,*)'H -> gamma gamma: ',xfac,cdabs(cat+caw)**2,
c    .          cdabs(caw)**2,cdabs(cat)**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)xfac0/xfac,xfac1/xfac,xfac2/xfac
c     write(6,*)xfac10/xfac,xfac20/xfac
c     write(6,*)xfac0/xxx0,xfac1/xxx0,xfac2/xxx0
c     write(6,*)xfac10/xxx0,xfac20/xxx0
c     xrho = 100.d0
c     amx = dsqrt(xrho)*amt
c     topscale0= amx/2
c     topscale = amt
c     dshift = dlog(topscale**2/topscale0**2)+0*4/3.d0
c     ctrun=-cdct(amx,amt)*dshift*alphas_hdec(amx,3)/pi
c     write(6,*)'Cfactor: ',xrho,
c    .     dreal(cfacq_hdec(0,amx,amt)+ctrun-1)/alphas_hdec(amx,3)*pi
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CAL =         2*CTL*(1+(1-CTL)*CF(CTL)) * CPTAU
       CATP = 0
       CABP = 0
       CAEP = 0
       CATP0 = 0
       CABP0 = 0
       CATP00 = 0
       CABP00 = 0
       CAEP00 = 0
       XFAC0= CDABS(CAT0+CAB0+CAC0+CAL+CAW+CPGAGA)**2
     .      * IOELW*GAGA_ELW(AMT,AMH)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CCTT = 4*AMT**2/AMH**2*DCMPLX(1D0,-EPS)
       CCAT = 4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT))
       CCT  = CFACQ_HDEC(0,AMH,AMT) - 1
c      write(6,*)'H -> gamma gamma: ',CAW,CCAT,CCT,ALPHAS_HDEC(AMH,3)/PI
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      CDUM0 = CAT0+CAW
c      CDUM  = CAT +CAW
c      CTOT0= CAW/CDUM0
c      CDT0 = CAT0/CAW
c      CDB0 = CAB0/CAW
c      CTOT = CAW/CDUM
c      CDT  = CAT/CAW
c      CDB  = CAB/CAW
c      write(6,*)'H -> gamma gamma',CTOT,CDT,CDB
c      write(6,*)'H -> gamma gamma',CTOT0,CDT0,CDB0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       DUM1 = CDABS(CAT+CAB+CAC+CAL+CAW)**2
c       CKOFQ = CKOFQ_HDEC(0,AMH**2/XRMT**2)
c       CFACQ = CKOFQ*ALPHAS_HDEC(AMH,3)/PI
c       CFACQ0 = -ALPHAS_HDEC(AMH,3)/PI
c       CATX = 4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT)) * CPT
c    .       * (1+CFACQ0)
c       DUM2 = CDABS(CATX+CAB+CAC+CAL+CAW)**2
c       write(6,*)'H -> gamma gamma: ',CKOFQ,CFACQ,DUM1
c       write(6,*)'H -> gamma gamma: ',-1.D0,CFACQ0,DUM2,DUM1/DUM2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       IF(ICOUPELW.EQ.0)THEN
c       XFAC0= CDABS(CAT00+CAB00+CAC00+CAL00+CAW00)**2*GAGA_ELW(AMT,AMH)
        XFAC0= DREAL(DCONJG(CAT0+CAB0+CAC0+CAL+CAW+CPGAGA)
     .       * (CAT00+CAB00+CAC00+CAL00+CAW00))
     .       * IOELW*GAGA_ELW(AMT,AMH)
       ENDIF
       IF(ISM4.NE.0)THEN
        XRMBP = RUNM_HDEC(AMH/2,7)*AMBP/RUNM_HDEC(AMBP,7)
        XRMTP = RUNM_HDEC(AMH/2,8)*AMTP/RUNM_HDEC(AMTP,8)
        CTTP = 4*XRMTP**2/AMH**2*DCMPLX(1D0,-EPS)
        CTBP = 4*XRMBP**2/AMH**2*DCMPLX(1D0,-EPS)
        CTEP = 4*AMEP**2/AMH**2*DCMPLX(1D0,-EPS)
        CATP00= 4/3D0 * 2*CTTP*(1+(1-CTTP)*CF(CTTP))
        CABP00= 1/3D0 * 2*CTBP*(1+(1-CTBP)*CF(CTBP))
        CAEP00=         2*CTEP*(1+(1-CTEP)*CF(CTEP))
        CATP0= 4/3D0 * 2*CTTP*(1+(1-CTTP)*CF(CTTP)) * CPTP
        CABP0= 1/3D0 * 2*CTBP*(1+(1-CTBP)*CF(CTBP)) * CPBP
        CATP = 4/3D0 * 2*CTTP*(1+(1-CTTP)*CF(CTTP)) * CPTP
     .       * CFACQ_HDEC(0,AMH,XRMTP)
        CABP = 1/3D0 * 2*CTBP*(1+(1-CTBP)*CF(CTBP)) * CPBP
     .       * CFACQ_HDEC(0,AMH,XRMBP)
        CAEP =         2*CTEP*(1+(1-CTEP)*CF(CTEP)) * CPEP
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       CATP = 0
c       CABP = 0
c       CAEP = 0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        IF(AMNUP.EQ.AMEP)THEN
         XXL = AMEP**2
        ELSE
         XXL =AMNUP**2*AMEP**2/(AMNUP**2-AMEP**2)*DLOG(AMNUP**2/AMEP**2)
        ENDIF
        IF(AMTP.EQ.AMBP)THEN
         XXQ = AMTP**2
        ELSE
         XXQ = AMTP**2*AMBP**2/(AMTP**2-AMBP**2)*DLOG(AMTP**2/AMBP**2)
        ENDIF
        FFT = -6
        FFT = 1
        CELW = GF/8/DSQRT(2.D0)/PI**2*(-49*AMT**2/2*FFT
     .       + 7*AMNUP**2/6 - 65*AMEP**2/6 - XXL
     .       - 237*AMTP**2/10 - 117*AMBP**2/10 - 3*XXQ)
        CELW = CELW*(1-0.614D0*AMH**2/4/AMW**2)
        XFAC0= CDABS(CAT0+CAB0+CAC0+CAL+CAW+CATP0+CABP0+CAEP+CPGAGA)**2
     .       * ((1+CELW)**2 - 1)
       IF(ICOUPELW.EQ.0)THEN
        XFAC0= DREAL(DCONJG(CAT0+CAB0+CAC0+CAL+CAW+CATP0+CABP0+CAEP
     .                     +CPGAGA)
     .       * (CAT00+CAB00+CAC00+CAL00+CAW00+CATP00+CABP00+CAEP00))
     .       * ((1+CELW)**2 - 1)
       ENDIF
c       write(6,*)'H -> ga ga: ',AMH,2*CELW*100,((1+CELW)**2-1)*100
c       write(6,*)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       XFAC0= 0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       ENDIF
       IF(IFERMPHOB.NE.0)THEN
c       write(6,*)'Fermiophobic: H -> gamma gamma ---> W loop'
        CAL = 0
        CAC = 0
        CAB = 0
        CAT = 0
        XFAC0= 0
c       write(6,*)CDABS(CAW)**2,CDABS(CAT+CAB+CAC+CAL+CAW)**2
       ENDIF
       XFAC = CDABS(CAT+CAB+CAC+CAL+CAW+CATP+CABP+CAEP+CPGAGA)**2
       HGA=HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*(XFAC+XFAC0)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      XXX0 = CDABS(CAT00+CAB00+CAC00+CAL+CAW)**2
c      XXX1 = CDABS(CAT+CAB+CAC+CAL+CAW)**2
c      write(6,*)'H->gaga: ',AMH,HGA/(XFAC+XFAC0)*XXX0*1.d6,
c    .                           HGA/(XFAC+XFAC0)*XXX1*1.d6
c      write(9,*)AMH,HGA/(XFAC+XFAC0)*XXX0*1.d6,
c    .               HGA/(XFAC+XFAC0)*XXX1*1.d6
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)'H -> gamma gamma: NLO QCD, approx ',
c    .  CDABS(CAT+CAB+CAC+CAL+CAW)**2/CDABS(CAT0+CAB0+CAC0+CAL+CAW)**2,
c    .  CDABS(CAT0*(1-ALPHAS_HDEC(AMH/2)/PI)+CAB0+CAC0+CAL+CAW)**2
c    . /CDABS(CAT0+CAB0+CAC0+CAL+CAW)**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)'H -> gamma gamma: ',XFAC,XFAC0,HGA,IOELW
c      write(6,*)'H -> gamma gamma: ',CAT0,CAB0,CAC0,CAL,CAW
c    .                               ,CATP0,CABP0,CAEP,CGAGA
c      write(6,*)'H -> gamma gamma: ',CAT00,CAB00,CAC00,CAL00,CAW00
c    .                               ,CATP00,CABP00,CAEP00
c      write(6,*)'H -> gamma gamma: ',AMH,XRMT,XRMB,XRMC
c      write(6,*)'H -> gamma gamma: ',
c    .            CDABS(CAT+CAB+CAC+CAL+CAW)**2/CDABS(CAT+CAW)**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)'H -> gamma gamma: ',
c    .           dsqrt(xfac)/8
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       write(6,*)'H gamma gamma: ',(16/9.d0-7)/8,(CAT0+CAW)/8,
c    .                              (CAT0+CAB0+CAC0+CAL+CAW)/8
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      HGA=HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*XFAC
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)'H -> gamma gamma: ',HGA,XFAC0/XFAC
c      write(6,*)'gaga: ',AMH,CAT,CAW,CAT+CAW
c      write(6,*)'gaga: ',AMH,CAT0,CAW,CAT0+CAW
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      YY0 = CDABS(CAT+CAC)**2 + CDABS(CAT0+CAC0)**2*GAGA_ELW(AMT,AMH)
c      YTT = HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*YY0 / HGA
c      YY0 = CDABS(CAB)**2 + CDABS(CAB0)**2*GAGA_ELW(AMT,AMH)
c      YBB = HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*YY0 / HGA
c      YY0 = CDABS(CAW)**2 + CDABS(CAW)**2*GAGA_ELW(AMT,AMH)
c      YWW = HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*YY0 / HGA
c      YY0 = CDABS(CAL)**2 + CDABS(CAL)**2*GAGA_ELW(AMT,AMH)
c      YLL = HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*YY0 / HGA
c      YY0 = 2*DREAL(DCONJG(CAT+CAC)*CAB)
c    .     + 2*DREAL(DCONJG(CAT0+CAC0)*CAB0)*GAGA_ELW(AMT,AMH)
c      YTB = HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*YY0 / HGA
c      YY0 = 2*DREAL(DCONJG(CAT+CAC)*CAW)
c    .     + 2*DREAL(DCONJG(CAT0+CAC0)*CAW)*GAGA_ELW(AMT,AMH)
c      YTW = HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*YY0 / HGA
c      YY0 = 2*DREAL(DCONJG(CAB)*CAW)
c    .     + 2*DREAL(DCONJG(CAB0)*CAW)*GAGA_ELW(AMT,AMH)
c      YBW = HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*YY0 / HGA
c      YY0 = 2*DREAL(DCONJG(CAT+CAC)*CAL)
c    .     + 2*DREAL(DCONJG(CAT0+CAC0)*CAL)*GAGA_ELW(AMT,AMH)
c      YTL = HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*YY0 / HGA
c      YY0 = 2*DREAL(DCONJG(CAB)*CAL)
c    .     + 2*DREAL(DCONJG(CAB0)*CAL)*GAGA_ELW(AMT,AMH)
c      YBL = HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*YY0 / HGA
c      YY0 = 2*DREAL(DCONJG(CAL)*CAW)
c    .     + 2*DREAL(DCONJG(CAL)*CAW)*GAGA_ELW(AMT,AMH)
c      YLW = HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*YY0 / HGA
c      write(52,('f4.0,7(g13.5)'))AMH,HGA,YTT,YBB,YWW,YTB,YTW,YBW
c    .                            ,YLL,YTL,YBL,YLW
c      write(6,*)'gaga: ',AMH,YTT+YBB+YWW+YTB+YTW+YBW+YLL+YTL+YBL+YLW
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)AMH,(XFAC/XFAC0*GAGA_ELW(AMT,AMH)-1)*100,
c    .               GAGA_ELW(AMT,AMH)*100
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      CAT0 = 4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT))
c      CAB0 = 1/3D0 * 2*CTB*(1+(1-CTB)*CF(CTB))
c      CAC0 = 4/3D0 * 2*CTC*(1+(1-CTC)*CF(CTC))
c      XFACLO = CDABS(CAT0+CAB0+CAC0+CAL+CAW)**2
c      write(54,('4(1X,E12.6)'))AMH,HGA,HGA*XFACLO/XFAC,XFAC/XFACLO-1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)CDABS(CAT0+CAW)**2,(16/9.D0-7)**2,
c    .           (16/9.D0-7)**2/CDABS(CAT0+CAW)**2,16/9.D0,CAT0,-7,CAW
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
C  H ---> Z GAMMA
      XRMC = RUNM_HDEC(AMH/2,4)*AMC/RUNM_HDEC(AMC,4)
      XRMB = RUNM_HDEC(AMH/2,5)*AMB/RUNM_HDEC(AMB,5)
      XRMT = RUNM_HDEC(AMH/2,6)*AMT/RUNM_HDEC(AMT,6)
      IF(AMH.LE.AMZ)THEN
       HZGA=0
      ELSE
       EPS=1.D-8
       TS = SS/CS
       FT = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS) * CPT
       FB = 3*1D0/3*(-1+4*1D0/3*SS)/DSQRT(SS*CS) * CPB
       FC = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS) * CPC
       FL = (-1+4*SS)/DSQRT(SS*CS) * CPTAU
c      CTT = 4*XRMT**2/AMH**2*DCMPLX(1D0,-EPS)
c      CTB = 4*XRMB**2/AMH**2*DCMPLX(1D0,-EPS)
c      CTC = 4*XRMC**2/AMH**2*DCMPLX(1D0,-EPS)
       CTT = 4*AMT**2/AMH**2*DCMPLX(1D0,-EPS)
       CTB = 4*AMB**2/AMH**2*DCMPLX(1D0,-EPS)
       CTC = 4*AMC**2/AMH**2*DCMPLX(1D0,-EPS)
       CTL = 4*AMTAU**2/AMH**2*DCMPLX(1D0,-EPS)
       CTW = 4*AMW**2/AMH**2*DCMPLX(1D0,-EPS)
c      CLT = 4*XRMT**2/AMZ**2*DCMPLX(1D0,-EPS)
c      CLB = 4*XRMB**2/AMZ**2*DCMPLX(1D0,-EPS)
c      CLC = 4*XRMC**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLT = 4*AMT**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLB = 4*AMB**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLC = 4*AMC**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLE = 4*AMTAU**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLW = 4*AMW**2/AMZ**2*DCMPLX(1D0,-EPS)
       CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
       CAB = FB*(CI1(CTB,CLB) - CI2(CTB,CLB))
       CAC = FC*(CI1(CTC,CLC) - CI2(CTC,CLC))
       CAL = FL*(CI1(CTL,CLE) - CI2(CTL,CLE))
       CAW = -1/DSQRT(TS)*(4*(3-TS)*CI2(CTW,CLW)
     .     + ((1+2/CTW)*TS - (5+2/CTW))*CI1(CTW,CLW)) * CPW
       CATP = 0
       CABP = 0
       CAEP = 0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)6*CI1(CTT,CLT),2*CI2(CTT,CLT)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      EPS = 1.d-30
c      QQ = 2*AMT - 1
c      CLT = 4*AMT**2/QQ**2*DCMPLX(1D0,-EPS)
c      CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
c      write(6,*)QQ,CAT
c      QQ = 2*AMT - 0.1d0
c      CLT = 4*AMT**2/QQ**2*DCMPLX(1D0,-EPS)
c      CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
c      write(6,*)QQ,CAT
c      QQ = 2*AMT - 0.01d0
c      CLT = 4*AMT**2/QQ**2*DCMPLX(1D0,-EPS)
c      CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
c      write(6,*)QQ,CAT
c      QQ = 2*AMT - 1.d-8
c      CLT = 4*AMT**2/QQ**2*DCMPLX(1D0,-EPS)
c      CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
c      write(6,*)QQ,CAT
c      QQ = 2*AMT
c      CLT = 4*AMT**2/QQ**2*DCMPLX(1D0,-EPS)
c      CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
c      write(6,*)QQ,CAT
c      QQ = 2*AMT + 1.d-8
c      CLT = 4*AMT**2/QQ**2*DCMPLX(1D0,-EPS)
c      CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
c      write(6,*)QQ,CAT
c      QQ = 2*AMT + 0.01d0
c      CLT = 4*AMT**2/QQ**2*DCMPLX(1D0,-EPS)
c      CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
c      write(6,*)QQ,CAT
c      QQ = 2*AMT + 0.1d0
c      CLT = 4*AMT**2/QQ**2*DCMPLX(1D0,-EPS)
c      CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
c      write(6,*)QQ,CAT
c      QQ = 2*AMT+1
c      CLT = 4*AMT**2/QQ**2*DCMPLX(1D0,-EPS)
c      CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
c      write(6,*)QQ,CAT
c      CLT = 4*AMT**2/AMZ**2*DCMPLX(1D0,-EPS)
c      CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       IF(ISM4.NE.0)THEN
        FTP = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS) * CPTP
        FBP = 3*1D0/3*(-1+4*1D0/3*SS)/DSQRT(SS*CS) * CPBP
        FEP = (-1+4*SS)/DSQRT(SS*CS) * CPEP
        CTTP = 4*AMTP**2/AMH**2*DCMPLX(1D0,-EPS)
        CTBP = 4*AMBP**2/AMH**2*DCMPLX(1D0,-EPS)
        CTEP = 4*AMEP**2/AMH**2*DCMPLX(1D0,-EPS)
        CLTP = 4*AMTP**2/AMZ**2*DCMPLX(1D0,-EPS)
        CLBP = 4*AMBP**2/AMZ**2*DCMPLX(1D0,-EPS)
        CLEP = 4*AMEP**2/AMZ**2*DCMPLX(1D0,-EPS)
        CATP = FTP*(CI1(CTTP,CLTP) - CI2(CTTP,CLTP))
        CABP = FBP*(CI1(CTBP,CLBP) - CI2(CTBP,CLBP))
        CAEP = FEP*(CI1(CTEP,CLEP) - CI2(CTEP,CLEP))
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c       CATP = 0
c       CABP = 0
c       CAEP = 0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       ENDIF
       IF(IFERMPHOB.NE.0)THEN
c       write(6,*)'Fermiophobic: H -> Z gamma ---> W loop'
        CAB = 0
        CAT = 0
        CAL = 0
c       write(6,*)CDABS(CAW)**2,CDABS(CAT+CAB+CAW)**2
       ENDIF
       FPTLIKE = CPZGA
       XFAC = CDABS(CAT+CAB+CAC+CAL+CAW+CATP+CABP+CAEP+FPTLIKE)**2
       ACOUP = DSQRT(2D0)*GF*AMZ**2*SS*CS/PI**2
       HZGA = GF/(4.D0*PI*DSQRT(2.D0))*AMH**3*(ALPH/PI)*ACOUP/16.D0
     .        *XFAC*(1-AMZ**2/AMH**2)**3
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)'H -> Z gamma: ',
c    .           1-CDABS(CAB+CAC+CAL+CAW+CATP+CABP+CAEP)**2/XFAC
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      YY0 = CDABS(CAT+CAC)**2
c      YTT = GF/(4.D0*PI*DSQRT(2.D0))*AMH**3*(ALPH/PI)*ACOUP/16.D0
c    .     * YY0*(1-AMZ**2/AMH**2)**3 / HZGA
c      YY0 = CDABS(CAB)**2
c      YBB = GF/(4.D0*PI*DSQRT(2.D0))*AMH**3*(ALPH/PI)*ACOUP/16.D0
c    .     * YY0*(1-AMZ**2/AMH**2)**3 / HZGA
c      YY0 = CDABS(CAW)**2
c      YWW = GF/(4.D0*PI*DSQRT(2.D0))*AMH**3*(ALPH/PI)*ACOUP/16.D0
c    .     * YY0*(1-AMZ**2/AMH**2)**3 / HZGA
c      YY0 = CDABS(CAL)**2
c      YLL = GF/(4.D0*PI*DSQRT(2.D0))*AMH**3*(ALPH/PI)*ACOUP/16.D0
c    .     * YY0*(1-AMZ**2/AMH**2)**3 / HZGA
c      YY0 = 2*DREAL(DCONJG(CAT+CAC)*CAB)
c      YTB = GF/(4.D0*PI*DSQRT(2.D0))*AMH**3*(ALPH/PI)*ACOUP/16.D0
c    .     * YY0*(1-AMZ**2/AMH**2)**3 / HZGA
c      YY0 = 2*DREAL(DCONJG(CAT+CAC)*CAW)
c      YTW = GF/(4.D0*PI*DSQRT(2.D0))*AMH**3*(ALPH/PI)*ACOUP/16.D0
c    .     * YY0*(1-AMZ**2/AMH**2)**3 / HZGA
c      YY0 = 2*DREAL(DCONJG(CAB)*CAW)
c      YBW = GF/(4.D0*PI*DSQRT(2.D0))*AMH**3*(ALPH/PI)*ACOUP/16.D0
c    .     * YY0*(1-AMZ**2/AMH**2)**3 / HZGA
c      YY0 = 2*DREAL(DCONJG(CAT+CAC)*CAL)
c      YTL = GF/(4.D0*PI*DSQRT(2.D0))*AMH**3*(ALPH/PI)*ACOUP/16.D0
c    .     * YY0*(1-AMZ**2/AMH**2)**3 / HZGA
c      YY0 = 2*DREAL(DCONJG(CAB)*CAL)
c      YBL = GF/(4.D0*PI*DSQRT(2.D0))*AMH**3*(ALPH/PI)*ACOUP/16.D0
c    .     * YY0*(1-AMZ**2/AMH**2)**3 / HZGA
c      YY0 = 2*DREAL(DCONJG(CAL)*CAW)
c      YLW = GF/(4.D0*PI*DSQRT(2.D0))*AMH**3*(ALPH/PI)*ACOUP/16.D0
c    .     * YY0*(1-AMZ**2/AMH**2)**3 / HZGA
c      write(53,('f4.0,11(g13.5)'))AMH,HZGA,YTT,YBB,YWW,YTB,YTW,YBW
c    .                            ,YLL,YTL,YBL,YLW
c      write(6,*)'Zga: ',AMH,YTT+YBB+YWW+YTB+YTW+YBW+YLL+YTL+YBL+YLW
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      XFAC0 = CDABS(CAT+CAB+CAL+CAW+CATP+CABP+CAEP)**2
c      write(6,*)'H -> Z gamma: ',XFAC/XFAC0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      ENDIF
C  H ---> W W
      IF(IONWZ.EQ.0)THEN
       IF(IFERMPHOB.NE.0)THEN
        CALL HTOVV_HDEC(0,AMH,AMW,GAMW,HTWW)
        HTWW = HTWW*HVVSELF(AMH)
       ELSE
        CALL HTOVV_HDEC(1,AMH,AMW,GAMW,HTWW)
        CALL HTOVV_HDEC(0,AMH,AMW,GAMW,HTWW0)
        IF(IOELW.EQ.0) HTWW = HTWW0
        HTWW = HTWW * SM4FACW
       ENDIF
       HWW = 3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/AMH**3*HTWW * CPW**2
       IF(ICOUPELW.EQ.0)THEN
        HWW = 3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/AMH**3
     .      * CPW * (HTWW0*(CPW-1)+HTWW)
       ENDIF
      ELSEIF(IONWZ.EQ.-1)THEN
       DLD=2D0
       DLU=2D0
       XM1 = 2D0*AMW-DLD
       XM2 = 2D0*AMW+DLU
      IF (AMH.LE.AMW) THEN
       HWW=0
      ELSE IF (AMH.LE.XM1) THEN
       CWW=3.D0*GF**2*AMW**4/16.D0/PI**3
       HWW=HV(AMW**2/AMH**2)*CWW*AMH * CPW**2
     .      * SM4FACW
       IF(ICOUPELW.EQ.0)THEN
        HWW=HV(AMW**2/AMH**2)*CWW*AMH
     .      * CPW * ((CPW-1)+SM4FACW)
       ENDIF
      ELSE IF (AMH.LT.XM2) THEN
       CWW=3.D0*GF**2*AMW**4/16.D0/PI**3
       XX(1) = XM1-1D0
       XX(2) = XM1
       XX(3) = XM2
       XX(4) = XM2+1D0
       YY(1)=HV(AMW**2/XX(1)**2)*CWW*XX(1) * SM4FACW
       YY(2)=HV(AMW**2/XX(2)**2)*CWW*XX(2) * SM4FACW
       YY(3)=HVV(XX(3),AMW**2/XX(3)**2)
     .      *HVVSELF(XX(3)) * SM4FACW
       YY(4)=HVV(XX(4),AMW**2/XX(4)**2)
     .      *HVVSELF(XX(4)) * SM4FACW
       IF(ICOUPELW.EQ.0)THEN
        YY(1)=HV(AMW**2/XX(1)**2)*CWW*XX(1)
     .       * CPW * ((CPW-1)+SM4FACW)
        YY(2)=HV(AMW**2/XX(2)**2)*CWW*XX(2)
     .       * CPW * ((CPW-1)+SM4FACW)
        YY(3)=HVV(XX(3),AMW**2/XX(3)**2)
     .       * CPW * ((CPW-1)+HVVSELF(XX(3)) * SM4FACW)
        YY(4)=HVV(XX(4),AMW**2/XX(4)**2)
     .       * CPW * ((CPW-1)+HVVSELF(XX(4)) * SM4FACW)
        ENDIF
       HWW = FINT_HDEC(AMH,XX,YY)
      ELSE
       HWW=HVV(AMH,AMW**2/AMH**2)
     .     *HVVSELF(AMH) * SM4FACW * CPW**2
       IF(ICOUPELW.EQ.0)THEN
        HWW=HVV(AMH,AMW**2/AMH**2)
     .      * CPW * ((CPW-1)+HVVSELF(AMH) * SM4FACW)
       ENDIF
      ENDIF
      ELSE
       DLD=2D0
       DLU=2D0
       XM1 = 2D0*AMW-DLD
       XM2 = 2D0*AMW+DLU
       IF (AMH.LE.XM1) THEN
        CALL HTOVV_HDEC(0,AMH,AMW,GAMW,HTWW)
        HWW = 3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/AMH**3*HTWW * CPW**2
     .      * SM4FACW
        IF(ICOUPELW.EQ.0)THEN
         HWW = 3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/AMH**3*HTWW
     .       * CPW * ((CPW-1)+SM4FACW)
        ENDIF
       ELSEIF (AMH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        CALL HTOVV_HDEC(0,XX(1),AMW,GAMW,HTWW)
        YY(1)=3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/XX(1)**3*HTWW * CPW**2
     .      * SM4FACW
        IF(ICOUPELW.EQ.0)THEN
         YY(1)=3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/XX(1)**3*HTWW
     .        * CPW * ((CPW-1) + SM4FACW)
        ENDIF
        CALL HTOVV_HDEC(0,XX(2),AMW,GAMW,HTWW)
        YY(2)=3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/XX(2)**3*HTWW * CPW**2
     .      * SM4FACW
        IF(ICOUPELW.EQ.0)THEN
         YY(2)=3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/XX(2)**3*HTWW * CPW**2
     .        * CPW * ((CPW-1) + SM4FACW)
        ENDIF
        YY(3)=HVV(XX(3),AMW**2/XX(3)**2)
     .       *HVVSELF(XX(3)) * SM4FACW * CPW**2
        YY(4)=HVV(XX(4),AMW**2/XX(4)**2)
     .       *HVVSELF(XX(4)) * SM4FACW * CPW**2
       IF(ICOUPELW.EQ.0)THEN
        YY(3)=HVV(XX(3),AMW**2/XX(3)**2)
     .      * CPW * ((CPW-1) + HVVSELF(XX(3)) * SM4FACW)
        YY(4)=HVV(XX(4),AMW**2/XX(4)**2)
     .      * CPW * ((CPW-1) + HVVSELF(XX(4)) * SM4FACW)
       ENDIF
        HWW = FINT_HDEC(AMH,XX,YY)
       ELSE
        HWW=HVV(AMH,AMW**2/AMH**2)
     .     *HVVSELF(AMH) * SM4FACW * CPW**2
        IF(ICOUPELW.EQ.0)THEN
         HWW=HVV(AMH,AMW**2/AMH**2)
     .      * CPW * ((CPW-1)+HVVSELF(AMH) * SM4FACW)
        ENDIF
       ENDIF
      ENDIF
c     HWW = HWW * SM4FACW
C  H ---> Z Z
      IF(IONWZ.EQ.0)THEN
       IF(IFERMPHOB.NE.0)THEN
        CALL HTOVV_HDEC(0,AMH,AMZ,GAMZ,HTZZ)
        HTZZ = HTZZ*HVVSELF(AMH)
       ELSE
        CALL HTOVV_HDEC(2,AMH,AMZ,GAMZ,HTZZ)
        CALL HTOVV_HDEC(0,AMH,AMZ,GAMZ,HTZZ0)
        IF(IOELW.EQ.0) HTZZ = HTZZ0
        HTZZ = HTZZ * SM4FACZ
       ENDIF
       HZZ = 3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/AMH**3*HTZZ * CPZ**2
       IF(ICOUPELW.EQ.0)THEN
        HZZ = 3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/AMH**3
     .      * CPZ * (HTZZ0*(CPZ-1)+HTZZ)
       ENDIF
      ELSEIF(IONWZ.EQ.-1)THEN
       DLD=2D0
       DLU=2D0
       XM1 = 2D0*AMZ-DLD
       XM2 = 2D0*AMZ+DLU
       IF (AMH.LE.AMZ) THEN
        HZZ=0
       ELSEIF (AMH.LE.XM1) THEN
        CZZ=3.D0*GF**2*AMZ**4/192.D0/PI**3*(7-40/3.D0*SS+160/9.D0*SS**2)
        HZZ=HV(AMZ**2/AMH**2)*CZZ*AMH * CPZ**2
     .      * SM4FACZ
        IF(ICOUPELW.EQ.0)THEN
         HZZ=HV(AMZ**2/AMH**2)*CZZ*AMH
     .      * CPZ * ((CPZ-1)+SM4FACZ)
        ENDIF
       ELSEIF (AMH.LT.XM2) THEN
        CZZ=3.D0*GF**2*AMZ**4/192.D0/PI**3*(7-40/3.D0*SS+160/9.D0*SS**2)
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        YY(1)=HV(AMZ**2/XX(1)**2)*CZZ*XX(1) * CPZ**2
     .      * SM4FACZ
        YY(2)=HV(AMZ**2/XX(2)**2)*CZZ*XX(2) * CPZ**2
     .      * SM4FACZ
        YY(3)=HVV(XX(3),AMZ**2/XX(3)**2)/2
     .       *HVVSELF(XX(3)) * SM4FACZ * CPZ**2
        YY(4)=HVV(XX(4),AMZ**2/XX(4)**2)/2
     .       *HVVSELF(XX(4)) * SM4FACZ * CPZ**2
        IF(ICOUPELW.EQ.0)THEN
         YY(1)=HV(AMZ**2/XX(1)**2)*CZZ*XX(1)
     .        * CPZ * ((CPZ-1)+SM4FACZ)
         YY(2)=HV(AMZ**2/XX(2)**2)*CZZ*XX(2)
     .        * CPZ * ((CPZ-1)+SM4FACZ)
         YY(3)=HVV(XX(3),AMZ**2/XX(3)**2)/2
     .        * CPZ * ((CPZ-1)+HVVSELF(XX(3)) * SM4FACZ)
         YY(4)=HVV(XX(4),AMZ**2/XX(4)**2)/2
     .        * CPZ * ((CPZ-1)+HVVSELF(XX(4)) * SM4FACZ)
        ENDIF
        HZZ = FINT_HDEC(AMH,XX,YY)
       ELSE
        HZZ=HVV(AMH,AMZ**2/AMH**2)/2.D0
     .     *HVVSELF(AMH) * SM4FACZ * CPZ**2
        IF(ICOUPELW.EQ.0)THEN
         HZZ=HVV(AMH,AMZ**2/AMH**2)/2.D0
     .      * CPZ * ((CPZ-1)+HVVSELF(AMH) * SM4FACZ)
        ENDIF
       ENDIF
      ELSE
       DLD=2D0
       DLU=2D0
       XM1 = 2D0*AMZ-DLD
       XM2 = 2D0*AMZ+DLU
       IF (AMH.LE.XM1) THEN
        CALL HTOVV_HDEC(0,AMH,AMZ,GAMZ,HTZZ)
        HZZ = 3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/AMH**3*HTZZ
     .      * SM4FACZ
        IF(ICOUPELW.EQ.0)THEN
         HZZ = 3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/AMH**3*HTZZ
     .       * CPZ * ((CPZ-1)+SM4FACZ)
        ENDIF
       ELSEIF (AMH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        CALL HTOVV_HDEC(0,XX(1),AMZ,GAMZ,HTZZ)
        YY(1)=3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/XX(1)**3*HTZZ * CPZ**2
     .       * SM4FACZ
        IF(ICOUPELW.EQ.0)THEN
         YY(1)=3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/XX(1)**3*HTZZ
     .        * CPZ * ((CPZ-1) + SM4FACZ)
        ENDIF
        CALL HTOVV_HDEC(0,XX(2),AMZ,GAMZ,HTZZ)
        YY(2)=3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/XX(2)**3*HTZZ * CPZ**2
     .       * SM4FACZ
        IF(ICOUPELW.EQ.0)THEN
         YY(2)=3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/XX(2)**3*HTZZ
     .        * CPZ * ((CPZ-1) + SM4FACZ)
        ENDIF
        YY(3)=HVV(XX(3),AMZ**2/XX(3)**2)/2
     .       *HVVSELF(XX(3)) * SM4FACZ * CPZ**2
        YY(4)=HVV(XX(4),AMZ**2/XX(4)**2)/2
     .       *HVVSELF(XX(4)) * SM4FACZ * CPZ**2
        IF(ICOUPELW.EQ.0)THEN
         YY(3)=HVV(XX(3),AMZ**2/XX(3)**2)/2
     .        * CPZ * ((CPZ-1)+HVVSELF(XX(3)))
         YY(4)=HVV(XX(4),AMZ**2/XX(4)**2)/2
     .        * CPZ * ((CPZ-1)+HVVSELF(XX(4)))
        ENDIF
        HZZ = FINT_HDEC(AMH,XX,YY)
       ELSE
        HZZ=HVV(AMH,AMZ**2/AMH**2)/2.D0
     .      *HVVSELF(AMH) * SM4FACZ * CPZ**2
       IF(ICOUPELW.EQ.0)THEN
        HZZ=HVV(AMH,AMZ**2/AMH**2)/2.D0
     .     * CPZ * ((CPZ-1)+HVVSELF(AMH) * SM4FACZ)
       ENDIF
       ENDIF
      ENDIF
c     HZZ = HZZ * SM4FACZ
c     write(62,*)AMH,1.D0+GF*AMH**2/16.D0/PI**2/DSQRT(2.D0)*2.800952D0,
c    .               HVVSELF(AMH)
c     write(91,*)AMH,HWW,HZZ
      
c     CALL HTOVV_HDEC(0,AMH,AMW,GAMW,HTWW0)
c     CALL HTOVV_HDEC(1,AMH,AMW,GAMW,HTWW1)
c     CALL HTOVV_HDEC(0,AMH,AMZ,GAMZ,HTZZ0)
c     CALL HTOVV_HDEC(2,AMH,AMZ,GAMZ,HTZZ1)
c     write(6,*)AMH,HTWW1/HTWW0,HTZZ1/HTZZ0
C
      HNUPNUP = 0
      HEPEP = 0
      HBPBP = 0
      HTPTP = 0
      IF(ISM4.NE.0)THEN
C  H ---> NUP NUP
       IF(AMH.LE.2*AMNUP) THEN
        HNUPNUP = 0
       ELSE
        HNUPNUP=HFF(AMH,(AMNUP/AMH)**2) * CPNUP**2
     .         *HFFSELF(AMH)
       ENDIF
C  H ---> EP EP
       IF(AMH.LE.2*AMEP) THEN
        HEPEP = 0
       ELSE
        HEPEP=HFF(AMH,(AMEP/AMH)**2) * CPEP**2
     .       *HFFSELF(AMH)
       ENDIF
C  H --> BP BP :
       IF(AMH.LE.2*AMBP) THEN
        HBPBP = 0
       ELSE
        HBP2=3.D0*HFF(AMH,(RMBP/AMH)**2) * CPBP**2
     .     *QCDH(RMBP**2/AMH**2)
     .     *HFFSELF(AMH)
        IF(HBP2.LT.0.D0) HBP2 = 0
        HBP1=3.D0*HFF(AMH,(AMBP/AMH)**2) * CPBP**2
     .     *TQCDH(AMBP**2/AMH**2)
     .     *HFFSELF(AMH)
        RAT = 2*AMBP/AMH
        HBPBP = QQINT_HDEC(RAT,HBP1,HBP2)
       ENDIF
C  H --> TP TP :
       IF(AMH.LE.2*AMTP) THEN
        HTPTP = 0
       ELSE
        HTP2=3.D0*HFF(AMH,(RMTP/AMH)**2) * CPTP**2
     .     *QCDH(RMTP**2/AMH**2)
     .     *HFFSELF(AMH)
        IF(HTP2.LT.0.D0) HTP2 = 0
        HTP1=3.D0*HFF(AMH,(AMTP/AMH)**2) * CPTP**2
     .     *TQCDH(AMTP**2/AMH**2)
     .     *HFFSELF(AMH)
        RAT = 2*AMTP/AMH
        HTPTP = QQINT_HDEC(RAT,HTP1,HTP2)
       ENDIF
      ENDIF
C    ==========  TOTAL WIDTH AND BRANCHING RATIOS 
C
      WTOT=HLL+HMM+HSS+HCC+HBB+HTT+HGG+HGA+HZGA+HWW+HZZ
     .    +HNUPNUP+HEPEP+HBPBP+HTPTP
c    .    +HEE
c     write(6,*)'BR(H -> ee) = ',AMH,HEE/WTOT
c     write(6,*)'part: ',HLL,HMM,HSS,HCC,HBB,HTT,HGG,HGA,HZGA,HWW,HZZ
c     write(6,*)HBB,HWW,HZZ
      IF(WTOT.NE.0.D0)THEN
       SMBRT=HTT/WTOT
       SMBRB=HBB/WTOT
       SMBRL=HLL/WTOT
       SMBRM=HMM/WTOT
       SMBRC=HCC/WTOT
       SMBRS=HSS/WTOT
       SMBRG=HGG/WTOT
       SMBRGA=HGA/WTOT
       SMBRZGA=HZGA/WTOT
       SMBRW=HWW/WTOT
       SMBRZ=HZZ/WTOT
       SMBRNUP=HNUPNUP/WTOT
       SMBREP=HEPEP/WTOT
       SMBRBP=HBPBP/WTOT
       SMBRTP=HTPTP/WTOT
      ELSE
       SMBRT=0
       SMBRB=0
       SMBRL=0
       SMBRM=0
       SMBRC=0
       SMBRS=0
       SMBRG=0
       SMBRGA=0
       SMBRZGA=0
       SMBRW=0
       SMBRZ=0
       SMBRNUP=0
       SMBREP=0
       SMBRBP=0
       SMBRTP=0
      ENDIF
      SMWDTH=WTOT

c     write(6,*)HLL,HMM
c     write(6,*)HSS,HCC,HBB,HTT
c     write(6,*)HGG,HGA,HZGA,HWW,HZZ
c     write(6,*)HNUPNUP,HEPEP,HBPBP,HTPTP
c     write(6,*)WTOT
c     write(6,*)SMBRT+SMBRB+SMBRL+SMBRM+SMBRC+SMBRS+SMBRG+SMBRGA
c    .         +SMBRZGA+SMBRW+SMBRZ+SMBRNUP+SMBREP+SMBRBP+SMBRTP

      AMH=AMXX

      endif

      IF(IHIGGS.GT.0)THEN

C +++++++++++++++++++++++  SUSY HIGGSSES +++++++++++++++++++++++
C
      CALL GAUGINO_HDEC(AMU,AM2,B,A,AMCHAR,AMNEUT,XMNEUT,AC1,AC2,AC3,
     .             AN1,AN2,AN3,ACNL,ACNR,AGDL,AGDA,AGDH,AGDC)
C
      TSC = (AMSQ+AMUR+AMDR)/3
      BSC = (AMSQ+AMUR+AMDR)/3
      CALL SFERMION_HDEC(TSC,BSC,AMSQ,AMUR,AMDR,AMEL,AMER,AL,AU,AD,AMU,
     .               AMST,AMSB,AMSL,AMSU,AMSD,AMSE,AMSN,AMSN1,
     .               GLEE,GLTT,GLBB,GHEE,GHTT,GHBB,
     .               GAEE,GATT,GABB,GCEN,GCTB)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     QSUSY = 1.D0/3
c     QSUSY = 1
c     QSUSY = 3
c     LOOP = 1
c     QSUSY1 = QSUSY
c     QSUSY2 = QSUSY
c     LOOP = 2
c     QSUSY1 = QSUSY
c     QSUSY2 = 1/QSUSY
c     write(6,*)'Loop, Factor = ?'
c     read(5,*)LOOP,QSUSY
c     QSUSY = DMIN1(AMSB(1),AMSB(2),AMGLU)*QSUSY
c     QSUSY = (AMSB(1)+AMSB(2)+AMGLU)/3*QSUSY
c     QSUSY = +0.8204315362167340D3
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     QSUSY = QSUSY1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c--real!!!
      QSUSY = 1
      LOOP = 2
      QSUSY1 = QSUSY
      QSUSY2 = QSUSY
C

c     write(6,*)'M_A, M_h, M_H, sin(alpha): ',AMA,AML,AMH,DSIN(A)

      ENDIF

c     write(6,*)'glt, glb = ',glt,glb
c     write(6,*)'ght, ghb = ',ght,ghb
c     write(6,*)'gat, gab = ',gat,gab

      IF(IHIGGS.EQ.1.OR.IHIGGS.EQ.5)THEN
C        =========================================================
C                           LIGHT CP EVEN HIGGS DECAYS
C        =========================================================
C     =============  RUNNING MASSES 
      RMS = RUNM_HDEC(AML,3)
      RMC = RUNM_HDEC(AML,4)
      RMB = RUNM_HDEC(AML,5)
      RMT = RUNM_HDEC(AML,6)
      RATCOUP = GLT/GLB
      HIGTOP = AML**2/AMT**2

      ASH=ALPHAS_HDEC(AML,3)
      AMC0=1.D8
      AMB0=2.D8
C     AMT0=3.D8
      AS3=ALPHAS_HDEC(AML,3)
      AMC0=AMC
      AS4=ALPHAS_HDEC(AML,3)
      AMB0=AMB
C     AMT0=AMT

C     =============== PARTIAL WIDTHS 
C  H ---> G G
       EPS=1.D-8
       NFEXT = 3
       ASG = AS3
       CTT = 4*AMT**2/AML**2*DCMPLX(1D0,-EPS)
       CTB = 4*AMB**2/AML**2*DCMPLX(1D0,-EPS)
       CAT = 2*CTT*(1+(1-CTT)*CF(CTT))*GLT
       CAB = 2*CTB*(1+(1-CTB)*CF(CTB))*GLB
       CTC = 4*AMC**2/AML**2*DCMPLX(1D0,-EPS)
       CAC = 2*CTC*(1+(1-CTC)*CF(CTC))*GLT
C

c this is for a check    
       bb = datan(tgbet2hdm)
c       print*,'1,glt/b',dcos(alph2hdm)/dsin(bb),glt,glb

c       print*,'2,glt',dcos(alph2hdm)/dsin(bb),glt
c       print*,'2,glb',-sin(alph2hdm)/dcos(bb),glb

c       print*,'v1',amt,amb,amc,cat,cab,cac
c end check

       IF(IOFSUSY.EQ.0) THEN 
       CSB1= 4*AMSB(1)**2/AML**2*DCMPLX(1D0,-EPS)
       CSB2= 4*AMSB(2)**2/AML**2*DCMPLX(1D0,-EPS)
       CST1= 4*AMST(1)**2/AML**2*DCMPLX(1D0,-EPS)
       CST2= 4*AMST(2)**2/AML**2*DCMPLX(1D0,-EPS)
       CXB1=-AMZ**2/AMSB(1)**2*CSB1*(1-CSB1*CF(CSB1))*GLBB(1,1)
       CXB2=-AMZ**2/AMSB(2)**2*CSB2*(1-CSB2*CF(CSB2))*GLBB(2,2)
       CXT1=-AMZ**2/AMST(1)**2*CST1*(1-CST1*CF(CST1))*GLTT(1,1)
       CXT2=-AMZ**2/AMST(2)**2*CST2*(1-CST2*CF(CST2))*GLTT(2,2)

       CSUL = 4*AMSU(1)**2/AML**2*DCMPLX(1D0,-EPS)
       CSUR = 4*AMSU(2)**2/AML**2*DCMPLX(1D0,-EPS)
       CSDL = 4*AMSD(1)**2/AML**2*DCMPLX(1D0,-EPS)
       CSDR = 4*AMSD(2)**2/AML**2*DCMPLX(1D0,-EPS)
       CXUL=2*(1.D0/2.D0-2.D0/3.D0*SS)*AMZ**2/AMSU(1)**2*DSIN(A+B)
     .      *CSUL*(1-CSUL*CF(CSUL))
       CXUR=2*(2.D0/3.D0*SS)*AMZ**2/AMSU(2)**2*DSIN(A+B)
     .      *CSUR*(1-CSUR*CF(CSUR))
       CXDL=2*(-1.D0/2.D0+1.D0/3.D0*SS)*AMZ**2/AMSD(1)**2*DSIN(A+B)
     .      *CSDL*(1-CSDL*CF(CSDL))
       CXDR=2*(-1.D0/3.D0*SS)*AMZ**2/AMSD(2)**2*DSIN(A+B)
     .      *CSDR*(1-CSDR*CF(CSDR))

       ELSE
       CXB1=0.D0 
       CXB2=0.D0 
       CXT1=0.D0 
       CXT2=0.D0 

       CXUL=0.D0 
       CXUR=0.D0 
       CXDL=0.D0 
       CXDR=0.D0 
       ENDIF

       FQCD=HGGQCD(ASG,NFEXT)
       SQCD=SGGQCD(ASG)
       XFAC = CDABS(CAT+CAB+CAC+CXB1+CXB2+CXT1+CXT2
     .             +CXUL+CXUR+CXDL+CXDR)**2*FQCD
     .      + DREAL(DCONJG(CAT+CAB+CAC+CXB1+CXB2+CXT1+CXT2
     .                    +CXUL+CXUR+CXDL+CXDR)
     .             *(CXB1+CXB2+CXT1+CXT2+CXUL+CXUR+CXDL+CXDR))*SQCD

c this is for a check
c       print*,'v2',xfac,CDABS(CAT+CAB+CAC)**2*FQCD
c Maggie question // mass dependent NLO QCD corrections?
c end check

       HGG=HVV(AML,0.D0)*(ASG/PI)**2*XFAC/8
c      write(6,*)'gg: ',CAT,CAB,CAC,CXB1+CXB2,CXT1+CXT2,
c    .                  CXUL+CXUR+CXDL+CXDR

c      write(6,*)'amhl, glb, glt: ',aml,glb,glt

c      print*,''
c      print*,'h decay widths'
c      print*,'hgg_NLO',hgg

C  H ---> G G* ---> G CC   TO BE ADDED TO H ---> CC
       NFEXT = 4
       ASG = AS4
       FQCD=HGGQCD(ASG,NFEXT)
       SQCD=SGGQCD(ASG)
       XFAC = CDABS(CAT+CAB+CAC+CXB1+CXB2+CXT1+CXT2
     .             +CXUL+CXUR+CXDL+CXDR)**2*FQCD
     .      + DREAL(DCONJG(CAT+CAB+CAC+CXB1+CXB2+CXT1+CXT2
     .                    +CXUL+CXUR+CXDL+CXDR)
     .             *(CXB1+CXB2+CXT1+CXT2+CXUL+CXUR+CXDL+CXDR))*SQCD
       DCC=HVV(AML,0.D0)*(ASG/PI)**2*XFAC/8 - HGG

C  H ---> G G* ---> G BB   TO BE ADDED TO H ---> BB
       NFEXT = 5
       ASG = ASH
       FQCD=HGGQCD(ASG,NFEXT)
       SQCD=SGGQCD(ASG)
       XFAC = CDABS(CAT+CAB+CAC+CXB1+CXB2+CXT1+CXT2
     .             +CXUL+CXUR+CXDL+CXDR)**2*FQCD
     .      + DREAL(DCONJG(CAT+CAB+CAC+CXB1+CXB2+CXT1+CXT2
     .                    +CXUL+CXUR+CXDL+CXDR)
     .             *(CXB1+CXB2+CXT1+CXT2+CXUL+CXUR+CXDL+CXDR))*SQCD
       DBB=HVV(AML,0.D0)*(ASG/PI)**2*XFAC/8 - HGG - DCC
       HGG=HVV(AML,0.D0)*(ASG/PI)**2*XFAC/8

C  H ---> G G: FULL NNNLO CORRECTIONS TO TOP LOOPS FOR NF=5
       FQCD0=HGGQCD(ASG,5)
       FQCD=HGGQCD2(ASG,5,AML,AMT)
       XFAC = CDABS(CAT+CAB+CAC)**2*(FQCD-FQCD0)
       HGG=HGG+HVV(AML,0.D0)*(ASG/PI)**2*XFAC/8

      IF(NFGG.EQ.3)THEN
       HGG = HGG - DBB - DCC
      ELSEIF(NFGG.EQ.4)THEN
       HGG = HGG - DBB
       DCC = 0
      ELSE
       DCC = 0
       DBB = 0
      ENDIF

c Alex
c     CAT0 = CAT/GLT
c     CAB0 = CAB/GLB
c     CAC0 = CAC/GLT
c     XFAC0 = CDABS(CAT0+CAB0+CAC0)**2*FQCD
c     HGG0=HVV(AML,0.D0)*(ASG/PI)**2*XFAC0/8
c     write(1,*)'c_gg= ',HGG/HGG0
c end Alex

c     print*,'hgg_NNLO',hgg

C  H ---> MU MU
      XGLM = GLB
      XGHM = GHB
      XGAM = GAB
      if(i2hdm.eq.1) then
         xglm = gllep
      endif
      IF(IOFSUSY.EQ.0) THEN
       CALL STAUSUSY_HDEC(GLB,GHB,GAB,XGLM,XGHM,XGAM,QSUSY,0)
      ENDIF
      IF(AML.LE.2*AMMUON) THEN
       HMM = 0
      ELSE
      HMM=HFF(AML,(AMMUON/AML)**2)*XGLM**2
      ENDIF

c      print*,'h -> mumu',hmm
C  H ---> TAU TAU
      XGLT = GLB
      XGHT = GHB
      XGAT = GAB
      if(i2hdm.eq.1) then
         xglt = gllep
      endif
      IF(IOFSUSY.EQ.0) THEN
       CALL STAUSUSY_HDEC(GLB,GHB,GAB,XGLT,XGHT,XGAT,QSUSY,1)
      ENDIF
      IF(AML.LE.2*AMTAU) THEN
       HLL = 0
      ELSE
      HLL=HFF(AML,(AMTAU/AML)**2)*XGLT**2
      ENDIF

c Alex
c     write(1,*)'c_tau= ',XGLT
c     write(1,*)'c0_tau= ',GLT
c     write(1,*)'Deltatau= ',(gat/xgat-1)/(1+gat/xgat/tgbet**2)
c end Alex

c     write(6,*)'h: tau/mu: ',HLL/HMM*AMMUON**2/AMTAU**2,XGLT**2/XGLM**2
c      print*,'h -> tautau',hll
C  H --> SS

      XGLS = GLB
      XGHS = GHB
      XGAS = GAB
      SSUSY = (AMSD(1)+AMSD(2)+AMGLU)/3*QSUSY
      IF(IOFSUSY.EQ.0) THEN
       CALL STRSUSY_HDEC(GLB,GHB,GAB,XGLS,XGHS,XGAS,SSUSY,LOOP)
      ENDIF
      IF(AML.LE.2*AMS) THEN
       HSS = 0
      ELSE
       HS1=3.D0*HFF(AML,(AMS/AML)**2)
     .    *XGLS**2
     .    *TQCDH(AMS**2/AML**2)
       HS2=3.D0*HFF(AML,(RMS/AML)**2)*XGLS**2
     .    *QCDH(RMS**2/AML**2)
       IF(HS2.LT.0.D0) HS2 = 0
       RAT = 2*AMS/AML
       HSS = QQINT_HDEC(RAT,HS1,HS2)
      ENDIF
c Alex
c     write(1,*)'c_s= ',XGLS
c     write(1,*)'c0_s= ',GLB
c end Alex

c      print*,'h -> ss',hss
C  H --> CC

      RATCOUP = 1
      IF(AML.LE.2*AMC) THEN
       HCC = 0
      ELSE
       HC1=3.D0*HFF(AML,(AMC/AML)**2)
     .    *GLT**2
     .    *TQCDH(AMC**2/AML**2)
       HC2=3.D0*HFF(AML,(RMC/AML)**2)*GLT**2
     .    *QCDH(RMC**2/AML**2)
     .   + DCC
       IF(HC2.LT.0.D0) HC2 = 0
       RAT = 2*AMC/AML
       HCC = QQINT_HDEC(RAT,HC1,HC2)
      ENDIF

c      print*,'h -> cc',hcc
C  H --> BB :

      XGLB = GLB
      XGHB = GHB
      XGAB = GAB
      QQ = AMB
      SUSY = 0
      XGLB = GLB
      SSUSY = (AMSB(1)+AMSB(2)+AMGLU)/3*QSUSY
      FSUSY = SUSYSCALE
      AS0 = ALPHAS_HDEC(FSUSY,3)
      IF(IOFSUSY.EQ.0) THEN
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       I0 = 0
       CALL DMBAPP_HDEC(I0,DGLB,DGHB,DGAB,FSUSY,LOOP)
       DELB1 = -DGAB/(1+1/TGBET**2)
       DELB0 = DELB1/(1-DELB1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       I0 = 1
       CALL DMBAPP_HDEC(I0,DGLB,DGHB,DGAB,FSUSY,LOOP)
       I0 = 1
       BSC = (AMSQ+AMUR+AMDR)/3
       DELB2 = -DGAB/(1+1/TGBET**2)
c      write(6,*)'Delta_b = ',DELB0
       XMB = RUNM_HDEC(FSUSY,5)/(1+DELB0)
C       XMB = AMB
c1357
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       if(islhai.ne.0) XMB = AMB
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       SUSY = COFSUSY_HDEC(I0,AMB,XMB,QQ)*AS0/PI - 2*DGLB
c      SUSY = COFSUSY_HDEC(I0,XMB,XMB,QQ)*AS0/PI - 2*DGLB
       CALL BOTSUSY_HDEC(GLB,GHB,GAB,XGLB,XGHB,XGAB,SSUSY,LOOP)
      ENDIF
      RATCOUP = GLT/XGLB
      IF(AML.LE.2*AMB) THEN
       HBB = 0
      ELSE
       HB1=3.D0*HFF(AML,(AMB/AML)**2)
     .    *(XGLB**2+XGLB*GLB*SUSY)
     .    *TQCDH(AMB**2/AML**2)
       HB2=3.D0*HFF(AML,(RMB/AML)**2)
     .    *(XGLB**2+XGLB*GLB*SUSY)
     .    *QCDH(RMB**2/AML**2)
     .   + DBB
       IF(HB2.LT.0.D0) HB2 = 0
       RAT = 2*AMB/AML
       HBB = QQINT_HDEC(RAT,HB1,HB2)
c      write(6,*)'par: ',COFSUSY_HDEC(I0,AMB,XMB,QQ)*AS0/PI,2*DGLB

c Alex
c      XB1=3.D0*HFF(AML,(AMB/AML)**2)
c    .    *TQCDH(AMB**2/AML**2)
c      XB2=3.D0*HFF(AML,(RMB/AML)**2)
c    .    *QCDH(RMB**2/AML**2,5)
c    .   + DBB
c      IF(XB2.LT.0.D0) HB2 = 0
c      RAT = 2*AMB/AML
c      XBB = QQINT_HDEC(RAT,XB1,XB2)
c      write(6,*)'h -> bb = ',AML,HBB,XBB
c      write(6,*)'rem/tot = ',XGLB*GLB*SUSY/XGLB**2,
c    .                        XGLB**2+XGLB*GLB*SUSY,XGLB**2
c      write(1,*)'c_b= ',XGLB
c      write(1,*)'c0_b= ',GLB
c      write(1,*)'Deltab= ',DELB0
c      write(1,*)'Deltab= ',DELB2
c      TANA = DTAN(A)
c      TANB = TGBET
c      DELB1 = DELB0/(1-DELB0)
c      DELB1 = DELB0
c      TEST = -DELB1/(1+DELB1)*(1+1/TANA/TANB)
c      YGLB = GLB*(1+TEST)
c      write(1,*)'Deltab= ',DELB1
c      write(1,*)'tgbet = ',TGBET
c      write(1,*)'tgalp = ',TANA
c      write(1,*)'c_b= ',YGLB
c      write(1,*)'A_b= ',AD
c end Alex

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      XXX = COFSUSY_HDEC(I0,AMB,XMB,QQ)
c      write(1,*)'h -> bb: ',XXX
c      write(1,*)'h -> bb: ',XXX*AS0/PI
c      write(1,*)'h -> bb: ',2*DGLB
c      write(1,*)'h -> bb: ',GLB,XGLB,SUSY
c      write(1,*)'h -> bb: ',HBB

c Alex
c      write(1,*)'c_b= ',XGLB
c      write(1,*)'c0_b= ',GLB
c      write(1,*)'Deltab= ',DELB0,-DGAB,TGBET
c      write(1,*)-DSIN(A),DCOS(B),-DSIN(A)/DCOS(B)
c      write(6,*)'h -> bb: ',XGLB**2,XGLB*GLB*SUSY/XGLB**2,
c    .                       (XGLB**2+XGLB*GLB*SUSY)/XGLB**2
c      write(6,*)'approx:  ',SUSY+2*DGLB,2*DGLB,SUSY
c      FAC = AS0/PI
c      write(6,*)'approx2: ',(SUSY+2*DGLB)/FAC,2*DGLB/FAC,SUSY/FAC
c end Alex

c      print*,'h -> bb',hbb,rmb
c      print*
c      print*,'h -> bb:',hbb,glb,xglb,glb*susy,2*dglb,susy,2*dglb+susy

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c--hMSSM?
c      sb0 = dsin(b)
c      cb0 = dcos(b)
c      c2b0 = cb0**2-sb0**2
c      amh0 =dsqrt(((ama**2+amz**2-aml**2)*(amz**2*cb0**2+ama**2*sb0**2)
c    .     -ama**2*amz**2*c2b0**2)/(amz**2*cb0**2+ama**2*sb0**2-aml**2))
c      amch0 = dsqrt(ama**2+amw**2)
c      a0 = -datan((amz**2+ama**2)*sb0*cb0
c    .            /(amz**2*cb0**2+ama**2*sb0**2-aml**2))
c      sa0 = dsin(a0)
c      ca0 = dcos(a0)
c      s2a0 = 2*sa0*ca0
c      c2a0 = ca0**2-sa0**2
c      sbpa0 = sb0*ca0+cb0*sa0
c      cbpa0 = cb0*ca0-sb0*sa0
c      deps = (aml**2*(ama**2+amz**2-aml**2)-ama**2*amz**2*c2b0**2)
c    .      / (amz**2*cb0**2+ama**2*sb0**2-aml**2)
c      ghhh0 = 3*c2a0*cbpa0              + 3*deps/amz**2*sa0**3/sb0
c      glll0 = 3*c2a0*sbpa0              + 3*deps/amz**2*ca0**3/sb0
c      ghll0 = 2*s2a0*sbpa0 - c2a0*cbpa0 + 3*deps/amz**2*sa0*ca0**2/sb0
c      glhh0 =-2*s2a0*cbpa0 - c2a0*sbpa0 + 3*deps/amz**2*sa0**2*ca0/sb0
c      ghaa0 =-c2b0*cbpa0                 + deps/amz**2*sa0*cb0**2/sb0
c      glaa0 = c2b0*sbpa0                 + deps/amz**2*ca0*cb0**2/sb0
c      write(6,*)'0: ',AML,AMH,AMA
c      write(6,*)'1: ',AML,AMH0,AMA
c      write(6,*)'0: ',AMCH,B,A
c      write(6,*)'1: ',AMCH0,B,A0
c      write(6,*)'   ',glb,xglb
c      write(6,*)'   ',ghb,xghb
c      write(6,*)'   ',gab,xgab
c      write(6,*)
c      write(6,*)
c      write(6,*)'HHH: ',glll,ghll,glhh,ghhh
c      write(6,*)'     ',glll0,ghll0,glhh0,ghhh0
c      write(6,*)
c      write(6,*)'HAA: ',glaa,ghaa
c      write(6,*)'     ',glaa0,ghaa0
c      write(6,*)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,('A3,4(1X,G15.8)'))'h: ',AMA,AML,SUSY+2*DGLB,
c    .                             SUSY/(SUSY+2*DGLB)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      HB2=3.D0*HFF(AML,(RMB/AML)**2)
c    .    *QCDH(RMB**2/AML**2,5)
c    .    *(1+ELW0(AML,RMB,-1.D0/3.D0,1.D0))
c    .    *HFFSELF(AML)
c    .    + DBB
c      IF(HB2.LT.0.D0) HB2 = 0
c      HB1=3.D0*HFF(AML,(AMB/AML)**2)
c    .    *TQCDH(AMB**2/AML**2)
c    .    *HFFSELF(AML)
c      RAT = 2*AMB/AML
c      HBB0 = QQINT_HDEC(RAT,HB1,HB2)
c      write(6,*)AML,XGLB,GLB,XGLB/GLB-1,SUSY,HBB/HBB0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(6,*)AML,HB1,HB2,HBB,GLB,XGLB,SUSY+2*DGLB,2*DGLB
c    .          ,XGLB**2+XGLB*GLB*SUSY
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      XB0=3.D0*HFF(AML,(AMB/AML)**2)
c    .    *GLB**2
c      XB1=3.D0*HFF(AML,(RMB/AML)**2)
c    .    *GLB**2
c    .    *QCDH(RMB**2/AML**2,5)
c    .   + DBB
c      XB2=3.D0*HFF(AML,(RMB/AML)**2)
c    .    *(XGLB**2+XGLB*GLB*SUSY)
c    .    *QCDH(RMB**2/AML**2,5)
c    .   + DBB
c      write(51,('5(1X,G15.8)'))AMA,AML,XB0,XB1,XB2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      write(51,('4(1X,G15.8)'))AMA,AML,SUSY+2*DGLB,SUSY/(SUSY+2*DGLB)
c      write(51,('4(1X,G15.8)'))AMA,AML,HBB,2*DGLB,XGLB,SUSY-1+2*DLGB,
c    .                          DSIN(A),DCOS(A)
c      write(51,*)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c      X1 = (QCDH(RMB**2/AML**2,5)*HFF(AML,(RMB/AML)**2)/
c    .       HFF(AML,(AMB/AML)**2)-1)
c      X2 = (SUSY-1)

c     RATCOUP = GLT/XGLB
c      HB1X=3.D0*HFF(AML,(AMB/AML)**2)
c    .    *XGLB**2
c    .    *TQCDH(AMB**2/AML**2)
c    .    /(BETA_HDEC(AMB**2/AML**2))**3
c    .    *SUSY
c      HB2X=3.D0*HFF(AML,(RMB/AML)**2)*XGLB**2
c    .    *QCDH(RMB**2/AML**2,5)
c    .    /(BETA_HDEC(RMB**2/AML**2))**3
c    .    *SUSY
c      HB1X=3.D0*HFF(AML,(RMB/AML)**2)*GLB**2
c    .    *QCDH(RMB**2/AML**2)
c    .    /(BETA_HDEC(RMB**2/AML**2))**3
c    .    *(SUSY+2*DGLB)

c     RATCOUP = 0
c     deltaqcd = QCDH(RMB**2/AML**2,5)
c     RATCOUP = GLT/XGLB
c     deltat = QCDH(RMB**2/AML**2,5) - deltaqcd

c      write(6,*)
c      write(6,*)'h:'
c      write(6,*)'MB,RUNMB,alpha_s: ',AMB,RMB,ASH
c      write(6,*)'Mh =              ',AML
c      write(6,*)'MA =              ',AMA
c      write(6,*)'Delta(mb) = ',-DGAB
c      write(6,*)'QCD           SUSY          APPROX',
c    .           '        APPROX/FULL  Gbh(QCD)    Gbh(SQCD):'
c      write(6,*)X1,X2+2*DGLB,2*DGLB,2*DGLB/(X2+2*DGLB),GLB,XGLB
c      write(6,*)'Resummation: ',(XGLB/GLB)**2-1
c      write(6,*)'Rest:        ',SUSY-1
c      write(6,*)'Rest:        ',SUSY-1,dtan(a),tgbet
c      write(6,*)AMSQ,AMUR,AMDR,(SUSY-1)/(X2+2*DGLB)
c      write(6,*)'Total SUSY:  ',(XGLB/GLB)**2*SUSY-1
c      write(6,*)'deltaqcd,t = ',deltaqcd,deltat
c      write(6,*)'Gamma(0)   = ',AMA,HB2X,HB1X
c      write(6,*)'Gamma(mb)  = ',HB2,HB1
c      write(6,*)
c      write(9,*)AMA,AML,HB2X,HB2X/SUSY,GLB,XGLB
c      write(6,*)'Rest: h      ',AMA,AML,(SUSY-1)/(X2+2*DGLB)
c      write(51,*)AMA,AML,(SUSY-1)/(X2+2*DGLB)
      ENDIF
C  H ---> TT
      RATCOUP = 0
      CALL TOPSUSY_HDEC(GLT,GHT,GAT,XGLTOP,XGHTOP,XGATOP,SCALE,1)
c     write(6,*)xgltop,xghtop,xgatop
      IF (AML.LE.2*AMT) THEN
       HTT=0.D0
      ELSE
       HT1=3.D0*HFF(AML,(AMT/AML)**2)*GLT**2
     .    *TQCDH(AMT**2/AML**2)
       HT2=3.D0*HFF(AML,(RMT/AML)**2)*GLT**2
     .    *QCDH(RMT**2/AML**2)
       IF(HT2.LT.0.D0) HT2 = 0
       RAT = 2*AMT/AML
       HTT = QQINT_HDEC(RAT,HT1,HT2)
      ENDIF

c MMM changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0)then
            dld=5.D0
            dlu=3.D0
            xm1 = 2d0*amt-dld
            xm2 = 2d0*amt+dlu
            if (aml.le.amt+amw+amb) then
               htt=0.d0
            elseif (aml.le.xm1) then
               factt=6.d0*gf**2*aml**3*amt**2/2.d0/128.d0/pi**3
               call HTOTT_hdec(aml,amt,amb,amw,amch,glt,glb,gat,gab,
     .              glvv,gzah,htt0)
               htt=factt*htt0
            elseif (aml.le.xm2) then
               XX(1) = XM1-1D0
               XX(2) = XM1
               XX(3) = XM2
               XX(4) = XM2+1D0

               factt=6.d0*gf**2*xx(1)**3*amt**2/2.d0/128.d0/pi**3
               call HTOTT_hdec(xx(1),amt,amb,amw,amch,glt,glb,gat,gab,
     .              glvv,gzah,htt0)
               yy(1)=factt*htt0

               factt=6.d0*gf**2*xx(2)**3*amt**2/2.d0/128.d0/pi**3
               call HTOTT_hdec(xx(2),amt,amb,amw,amch,glt,glb,gat,gab,
     .              glvv,gzah,htt0)
               yy(2)=factt*htt0

               xmt = runm_hdec(xx(3),6)
               ht1=3.d0*hff(xx(3),(amt/xx(3))**2)*glt**2
     .              *tqcdh(amt**2/xx(3)**2)
               ht2=3.d0*hff(xx(3),(xmt/xx(3))**2)*glt**2
     .              *qcdh(xmt**2/xx(3)**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2*amt/xx(3)
               yy(3) = qqint_hdec(rat,ht1,ht2)

               xmt = runm_hdec(xx(4),6,1)
               ht1=3.d0*hff(xx(4),(amt/xx(4))**2)*glt**2
     .              *tqcdh(amt**2/xx(4)**2)
               ht2=3.d0*hff(xx(4),(xmt/xx(4))**2)*glt**2
     .              *qcdh(xmt**2/xx(4)**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2*amt/xx(4)
               yy(4) = qqint_hdec(rat,ht1,ht2)

               htt=fint_hdec(aml,xx,yy)
            else
               ht1=3.d0*hff(aml,(amt/aml)**2)*glt**2
     .              *tqcdh(amt**2/aml**2)
               ht2=3.d0*hff(aml,(rmt/aml)**2)*glt**2
     .              *qcdh(rmt**2/aml**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2.D0*amt/aml
               htt = qqint_hdec(rat,ht1,ht2)
            endif
         else
            if (aml.le.2.d0*amt) then
               htt=0.d0
            else
               ht1=3.d0*hff(aml,(amt/aml)**2)*glt**2
     .              *tqcdh(amt**2/aml**2)
               ht2=3.d0*hff(aml,(rmt/aml)**2)*glt**2
     .              *qcdh(rmt**2/aml**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2.D0*amt/aml
               htt = qqint_hdec(rat,ht1,ht2)
            endif
         endif
      endif
c end MMM changed 22/8/2013

c Alex
c     write(1,*)'c_t= ',XGLTOP
c     write(1,*)'c0_t= ',GLT
c end Alex

c      print*,'h -> tt',htt
C  H ---> GAMMA GAMMA
       EPS=1.D-8
       XRMC = RUNM_HDEC(AML/2,4)*AMC/RUNM_HDEC(AMC,4)
       XRMB = RUNM_HDEC(AML/2,5)*AMB/RUNM_HDEC(AMB,5)
       XRMT = RUNM_HDEC(AML/2,6)*AMT/RUNM_HDEC(AMT,6)

       CTT = 4*XRMT**2/AML**2*DCMPLX(1D0,-EPS)
       CTB = 4*XRMB**2/AML**2*DCMPLX(1D0,-EPS)
       CTC = 4*XRMC**2/AML**2*DCMPLX(1D0,-EPS)
       CTL = 4*AMTAU**2/AML**2*DCMPLX(1D0,-EPS)
       CTW = 4*AMW**2/AML**2*DCMPLX(1D0,-EPS)
       CTH = 4*AMCH**2/AML**2*DCMPLX(1D0,-EPS)
       CAT = 4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT))*GLT
     .     * CFACQ_HDEC(0,AML,XRMT)

c       print*,'cat_LO',4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT))*GLT
c       print*,'cat_NLO',cat
c       print*,'h-top-top coupling',glt
c       print*,'4*rmt**2/aml**2',ctt
c       print*,'rmt',rmt
c       print*,'aml',aml

       CAB = 1/3D0 * 2*CTB*(1+(1-CTB)*CF(CTB))*GLB
     .     * CFACQ_HDEC(0,AML,XRMB)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      CALL BOTSUSY_HDEC(GLB,GHB,GAB,XGLB,XGHB,XGAB,SSUSY,LOOP)
c      CAB = 1/3D0 * 2*CTB*(1+(1-CTB)*CF(CTB))*XGLB
c    .     * CFACQ_HDEC(0,AML,XRMB)
c      write(6,*)CTB,XGLB,CFACQ_HDEC(0,AML,XRMB)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CAC = 4/3D0 * 2*CTC*(1+(1-CTC)*CF(CTC))*GLT
     .     * CFACQ_HDEC(0,AML,XRMC)
       CAL = 1.D0  * 2*CTL*(1+(1-CTL)*CF(CTL))*GLB
       if(i2hdm.eq.1) then
          CAL = 1.D0  * 2*CTL*(1+(1-CTL)*CF(CTL))*gllep
       endif
       CAW = -(2+3*CTW+3*CTW*(2-CTW)*CF(CTW))*GLVV
       CAH = -AMZ**2/2/AMCH**2*CTH*(1-CTH*CF(CTH))*GLPM

c      write(6,*)'gamma gamma: ',AML,AMCH,-3*CTH*(1-CTH*CF(CTH))

c       print*,'amz,amch,ghlh+h-',amz,amch,glpm,cth
c       print*,'fac1',-AMZ**2/2/AMCH**2*glpm
c       print*,'fac2',CTH*(1-CTH*CF(CTH))
c       write(6,*)'CAH,AMZ,AMCH,CTH,CF(CTH),GLPM: ',
c    .             CAH,AMZ,AMCH,CTH,CF(CTH),GLPM
       IF(IOFSUSY.EQ.0) THEN 
        RMSU1 = RUNMS_HDEC(AML/2,AMSU(1))
        RMSU2 = RUNMS_HDEC(AML/2,AMSU(2))
        RMSD1 = RUNMS_HDEC(AML/2,AMSD(1))
        RMSD2 = RUNMS_HDEC(AML/2,AMSD(2))
        RMSB1 = RUNMS_HDEC(AML/2,AMSB(1))
        RMSB2 = RUNMS_HDEC(AML/2,AMSB(2))
        RMST1 = RUNMS_HDEC(AML/2,AMST(1))
        RMST2 = RUNMS_HDEC(AML/2,AMST(2))
        CX1 = 4*AMCHAR(1)**2/AML**2*DCMPLX(1D0,-EPS)
        CX2 = 4*AMCHAR(2)**2/AML**2*DCMPLX(1D0,-EPS)
        CSB1= 4*RMSB1**2/AML**2*DCMPLX(1D0,-EPS)
        CSB2= 4*RMSB2**2/AML**2*DCMPLX(1D0,-EPS)
        CST1= 4*RMST1**2/AML**2*DCMPLX(1D0,-EPS)
        CST2= 4*RMST2**2/AML**2*DCMPLX(1D0,-EPS)
        CSL1= 4*AMSL(1)**2/AML**2*DCMPLX(1D0,-EPS)
        CSL2= 4*AMSL(2)**2/AML**2*DCMPLX(1D0,-EPS)
        CAX1= AMW/XMCHAR(1) * 2*CX1*(1+(1-CX1)*CF(CX1))*2*AC2(1,1) 
        CAX2= AMW/XMCHAR(2) * 2*CX2*(1+(1-CX2)*CF(CX2))*2*AC2(2,2) 

        CSEL = 4*AMSE(1)**2/AML**2*DCMPLX(1D0,-EPS)
        CSER = 4*AMSE(2)**2/AML**2*DCMPLX(1D0,-EPS)
        CSUL = 4*RMSU1**2/AML**2*DCMPLX(1D0,-EPS)
        CSUR = 4*RMSU2**2/AML**2*DCMPLX(1D0,-EPS)
        CSDL = 4*RMSD1**2/AML**2*DCMPLX(1D0,-EPS)
        CSDR = 4*RMSD2**2/AML**2*DCMPLX(1D0,-EPS)
        CXEL=2*(-1/2D0+SS)*AMZ**2/AMSE(1)**2*DSIN(A+B)
     .       *CSEL*(1-CSEL*CF(CSEL))
        CXER=-2*(SS)*AMZ**2/AMSE(2)**2*DSIN(A+B)
     .       *CSER*(1-CSER*CF(CSER))
        CXUL=2*4.D0/3.D0*(1.D0/2.D0-2.D0/3.D0*SS)
     .       *AMZ**2/AMSU(1)**2*DSIN(A+B)*CSUL*(1-CSUL*CF(CSUL))
     .      * CFACSQ_HDEC(AML,RMSU1)
        CXUR=2*4.D0/3.D0*(2.D0/3.D0*SS)
     .       *AMZ**2/AMSU(2)**2*DSIN(A+B)*CSUR*(1-CSUR*CF(CSUR))
     .      * CFACSQ_HDEC(AML,RMSU2)
        CXDL=2/3.D0*(-1.D0/2.D0+1.D0/3.D0*SS)
     .       *AMZ**2/AMSD(1)**2*DSIN(A+B)*CSDL*(1-CSDL*CF(CSDL))
     .      * CFACSQ_HDEC(AML,RMSD1)
        CXDR=2/3.D0*(-1.D0/3.D0*SS)
     .       *AMZ**2/AMSD(2)**2*DSIN(A+B)*CSDR*(1-CSDR*CF(CSDR))
     .      * CFACSQ_HDEC(AML,RMSD2)

        CXB1=-1/3D0*AMZ**2/AMSB(1)**2*CSB1*(1-CSB1*CF(CSB1))*GLBB(1,1)
     .      * CFACSQ_HDEC(AML,RMSB1)
        CXB2=-1/3D0*AMZ**2/AMSB(2)**2*CSB2*(1-CSB2*CF(CSB2))*GLBB(2,2)
     .      * CFACSQ_HDEC(AML,RMSB2)
        CXT1=-4/3D0*AMZ**2/AMST(1)**2*CST1*(1-CST1*CF(CST1))*GLTT(1,1)
     .      * CFACSQ_HDEC(AML,RMST1)
        CXT2=-4/3D0*AMZ**2/AMST(2)**2*CST2*(1-CST2*CF(CST2))*GLTT(2,2)
     .      * CFACSQ_HDEC(AML,RMST2)
        CSL1= 4*AMSL(1)**2/AML**2*DCMPLX(1D0,-EPS)
        CSL2= 4*AMSL(2)**2/AML**2*DCMPLX(1D0,-EPS)
        CXL1=      -AMZ**2/AMSL(1)**2*CSL1*(1-CSL1*CF(CSL1))*GLEE(1,1)
        CXL2=      -AMZ**2/AMSL(2)**2*CSL2*(1-CSL2*CF(CSL2))*GLEE(2,2)
        XFAC = CDABS(CAT+CAB+CAC+CAL+CAW+CAH+CAX1+CAX2
     .      +  CXEL+CXER+CXUL+CXUR+CXDL+CXDR
     .      +  CXB1+CXB2+CXT1+CXT2+CXL1+CXL2)**2
c      write(6,*)'hga: ',CAH,GLPM
       ELSE 
        XFAC = CDABS(CAT+CAB+CAC+CAL+CAW+CAH)**2
       ENDIF
       HGA=HVV(AML,0.D0)*(ALPH/PI)**2/16.D0*XFAC
c      write(6,*)'h -> ga ga: ',XFAC,CAT,CAB,CAC,CAL,CAW,CAH
c      write(6,*)'h -> ga ga: ',GLPM

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CAT0 = CAT/GLT
       CAB0 = CAB/GLB
       CAC0 = CAC/GLT
       CAL0 = CAL/GLB
       CAW0 = CAW/GLVV
       XFAC0 = CDABS(CAT0+CAB0+CAC0+CAL0+CAW0)**2
       XCLGAGA = XFAC/XFAC0
c      write(6,*)'hgaga: ',aml,xclgaga
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c       print*,'h -> gamgam',hga,aml,amch
c       print*,'cat,cab',cat,cab
c       print*,'cac,cal',cac,cal
c       print*,'caw,cah',caw,cah
c       print*,'charged Higgs loop',cah
c       print*,'charged Higgs loop',cth,glpm,cf(cth)
c      CAH = -AMZ**2/2/AMCH**2*CTH*(1-CTH*CF(CTH))*GLPM
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       XFACQ = CDABS(CAT+CAB+CAC+CAL+CAW+CAH)**2
       XFACS = CDABS(CAT+CAB+CAC+CAL+CAW+CAH+CAX1+CAX2
     .      +  CXL1+CXL2)**2
       XFACSQ = CDABS(CAT+CAB+CAC+CAL+CAW+CAH+CAX1+CAX2
     .      +  CXB1+CXB2+CXT1+CXT2+CXL1+CXL2)**2
       HGA0 = HGA*XFACSQ/XFAC
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      CTT = 4*AMT**2/AML**2*DCMPLX(1D0,-EPS)
c      CTB = 4*AMB**2/AML**2*DCMPLX(1D0,-EPS)
c      CTC = 4*AMC**2/AML**2*DCMPLX(1D0,-EPS)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CAC0 = 4/3D0 * 2*CTC*(1+(1-CTC)*CF(CTC))*GLT
       CAT0 = 4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT))*GLT
       CAB0 = 1/3D0 * 2*CTB*(1+(1-CTB)*CF(CTB))*GLB
       CXB10= -1/3D0*AMZ**2/AMSB(1)**2*CSB1*(1-CSB1*CF(CSB1))*GLBB(1,1)
       CXB20= -1/3D0*AMZ**2/AMSB(2)**2*CSB2*(1-CSB2*CF(CSB2))*GLBB(2,2)
       CXT10= -4/3D0*AMZ**2/AMST(1)**2*CST1*(1-CST1*CF(CST1))*GLTT(1,1)
       CXT20= -4/3D0*AMZ**2/AMST(2)**2*CST2*(1-CST2*CF(CST2))*GLTT(2,2)
       XFACLOQ = CDABS(CAT0+CAB0+CAC0+CAL+CAW+CAH)**2
       CXUL0=2*4.D0/3.D0*(1.D0/2.D0-2.D0/3.D0*SS)
     .      *AMZ**2/AMSU(1)**2*DSIN(A+B)*CSUL*(1-CSUL*CF(CSUL))
       CXUR0=2*4.D0/3.D0*(2.D0/3.D0*SS)
     .      *AMZ**2/AMSU(2)**2*DSIN(A+B)*CSUR*(1-CSUR*CF(CSUR))
       CXDL0=2/3.D0*(-1.D0/2.D0+1.D0/3.D0*SS)
     .      *AMZ**2/AMSD(1)**2*DSIN(A+B)*CSDL*(1-CSDL*CF(CSDL))
       CXDR0=2/3.D0*(-1.D0/3.D0*SS)
     .      *AMZ**2/AMSD(2)**2*DSIN(A+B)*CSDR*(1-CSDR*CF(CSDR))
       XFACLO = CDABS(CAT0+CAB0+CAC0+CAL+CAW+CAH+CAX1+CAX2
     .      +  CXEL+CXER+CXUL0+CXUR0+CXDL0+CXDR0
     .      +  CXB10+CXB20+CXT10+CXT20+CXL1+CXL2)**2
       CSQ = 1+3*ALPHAS_HDEC(AML,3)
       XFACSQL = CDABS(CAT+CAB+CAC+CAL+CAW+CAH+CAX1+CAX2
     .      +  CXEL+CXER+(CXUL0+CXUR0+CXDL0+CXDR0
     .      +  CXB10+CXB20+CXT10+CXT20)*CSQ+CXL1+CXL2)**2
      XFACSM = CDABS(CAT/GLT+CAB/GLB+CAC/GLT+CAL/GLB+CAW/GLVV)**2
      XFAC0  = CDABS(CAT+CAB+CAC+CAL+CAW+CXL1)**2

c Alex
c     write(1,*)'c_gaga= ',XCLGAGA
c     write(1,*)'h -> gaga: ',XFAC,CDABS(CAT+CAB+CAC+CAL+CAW)**2
c    .                       ,XFAC/CDABS(CAT+CAB+CAC+CAL+CAW)**2
c     write(1,*)'H_SM -> gaga: ',XFAC0
c     write(1,*)'h -> gaga:    ',CDABS(CAT+CAB+CAC+CAL+CAW+CAH+CAX1+CAX2
c    .      +  CXEL+CXER+CXUL+CXUR+CXDL+CXDR
c    .      +  CXB1+CXB2+CXT1+CXT2+CXL1+CXL2)**2
c     write(1,*)'h -> gaga:    ',CDABS(CAT+CAB+CAC+CAL+CAW)**2
c     write(1,*)'h -> gaga:    ',CDABS(CAT+CAB+CAC+CAL+CAW+CAH+CAX1+CAX2
c    .      +  0*(CXEL+CXER+CXUL+CXUR+CXDL+CXDR
c    .      +  CXB1+CXB2+CXT1+CXT2+CXL1+CXL2))**2
c     write(1,*)'h -> gaga:    ',CDABS(CAT+CAB+CAC+CAL+CAW+0*CAH
c    .      +  0*CAX1+0*CAX2
c    .      +  1*CXEL+0*CXER+0*CXUL+0*CXUR+0*CXDL+0*CXDR
c    .      +  0*CXB1+0*CXB2+0*CXT1+0*CXT2+0*CXL1+0*CXL2)**2
c     write(1,*)'H_SM -> gaga: ',XFAC0
c     write(1,*)
c     write(1,*)'h -> gaga:    ',CDABS(CAT+CAB+CAC+CAL+CAW+CAH+CAX1+CAX2
c    .      +  CXEL+CXER+CXUL+CXUR+CXDL+CXDR
c    .      +  CXB1+CXB2+CXT1+CXT2+CXL1+CXL2)**2
c end Alex

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
C  H ---> Z GAMMA
      XRMC = RUNM_HDEC(AML/2,4)*AMC/RUNM_HDEC(AMC,4)
      XRMB = RUNM_HDEC(AML/2,5)*AMB/RUNM_HDEC(AMB,5)
      XRMT = RUNM_HDEC(AML/2,6)*AMT/RUNM_HDEC(AMT,6)
c     print*,'xrmc,xrmb,xrmt ',xrmc,xrmb,xrmt
      IF(AML.LE.AMZ)THEN
       HZGA=0
      ELSE
       TS = SS/CS
       FT = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS)*GLT
       FB = 3*1D0/3*(-1+4*1D0/3*SS)/DSQRT(SS*CS)*GLB
       FC = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS)*GLT
       FL = (-1+4*SS)/DSQRT(SS*CS)*GLB
       if(i2hdm.eq.1) then
          FL = (-1+4*SS)/DSQRT(SS*CS)*gllep
       endif
       EPS=1.D-8
c      CTT = 4*XRMT**2/AML**2*DCMPLX(1D0,-EPS)
c      CTB = 4*XRMB**2/AML**2*DCMPLX(1D0,-EPS)
c      CTC = 4*XRMC**2/AML**2*DCMPLX(1D0,-EPS)
       CTT = 4*AMT**2/AML**2*DCMPLX(1D0,-EPS)
       CTB = 4*AMB**2/AML**2*DCMPLX(1D0,-EPS)
       CTC = 4*AMC**2/AML**2*DCMPLX(1D0,-EPS)
       CTL = 4*AMTAU**2/AML**2*DCMPLX(1D0,-EPS)
       CTW = 4*AMW**2/AML**2*DCMPLX(1D0,-EPS)
       CTH = 4*AMCH**2/AML**2*DCMPLX(1D0,-EPS)
c      CLT = 4*XRMT**2/AMZ**2*DCMPLX(1D0,-EPS)
c      CLB = 4*XRMB**2/AMZ**2*DCMPLX(1D0,-EPS)
c      CLC = 4*XRMC**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLT = 4*AMT**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLB = 4*AMB**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLC = 4*AMC**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLE = 4*AMTAU**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLW = 4*AMW**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLH = 4*AMCH**2/AMZ**2*DCMPLX(1D0,-EPS)
       CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
       CAB = FB*(CI1(CTB,CLB) - CI2(CTB,CLB))
       CAC = FC*(CI1(CTC,CLC) - CI2(CTC,CLC))
       CAL = FL*(CI1(CTL,CLE) - CI2(CTL,CLE))
       CAW = -1/DSQRT(TS)*(4*(3-TS)*CI2(CTW,CLW)
     .     + ((1+2/CTW)*TS - (5+2/CTW))*CI1(CTW,CLW))*GLVV
       CAH = (1-2*SS)/DSQRT(SS*CS)*AMZ**2/2/AMCH**2*CI1(CTH,CLH)*GLPM
       XFAC = CDABS(CAT+CAB+CAC+CAL+CAW+CAH)**2
       ACOUP = DSQRT(2D0)*GF*AMZ**2*SS*CS/PI**2
       HZGA = GF/(4.D0*PI*DSQRT(2.D0))*AML**3*(ALPH/PI)*ACOUP/16.D0
     .        *XFAC*(1-AMZ**2/AML**2)**3
c      write(6,*)'Z gamma:     ',AML,AMCH,6*CI1(CTH,CLH)

      ENDIF

c      print*,'h -> Zgam',hzga
C  H ---> W W
      IF(IONWZ.EQ.0)THEN
       CALL HTOVV_HDEC(0,AML,AMW,GAMW,HTWW)
       HWW = 3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/AML**3*HTWW*GLVV**2
      ELSEIF(IONWZ.EQ.-1)THEN
       DLD=2D0
       DLU=2D0
       XM1 = 2D0*AMW-DLD
       XM2 = 2D0*AMW+DLU
       IF (AML.LE.XM1) THEN
        CALL HTOVV_HDEC(0,AML,AMW,GAMW,HTWW)
        HWW = 3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/AML**3*HTWW*GLVV**2
       ELSEIF (AML.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        CALL HTOVV_HDEC(0,XX(1),AMW,GAMW,HTWW)
        YY(1)=3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/XX(1)**3*HTWW
        CALL HTOVV_HDEC(0,XX(2),AMW,GAMW,HTWW)
        YY(2)=3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/XX(2)**3*HTWW
        YY(3)=HVV(XX(3),AMW**2/XX(3)**2)
        YY(4)=HVV(XX(4),AMW**2/XX(4)**2)
        HWW = FINT_HDEC(AML,XX,YY)*GLVV**2
       ELSE
        HWW=HVV(AML,AMW**2/AML**2)*GLVV**2
       ENDIF
      ELSE
      DLD=2D0
      DLU=2D0
      XM1 = 2D0*AMW-DLD
      XM2 = 2D0*AMW+DLU
      IF (AML.LE.AMW) THEN
       HWW=0
      ELSE IF (AML.LE.XM1) THEN
       CWW=3.D0*GF**2*AMW**4/16.D0/PI**3
       HWW=HV(AMW**2/AML**2)*CWW*AML*GLVV**2
      ELSE IF (AML.LT.XM2) THEN
       CWW=3.D0*GF**2*AMW**4/16.D0/PI**3
       XX(1) = XM1-1D0
       XX(2) = XM1
       XX(3) = XM2
       XX(4) = XM2+1D0
       YY(1)=HV(AMW**2/XX(1)**2)*CWW*XX(1)
       YY(2)=HV(AMW**2/XX(2)**2)*CWW*XX(2)
       YY(3)=HVV(XX(3),AMW**2/XX(3)**2)
       YY(4)=HVV(XX(4),AMW**2/XX(4)**2)
       HWW = FINT_HDEC(AML,XX,YY)*GLVV**2
      ELSE
       HWW=HVV(AML,AMW**2/AML**2)*GLVV**2
      ENDIF
      ENDIF

c      print*,'h -> WW',hww
C  H ---> Z Z
      IF(IONWZ.EQ.0)THEN
       CALL HTOVV_HDEC(0,AML,AMZ,GAMZ,HTZZ)
       HZZ = 3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/AML**3*HTZZ*GLVV**2
      ELSEIF(IONWZ.EQ.-1)THEN
       DLD=2D0
       DLU=2D0
       XM1 = 2D0*AMZ-DLD
       XM2 = 2D0*AMZ+DLU
       IF (AML.LE.XM1) THEN
        CALL HTOVV_HDEC(0,AML,AMZ,GAMZ,HTZZ)
        HZZ = 3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/AML**3*HTZZ*GLVV**2
       ELSEIF (AML.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        CALL HTOVV_HDEC(0,XX(1),AMZ,GAMZ,HTZZ)
        YY(1)=3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/XX(1)**3*HTZZ
        CALL HTOVV_HDEC(0,XX(2),AMZ,GAMZ,HTZZ)
        YY(2)=3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/XX(2)**3*HTZZ
        YY(3)=HVV(XX(3),AMZ**2/XX(3)**2)/2
        YY(4)=HVV(XX(4),AMZ**2/XX(4)**2)/2
        HZZ = FINT_HDEC(AML,XX,YY)*GLVV**2
       ELSE
        HZZ=HVV(AML,AMZ**2/AML**2)/2.D0*GLVV**2
       ENDIF
      ELSE
      DLD=2D0
      DLU=2D0
      XM1 = 2D0*AMZ-DLD
      XM2 = 2D0*AMZ+DLU
      IF (AML.LE.AMZ) THEN
       HZZ=0
      ELSE IF (AML.LE.XM1) THEN
       CZZ=3.D0*GF**2*AMZ**4/192.D0/PI**3*(7-40/3.D0*SS+160/9.D0*SS**2)
       HZZ=HV(AMZ**2/AML**2)*CZZ*AML*GLVV**2
      ELSE IF (AML.LT.XM2) THEN
       CZZ=3.D0*GF**2*AMZ**4/192.D0/PI**3*(7-40/3.D0*SS+160/9.D0*SS**2)
       XX(1) = XM1-1D0
       XX(2) = XM1
       XX(3) = XM2
       XX(4) = XM2+1D0
       YY(1)=HV(AMZ**2/XX(1)**2)*CZZ*XX(1)
       YY(2)=HV(AMZ**2/XX(2)**2)*CZZ*XX(2)
       YY(3)=HVV(XX(3),AMZ**2/XX(3)**2)/2D0
       YY(4)=HVV(XX(4),AMZ**2/XX(4)**2)/2D0
       HZZ = FINT_HDEC(AML,XX,YY)*GLVV**2
      ELSE
       HZZ=HVV(AML,AMZ**2/AML**2)/2.D0*GLVV**2
      ENDIF
      ENDIF

c Alex
c     write(1,*)'c_V= ',GLVV
c end Alex

c      print*,'h -> ZZ',hzz
C  H ---> A A
      IF (AML.LE.2.D0*AMA) THEN
      HAA=0
      ELSE
      HAA=GF/16.D0/DSQRT(2D0)/PI*AMZ**4/AML
     .   *BETA_HDEC(AMA**2/AML**2)*GLAA**2
      ENDIF

c MMM changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=0.1d0
            dlu=0.1d0
            xm1 = 2d0*ama-dld
            xm2 = 2d0*ama+dlu
            if (aml.le.ama) then
               haa = 0d0
            elseif (aml.le.xm1) then
               xa=ama**2/aml**2
               xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .              *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xa-1.d0)))
     .              /dsqrt(4.d0*xa-1.d0)
               xa2=3*gf**2/32.d0/pi**3*amz**4/aml*glaa**2*gab**2*amb**2
               haa=xa1*xa2
            elseif (aml.le.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               xa=ama**2/xx(1)**2
               xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .              *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xa-1.d0)))
     .              /dsqrt(4.d0*xa-1.d0)
               xa2=3*gf**2/32.d0/pi**3*amz**4/xx(1)*gab**2*amb**2
               yy(1)=xa1*xa2
               xa=ama**2/xx(2)**2
               xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .              *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xa-1.d0)))
     .              /dsqrt(4.d0*xa-1.d0)
               xa2=3*gf**2/32.d0/pi**3*amz**4/xx(2)*gab**2*amb**2
               yy(2)=xa1*xa2
               yy(3)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(3)
     .              *beta_hdec(ama**2/xx(3)**2)
               yy(4)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(4)
     .              *beta_hdec(ama**2/xx(4)**2)
               haa = fint_hdec(aml,xx,yy)*glaa**2
            else
               haa=gf/16d0/dsqrt(2d0)/pi*amz**4/aml*
     .              beta_hdec(ama**2/aml**2)*glaa**2
            endif
         else
            if (aml.le.2*ama) then
               haa=0
            else
               haa=gf/16d0/dsqrt(2d0)/pi*amz**4/aml*
     .              beta_hdec(ama**2/aml**2)*glaa**2
            endif
         endif
      endif
c end MMM changed 22/8/2013

c      print*,'h -> AA',haa

C  h ---> H+ H- 

      if(i2hdm.eq.1) then
         if (aml.le.2*amch) then
            hlchch=0.D0
         else
            hlchch=gf/8d0/dsqrt(2d0)/pi*amz**4/aml*
     .           beta_hdec(amch**2/aml**2)*glpm**2
         endif
      elseif(i2hdm.eq.0) then
         hlchch=0.D0
      endif

c     print*,'h -> H+H-',hlchch

C  H ---> A Z
      IF (AML.LE.AMZ+AMA) THEN
      HAZ=0
      ELSE
      CAZ=LAMB_HDEC(AMA**2/AML**2,AMZ**2/AML**2)
     .   *LAMB_HDEC(AML**2/AMZ**2,AMA**2/AMZ**2)**2
      HAZ=GF/8.D0/DSQRT(2D0)/PI*AMZ**4/AML*CAZ*GZAL**2
      ENDIF

c MMM changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=1d0
            dlu=8d0
            xm1 = ama+amz-dld
            xm2 = ama+amz+dlu
            if (aml.lt.ama) then
               haz=0
            elseif (aml.lt.xm1) then
               if(aml.le.dabs(amz-ama))then
                  haz=0
               else
                  haz=9.d0*gf**2/8.d0/pi**3*amz**4*aml*gzal**2*
     .                 (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .                 *hvh((ama/aml)**2,(amz/aml)**2)
               endif
            elseif (aml.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(1)*
     .              (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .              *hvh((ama/xx(1))**2,(amz/xx(1))**2)
               yy(2)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(2)*
     .              (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .              *hvh((ama/xx(2))**2,(amz/xx(2))**2)
               caz=lamb_hdec(ama**2/xx(3)**2,amz**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amz**2,ama**2/amz**2)**2
               yy(3)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(3)*caz
               caz=lamb_hdec(ama**2/xx(4)**2,amz**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amz**2,ama**2/amz**2)**2
               yy(4)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(4)*caz
               haz = fint_hdec(aml,xx,yy)*gzal**2
            else
               caz=lamb_hdec(ama**2/aml**2,amz**2/aml**2)
     .              *lamb_hdec(aml**2/amz**2,ama**2/amz**2)**2
               haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/aml*caz*gzal**2
            endif
         else
            if (aml.lt.amz+ama) then
               haz=0
            else
               caz=lamb_hdec(ama**2/aml**2,amz**2/aml**2)
     .              *lamb_hdec(aml**2/amz**2,ama**2/amz**2)**2
               haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/aml*caz*gzal**2
            endif
         endif
      endif
c end MMM changed 22/8/2013

c     print*,'h -> AZ',haz

C  H ---> H+ W+

      IF (AML.LE.AMW+AMCH) THEN
      HHW=0
      ELSE
      CHW=LAMB_HDEC(AMCH**2/AML**2,AMW**2/AML**2)
     .   *LAMB_HDEC(AML**2/AMW**2,AMCH**2/AMW**2)**2
      HHW=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/aml*chw*ghvv**2
      ENDIF

c MMM changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=3d0
            dlu=9d0
            xm1 = amch+amw-dld
            xm2 = amch+amw+dlu
            if (aml.lt.amch) then
               hhw=0.d0
            elseif (aml.lt.xm1) then
               if(aml.le.dabs(amw-amch))then
                  hhw=0
               else
c ---- this is for a numeric check ----
                  iprint = 10

                  ivegas(1) = 30000
                  ivegas(2) = 5
                  ivegas(3) = 120000
                  ivegas(4) = 10

                  amhi = aml
                  amhj = amch
                  amhk = amw
                  gamtoti = 1.D0
                  gamtotj = 1.D0
                  gamtotk = gamw

c ---- initialization of VEGAS ----

c                  call RSTART(12,34,56,78)

c                  call INTEG(hvhinteg,2,iprint,ivegas,result,relative)

c                  print*,'result',result
c ---- end numeric check

                  hhw=9.d0*gf**2/16.d0/pi**3*amw**4*aml*ghvv**2*2
     .                 *hvh((amch/aml)**2,(amw/aml)**2)
               endif
            elseif (aml.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)*2
     .              *hvh((amch/xx(1))**2,(amw/xx(1))**2)
               yy(2)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)*2
     .              *hvh((amch/xx(2))**2,(amw/xx(2))**2)
               chw=lamb_hdec(amch**2/xx(3)**2,amw**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amw**2,amch**2/amw**2)**2
               yy(3)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*chw
               chw=lamb_hdec(amch**2/xx(4)**2,amw**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amw**2,amch**2/amw**2)**2
               yy(4)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*chw
               hhw=fint_hdec(aml,xx,yy)*ghvv**2
            else
               chw=lamb_hdec(amch**2/aml**2,amw**2/aml**2)
     .              *lamb_hdec(aml**2/amw**2,amch**2/amw**2)**2
               hhw=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/aml*chw*ghvv**2
            endif
         else
            if (aml.lt.amw+amch) then
               hhw=0.d0
            else
               chw=lamb_hdec(amch**2/aml**2,amw**2/aml**2)
     .              *lamb_hdec(aml**2/amw**2,amch**2/amw**2)**2
               hhw=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/aml*chw*ghvv**2
            endif
         endif
      endif
c end MMM changed 22/8/2013

c     print*,'h -> H+W- + H-W+',hhw,hhw/2.D0

C  ============================ SUSY DECAYS 
      IF(IOFSUSY.EQ.0) THEN
C
C  HL ----> CHARGINOS
C
      DO 711 I=1,2
      DO 711 J=1,2
      IF (AML.GT.AMCHAR(I)+AMCHAR(J)) THEN
      WHLCH(I,J)=GF*AMW**2/(2*PI*DSQRT(2.D0))/AML 
     .     *LAMB_HDEC(AMCHAR(I)**2/AML**2,AMCHAR(J)**2/AML**2)
     .     *( (AC2(I,J)**2+AC2(J,I)**2)*(AML**2-AMCHAR(I)
     .         **2-AMCHAR(J)**2)-4.D0*AC2(I,J)*AC2(J,I)* 
     .         XMCHAR(I)*XMCHAR(J) ) 
      ELSE
      WHLCH(I,J)=0.D0
      ENDIF
      WHLCHT=WHLCH(1,1)+WHLCH(1,2)+WHLCH(2,1)+WHLCH(2,2)
 711  CONTINUE
C
C  HL ----> NEUTRALINOS 
C
      DO 712 I=1,4
      DO 712 J=1,4
      IF (AML.GT.AMNEUT(I)+AMNEUT(J)) THEN
      WHLNE(I,J)=GF*AMW**2/(2*PI*DSQRT(2.D0))/AML 
     .         *AN2(I,J)**2*(AML**2-(XMNEUT(I)+XMNEUT(J))**2)
     .         *LAMB_HDEC(AMNEUT(I)**2/AML**2,AMNEUT(J)**2/AML**2)
      ELSE 
      WHLNE(I,J)=0.D0
      ENDIF
 712  CONTINUE
      WHLNET= WHLNE(1,1)+WHLNE(1,2)+WHLNE(1,3)+WHLNE(1,4)
     .       +WHLNE(2,1)+WHLNE(2,2)+WHLNE(2,3)+WHLNE(2,4)
     .       +WHLNE(3,1)+WHLNE(3,2)+WHLNE(3,3)+WHLNE(3,4)
     .       +WHLNE(4,1)+WHLNE(4,2)+WHLNE(4,3)+WHLNE(4,4)
CCC
C  HL ----> SLEPTONS 
C
      IF (AML.GT.2.D0*AMSE(1)) THEN
      WHLSLEL=2*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AML*DSIN(B+A)**2
     .      *BETA_HDEC(AMSE(1)**2/AML**2)*(-0.5D0+SS)**2
      ELSE
      WHLSLEL=0.D0
      ENDIF

      IF (AML.GT.2.D0*AMSE(2)) THEN
      WHLSLER=2*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AML*DSIN(B+A)**2
     .      *BETA_HDEC(AMSE(2)**2/AML**2)*SS**2
      ELSE
      WHLSLER=0.D0
      ENDIF

      WHLSLNL=0.D0
      IF (AML.GT.2.D0*AMSN1(1)) THEN
      WHLSLNL=2*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AML*DSIN(B+A)**2
     .      *BETA_HDEC(AMSN1(1)**2/AML**2)*0.5D0**2
      ENDIF
      IF (AML.GT.2.D0*AMSN(1)) THEN
      WHLSLNL=WHLSLNL + GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AML*DSIN(B+A)**2
     .      *BETA_HDEC(AMSN(1)**2/AML**2)*0.5D0**2
      ENDIF

      DO 718 I=1,2
      DO 718 J=1,2
      IF(AML.GT.AMSL(I)+AMSL(J)) THEN
      WHLSTAU(I,J)=GF*AMZ**4/2.D0/DSQRT(2.D0)/PI*GLEE(I,J)**2*
     .      LAMB_HDEC(AMSL(I)**2/AML**2,AMSL(J)**2/AML**2)/AML
      ELSE
      WHLSTAU(I,J)=0.D0
      ENDIF
 718  CONTINUE

      WHLSLT=WHLSTAU(1,1)+WHLSTAU(2,1)+WHLSTAU(1,2)+WHLSTAU(2,2) 
     .       +WHLSLEL+WHLSLER+WHLSLNL
C
C  HL ----> SQUARKS 
C
      IF (AML.GT.2.D0*AMSU(1)) THEN
      WHLSQUL=6*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AML*DSIN(B+A)**2
     .      *BETA_HDEC(AMSU(1)**2/AML**2)*(0.5D0-2.D0/3.D0*SS)**2
      ELSE
      WHLSQUL=0.D0
      ENDIF

      IF (AML.GT.2.D0*AMSU(2)) THEN
      WHLSQUR=6*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AML*DSIN(B+A)**2
     .      *BETA_HDEC(AMSU(2)**2/AML**2)*(-2.D0/3.D0*SS)**2
      ELSE
      WHLSQUR=0.D0
      ENDIF

      IF (AML.GT.2.D0*AMSD(1)) THEN
      WHLSQDL=6*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AML*DSIN(B+A)**2
     .      *BETA_HDEC(AMSD(1)**2/AML**2)*(-0.5D0+1.D0/3.D0*SS)**2
      ELSE
      WHLSQDL=0.D0
      ENDIF

      IF (AML.GT.2.D0*AMSD(2)) THEN
      WHLSQDR=6*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AML*DSIN(B+A)**2
     .      *BETA_HDEC(AMSD(2)**2/AML**2)*(+1.D0/3.D0*SS)**2
      ELSE
      WHLSQDR=0.D0
      ENDIF

      WHLSQ=WHLSQUL+WHLSQUR+WHLSQDL+WHLSQDR
      
C
C  HL ----> STOPS 
      SUSY = 1
      DO 713 I=1,2
      DO 713 J=1,2
c     QSQ = (YMST(I)+YMST(J))/2
      QSQ = AML
      SUSY = 1
      IF(AML.GT.YMST(I)+YMST(J)) THEN
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CALL SQMBAPP_HDEC(QSQ)
       SUSY = 1+SQSUSY_HDEC(1,1,I,J,QSQ,0,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       WHLST(I,J)=3*GF*AMZ**4/2.D0/DSQRT(2.D0)/PI*YLTT(I,J)**2*
     .      LAMB_HDEC(YMST(I)**2/AML**2,YMST(J)**2/AML**2)/AML
     .          *SUSY
c      write(6,*)'h -> stop: ',I,J,AML,YMST(I),YMST(J),SUSY-1,
c    .           WHLST(I,J)/SUSY,WHLST(I,J)
c      write(6,*)'h -> stop: ',I,J,AML,YMST(I),YMST(J),SUSY-1
      ELSE
      WHLST(I,J)=0.D0
      ENDIF
 713  CONTINUE
C
C  HL ----> SBOTTOMS 
      SUSY = 1
      DO 714 I=1,2
      DO 714 J=1,2
c     QSQ = (YMSB(I)+YMSB(J))/2
      QSQ = AML
      IF(AML.GT.YMSB(I)+YMSB(J)) THEN
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CALL SQMBAPP_HDEC(QSQ)
       SUSY = 1+SQSUSY_HDEC(1,2,I,J,QSQ,0,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       WHLSB(I,J)=3*GF*AMZ**4/2.D0/DSQRT(2.D0)/PI*YLBB(I,J)**2*
     .       LAMB_HDEC(YMSB(I)**2/AML**2,YMSB(J)**2/AML**2)/AML
     .      *SUSY
c      write(6,*)'h -> sbot: ',I,J,AML,YMSB(I),YMSB(J),SUSY-1,
c    .           WHLSB(I,J)/SUSY,WHLSB(I,J)
c      write(6,*)'h -> sbot: ',I,J,AML,YMSB(I),YMSB(J),SUSY-1
      ELSE
      WHLSB(I,J)=0.D0
      ENDIF
 714  CONTINUE
C
      WHLSTT=WHLST(1,1)+WHLST(1,2)+WHLST(2,1)+WHLST(2,2) 
      WHLSBB=WHLSB(1,1)+WHLSB(1,2)+WHLSB(2,1)+WHLSB(2,2) 
      WHLSQT=WHLSTT+WHLSBB+WHLSQ

      ELSE 
      WHLCHT=0.D0
      WHLNET=0.D0
      WHLSLT=0.D0
      WHLSQT=0.D0
C--Change thanks to Elzbieta Richter-Was
      DO I=1,2
       DO J=1,2
        WHLCH(I,J)=0.D0
        WHLST(I,J)=0.D0
        WHLSB(I,J)=0.D0
        WHLSTAU(I,J)=0.D0
       ENDDO
      ENDDO
      DO I=1,4
       DO J=1,4
        WHLNE(I,J)=0.D0
       ENDDO
      ENDDO
      ENDIF

      IF(IGOLD.NE.0)THEN
C   HL ---> GOLDSTINOS
       DO 710 I=1,4
       IF (AML.GT.AMNEUT(I)) THEN
        WHLGD(I)=AML**5/AXMPL**2/AXMGD**2/48.D0/PI*
     .           (1.D0-AMNEUT(I)**2/AML**2)**4*AGDL(I)**2
       ELSE
        WHLGD(I)=0.D0
       ENDIF
 710   CONTINUE
       WHLGDT=WHLGD(1)+WHLGD(2)+WHLGD(3)+WHLGD(4)
      ELSE
       WHLGDT=0
      ENDIF

C    ==========  TOTAL WIDTH AND BRANCHING RATIOS 
      WTOT=HLL+HMM+HSS+HCC+HBB+HTT+HGG+HGA+HZGA+HWW+HZZ+HAA+HAZ+HHW
     .    +WHLCHT+WHLNET+WHLSLT+WHLSQT + WHLGDT

c     write(6,*)'h:',WTOT,HLL,HMM,HSS,HCC,HBB,HTT,HGG,HGA,HZGA,HWW,HZZ
c    .   ,HAA,HAZ,HHW,WHLCHT,WHLNET,WHLSLT,WHLSQT , WHLGDT
c     write(6,*)'h:',HGA

      wtot = wtot + hlchch
      hlbrchch=hlchch/wtot

c     print*,'wtot',wtot

      HLBRT=HTT/WTOT
      HLBRB=HBB/WTOT
      HLBRL=HLL/WTOT
      HLBRM=HMM/WTOT
      HLBRS=HSS/WTOT
      HLBRC=HCC/WTOT
      HLBRG=HGG/WTOT
      HLBRGA=HGA/WTOT
      HLBRZGA=HZGA/WTOT
      HLBRW=HWW/WTOT
      HLBRZ=HZZ/WTOT
      HLBRA=HAA/WTOT
      HLBRAZ=HAZ/WTOT
      HLBRHW=HHW/WTOT
      DO 811 I=1,2
      DO 811 J=1,2
      HLBRSC(I,J)=WHLCH(I,J)/WTOT
811   CONTINUE
      DO 812 I=1,4
      DO 812 J=1,4
      HLBRSN(I,J)=WHLNE(I,J)/WTOT
812   CONTINUE
      HLBRCHT=WHLCHT/WTOT 
      HLBRNET=WHLNET/WTOT 
      HLBRSL=WHLSLT/WTOT 
      HLBRSQ=WHLSQ/WTOT 
      HLBRSQT=WHLSQT/WTOT 
      HLBRGD =WHLGDT/WTOT
      HLWDTH=WTOT

      BHLSLNL = WHLSLNL/WTOT
      BHLSLEL = WHLSLEL/WTOT
      BHLSLER = WHLSLER/WTOT
      BHLSQUL = WHLSQUL/WTOT
      BHLSQUR = WHLSQUR/WTOT
      BHLSQDL = WHLSQDL/WTOT
      BHLSQDR = WHLSQDR/WTOT
      DO I = 1,2
       DO J = 1,2
        BHLST(I,J) = WHLST(I,J)/WTOT
        BHLSB(I,J) = WHLSB(I,J)/WTOT
        BHLSTAU(I,J) = WHLSTAU( I,J)/WTOT
       ENDDO
      ENDDO

      ENDIF

      IF(IHIGGS.GT.1)THEN
      

C        =========================================================
C                       CHARGED HIGGS DECAYS
C        =========================================================
      TB=TGBET
C     =============  RUNNING MASSES 
      RMS = RUNM_HDEC(AMCH,3)
      RMC = RUNM_HDEC(AMCH,4)
      RMB = RUNM_HDEC(AMCH,5)
      RMT = RUNM_HDEC(AMCH,6)
      ASH=ALPHAS_HDEC(AMCH,3)
C     =============== PARTIAL WIDTHS 
C  H+ ---> MU NMU
      XGAM = GAB
      if(i2hdm.eq.1) then
         xgam = galep
      endif
      IF(IOFSUSY.EQ.0) THEN
       CALL STAUSUSY_HDEC(GLB,GHB,GAB,XGLM,XGHM,XGAM,QSUSY,0)
      ENDIF
      IF(AMCH.LE.AMMUON) THEN
       HMN = 0
      ELSE
      HMN=CFF(AMCH,XGAM,(AMMUON/AMCH)**2,0.D0)
      ENDIF

c     print*,''
c     print*,'H+ decay widths'
c     print*,'H+ -> nu mu',hmn

C  H+ ---> TAU NTAU
      XGAT = GAB
      if(i2hdm.eq.1) then
         xgat = galep
      endif
      IF(IOFSUSY.EQ.0) THEN
       CALL STAUSUSY_HDEC(GLB,GHB,GAB,XGLT,XGHT,XGAT,QSUSY,1)
      ENDIF
      IF(AMCH.LE.AMTAU) THEN
       HLN = 0
      ELSE
      HLN=CFF(AMCH,XGAT,(AMTAU/AMCH)**2,0.D0)
      ENDIF

c     print*,'H+ -> tau ntau',hln

C  H+ --> SU
      EPS = 1.D-12
      RATX = 1
      SSUSY = (AMSD(1)+AMSD(2)+AMGLU)/3*QSUSY
C     SSUSY = (AMSD(1)+AMSD(2))/2*QSUSY
      IF(IOFSUSY.EQ.0) THEN
       CALL STRSUSY_HDEC(GLB,GHB,GAB,XGLS,XGHS,XGAS,SSUSY,LOOP)
       RATX = XGAS/GAB
      ENDIF
      IF(AMCH.LE.AMS+EPS) THEN
       HSU = 0
      ELSE
       HSU1=3.D0*VUS**2*CQCDM(AMCH,TB,(AMS/AMCH)**2,EPS,RATX)
       HSU2=3.D0*VUS**2*CQCD(AMCH,TB,(RMS/AMCH)**2,EPS,RATX)
c MMM changed 21/8/13
       if(i2hdm.eq.1) then
          hsu1=3.d0*vus**2*
     .         cqcdm2hdm(amch,gab,gat,(ams/amch)**2,eps,ratx)
          hsu2=3.d0*vus**2*cqcd2hdm(amch,gab,gat,(rms/amch)**2,eps,ratx)
       endif
c end MMM changed 21/8/13
       IF(HSU2.LT.0.D0) HSU2 = 0
       RAT = AMS/AMCH
       HSU = QQINT_HDEC(RAT,HSU1,HSU2)
      ENDIF

c     print*,'H+ -> su',hsu,3.d0*vus**2*
c    .         cqcdm2hdm(amch,gab,gat,(ams/amch)**2,eps,ratx)

C  H+ --> CS
      RATX = RMS/AMS
      RATY = 1
      SSUSY = (AMSD(1)+AMSD(2)+AMGLU)/3*QSUSY
C     SSUSY = (AMSD(1)+AMSD(2))/2*QSUSY
      IF(IOFSUSY.EQ.0) THEN
       CALL STRSUSY_HDEC(GLB,GHB,GAB,XGLS,XGHS,XGAS,SSUSY,LOOP)
       RATX = RMS/AMS*XGAS/GAB
       RATY = XGAS/GAB
      ENDIF
      IF(AMCH.LE.AMS+AMC) THEN
       HSC = 0
      ELSE
       HSC1=3.D0*CQCDM(AMCH,TB,(AMS/AMCH)**2,(AMC/AMCH)**2,RATX)*VCS**2
       HSC2=3.D0*CQCD(AMCH,TB,(RMS/AMCH)**2,(RMC/AMCH)**2,RATY)*VCS**2
c MMM changed 21/8/13
       if(i2hdm.eq.1) then
          hsc1=3.d0*VCS**2*
     .         cqcdm2hdm(amch,gab,gat,(ams/amch)**2,(amc/amch)**2,ratx)
          hsc2=3.d0*VCS**2*
     .         cqcd2hdm(amch,gab,gat,(rms/amch)**2,(rmc/amch)**2,raty)
c         print*,'rms,rmc',hsc1,hsc2,rms,rmc
       endif
c end MMM changed 21/8/13
       IF(HSC2.LT.0.D0) HSC2 = 0
       RAT = (AMS+AMC)/AMCH
       HSC = QQINT_HDEC(RAT,HSC1,HSC2)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      HSC1=3.D0*CQCDM(AMCH,TB,(AMS/AMCH)**2,(AMC/AMCH)**2,1.D0)
c      HSC2=3.D0*CQCD(AMCH,TB,(RMS/AMCH)**2,(RMC/AMCH)**2,1.D0)
c      IF(HSC2.LT.0.D0) HSC2 = 0
c      RAT = (AMS+AMC)/AMCH
c      HSC0 = QQINT_HDEC(RAT,HSC1,HSC2)
c      write(6,*)'H+- --> cs: ',AMCH,HSC,HSC0,HSC/HSC0,RATX**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      ENDIF
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(53,*)AMCH,HSC
c     write(6,*)'H+ -> cs: ',AMCH,HSC,QSUSY,LOOP,RATX,RATY,SSUSY
c     write(6,*)AMCH,HSC,gab,xgas
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     print*,'H+ -> cs',hsc

c Maggie added 16/10/2013
C  H+ --> CD
      RATX = 1
      EPS = 1D-12
      IF(AMCH.LE.AMC) THEN
       HCD = 0
      ELSE
       HCD1=3.D0*CQCDM(AMCH,TB,EPS,(AMC/AMCH)**2,RATX)*VCD**2
       HCD2=3.D0*CQCD(AMCH,TB,EPS,(RMC/AMCH)**2,RATX)*VCD**2
       if(i2hdm.eq.1) then
          hCD1=3.d0*vcd**2*
     .         cqcdm2hdm(amch,gab,gat,eps,(amc/amch)**2,ratx)
          hCD2=3.d0*vcd**2*
     .         cqcd2hdm(amch,gab,gat,eps,(rmc/amch)**2,ratx)
       endif
       IF(HCD2.LT.0.D0) HCD2 = 0
       RAT = (EPS+AMC)/AMCH
       HCD = QQINT_HDEC(RAT,HCD1,HCD2)
      ENDIF

c     print*,'H+ -> cd',hcd
c end Maggie added 16/10/2013

C  H+ --> CB
      RATX = 1
      QQ = AMB
      SUSY = 0
      XGAB = GAB
c     SSUSY = AMCH
      SSUSY = (AMSB(1)+AMSB(2)+AMGLU)/3*QSUSY
C     SSUSY = (AMSB(1)+AMSB(2))/2*QSUSY
      AS0 = ALPHAS_HDEC(SSUSY,3)
      IF(IOFSUSY.EQ.0) THEN
       I0 = 1
c      write(6,*)
c      write(6,*)'H+ -> cb: ',amch
c      write(6,*)
       CALL DMBAPP_HDEC(I0,DGLB,DGHB,DGAB,SSUSY,LOOP)
       I0 = 1
       BSC = (AMSQ+AMUR+AMDR)/3
c      XMB = RUNM_HDEC(BSC,5,0)
       XMB = AMB
c      SUSY = COFSUSY_HDEC(I0,AMB,XMB,QQ)*AS0/PI - 2*DGLB
c      write(6,*)
c      write(6,*)'H+ -> cb: ',amch
c      write(6,*)
       CALL BOTSUSY_HDEC(GLB,GHB,GAB,XGLB,XGHB,XGAB,SSUSY,LOOP)
      ENDIF
      RATX = XGAB/GAB
c     write(6,*)'ratio = ',ratx
      IF(AMCH.LE.AMB+AMC) THEN
       HBC = 0
      ELSE
       HBC1=3.D0*VCB**2*CQCDM(AMCH,TB,(AMB/AMCH)**2,(AMC/AMCH)**2,RATX)
       HBC2=3.D0*VCB**2*CQCD(AMCH,TB,(RMB/AMCH)**2,(RMC/AMCH)**2,RATX)
c MMM changed 21/8/13
       if(i2hdm.eq.1) then
          hbc1=3.d0*vcb**2*
     .         cqcdm2hdm(amch,gab,gat,(amb/amch)**2,(amc/amch)**2,ratx)
          hbc2=3.d0*vcb**2*
     .         cqcd2hdm(amch,gab,gat,(rmb/amch)**2,(rmc/amch)**2,ratx)
       endif
c end MMM changed 21/8/13
       IF(HBC2.LT.0.D0) HBC2 = 0
       RAT = (AMB+AMC)/AMCH
       HBC = QQINT_HDEC(RAT,HBC1,HBC2)
      ENDIF

c     print*,'H+ -> cb',hbc

C  H+ --> BU
      EPS = 1.D-12
      IF(AMCH.LE.AMB+EPS) THEN
       HBU = 0
      ELSE
       HBU1=3.D0*VUB**2*CQCDM(AMCH,TB,(AMB/AMCH)**2,EPS,RATX)
       HBU2=3.D0*VUB**2*CQCD(AMCH,TB,(RMB/AMCH)**2,EPS,RATX)
c MMM changed 21/8/13
       if(i2hdm.eq.1) then
          hbu1=3.d0*vub**2*
     .         cqcdm2hdm(amch,gab,gat,(amb/amch)**2,eps,ratx)
          hbu2=3.d0*vub**2*
     .         cqcd2hdm(amch,gab,gat,(rmb/amch)**2,eps,ratx)
       endif
c end MMM changed 21/8/13
       IF(HBU2.LT.0.D0) HBU2 = 0
       RAT = AMB/AMCH
       HBU = QQINT_HDEC(RAT,HBU1,HBU2)
      ENDIF

c     print*,'H+ -> ub',hbu

C  H+ --> TD :
      EPS = 1.D-12
      IF(IONSH.EQ.0)THEN
       DLD=2D0
       DLU=2D0
       XM1 = AMT-DLD
       XM2 = AMT+DLU
       IF (AMCH.LE.AMW) THEN
        HDT=0.D0
       ELSEIF (AMCH.LE.XM1) THEN
        FACTB=3.D0*GF**2*AMCH*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(AMCH,AMT,EPS,AMW,i2hdm,gat,gab,CTT0)
        HDT=VTD**2*FACTB*CTT0
       ELSEIF (AMCH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        FACTB=3.D0*GF**2*XX(1)*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(XX(1),AMT,EPS,AMW,i2hdm,gat,gab,CTT0)
        YY(1)=VTD**2*FACTB*CTT0
        FACTB=3.D0*GF**2*XX(2)*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(XX(2),AMT,EPS,AMW,i2hdm,gat,gab,CTT0)
        YY(2)=VTD**2*FACTB*CTT0
        XMB = RUNM_HDEC(XX(3),5)
        XMT = RUNM_HDEC(XX(3),6)
        XYZ2 = 3.D0*CQCD(XX(3),TB,(EPS/XX(3))**2,(XMT/XX(3))**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           XYZ2 = 3.D0*
     .       CQCD2HDM(XX(3),gab,gat,(EPS/XX(3))**2,(XMT/XX(3))**2,RATX)
        endif
c end MMM changed 21/8/13
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        XYZ1 = 3.D0*CQCDM(XX(3),TB,(EPS/XX(3))**2,(AMT/XX(3))**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
        XYZ1 = 3.D0*
     .      CQCDM2HDM(XX(3),gab,gat,(EPS/XX(3))**2,(AMT/XX(3))**2,RATX)
      endif
c end MMM changed 21/8/13
        RAT = (AMT)/XX(3)
        YY(3) = VTD**2*QQINT_HDEC(RAT,XYZ1,XYZ2)
        XMB = RUNM_HDEC(XX(4),5)
        XMT = RUNM_HDEC(XX(4),6)
        XYZ2 = 3.D0*CQCD(XX(4),TB,(EPS/XX(4))**2,(XMT/XX(4))**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           XYZ2 = 3.D0*
     .       CQCD2HDM(XX(4),gab,gat,(EPS/XX(4))**2,(XMT/XX(4))**2,RATX)
        endif
c end MMM changed 21/8/13
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        XYZ1 = 3.D0*CQCDM(XX(4),TB,(EPS/XX(4))**2,(AMT/XX(4))**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
        XYZ1 = 3.D0*
     .      CQCDM2HDM(XX(4),gab,gat,(EPS/XX(4))**2,(AMT/XX(4))**2,RATX)
      endif
c end MMM changed 21/8/13
        RAT = (AMT)/XX(4)
        YY(4) = VTD**2*QQINT_HDEC(RAT,XYZ1,XYZ2)
        HDT = FINT_HDEC(AMCH,XX,YY)
       ELSE
        HDT2=3.D0*VTD**2*CQCD(AMCH,TB,(EPS/AMCH)**2,(RMT/AMCH)**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           HDT2=3.D0*VTD**2*
     .          CQCD2HDM(AMCH,gab,gat,(EPS/AMCH)**2,(RMT/AMCH)**2,RATX)
        endif
c end MMM changed 21/8/13
        IF(HDT2.LT.0.D0) HDT2 = 0
        HDT1=3.D0*VTD**2*CQCDM(AMCH,TB,(EPS/AMCH)**2,(AMT/AMCH)**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           HDT1=3.D0*VTD**2*
     .          CQCDM2HDM(AMCH,gab,gat,(EPS/AMCH)**2,(AMT/AMCH)**2,RATX)
      endif
c end MMM changed 21/8/13
        RAT = (AMT)/AMCH
        HDT = QQINT_HDEC(RAT,HDT1,HDT2)
       ENDIF
      ELSE
       IF (AMCH.LE.AMT) THEN
        HDT=0.D0
       ELSE
        HDT2=3.D0*VTD**2*CQCD(AMCH,TB,EPS,(RMT/AMCH)**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           HDT2=3.D0*VTD**2*
     .          CQCD2HDM(AMCH,gab,gat,EPS,(RMT/AMCH)**2,RATX)
        endif
c end MMM changed 21/8/13
        IF(HDT2.LT.0.D0) HDT2 = 0
        HDT1=3.D0*VTD**2*CQCDM(AMCH,TB,EPS,(AMT/AMCH)**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           HDT1=3.D0*VTD**2*
     .          CQCDM2HDM(AMCH,gab,gat,EPS,(AMT/AMCH)**2,RATX)
      endif
c end MMM changed 21/8/13
        RAT = (AMT)/AMCH
        HDT = QQINT_HDEC(RAT,HDT1,HDT2)
       ENDIF
      ENDIF

c     print*,'H+ -> td',hdt

C  H+ --> TS :
      EPS = 1.D-12
      RATX = RMS/AMS
      RATY = 1
      XGAS = GAB
      SSUSY = (AMSD(1)+AMSD(2)+AMGLU)/3*QSUSY
C     SSUSY = (AMSD(1)+AMSD(2))/2*QSUSY
      IF(IOFSUSY.EQ.0) THEN
       CALL STRSUSY_HDEC(GLB,GHB,GAB,XGLS,XGHS,XGAS,SSUSY,LOOP)
       RATX = RMS/AMS*XGAS/GAB
       RATY = XGAS/GAB
      ENDIF
      IF(IONSH.EQ.0)THEN
       DLD=2D0
       DLU=2D0
       XM1 = AMT+AMS-DLD
       XM2 = AMT+AMS+DLU
       IF (AMCH.LE.AMW+2*AMS) THEN
        HST=0.D0
       ELSEIF (AMCH.LE.XM1) THEN
        FACTB=3.D0*GF**2*AMCH*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(AMCH,AMT,EPS,AMW,i2hdm,gat,xgas,CTT0)
c       CALL CTOTT_HDEC(AMCH,AMT,EPS,AMW,i2hdm,gat,gab,CTT1)
c       write(6,*)'H+ -> ts: ',amch,ctt0/ctt1
        HST=VTS**2*FACTB*CTT0
       ELSEIF (AMCH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        FACTB=3.D0*GF**2*XX(1)*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(XX(1),AMT,EPS,AMW,i2hdm,gat,xgas,CTT0)
        YY(1)=VTS**2*FACTB*CTT0
        FACTB=3.D0*GF**2*XX(2)*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(XX(2),AMT,EPS,AMW,i2hdm,gat,xgas,CTT0)
        YY(2)=VTS**2*FACTB*CTT0
        XMS = RUNM_HDEC(XX(3),3)
        XMT = RUNM_HDEC(XX(3),6)
        XYZ2 = 3.D0*CQCD(XX(3),TB,(XMS/XX(3))**2,(XMT/XX(3))**2,RATY)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           XYZ2 = 3.D0*
     .       CQCD2HDM(XX(3),gab,gat,(XMS/XX(3))**2,(XMT/XX(3))**2,RATY)
        endif
c end MMM changed 21/8/13
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        XYZ1 = 3.D0*CQCDM(XX(3),TB,(AMS/XX(3))**2,(AMT/XX(3))**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
        XYZ1 = 3.D0*
     .      CQCDM2HDM(XX(3),gab,gat,(AMS/XX(3))**2,(AMT/XX(3))**2,RATX)
      endif
c end MMM changed 21/8/13
        RAT = (AMS+AMT)/XX(3)
        YY(3) = VTS**2*QQINT_HDEC(RAT,XYZ1,XYZ2)
        XMS = RUNM_HDEC(XX(4),3)
        XMT = RUNM_HDEC(XX(4),6)
        XYZ2 = 3.D0*CQCD(XX(4),TB,(XMS/XX(4))**2,(XMT/XX(4))**2,RATY)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           XYZ2 = 3.D0*
     .       CQCD2HDM(XX(4),gab,gat,(XMS/XX(4))**2,(XMT/XX(4))**2,RATY)
        endif
c end MMM changed 21/8/13
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        XYZ1 = 3.D0*CQCDM(XX(4),TB,(AMS/XX(4))**2,(AMT/XX(4))**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
        XYZ1 = 3.D0*
     .      CQCDM2HDM(XX(4),gab,gat,(AMS/XX(4))**2,(AMT/XX(4))**2,RATX)
      endif
c end MMM changed 21/8/13
        RAT = (AMS+AMT)/XX(4)
        YY(4) = VTS**2*QQINT_HDEC(RAT,XYZ1,XYZ2)
        HST = FINT_HDEC(AMCH,XX,YY)
       ELSE
        HST2=3.D0*VTS**2*CQCD(AMCH,TB,(RMS/AMCH)**2,(RMT/AMCH)**2,RATY)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           HST2=3.D0*VTS**2*
     .          CQCD2HDM(AMCH,gab,gat,(RMS/AMCH)**2,(RMT/AMCH)**2,RATY)
c          print*,'rms,rmt',rms,rmt
        endif
c end MMM changed 21/8/13
        IF(HST2.LT.0.D0) HST2 = 0
        HST1=3.D0*VTS**2*CQCDM(AMCH,TB,(AMS/AMCH)**2,(AMT/AMCH)**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           HST1=3.D0*VTS**2*
     .          CQCDM2HDM(AMCH,gab,gat,(AMS/AMCH)**2,(AMT/AMCH)**2,RATX)
      endif
c end MMM changed 21/8/13
        RAT = (AMS+AMT)/AMCH
        HST = QQINT_HDEC(RAT,HST1,HST2)
       ENDIF
      ELSE
       IF (AMCH.LE.AMT+AMS) THEN
        HST=0.D0
       ELSE
        HST2=3.D0*VTS**2*CQCD(AMCH,TB,(RMS/AMCH)**2,(RMT/AMCH)**2,RATY)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           HST2=3.D0*VTS**2*
     .          CQCD2HDM(AMCH,gab,gat,(RMS/AMCH)**2,(RMT/AMCH)**2,RATY)
        endif
c end MMM changed 21/8/13
        IF(HST2.LT.0.D0) HST2 = 0
        HST1=3.D0*VTS**2*CQCDM(AMCH,TB,(AMS/AMCH)**2,(AMT/AMCH)**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           HST1=3.D0*VTS**2*
     .          CQCDM2HDM(AMCH,gab,gat,(AMS/AMCH)**2,(AMT/AMCH)**2,RATX)
      endif
c end MMM changed 21/8/13
        RAT = (AMS+AMT)/AMCH
        HST = QQINT_HDEC(RAT,HST1,HST2)
       ENDIF
      ENDIF

c     print*,'H+ -> ts',hst

C  H+ --> TB :
      RATX = RMB/AMB
      RATY = 1
      XGAB = GAB
      SSUSY = (AMSB(1)+AMSB(2)+AMGLU)/3*QSUSY
C     SSUSY = (AMSB(1)+AMSB(2))/3*QSUSY
      IF(IOFSUSY.EQ.0) THEN
       CALL BOTSUSY_HDEC(GLB,GHB,GAB,XGLB,XGHB,XGAB,SSUSY,LOOP)
       RATX = RMB/AMB*XGAB/GAB
       RATY = XGAB/GAB
      ENDIF
      IF(IONSH.EQ.0)THEN
       DLD=2D0
       DLU=2D0
       XM1 = AMT+AMB-DLD
       XM2 = AMT+AMB+DLU
       IF (AMCH.LE.AMW+2*AMB) THEN
        HBT=0.D0
       ELSEIF (AMCH.LE.XM1) THEN
        FACTB=3.D0*GF**2*AMCH*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(AMCH,AMT,AMB,AMW,i2hdm,gat,xgab,CTT0)
c       CALL CTOTT_HDEC(AMCH,AMT,AMB,AMW,i2hdm,gat,gab,CTT1)
c       write(6,*)'H+ -> tb: ',amch,ctt0/ctt1
        HBT=VTB**2*FACTB*CTT0
       ELSEIF (AMCH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        FACTB=3.D0*GF**2*XX(1)*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(XX(1),AMT,AMB,AMW,i2hdm,gat,xgab,CTT0)
        YY(1)=VTB**2*FACTB*CTT0
        FACTB=3.D0*GF**2*XX(2)*AMT**4/32.D0/PI**3*gat**2
        CALL CTOTT_HDEC(XX(2),AMT,AMB,AMW,i2hdm,gat,xgab,CTT0)
        YY(2)=VTB**2*FACTB*CTT0
        XMB = RUNM_HDEC(XX(3),5)
        XMT = RUNM_HDEC(XX(3),6)
        XYZ2 = 3.D0*CQCD(XX(3),TB,(XMB/XX(3))**2,(XMT/XX(3))**2,RATY)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           XYZ2 = 3.D0*
     .       CQCD2HDM(XX(3),gab,gat,(XMB/XX(3))**2,(XMT/XX(3))**2,RATY)
        endif
c end MMM changed 21/8/13
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        XYZ1 = 3.D0*CQCDM(XX(3),TB,(AMB/XX(3))**2,(AMT/XX(3))**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
        XYZ1 = 3.D0*
     .      CQCDM2HDM(XX(3),gab,gat,(AMB/XX(3))**2,(AMT/XX(3))**2,RATX)
      endif
c end MMM changed 21/8/13
        RAT = (AMB+AMT)/XX(3)
        YY(3) = VTB**2*QQINT_HDEC(RAT,XYZ1,XYZ2)
        XMB = RUNM_HDEC(XX(4),5)
        XMT = RUNM_HDEC(XX(4),6)
        XYZ2 = 3.D0*CQCD(XX(4),TB,(XMB/XX(4))**2,(XMT/XX(4))**2,RATY)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           XYZ2 = 3.D0*
     .       CQCD2HDM(XX(4),gab,gat,(XMB/XX(4))**2,(XMT/XX(4))**2,RATY)
        endif
c end MMM changed 21/8/13
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        XYZ1 = 3.D0*CQCDM(XX(4),TB,(AMB/XX(4))**2,(AMT/XX(4))**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
        XYZ1 = 3.D0*
     .      CQCDM2HDM(XX(4),gab,gat,(AMB/XX(4))**2,(AMT/XX(4))**2,RATX)
      endif
c end MMM changed 21/8/13
        RAT = (AMB+AMT)/XX(4)
        YY(4) = VTB**2*QQINT_HDEC(RAT,XYZ1,XYZ2)
        HBT = FINT_HDEC(AMCH,XX,YY)
       ELSE
        HBT2=3.D0*VTB**2*CQCD(AMCH,TB,(RMB/AMCH)**2,(RMT/AMCH)**2,RATY)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           HBT2=3.D0*VTB**2*
     .          CQCD2HDM(AMCH,gab,gat,(RMB/AMCH)**2,(RMT/AMCH)**2,RATY)
c          print*,'rmb,rmt',rmb,rmt
        endif
c end MMM changed 21/8/13
        IF(HBT2.LT.0.D0) HBT2 = 0
        HBT1=3.D0*VTB**2*CQCDM(AMCH,TB,(AMB/AMCH)**2,(AMT/AMCH)**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           HBT1=3.D0*VTB**2*
     .          CQCDM2HDM(AMCH,gab,gat,(AMB/AMCH)**2,(AMT/AMCH)**2,RATX)
      endif
c end MMM changed 21/8/13
        RAT = (AMB+AMT)/AMCH
        HBT = QQINT_HDEC(RAT,HBT1,HBT2)
       ENDIF
      ELSE
       IF (AMCH.LE.AMT+AMB) THEN
        HBT=0.D0
       ELSE
        HBT2=3.D0*VTB**2*CQCD(AMCH,TB,(RMB/AMCH)**2,(RMT/AMCH)**2,RATY)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           HBT2=3.D0*VTB**2*
     .          CQCD2HDM(AMCH,gab,gat,(RMB/AMCH)**2,(RMT/AMCH)**2,RATY)
        endif
c end MMM changed 21/8/13
        IF(HBT2.LT.0.D0) HBT2 = 0
        HBT1=3.D0*VTB**2*CQCDM(AMCH,TB,(AMB/AMCH)**2,(AMT/AMCH)**2,RATX)
c MMM changed 21/8/13
        if(i2hdm.eq.1) then
           HBT1=3.D0*VTB**2*
     .          CQCDM2HDM(AMCH,gab,gat,(AMB/AMCH)**2,(AMT/AMCH)**2,RATX)
      endif
c end MMM changed 21/8/13
        RAT = (AMB+AMT)/AMCH
        HBT = QQINT_HDEC(RAT,HBT1,HBT2)
       ENDIF
      ENDIF

c     print*,'H+ -> tb',hbt

c  H+ ---> W h
      IF(IONSH.EQ.0)THEN
       DLD=3D0
       DLU=5D0
       XM1 = AMW+AML-DLD
       XM2 = AMW+AML+DLU
       IF (AMCH.LT.AML) THEN
        HWH=0
       ELSEIF (AMCH.LE.XM1) THEN
        IF(AMCH.LE.DABS(AMW-AML))THEN
         HWH=0
        ELSE
         HWH=9.D0*GF**2/16.D0/PI**3*AMW**4*AMCH*GHVV**2
     .      *HVH((AML/AMCH)**2,(AMW/AMCH)**2)
        ENDIF
       ELSEIF (AMCH.LT.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        YY(1) = 9.D0*GF**2/16.D0/PI**3*AMW**4*XX(1)
     .         *HVH((AML/XX(1))**2,(AMW/XX(1))**2)
        YY(2) = 9.D0*GF**2/16.D0/PI**3*AMW**4*XX(2)
     .         *HVH((AML/XX(2))**2,(AMW/XX(2))**2)
        CWH=LAMB_HDEC(AML**2/XX(3)**2,AMW**2/XX(3)**2)
     .     *LAMB_HDEC(XX(3)**2/AMW**2,AML**2/AMW**2)**2
        YY(3)=GF/8.D0/DSQRT(2D0)/PI*AMW**4/XX(3)*CWH
        CWH=LAMB_HDEC(AML**2/XX(4)**2,AMW**2/XX(4)**2)
     .     *LAMB_HDEC(XX(4)**2/AMW**2,AML**2/AMW**2)**2
        YY(4)=GF/8.D0/DSQRT(2D0)/PI*AMW**4/XX(4)*CWH
        HWH = FINT_HDEC(AMCH,XX,YY)*GHVV**2
       ELSE
        CWH=LAMB_HDEC(AML**2/AMCH**2,AMW**2/AMCH**2)
     .     *LAMB_HDEC(AMCH**2/AMW**2,AML**2/AMW**2)**2
        HWH=GF/8.D0/DSQRT(2D0)/PI*AMW**4/AMCH*GHVV**2*CWH
       ENDIF
      ELSE
       IF (AMCH.LT.AMW+AML) THEN
        HWH=0
       ELSE
        CWH=LAMB_HDEC(AML**2/AMCH**2,AMW**2/AMCH**2)
     .     *LAMB_HDEC(AMCH**2/AMW**2,AML**2/AMW**2)**2
        HWH=GF/8.D0/DSQRT(2D0)/PI*AMW**4/AMCH*GHVV**2*CWH
       ENDIF
      ENDIF

c     print*,'H+ -> W+ h',hwh

c MMM changed 21/8/2013
c  H+ ---> W H
      if(i2hdm.eq.1) then
      IF(IONSH.EQ.0)THEN
       DLD=3D0
       DLU=5D0
       XM1 = AMW+AMH-DLD
       XM2 = AMW+AMH+DLU
       IF (AMCH.LT.AMH) THEN
        HWHH=0
       ELSEIF (AMCH.LE.XM1) THEN
        IF(AMCH.LE.DABS(AMW-AMH))THEN
         HWHH=0
        ELSE
         HWHH=9.D0*GF**2/16.D0/PI**3*AMW**4*AMCH*GLVV**2
     .      *HVH((AMH/AMCH)**2,(AMW/AMCH)**2)
        ENDIF
       ELSEIF (AMCH.LT.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        YY(1) = 9.D0*GF**2/16.D0/PI**3*AMW**4*XX(1)
     .         *HVH((AMH/XX(1))**2,(AMW/XX(1))**2)
        YY(2) = 9.D0*GF**2/16.D0/PI**3*AMW**4*XX(2)
     .         *HVH((AMH/XX(2))**2,(AMW/XX(2))**2)
        CWH=LAMB_HDEC(AMH**2/XX(3)**2,AMW**2/XX(3)**2)
     .     *LAMB_HDEC(XX(3)**2/AMW**2,AMH**2/AMW**2)**2
        YY(3)=GF/8.D0/DSQRT(2D0)/PI*AMW**4/XX(3)*CWH
        CWH=LAMB_HDEC(AMH**2/XX(4)**2,AMW**2/XX(4)**2)
     .     *LAMB_HDEC(XX(4)**2/AMW**2,AMH**2/AMW**2)**2
        YY(4)=GF/8.D0/DSQRT(2D0)/PI*AMW**4/XX(4)*CWH
        HWHH = FINT_HDEC(AMCH,XX,YY)*GLVV**2
       ELSE
        CWH=LAMB_HDEC(AMH**2/AMCH**2,AMW**2/AMCH**2)
     .     *LAMB_HDEC(AMCH**2/AMW**2,AMH**2/AMW**2)**2
        HWHH=GF/8.D0/DSQRT(2D0)/PI*AMW**4/AMCH*GLVV**2*CWH
       ENDIF
      ELSE
       IF (AMCH.LT.AMW+AMH) THEN
        HWHH=0
       ELSE
        CWH=LAMB_HDEC(AMH**2/AMCH**2,AMW**2/AMCH**2)
     .     *LAMB_HDEC(AMCH**2/AMW**2,AMH**2/AMW**2)**2
        HWHH=GF/8.D0/DSQRT(2D0)/PI*AMW**4/AMCH*GLVV**2*CWH
       ENDIF
      ENDIF
      endif

      if(i2hdm.eq.0) then
         HWHH=0.D0
      endif

c     print*,'H+ -> W+ H',hwhh
c end MMM changed 21/8/2013

C  H+ ---> W A
      IF(IONSH.EQ.0)THEN
       IF (AMCH.LT.AMA) THEN
        HWA=0
       ELSEIF (AMCH.LT.AMW+AMA) THEN
        IF(AMCH.LE.DABS(AMW-AMA))THEN
         HWA=0
        ELSE
         HWA=9.D0*GF**2/16.D0/PI**3*AMW**4*AMCH
     .      *HVH((AMA/AMCH)**2,(AMW/AMCH)**2)
        ENDIF
       ELSE
        HWA=0.D0
       ENDIF
      ELSE
       IF (AMCH.LT.AMW+AMA) THEN
        HWA=0
       ELSE
        HWA=0.D0
       ENDIF
      ENDIF

c MMM changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0)then
            dld=3d0
            dlu=5d0
            xm1 = amw+ama-dld
            xm2 = amw+ama+dlu
            if (amch.lt.ama) then
               hwa=0
            elseif (amch.le.xm1) then
               if(amch.le.dabs(amw-ama))then
                  hwa=0
               else
                  hwa=9.d0*gf**2/16.d0/pi**3*amw**4*amch
     .                 *hvh((ama/amch)**2,(amw/amch)**2)

                  ivegas(1) = 30000
                  ivegas(2) = 5
                  ivegas(3) = 120000
                  ivegas(4) = 10

                  iprint = 0

                  amhi = amch
                  amhj = amw
                  amhk = ama
                  gamtoti = 1.D0
                  gamtotj = 1.D0
                  gamtotk = gamw

c ---- initialization of VEGAS ----

c                  call RSTART(12,34,56,78)

c                  call INTEG(hvhinteg,2,iprint,ivegas,result,relative)

c                  hwa=9.d0*gf**2/16.d0/pi**3*amw**4*amch*result
               endif
            elseif (amch.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)
     .              *hvh((ama/xx(1))**2,(amw/xx(1))**2)
               yy(2) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)
     .              *hvh((ama/xx(2))**2,(amw/xx(2))**2)
               cwh=lamb_hdec(ama**2/xx(3)**2,amw**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amw**2,ama**2/amw**2)**2
               yy(3)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*cwh
               cwh=lamb_hdec(ama**2/xx(4)**2,amw**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amw**2,ama**2/amw**2)**2
               yy(4)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*cwh
               hwa = fint_hdec(amch,xx,yy)
            else
               cwh=lamb_hdec(ama**2/amch**2,amw**2/amch**2)
     .              *lamb_hdec(amch**2/amw**2,ama**2/amw**2)**2
               hwa=gf/8.d0/dsqrt(2d0)/pi*amw**4/amch*cwh
            endif
         else
            if (amch.lt.amw+ama) then
               hwa=0
            else
               cwh=lamb_hdec(ama**2/amch**2,amw**2/amch**2)
     .              *lamb_hdec(amch**2/amw**2,ama**2/amw**2)**2
               hwa=gf/8.d0/dsqrt(2d0)/pi*amw**4/amch*cwh
            endif
         endif
      endif
c end MMM changed 22/8/2013

c     print*,'H+ -> W+ A',hwa

C  ======================= SUSY DECAYS 
      IF(IOFSUSY.EQ.0) THEN
C
C  H+ ----> CHARGINOS+NEUTRALINOS
C
      DO 751 I=1,2
      DO 751 J=1,4
      IF (AMCH.GT.AMCHAR(I)+AMNEUT(J)) THEN
      WHCCN(I,J)=GF*AMW**2/(2*PI*DSQRT(2.D0))/AMCH
     .   *LAMB_HDEC(AMCHAR(I)**2/AMCH**2,AMNEUT(J)**2/AMCH**2)*(
     .   (ACNL(I,J)**2+ACNR(I,J)**2)*(AMCH**2-AMCHAR(I)**2-XMNEUT(J)
     .   **2)-4.D0*ACNL(I,J)*ACNR(I,J)*XMCHAR(I)*XMNEUT(J) )
      ELSE
      WHCCN(I,J)=0.D0
      ENDIF
 751  CONTINUE

      WHCCNT=WHCCN(1,1)+WHCCN(1,2)+WHCCN(1,3)+WHCCN(1,4)
     .      +WHCCN(2,1)+WHCCN(2,2)+WHCCN(2,3)+WHCCN(2,4)
C
C  H+ ----> SLEPTONS 
C
      IF (AMCH.GT.AMSE(1)+AMSN1(1)) THEN
      WHCSL00=2*GF/4.D0/DSQRT(2D0)/PI*AMW**4/AMCH*DSIN(2.D0*B)**2
     .     *LAMB_HDEC(AMSE(1)**2/AMCH**2,AMSN1(1)**2/AMCH**2)
      ELSE 
      WHCSL00=0.D0
      ENDIF

      IF (AMCH.GT.AMSL(1)+AMSN(1)) THEN
      WHCSL11=GF/2.D0/DSQRT(2D0)/PI*AMW**4/AMCH*GCEN(1,1)**2
     .     *LAMB_HDEC(AMSL(1)**2/AMCH**2,AMSN(1)**2/AMCH**2)
      ELSE 
      WHCSL11=0.D0
      ENDIF

      IF (AMCH.GT.AMSL(2)+AMSN(1)) THEN
      WHCSL21=GF/2.D0/DSQRT(2D0)/PI*AMW**4/AMCH*GCEN(1,2)**2
     .     *LAMB_HDEC(AMSL(2)**2/AMCH**2,AMSN(1)**2/AMCH**2)
      ELSE 
      WHCSL21=0.D0
      ENDIF

      WHCSLT=WHCSL00+WHCSL11+WHCSL21

C
C  H+ ----> SQUARKS 
C
      IF (AMCH.GT.AMSU(1)+AMSD(1)) THEN
      WHCSQ=6*GF/4.D0/DSQRT(2D0)/PI*AMW**4/AMCH*DSIN(2.D0*B)**2
     .     *LAMB_HDEC(AMSU(1)**2/AMCH**2,AMSD(1)**2/AMCH**2)
      ELSE 
      WHCSQ=0.D0
      ENDIF
C
      DO 753 I=1,2
      DO 753 J=1,2
      IF(AMCH.GT.AMST(I)+AMSB(J)) THEN
      WHCSTB(I,J)=3*GF*AMW**4/2.D0/DSQRT(2.D0)/PI*GCTB(I,J)**2
     .      *LAMB_HDEC(AMST(I)**2/AMCH**2,AMSB(J)**2/AMCH**2)/AMCH
      ELSE
      WHCSTB(I,J)=0.D0
      ENDIF

 753  CONTINUE
C
      WHCSQT=WHCSQ+WHCSTB(1,1)+WHCSTB(1,2)+WHCSTB(2,1)+WHCSTB(2,2) 

      ELSE 
      WHCCNT=0.D0
      WHCSLT=0.D0
      WHCSQT=0.D0
      WHCGDT=0.D0
C--Change thanks to Elzbieta Richter-Was
      DO I=1,2
       DO J=1,2
        WHCSTB(I,J)=0.D0
       ENDDO
      ENDDO
      DO I=1,2
       DO J=1,4
        WHCCN(I,J)=0.D0
       ENDDO
      ENDDO
      ENDIF

      IF(IGOLD.NE.0)THEN
C   HC ---> GOLDSTINOS
       DO 750 I=1,2
       IF (AMCH.GT.AMCHAR(I)) THEN
        WHCGD(I)=AMCH**5/AXMPL**2/AXMGD**2/48.D0/PI*
     .           (1.D0-AMCHAR(I)**2/AMCH**2)**4*AGDC(I)**2
       ELSE
        WHCGD(I)=0.D0
       ENDIF
 750   CONTINUE
       WHCGDT=WHCGD(1)+WHCGD(2)
      ELSE
       WHCGDT=0
      ENDIF
C
C    ==========  TOTAL WIDTH AND BRANCHING RATIOS 
C
      WTOT=HLN+HMN+HSU+HBU+HSC+HBC+HBT+HWH+HWA+WHCCNT+WHCSLT+WHCSQT
     .    +WHCGDT

c MMM changed 21/8/2013
      WTOT=WTOT+HCD+HST+HDT

      if(i2hdm.eq.1) then
         wtot = wtot+hwhh
         HCBRWHH=hwhh/wtot
      endif
      if(i2hdm.eq.0) then
         hcbrwhh=0.D0
      endif
c end MMM changed 21/8/2013
      HCBRCD=HCD/WTOT
      HCBRTS=HST/WTOT
      HCBRTD=HDT/WTOT

c     print*,'wtot',wtot

      HCBRL=HLN/WTOT
      HCBRM=HMN/WTOT
      HCBRS=HSU/WTOT
      HCBRBU=HBU/WTOT
      HCBRC=HSC/WTOT
      HCBRB=HBC/WTOT
      HCBRT=HBT/WTOT
      HCBRW=HWH/WTOT
      HCBRA=HWA/WTOT
      DO 851 I=1,2
      DO 851 J=1,4
      HCBRSU(I,J)=WHCCN(I,J)/WTOT
851   CONTINUE
      HCBRCNT=WHCCNT/WTOT
      HCBRSL=WHCSLT/WTOT 
      HCBRSQ=WHCSQ/WTOT 
      HCBRSQT=WHCSQT/WTOT 
      DO 853 I=1,2
      DO 853 J=1,2
      HCBRSTB(I,J)=WHCSTB(I,J)/WTOT
853   CONTINUE
      HCBRGD=WHCGDT/WTOT
      HCWDTH=WTOT

      BHCSL00 = WHCSL00/WTOT
      BHCSL11 = WHCSL11/WTOT
      BHCSL21 = WHCSL21/WTOT
      BHCSQ = WHCSQ/WTOT
      DO I = 1,2
       DO J = 1,2
        BHCSTB(I,J) = WHCSTB(I,J)/WTOT
       ENDDO
      ENDDO

      GAMC0 = WTOT

      ENDIF

      IF(IHIGGS.EQ.2.OR.IHIGGS.EQ.5)THEN
     
C        =========================================================
C                       HEAVY CP EVEN HIGGS DECAYS
C        =========================================================
C     =============  RUNNING MASSES 
      RMS = RUNM_HDEC(AMH,3)
      RMC = RUNM_HDEC(AMH,4)
      RMB = RUNM_HDEC(AMH,5)
      RMT = RUNM_HDEC(AMH,6)
      RATCOUP = GHT/GHB
      HIGTOP = AMH**2/AMT**2

      ASH=ALPHAS_HDEC(AMH,3)
      AMC0=1.D8
      AMB0=2.D8
C     AMT0=3.D8
      AS3=ALPHAS_HDEC(AMH,3)
      AMC0=AMC
      AS4=ALPHAS_HDEC(AMH,3)
      AMB0=AMB
C     AMT0=AMT

C     =============== PARTIAL WIDTHS 
C  H ---> G G
       EPS=1.D-8
       NFEXT = 3
       ASG = AS3
       CTT = 4*AMT**2/AMH**2*DCMPLX(1D0,-EPS)
       CTB = 4*AMB**2/AMH**2*DCMPLX(1D0,-EPS)
       CAT = 2*CTT*(1+(1-CTT)*CF(CTT))*GHT
       CAB = 2*CTB*(1+(1-CTB)*CF(CTB))*GHB
       CTC = 4*AMC**2/AMH**2*DCMPLX(1D0,-EPS)
       CAC = 2*CTC*(1+(1-CTC)*CF(CTC))*GHT
C
       IF(IOFSUSY.EQ.0) THEN 
       CSB1= 4*AMSB(1)**2/AMH**2*DCMPLX(1D0,-EPS)
       CSB2= 4*AMSB(2)**2/AMH**2*DCMPLX(1D0,-EPS)
       CST1= 4*AMST(1)**2/AMH**2*DCMPLX(1D0,-EPS)
       CST2= 4*AMST(2)**2/AMH**2*DCMPLX(1D0,-EPS)
C
       CXB1=-AMZ**2/AMSB(1)**2*CSB1*(1-CSB1*CF(CSB1))*GHBB(1,1)
       CXB2=-AMZ**2/AMSB(2)**2*CSB2*(1-CSB2*CF(CSB2))*GHBB(2,2)
       CXT1=-AMZ**2/AMST(1)**2*CST1*(1-CST1*CF(CST1))*GHTT(1,1)
       CXT2=-AMZ**2/AMST(2)**2*CST2*(1-CST2*CF(CST2))*GHTT(2,2)
C
       CSUL = 4*AMSU(1)**2/AMH**2*DCMPLX(1D0,-EPS)
       CSUR = 4*AMSU(2)**2/AMH**2*DCMPLX(1D0,-EPS)
       CSDL = 4*AMSD(1)**2/AMH**2*DCMPLX(1D0,-EPS)
       CSDR = 4*AMSD(2)**2/AMH**2*DCMPLX(1D0,-EPS)
       CXUL=-2*(1.D0/2.D0-2.D0/3.D0*SS)*AMZ**2/AMSU(1)**2*DCOS(A+B)
     .      *CSUL*(1-CSUL*CF(CSUL))
       CXUR=-2*(2.D0/3.D0*SS)*AMZ**2/AMSU(2)**2*DCOS(A+B)
     .      *CSUR*(1-CSUR*CF(CSUR))
       CXDL=-2*(-1.D0/2.D0+1.D0/3.D0*SS)*AMZ**2/AMSD(1)**2*DCOS(A+B)
     .      *CSDL*(1-CSDL*CF(CSDL))
       CXDR=-2*(-1.D0/3.D0*SS)*AMZ**2/AMSD(2)**2*DCOS(A+B)
     .      *CSDR*(1-CSDR*CF(CSDR))
       ELSE
       CXB1=0.D0 
       CXB2=0.D0 
       CXT1=0.D0 
       CXT2=0.D0 
       CXUL=0.D0
       CXUR=0.D0
       CXDL=0.D0
       CXDR=0.D0
       ENDIF

       FQCD=HGGQCD(ASG,NFEXT)
       SQCD=SGGQCD(ASG)
       XFAC = CDABS(CAT+CAB+CAC+CXB1+CXB2+CXT1+CXT2
     .             +CXUL+CXUR+CXDL+CXDR)**2*FQCD
     .      + DREAL(DCONJG(CAT+CAB+CAC+CXB1+CXB2+CXT1+CXT2
     .                    +CXUL+CXUR+CXDL+CXDR)
     .             *(CXB1+CXB2+CXT1+CXT2+CXUL+CXUR+CXDL+CXDR))*SQCD
       HGG=HVV(AMH,0.D0)*(ASG/PI)**2*XFAC/8

c      write(6,*)'ghb, ght: ',ghb,ght

c      print*,''
c      print*,'H decay widths'
c      print*,'hgg_NLO',hgg

C  H ---> G G* ---> G CC   TO BE ADDED TO H ---> CC
       NFEXT = 4
       ASG = AS4
       FQCD=HGGQCD(ASG,NFEXT)
       SQCD=SGGQCD(ASG)
       XFAC = CDABS(CAT+CAB+CAC+CXB1+CXB2+CXT1+CXT2
     .             +CXUL+CXUR+CXDL+CXDR)**2*FQCD
     .      + DREAL(DCONJG(CAT+CAB+CAC+CXB1+CXB2+CXT1+CXT2
     .                    +CXUL+CXUR+CXDL+CXDR)
     .             *(CXB1+CXB2+CXT1+CXT2+CXUL+CXUR+CXDL+CXDR))*SQCD
       DCC=HVV(AMH,0.D0)*(ASG/PI)**2*XFAC/8 - HGG

C  H ---> G G* ---> G BB   TO BE ADDED TO H ---> BB
       NFEXT = 5
       ASG = ASH
       FQCD=HGGQCD(ASG,NFEXT)
       SQCD=SGGQCD(ASG)
       XFAC = CDABS(CAT+CAB+CAC+CXB1+CXB2+CXT1+CXT2
     .             +CXUL+CXUR+CXDL+CXDR)**2*FQCD
     .      + DREAL(DCONJG(CAT+CAB+CAC+CXB1+CXB2+CXT1+CXT2
     .                    +CXUL+CXUR+CXDL+CXDR)
     .             *(CXB1+CXB2+CXT1+CXT2+CXUL+CXUR+CXDL+CXDR))*SQCD
       DBB=HVV(AMH,0.D0)*(ASG/PI)**2*XFAC/8 - HGG - DCC
       HGG=HVV(AMH,0.D0)*(ASG/PI)**2*XFAC/8

C  H ---> G G: FULL NNNLO CORRECTIONS TO TOP LOOPS FOR NF=5
       FQCD0=HGGQCD(ASG,5)
       FQCD=HGGQCD2(ASG,5,AMH,AMT)
       XFAC = CDABS(CAT+CAB+CAC)**2*(FQCD-FQCD0)
       HGG=HGG+HVV(AMH,0.D0)*(ASG/PI)**2*XFAC/8

      IF(NFGG.EQ.3)THEN
       HGG = HGG - DBB - DCC
      ELSEIF(NFGG.EQ.4)THEN
       HGG = HGG - DBB
       DCC = 0
      ELSE
       DCC = 0
       DBB = 0
      ENDIF

c      print*,'hgg_NNLO',hgg

C  H ---> MU MU
      XGLM = GLB
      XGHM = GHB
      XGAM = GAB
      if(i2hdm.eq.1) then
         xghm = ghlep
      endif
      IF(IOFSUSY.EQ.0) THEN
       CALL STAUSUSY_HDEC(GLB,GHB,GAB,XGLM,XGHM,XGAM,QSUSY,0)
      ENDIF
      IF(AMH.LE.2*AMMUON) THEN
       HMM = 0
      ELSE
      HMM=HFF(AMH,(AMMUON/AMH)**2)*XGHM**2
      ENDIF

c      print*,'H -> mumu',hmm
C  H ---> LL
      XGLT = GLB
      XGHT = GHB
      XGAT = GAB
      if(i2hdm.eq.1) then
         xght = ghlep
      endif
      IF(IOFSUSY.EQ.0) THEN
       CALL STAUSUSY_HDEC(GLB,GHB,GAB,XGLT,XGHT,XGAT,QSUSY,1)
      ENDIF
      IF(AMH.LE.2*AMTAU) THEN
       HLL = 0
      ELSE
      HLL=HFF(AMH,(AMTAU/AMH)**2)*XGHT**2
      ENDIF

c     write(6,*)'H: tau/mu: ',HLL/HMM*AMMUON**2/AMTAU**2,XGHT**2/XGHM**2
c       print*,'H -> tautau',hll
C  H --> SS
      ash = as3
      XGLS = GLB
      XGHS = GHB
      XGAS = GAB
      SSUSY = (AMSD(1)+AMSD(2)+AMGLU)/3*QSUSY
C     SSUSY = (AMSD(1)+AMSD(2))/3*QSUSY
      IF(IOFSUSY.EQ.0) THEN
       CALL STRSUSY_HDEC(GLB,GHB,GAB,XGLS,XGHS,XGAS,SSUSY,LOOP)
      ENDIF
      IF(AMH.LE.2*AMS) THEN
       HSS = 0
      ELSE
       HS1=3.D0*HFF(AMH,(AMS/AMH)**2)
     .    *XGHS**2
     .    *TQCDH(AMS**2/AMH**2)
       HS2=3.D0*HFF(AMH,(RMS/AMH)**2)*XGHS**2
     .    *QCDH(RMS**2/AMH**2)
       IF(HS2.LT.0.D0) HS2 = 0
       RAT = 2*AMS/AMH
       HSS = QQINT_HDEC(RAT,HS1,HS2)
      ENDIF

c      print*,'H -> ss',hss
C  H --> CC
      RATCOUP = 1
      IF(AMH.LE.2*AMC) THEN
       HCC = 0
      ELSE
       HC1=3.D0*HFF(AMH,(AMC/AMH)**2)
     .    *GHT**2
     .    *TQCDH(AMC**2/AMH**2)
       HC2=3.D0*HFF(AMH,(RMC/AMH)**2)*GHT**2
     .    *QCDH(RMC**2/AMH**2)
     .   + DCC
       IF(HC2.LT.0.D0) HC2 = 0
       RAT = 2*AMC/AMH
       HCC = QQINT_HDEC(RAT,HC1,HC2)
      ENDIF

c      print*,'H -> cc',hcc
C  H --> BB :
      QQ = AMB
      SUSY = 0
      XGHB = GHB
      SSUSY = (AMSB(1)+AMSB(2)+AMGLU)/3*QSUSY
      FSUSY = SUSYSCALE
      AS0 = ALPHAS_HDEC(FSUSY,3)
      IF(IOFSUSY.EQ.0) THEN
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       I0 = 0
       CALL DMBAPP_HDEC(I0,DGLB,DGHB,DGAB,FSUSY,LOOP)
       DELB1 = -DGAB/(1+1/TGBET**2)
       DELB0 = DELB1/(1-DELB1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       I0 = 1
       CALL DMBAPP_HDEC(I0,DGLB,DGHB,DGAB,FSUSY,LOOP)
       I0 = 2
       BSC = (AMSQ+AMUR+AMDR)/3
       XMB = RUNM_HDEC(FSUSY,5)/(1+DELB0)
C       XMB = AMB
c1357
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       if(islhai.ne.0) XMB = AMB
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       SUSY = COFSUSY_HDEC(I0,AMB,XMB,QQ)*AS0/PI - 2*DGHB
       CALL BOTSUSY_HDEC(GLB,GHB,GAB,XGLB,XGHB,XGAB,SSUSY,LOOP)
      ENDIF
      RATCOUP = GHT/XGHB
      IF(AMH.LE.2*AMB) THEN
       HBB = 0
      ELSE
       HB1=3.D0*HFF(AMH,(AMB/AMH)**2)
     .    *(XGHB**2+XGHB*GHB*SUSY)
     .    *TQCDH(AMB**2/AMH**2)
       HB2=3.D0*HFF(AMH,(RMB/AMH)**2)
     .    *(XGHB**2+XGHB*GHB*SUSY)
     .    *QCDH(RMB**2/AMH**2)
     .   + DBB
       IF(HB2.LT.0.D0) HB2 = 0
       RAT = 2*AMB/AMH
       HBB = QQINT_HDEC(RAT,HB1,HB2)
c     write(6,*)'H -> bb:  ',RAT,HB1,HB2
c     write(6,*)'H -> bb:  ',GHB,XGHB,SUSY
c     write(6,*)'H -> bb1: ',COFSUSY_HDEC(I0,AMB,XMB,QQ),DGHB

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      XXX = COFSUSY_HDEC(I0,AMB,XMB,QQ)
c      write(1,*)'H -> bb: ',XXX*AS0/PI
c      write(1,*)'H -> bb: ',2*DGHB
c      write(1,*)'H -> bb: ',GHB,XGHB,SUSY
c      write(6,('A3,4(1X,G15.8)'))'H: ',AMA,AMH,SUSY+2*DGHB,
c    .                             SUSY/(SUSY+2*DGHB)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      ENDIF
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'H -> bb: ',XGHB**2,XGHB*GHB*SUSY/XGHB**2,
c    .                      (XGHB**2+XGHB*GHB*SUSY)/XGHB**2
c     write(6,*)'approx:  ',SUSY+2*DGHB,2*DGHB,SUSY
c     FAC = AS0/PI
c     write(6,*)'approx2: ',(SUSY+2*DGHB)/FAC,2*DGHB/FAC,SUSY/FAC
c     write(51,*)AMH,HBB
c     write(51,*)GHB,XGHB
c     write(51,*)SUSY,COFSUSY_HDEC(I0,AMB,XMB,QQ)*AS0/PI,-2*DGHB
c     write(51,*)SSUSY,AS0,QSUSY,(AMSB(1)+AMSB(2)+AMGLU)/3
c     write(6,*)'H -> bb: ',AMH,HBB,QSUSY1,LOOP
c     write(6,*)GHB,XGHB,XGHB**2+XGHB*GHB*SUSY
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c      print*,'H -> bb',hbb
c      print*
c      print*,'H -> bb:',hbb,ghb,xghb,ghb*susy,2*dghb,susy,2*dghb+susy
C  H ---> TT
      RATCOUP = 0
      CALL TOPSUSY_HDEC(GLT,GHT,GAT,XGLTOP,XGHTOP,XGATOP,SCALE,1)
c     write(6,*)xgltop,xghtop,xgatop
      if(i2hdm.eq.0) then
      IF(IONSH.EQ.0)THEN
       DLD=3D0
       DLU=5D0
       XM1 = 2D0*AMT-DLD
       XM2 = 2D0*AMT+DLU
       IF (AMH.LE.AMT+AMW+AMB) THEN
        HTT=0.D0
       ELSEIF (AMH.LE.XM1) THEN
        FACTT=6.D0*GF**2*AMH**3*AMT**2/2.D0/128.D0/PI**3
        call HTOTT_hdec(amh,amt,amb,amw,amch,ght,ghb,gat,gab,
     .              ghvv,gzal,htt0)
        HTT=FACTT*HTT0
       ELSEIF (AMH.LE.XM2) THEN
        ZZMA=AMAR
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        CALL AMHAMA_HDEC(2,XX(1),TGBET)
        FACTT=6.D0*GF**2*XX(1)**3*AMT**2/2.D0/128.D0/PI**3
        call HTOTT_hdec(xx(1),amt,amb,amw,amch,ght,ghb,gat,gab,
     .              ghvv,gzal,htt0)
        YY(1)=FACTT*HTT0
        CALL AMHAMA_HDEC(2,XX(2),TGBET)
        FACTT=6.D0*GF**2*XX(2)**3*AMT**2/2.D0/128.D0/PI**3
        call HTOTT_hdec(xx(2),amt,amb,amw,amch,ght,ghb,gat,gab,
     .              ghvv,gzal,htt0)
        YY(2)=FACTT*HTT0
        CALL AMHAMA_HDEC(2,XX(3),TGBET)
        XMT = RUNM_HDEC(XX(3),6)
        HT1=3.D0*HFF(XX(3),(AMT/XX(3))**2)*GHT**2
     .    *TQCDH(AMT**2/XX(3)**2)
        HT2=3.D0*HFF(XX(3),(XMT/XX(3))**2)*GHT**2
     .    *QCDH(XMT**2/XX(3)**2)
        IF(HT2.LT.0.D0) HT2 = 0
        RAT = 2*AMT/XX(3)
        YY(3) = QQINT_HDEC(RAT,HT1,HT2)
        CALL AMHAMA_HDEC(2,XX(4),TGBET)
        XMT = RUNM_HDEC(XX(4),6)
        HT1=3.D0*HFF(XX(4),(AMT/XX(4))**2)*GHT**2
     .    *TQCDH(AMT**2/XX(4)**2)
        HT2=3.D0*HFF(XX(4),(XMT/XX(4))**2)*GHT**2
     .    *QCDH(XMT**2/XX(4)**2)
        IF(HT2.LT.0.D0) HT2 = 0
        RAT = 2*AMT/XX(4)
        YY(4) = QQINT_HDEC(RAT,HT1,HT2)
        AMA = ZZMA
        CALL twohdmcp_hdec(TGBET)
        HTT=FINT_HDEC(AMH,XX,YY)
       ELSE
        HT1=3.D0*HFF(AMH,(AMT/AMH)**2)*GHT**2
     .    *TQCDH(AMT**2/AMH**2)
        HT2=3.D0*HFF(AMH,(RMT/AMH)**2)*GHT**2
     .    *QCDH(RMT**2/AMH**2)
        IF(HT2.LT.0.D0) HT2 = 0
        RAT = 2*AMT/AMH
        HTT = QQINT_HDEC(RAT,HT1,HT2)
       ENDIF
      ELSE
       IF (AMH.LE.2.D0*AMT) THEN
        HTT=0.D0
       ELSE
        HT1=3.D0*HFF(AMH,(AMT/AMH)**2)*GHT**2
     .    *TQCDH(AMT**2/AMH**2)
        HT2=3.D0*HFF(AMH,(RMT/AMH)**2)*GHT**2
     .    *QCDH(RMT**2/AMH**2)
        IF(HT2.LT.0.D0) HT2 = 0
        RAT = 2*AMT/AMH
        HTT = QQINT_HDEC(RAT,HT1,HT2)
       ENDIF
      ENDIF
      endif

c MMM changed 21/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0)then
            dld=5.D0
            dlu=3.D0
            xm1 = 2d0*amt-dld
            xm2 = 2d0*amt+dlu
            if (amh.le.amt+amw+amb) then
               htt=0.d0
            elseif (amh.le.xm1) then
               factt=6.d0*gf**2*amh**3*amt**2/2.d0/128.d0/pi**3
               call HTOTT_hdec(amh,amt,amb,amw,amch,ght,ghb,gat,gab,
     .              ghvv,gzal,htt0)
               htt=factt*htt0
            elseif (amh.le.xm2) then
               XX(1) = XM1-1D0
               XX(2) = XM1
               XX(3) = XM2
               XX(4) = XM2+1D0

               factt=6.d0*gf**2*xx(1)**3*amt**2/2.d0/128.d0/pi**3
               call HTOTT_hdec(xx(1),amt,amb,amw,amch,ght,ghb,gat,gab,
     .              ghvv,gzal,htt0)
               yy(1)=factt*htt0

               factt=6.d0*gf**2*xx(2)**3*amt**2/2.d0/128.d0/pi**3
               call HTOTT_hdec(xx(2),amt,amb,amw,amch,ght,ghb,gat,gab,
     .              ghvv,gzal,htt0)
               yy(2)=factt*htt0

               xmt = runm_hdec(xx(3),6)
               ht1=3.d0*hff(xx(3),(amt/xx(3))**2)*ght**2
     .              *tqcdh(amt**2/xx(3)**2)
               ht2=3.d0*hff(xx(3),(xmt/xx(3))**2)*ght**2
     .              *qcdh(xmt**2/xx(3)**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2*amt/xx(3)
               yy(3) = qqint_hdec(rat,ht1,ht2)

               xmt = runm_hdec(xx(4),6)
               ht1=3.d0*hff(xx(4),(amt/xx(4))**2)*ght**2
     .              *tqcdh(amt**2/xx(4)**2)
               ht2=3.d0*hff(xx(4),(xmt/xx(4))**2)*ght**2
     .              *qcdh(xmt**2/xx(4)**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2*amt/xx(4)
               yy(4) = qqint_hdec(rat,ht1,ht2)

               htt=fint_hdec(amh,xx,yy)
            else
               ht1=3.d0*hff(amh,(amt/amh)**2)*ght**2
     .              *tqcdh(amt**2/amh**2)
               ht2=3.d0*hff(amh,(rmt/amh)**2)*ght**2
     .              *qcdh(rmt**2/amh**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2.D0*amt/amh
               htt = qqint_hdec(rat,ht1,ht2)
            endif
         else
            if (amh.le.2.d0*amt) then
               htt=0.d0
            else
               ht1=3.d0*hff(amh,(amt/amh)**2)*ght**2
     .              *tqcdh(amt**2/amh**2)
               ht2=3.d0*hff(amh,(rmt/amh)**2)*ght**2
     .              *qcdh(rmt**2/amh**2)
               if(ht2.lt.0.d0) ht2 = 0
               rat = 2.D0*amt/amh
               htt = qqint_hdec(rat,ht1,ht2)
            endif
         endif
      endif
c end MMM changed 21/8/2103

c      print*,'H -> tt',htt

C  H ---> GAMMA GAMMA
       EPS=1.D-8
       XRMC = RUNM_HDEC(AMH/2,4)*AMC/RUNM_HDEC(AMC,4)
       XRMB = RUNM_HDEC(AMH/2,5)*AMB/RUNM_HDEC(AMB,5)
       XRMT = RUNM_HDEC(AMH/2,6)*AMT/RUNM_HDEC(AMT,6)
       CTT = 4*XRMT**2/AMH**2*DCMPLX(1D0,-EPS)
       CTB = 4*XRMB**2/AMH**2*DCMPLX(1D0,-EPS)
       CTL = 4*AMTAU**2/AMH**2*DCMPLX(1D0,-EPS)
       CTW = 4*AMW**2/AMH**2*DCMPLX(1D0,-EPS)
       CTH = 4*AMCH**2/AMH**2*DCMPLX(1D0,-EPS)
       CTC = 4*XRMC**2/AMH**2*DCMPLX(1D0,-EPS)
       CAC = 4/3D0 * 2*CTC*(1+(1-CTC)*CF(CTC))*GHT
     .     * CFACQ_HDEC(0,AMH,XRMC)
       CAT = 4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT))*GHT
     .     * CFACQ_HDEC(0,AMH,XRMT)
       CAB = 1/3D0 * 2*CTB*(1+(1-CTB)*CF(CTB))*GHB
     .     * CFACQ_HDEC(0,AMH,XRMB)
       CAL = 1.D0  * 2*CTL*(1+(1-CTL)*CF(CTL))*GHB
       if(i2hdm.eq.1) then
          CAL = 1.D0  * 2*CTL*(1+(1-CTL)*CF(CTL))*ghlep
       endif
       CAW = -(2+3*CTW+3*CTW*(2-CTW)*CF(CTW))*GHVV
       CAH = -AMZ**2/2/AMCH**2*CTH*(1-CTH*CF(CTH))*GHPM
       IF(IOFSUSY.EQ.0) THEN 
        RMSU1 = RUNMS_HDEC(AMH/2,AMSU(1))
        RMSU2 = RUNMS_HDEC(AMH/2,AMSU(2))
        RMSD1 = RUNMS_HDEC(AMH/2,AMSD(1))
        RMSD2 = RUNMS_HDEC(AMH/2,AMSD(2))
        RMSB1 = RUNMS_HDEC(AMH/2,AMSB(1))
        RMSB2 = RUNMS_HDEC(AMH/2,AMSB(2))
        RMST1 = RUNMS_HDEC(AMH/2,AMST(1))
        RMST2 = RUNMS_HDEC(AMH/2,AMST(2))
        CX1 = 4*AMCHAR(1)**2/AMH**2*DCMPLX(1D0,-EPS)
        CX2 = 4*AMCHAR(2)**2/AMH**2*DCMPLX(1D0,-EPS)
        CAX1= AMW/XMCHAR(1) * 2*CX1*(1+(1-CX1)*CF(CX1))*2*AC1(1,1) 
        CAX2= AMW/XMCHAR(2) * 2*CX2*(1+(1-CX2)*CF(CX2))*2*AC1(2,2) 
        CSL1= 4*AMSL(1)**2/AMH**2*DCMPLX(1D0,-EPS)
        CSL2= 4*AMSL(2)**2/AMH**2*DCMPLX(1D0,-EPS)
        CSB1= 4*RMSB1**2/AMH**2*DCMPLX(1D0,-EPS)
        CSB2= 4*RMSB2**2/AMH**2*DCMPLX(1D0,-EPS)
        CST1= 4*RMST1**2/AMH**2*DCMPLX(1D0,-EPS)
        CST2= 4*RMST2**2/AMH**2*DCMPLX(1D0,-EPS)

        CSEL = 4*AMSE(1)**2/AMH**2*DCMPLX(1D0,-EPS)
        CSER = 4*AMSE(2)**2/AMH**2*DCMPLX(1D0,-EPS)
        CSUL = 4*RMSU1**2/AMH**2*DCMPLX(1D0,-EPS)
        CSUR = 4*RMSU2**2/AMH**2*DCMPLX(1D0,-EPS)
        CSDL = 4*RMSD1**2/AMH**2*DCMPLX(1D0,-EPS)
        CSDR = 4*RMSD2**2/AMH**2*DCMPLX(1D0,-EPS)
        CXEL=-2*(-1/2D0+SS)*AMZ**2/AMSE(1)**2*DCOS(A+B)
     .       *CSEL*(1-CSEL*CF(CSEL))
        CXER=2*(SS)*AMZ**2/AMSE(2)**2*DCOS(A+B)
     .       *CSER*(1-CSER*CF(CSER))
        CXUL=-2*4.D0/3.D0*(1.D0/2.D0-2.D0/3.D0*SS)
     .       *AMZ**2/AMSU(1)**2*DCOS(A+B)*CSUL*(1-CSUL*CF(CSUL))
     .      * CFACSQ_HDEC(AMH,RMSU1)
        CXUR=-2*4.D0/3.D0*(2.D0/3.D0*SS)
     .       *AMZ**2/AMSU(2)**2*DCOS(A+B)*CSUR*(1-CSUR*CF(CSUR))
     .      * CFACSQ_HDEC(AMH,RMSU2)
        CXDL=-2/3.D0*(-1.D0/2.D0+1.D0/3.D0*SS)
     .       *AMZ**2/AMSD(1)**2*DCOS(A+B)*CSDL*(1-CSDL*CF(CSDL))
     .      * CFACSQ_HDEC(AMH,RMSD1)
        CXDR=-2/3.D0*(-1.D0/3.D0*SS)
     .       *AMZ**2/AMSD(2)**2*DCOS(A+B)*CSDR*(1-CSDR*CF(CSDR))
     .      * CFACSQ_HDEC(AMH,RMSD2)

        CXB1= -1/3D0*AMZ**2/AMSB(1)**2*CSB1*(1-CSB1*CF(CSB1))*GHBB(1,1)
     .      * CFACSQ_HDEC(AMH,RMSB1)
        CXB2= -1/3D0*AMZ**2/AMSB(2)**2*CSB2*(1-CSB2*CF(CSB2))*GHBB(2,2)
     .      * CFACSQ_HDEC(AMH,RMSB2)
        CXT1= -4/3D0*AMZ**2/AMST(1)**2*CST1*(1-CST1*CF(CST1))*GHTT(1,1)
     .      * CFACSQ_HDEC(AMH,RMST1)
        CXT2= -4/3D0*AMZ**2/AMST(2)**2*CST2*(1-CST2*CF(CST2))*GHTT(2,2)
     .      * CFACSQ_HDEC(AMH,RMST2)
        CXL1=       -AMZ**2/AMSL(1)**2*CSL1*(1-CSL1*CF(CSL1))*GHEE(1,1)
        CXL2=       -AMZ**2/AMSL(2)**2*CSL2*(1-CSL2*CF(CSL2))*GHEE(2,2)
        XFAC = CDABS(CAT+CAB+CAC+CAL+CAW+CAH+CAX1+CAX2
     .       +  CXEL+CXER+CXUL+CXUR+CXDL+CXDR
     .       +  CXB1+CXB2+CXT1+CXT2+CXL1+CXL2)**2
       ELSE 
        XFAC = CDABS(CAT+CAB+CAC+CAL+CAW+CAH)**2
       ENDIF
       HGA=HVV(AMH,0.D0)*(ALPH/PI)**2/16.D0*XFAC

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      CAC0= 4/3D0 * 2*CTC*(1+(1-CTC)*CF(CTC))*GHT
c      CAT0= 4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT))*GHT
c      CAB0= 1/3D0 * 2*CTB*(1+(1-CTB)*CF(CTB))*GHB

c      CXUL0=-2*4.D0/3.D0*(1.D0/2.D0-2.D0/3.D0*SS)
c    .       *AMZ**2/AMSU(1)**2*DCOS(A+B)*CSUL*(1-CSUL*CF(CSUL))
c      CXUR0=-2*4.D0/3.D0*(2.D0/3.D0*SS)
c    .       *AMZ**2/AMSU(2)**2*DCOS(A+B)*CSUR*(1-CSUR*CF(CSUR))
c      CXDL0=-2/3.D0*(-1.D0/2.D0+1.D0/3.D0*SS)
c    .       *AMZ**2/AMSD(1)**2*DCOS(A+B)*CSDL*(1-CSDL*CF(CSDL))
c      CXDR0=-2/3.D0*(-1.D0/3.D0*SS)
c    .       *AMZ**2/AMSD(2)**2*DCOS(A+B)*CSDR*(1-CSDR*CF(CSDR))
c      CXB10= -1/3D0*AMZ**2/AMSB(1)**2*CSB1*(1-CSB1*CF(CSB1))*GHBB(1,1)
c      CXB20= -1/3D0*AMZ**2/AMSB(2)**2*CSB2*(1-CSB2*CF(CSB2))*GHBB(2,2)
c      CXT10= -4/3D0*AMZ**2/AMST(1)**2*CST1*(1-CST1*CF(CST1))*GHTT(1,1)
c      CXT20= -4/3D0*AMZ**2/AMST(2)**2*CST2*(1-CST2*CF(CST2))*GHTT(2,2)

c      XFAC0 = CDABS(CAT0+CAB0+CAC0+CAL+CAW+CAH+CAX1+CAX2
c    .       +  CXEL+CXER+CXUL0+CXUR0+CXDL0+CXDR0
c    .       +  CXB10+CXB20+CXT10+CXT20+CXL1+CXL2)**2
c      XFAC0 = CDABS(CAT0+CAB0+CAC0+CAL+CAW+CAH+CAX1+CAX2
c    .       +  CXEL+CXER+CXUL+CXUR+CXDL+CXDR
c    .       +  CXB1+CXB2+CXT1+CXT2+CXL1+CXL2)**2
c      XCHGAGA = XFAC/XFAC0
c      write(6,*)'Hgaga: ',amh,xchgaga,hga,hga*xchgaga
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      CAT0 = CAT/GHT
c      CAB0 = CAB/GHB
c      CAC0 = CAC/GHT
c      CAL0 = CAL/GHB
c      CAW0 = CAW/GHVV
c      XFAC0 = CDABS(CAT0+CAB0+CAC0+CAL0+CAW0)**2
c      XCHGAGA = XFAC/XFAC0
c      write(6,*)'Hgaga: ',amh,xchgaga
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c       print*,'H -> gamgam',hga
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      XFACQ = CDABS(CAT+CAB+CAC+CAL+CAW+CAH)**2
c      XFACS = CDABS(CAT+CAB+CAC+CAL+CAW+CAH+CAX1+CAX2
c    .      +  CXL1+CXL2)**2
c      XFACSQ = CDABS(CAT+CAB+CAC+CAL+CAW+CAH+CAX1+CAX2
c    .      +  CXB1+CXB2+CXT1+CXT2+CXL1+CXL2)**2
c      HGA0 = HGA*XFACSQ/XFAC
c      CAC0 = 4/3D0 * 2*CTC*(1+(1-CTC)*CF(CTC))*GHT
c      CAT0 = 4/3D0 * 2*CTT*(1+(1-CTT)*CF(CTT))*GHT
c      CAB0 = 1/3D0 * 2*CTB*(1+(1-CTB)*CF(CTB))*GHB
c      CXB10= -1/3D0*AMZ**2/AMSB(1)**2*CSB1*(1-CSB1*CF(CSB1))*GHBB(1,1)
c      CXB20= -1/3D0*AMZ**2/AMSB(2)**2*CSB2*(1-CSB2*CF(CSB2))*GHBB(2,2)
c      CXT10= -4/3D0*AMZ**2/AMST(1)**2*CST1*(1-CST1*CF(CST1))*GHTT(1,1)
c      CXT20= -4/3D0*AMZ**2/AMST(2)**2*CST2*(1-CST2*CF(CST2))*GHTT(2,2)
c      XFACLOQ = CDABS(CAT0+CAB0+CAC0+CAL+CAW+CAH)**2
c      CXUL0=-2*4.D0/3.D0*(1.D0/2.D0-2.D0/3.D0*SS)
c    .      *AMZ**2/AMSU(1)**2*DCOS(A+B)*CSUL*(1-CSUL*CF(CSUL))
c      CXUR0=-2*4.D0/3.D0*(2.D0/3.D0*SS)
c    .      *AMZ**2/AMSU(2)**2*DCOS(A+B)*CSUR*(1-CSUR*CF(CSUR))
c      CXDL0=-2/3.D0*(-1.D0/2.D0+1.D0/3.D0*SS)
c    .      *AMZ**2/AMSD(1)**2*DCOS(A+B)*CSDL*(1-CSDL*CF(CSDL))
c      CXDR0=-2/3.D0*(-1.D0/3.D0*SS)
c    .      *AMZ**2/AMSD(2)**2*DCOS(A+B)*CSDR*(1-CSDR*CF(CSDR))
c      XFACLO = CDABS(CAT0+CAB0+CAC0+CAL+CAW+CAH+CAX1+CAX2
c    .      +  CXEL+CXER+CXUL0+CXUR0+CXDL0+CXDR0
c    .      +  CXB10+CXB20+CXT10+CXT20+CXL1+CXL2)**2
c      CSQ = 1+3*ALPHAS_HDEC(AMH,3)
c      XFACSQL = CDABS(CAT+CAB+CAC+CAL+CAW+CAH+CAX1+CAX2
c    .      +  CXEL+CXER+(CXUL0+CXUR0+CXDL0+CXDR0
c    .      +  CXB10+CXB20+CXT10+CXT20)*CSQ+CXL1+CXL2)**2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
C  H ---> Z GAMMA
      XRMC = RUNM_HDEC(AMH/2,4)*AMC/RUNM_HDEC(AMC,4)
      XRMB = RUNM_HDEC(AMH/2,5)*AMB/RUNM_HDEC(AMB,5)
      XRMT = RUNM_HDEC(AMH/2,6)*AMT/RUNM_HDEC(AMT,6)
c     print*,'xrmc,xrmb,xrmt ',xrmc,xrmb,xrmt
      IF(AMH.LE.AMZ)THEN
       HZGA=0
      ELSE
       TS = SS/CS
       FT = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS)*GHT
       FB = 3*1D0/3*(-1+4*1D0/3*SS)/DSQRT(SS*CS)*GHB
       FC = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS)*GHT
       FL = (-1+4*SS)/DSQRT(SS*CS)*GHB
       if(i2hdm.eq.1) then
          FL = (-1+4*SS)/DSQRT(SS*CS)*ghlep
       endif
       EPS=1.D-8
c      CTT = 4*XRMT**2/AMH**2*DCMPLX(1D0,-EPS)
c      CTB = 4*XRMB**2/AMH**2*DCMPLX(1D0,-EPS)
c      CTC = 4*XRMC**2/AMH**2*DCMPLX(1D0,-EPS)
       CTT = 4*AMT**2/AMH**2*DCMPLX(1D0,-EPS)
       CTB = 4*AMB**2/AMH**2*DCMPLX(1D0,-EPS)
       CTC = 4*AMC**2/AMH**2*DCMPLX(1D0,-EPS)
       CTL = 4*AMTAU**2/AMH**2*DCMPLX(1D0,-EPS)
       CTW = 4*AMW**2/AMH**2*DCMPLX(1D0,-EPS)
       CTH = 4*AMCH**2/AMH**2*DCMPLX(1D0,-EPS)
c      CLT = 4*XRMT**2/AMZ**2*DCMPLX(1D0,-EPS)
c      CLB = 4*XRMB**2/AMZ**2*DCMPLX(1D0,-EPS)
c      CLC = 4*XRMC**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLT = 4*AMT**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLB = 4*AMB**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLC = 4*AMC**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLE = 4*AMTAU**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLW = 4*AMW**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLH = 4*AMCH**2/AMZ**2*DCMPLX(1D0,-EPS)
       CAT = FT*(CI1(CTT,CLT) - CI2(CTT,CLT))
       CAB = FB*(CI1(CTB,CLB) - CI2(CTB,CLB))
       CAC = FC*(CI1(CTC,CLC) - CI2(CTC,CLC))
       CAL = FL*(CI1(CTL,CLE) - CI2(CTL,CLE))
       CAW = -1/DSQRT(TS)*(4*(3-TS)*CI2(CTW,CLW)
     .     + ((1+2/CTW)*TS - (5+2/CTW))*CI1(CTW,CLW))*GHVV
       CAH = (1-2*SS)/DSQRT(SS*CS)*AMZ**2/2/AMCH**2*CI1(CTH,CLH)*GHPM
       XFAC = CDABS(CAT+CAB+CAC+CAL+CAW+CAH)**2
       ACOUP = DSQRT(2D0)*GF*AMZ**2*SS*CS/PI**2
       HZGA = GF/(4.D0*PI*DSQRT(2.D0))*AMH**3*(ALPH/PI)*ACOUP/16.D0
     .        *XFAC*(1-AMZ**2/AMH**2)**3
      ENDIF

c      print*,'H -> Zgam',hzga
C  H ---> W W
      IF(IONWZ.EQ.0)THEN
       CALL HTOVV_HDEC(0,AMH,AMW,GAMW,HTWW)
       HWW = 3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/AMH**3*HTWW*GHVV**2
      ELSEIF(IONWZ.EQ.-1)THEN
       DLD=2D0
       DLU=2D0
       XM1 = 2D0*AMW-DLD
       XM2 = 2D0*AMW+DLU
       IF (AMH.LE.XM1) THEN
        CALL HTOVV_HDEC(0,AMH,AMW,GAMW,HTWW)
        HWW = 3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/AMH**3*HTWW*GHVV**2
       ELSEIF (AMH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        CALL HTOVV_HDEC(0,XX(1),AMW,GAMW,HTWW)
        YY(1)=3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/XX(1)**3*HTWW
        CALL HTOVV_HDEC(0,XX(2),AMW,GAMW,HTWW)
        YY(2)=3D0/2D0*GF*AMW**4/DSQRT(2D0)/PI/XX(2)**3*HTWW
        YY(3)=HVV(XX(3),AMW**2/XX(3)**2)
        YY(4)=HVV(XX(4),AMW**2/XX(4)**2)
        HWW = FINT_HDEC(AMH,XX,YY)*GHVV**2
       ELSE
        HWW=HVV(AMH,AMW**2/AMH**2)*GHVV**2
       ENDIF
      ELSE
      DLD=2D0
      DLU=2D0
      XM1 = 2D0*AMW-DLD
      XM2 = 2D0*AMW+DLU
      IF (AMH.LE.AMW) THEN
       HWW=0
      ELSE IF (AMH.LE.XM1) THEN
       CWW=3.D0*GF**2*AMW**4/16.D0/PI**3
       HWW=HV(AMW**2/AMH**2)*CWW*AMH*GHVV**2
      ELSE IF (AMH.LT.XM2) THEN
       CWW=3.D0*GF**2*AMW**4/16.D0/PI**3
       XX(1) = XM1-1D0
       XX(2) = XM1
       XX(3) = XM2
       XX(4) = XM2+1D0
       YY(1)=HV(AMW**2/XX(1)**2)*CWW*XX(1)
       YY(2)=HV(AMW**2/XX(2)**2)*CWW*XX(2)
       YY(3)=HVV(XX(3),AMW**2/XX(3)**2)
       YY(4)=HVV(XX(4),AMW**2/XX(4)**2)
       HWW = FINT_HDEC(AMH,XX,YY)*GHVV**2
      ELSE
       HWW=HVV(AMH,AMW**2/AMH**2)*GHVV**2
      ENDIF
      ENDIF

c      print*,'H -> WW',hww
C  H ---> Z Z
      IF(IONWZ.EQ.0)THEN
       CALL HTOVV_HDEC(0,AMH,AMZ,GAMZ,HTZZ)
       HZZ = 3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/AMH**3*HTZZ*GHVV**2
      ELSEIF(IONWZ.EQ.-1)THEN
       DLD=2D0
       DLU=2D0
       XM1 = 2D0*AMZ-DLD
       XM2 = 2D0*AMZ+DLU
       IF (AMH.LE.XM1) THEN
        CALL HTOVV_HDEC(0,AMH,AMZ,GAMZ,HTZZ)
        HZZ = 3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/AMH**3*HTZZ*GHVV**2
       ELSEIF (AMH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        CALL HTOVV_HDEC(0,XX(1),AMZ,GAMZ,HTZZ)
        YY(1)=3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/XX(1)**3*HTZZ
        CALL HTOVV_HDEC(0,XX(2),AMZ,GAMZ,HTZZ)
        YY(2)=3D0/4D0*GF*AMZ**4/DSQRT(2D0)/PI/XX(2)**3*HTZZ
        YY(3)=HVV(XX(3),AMZ**2/XX(3)**2)/2
        YY(4)=HVV(XX(4),AMZ**2/XX(4)**2)/2
        HZZ = FINT_HDEC(AMH,XX,YY)*GHVV**2
       ELSE
        HZZ=HVV(AMH,AMZ**2/AMH**2)/2.D0*GHVV**2
       ENDIF
      ELSE
      DLD=2D0
      DLU=2D0
      XM1 = 2D0*AMZ-DLD
      XM2 = 2D0*AMZ+DLU
      IF (AMH.LE.AMZ) THEN
       HZZ=0
      ELSE IF (AMH.LE.XM1) THEN
       CZZ=3.D0*GF**2*AMZ**4/192.D0/PI**3*(7-40/3.D0*SS+160/9.D0*SS**2)
       HZZ=HV(AMZ**2/AMH**2)*CZZ*AMH*GHVV**2
      ELSE IF (AMH.LT.XM2) THEN
       CZZ=3.D0*GF**2*AMZ**4/192.D0/PI**3*(7-40/3.D0*SS+160/9.D0*SS**2)
       XX(1) = XM1-1D0
       XX(2) = XM1
       XX(3) = XM2
       XX(4) = XM2+1D0
       YY(1)=HV(AMZ**2/XX(1)**2)*CZZ*XX(1)
       YY(2)=HV(AMZ**2/XX(2)**2)*CZZ*XX(2)
       YY(3)=HVV(XX(3),AMZ**2/XX(3)**2)/2D0
       YY(4)=HVV(XX(4),AMZ**2/XX(4)**2)/2D0
       HZZ = FINT_HDEC(AMH,XX,YY)*GHVV**2
      ELSE
       HZZ=HVV(AMH,AMZ**2/AMH**2)/2.D0*GHVV**2
      ENDIF
      ENDIF

c      print*,'H -> ZZ',hzz
C  H ---> h h
      if(i2hdm.eq.0) then
      IF(IONSH.EQ.0)THEN
      if(islhai.eq.0)then
       ZZMA = AMAR
       AMREAL = AMH
       AMA = 1.D0
       AMLOW = AMH
12345  CALL twohdmcp_hdec(TGBET)
       IF(AMLR.LT.0.D0)THEN
        AMA = AMAR + 1
        GOTO 12345
       ENDIF
       AMLOW = AMH
       AMDEL = AMREAL - AMLOW
       DLD = 0.3D0*(TGBET-1.3D0)
       DLD = DMAX1(0.1D0,DLD)
       DLU=DLD
       AMA = ZZMA
       CALL twohdmcp_hdec(TGBET)
       XM1 = 2*AML-DLD
       XM2 = 2*AML+DLU
       IF (AMH.LE.AML) THEN
        HHH=0
       ELSEIF (AMH.LT.XM1) THEN
        XH=AML**2/AMH**2
        XH1=(XH-1.D0)*(2.D0-.5D0*DLOG(XH))+(1.D0-5.D0*XH)
     .    *(DATAN((2.D0*XH-1.D0)/DSQRT(4.D0*XH-1.D0))
     .     -DATAN(1.D0/DSQRT(4.D0*XH-1.D0)))/DSQRT(4.D0*XH-1.D0)
        xh2=3*gf**2/32.d0/pi**3*amz**4/amh*glb**2*amb**2
        HHH=XH1*XH2
       ELSEIF (AMH.LT.XM2) THEN
        IFLON0 = 0
        IFLON1 = 0
        ZZMA=AMAR
        AMACRIT = AMAR
        AMA0 = AMAR
        AMA1 = AMAR
510     AMA0 = AMA0 - 1
        AMA1 = AMA1 + 1
        AMA = AMA0
        CALL twohdmcp_hdec(TGBET)
        IF(AMH.LT.2*AML) THEN
         IFLON0 = -1
        ELSE
         IFLON0 = 1
        ENDIF
        AMA = AMA1
        CALL twohdmcp_hdec(TGBET)
        IF(AMH.LT.2*AML) THEN
         IFLON1 = -1
        ELSE
         IFLON1 = 1
        ENDIF
        IF(IFLON0*IFLON1.NE.-1) GOTO 510
501     AMA = (AMA0+AMA1)/2
        CALL twohdmcp_hdec(TGBET)
        IF(AMH.LT.2*AML) THEN
         IF(IFLON0.EQ.-1) THEN
          AMA0 = AMAR
         ELSE
          AMA1 = AMAR
         ENDIF
        ELSE
         IF(IFLON0.EQ.-1) THEN
          AMA1 = AMAR
         ELSE
          AMA0 = AMAR
         ENDIF
        ENDIF
        AMACRIT = (AMA0+AMA1)/2
        DEL = 1.D-8
        AMDEL = 2*DABS(AMA1-AMA0)/(AMA1+AMA0)
        IF(AMDEL.GT.DEL) GOTO 501
       AMA = AMACRIT
       CALL twohdmcp_hdec(TGBET)
       YM1 = AMACRIT
       YM2 = AMACRIT
       AMA0 = AMACRIT
       AMA1 = AMACRIT
       DELSTEP = 1.D0
511    AMA0 = AMA0 - DELSTEP
       AMA1 = AMA1 + DELSTEP
       AMA = AMACRIT
       CALL twohdmcp_hdec(TGBET)
       IF(AMH.LT.2*AML-DLD) THEN
        IFLONC = -1
       ELSE
        IFLONC = 1
       ENDIF
       AMA = AMA0
       CALL twohdmcp_hdec(TGBET)
       IF(AMH.LT.2*AML-DLD) THEN
        IFLON0 = -1
       ELSE
        IFLON0 = 1
       ENDIF
       AMA = AMA1
       CALL twohdmcp_hdec(TGBET)
       IF(AMH.LT.2*AML-DLD) THEN
        IFLON1 = -1
       ELSE
        IFLON1 = 1
       ENDIF
       IF(IFLON0*IFLONC.NE.-1.AND.IFLONC*IFLON1.NE.-1) GOTO 511
       IF(IFLON0*IFLONC.EQ.-1) THEN
         AMA1 = AMACRIT
         IFLON1 = IFLONC
       ELSE
         AMA0 = AMACRIT
         IFLON0 = IFLONC
       ENDIF
512    AMA = (AMA0+AMA1)/2
       CALL twohdmcp_hdec(TGBET)
       IF(AMH.LT.2*AML-DLD) THEN
        IF(IFLON0.EQ.-1) THEN
         AMA0 = AMAR
        ELSE
         AMA1 = AMAR
        ENDIF
       ELSE
        IF(IFLON0.EQ.-1) THEN
         AMA1 = AMAR
        ELSE
         AMA0 = AMAR
        ENDIF
       ENDIF
       YM1 = (AMA0+AMA1)/2
       DEL = 1.D-8
       AMDEL = 2*DABS(AMA1-AMA0)/(AMA1+AMA0)
       IF(AMDEL.GT.DEL) GOTO 512
       AMA = YM1
       CALL twohdmcp_hdec(TGBET)
       AMA0 = AMACRIT
       AMA1 = AMACRIT
       DELSTEP = 1.D0
513    AMA0 = AMA0 - DELSTEP
       AMA1 = AMA1 + DELSTEP
       AMA = AMACRIT
       CALL twohdmcp_hdec(TGBET)
       IF(AMH.LT.2*AML+DLU) THEN
        IFLONC = -1
       ELSE
        IFLONC = 1
       ENDIF
       AMA = AMA0
       CALL twohdmcp_hdec(TGBET)
       IF(AMH.LT.2*AML+DLU) THEN
        IFLON0 = -1
       ELSE
        IFLON0 = 1
       ENDIF
       AMA = AMA1
       CALL twohdmcp_hdec(TGBET)
       IF(AMH.LT.2*AML+DLU) THEN
        IFLON1 = -1
       ELSE
        IFLON1 = 1
       ENDIF
       IF(IFLON0*IFLONC.NE.-1.AND.IFLONC*IFLON1.NE.-1) GOTO 513
       IF(IFLON0*IFLONC.EQ.-1) THEN
         AMA1 = AMACRIT
         IFLON1 = IFLONC
       ELSE
         AMA0 = AMACRIT
         IFLON0 = IFLONC
       ENDIF
514    AMA = (AMA0+AMA1)/2
       CALL twohdmcp_hdec(TGBET)
       IF(AMH.LT.2*AML+DLU) THEN
        IF(IFLON0.EQ.-1) THEN
         AMA0 = AMAR
        ELSE
         AMA1 = AMAR
        ENDIF
       ELSE
        IF(IFLON0.EQ.-1) THEN
         AMA1 = AMAR
        ELSE
         AMA0 = AMAR
        ENDIF
       ENDIF
       YM2 = (AMA0+AMA1)/2
       DEL = 1.D-8
       AMDEL = 2*DABS(AMA1-AMA0)/(AMA1+AMA0)
       IF(AMDEL.GT.DEL) GOTO 514
       AMA = YM2
       CALL twohdmcp_hdec(TGBET)
       DEL = 1.D-4
        XX(1) = YM1 - DEL
        XX(2) = YM1
        XX(3) = YM2
        XX(4) = YM2 + DEL
        AMAR = ZZMA
        DO J=1,4
         AMA = XX(J)
         CALL twohdmcp_hdec(TGBET)
         XX(J) = AMH
         IF(AMH.GE.2*AML)THEN
          YY(J)=GF/16D0/DSQRT(2D0)/PI*AMZ**4/XX(J)
     .          *BETA_HDEC(AML**2/XX(J)**2)
         ELSEIF(AMH.LE.AML)THEN
          YY(J) = 0
         ELSE
          XH=AML**2/XX(J)**2
          XH1=(XH-1.D0)*(2.D0-.5D0*DLOG(XH))+(1.D0-5.D0*XH)
     .    *(DATAN((2.D0*XH-1.D0)/DSQRT(4.D0*XH-1.D0))
     .     -DATAN(1.D0/DSQRT(4.D0*XH-1.D0)))/DSQRT(4.D0*XH-1.D0)
          XH2=3*GF**2/32.D0/PI**3*AMZ**4/XX(J)*GLB**2*AMB**2
          YY(J)=XH1*XH2
         ENDIF
        ENDDO
        AMA = ZZMA
        CALL twohdmcp_hdec(TGBET)
        HHH = FINT_HDEC(AMH,XX,YY)*GHLL**2
       ELSE
        HHH=GF/16D0/DSQRT(2D0)/PI*AMZ**4/AMH*BETA_HDEC(AML**2/AMH**2)
     .      *GHLL**2
        ii0 = 1
        if(imodel.eq.10)then
         ii0 = 0
        endif
        corr = h2hh_hdec(ii0)
c       corr1 = cofhll_hdec(ii0,amh,aml,amt)
c       write(6,*)'H --> hh: ',hhh,corr,corr1
c       write(6,*)'H --> hh: ',imodel,ii0,hhh,corr
        HHH=HHH*(1+corr/2)**2
       ENDIF
      else
       DLD=0.1D0
       DLU=0.1D0
       XM1 = 2D0*AML-DLD
       XM2 = 2D0*AML+DLU
       IF (AMH.LE.AML) THEN
        HHH = 0D0
       ELSEIF (AMH.LE.XM1) THEN
        XH=AML**2/AMH**2
        XH1=(XH-1.D0)*(2.D0-.5D0*DLOG(XH))+(1.D0-5.D0*XH)
     .  *(DATAN((2.D0*XH-1.D0)/DSQRT(4.D0*XH-1.D0))
     .   -DATAN(1.D0/DSQRT(4.D0*XH-1.D0)))/DSQRT(4.D0*XH-1.D0)
        XH2=3*GF**2/32.D0/PI**3*AMZ**4/AMH*GLB**2*AMB**2
        HHH=XH1*XH2
       ELSEIF (AMH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        XH=AML**2/XX(1)**2
        XH1=(XH-1.D0)*(2.D0-.5D0*DLOG(XH))+(1.D0-5.D0*XH)
     .  *(DATAN((2.D0*XH-1.D0)/DSQRT(4.D0*XH-1.D0))
     .   -DATAN(1.D0/DSQRT(4.D0*XH-1.D0)))/DSQRT(4.D0*XH-1.D0)
        XH2=3*GF**2/32.D0/PI**3*AMZ**4/XX(1)*GLB**2*AMB**2
        YY(1)=XH1*XH2
        XH=AML**2/XX(2)**2
        XH1=(XH-1.D0)*(2.D0-.5D0*DLOG(XH))+(1.D0-5.D0*XH)
     .  *(DATAN((2.D0*XH-1.D0)/DSQRT(4.D0*XH-1.D0))
     .   -DATAN(1.D0/DSQRT(4.D0*XH-1.D0)))/DSQRT(4.D0*XH-1.D0)
        XH2=3*GF**2/32.D0/PI**3*AMZ**4/XX(2)*GLB**2*AMB**2
        YY(2)=XH1*XH2
        YY(3)=GF/16D0/DSQRT(2D0)/PI*AMZ**4/XX(3)
     .        *BETA_HDEC(AML**2/XX(3)**2)
        YY(4)=GF/16D0/DSQRT(2D0)/PI*AMZ**4/XX(4)
     .        *BETA_HDEC(AML**2/XX(4)**2)
        HHH = FINT_HDEC(AMH,XX,YY)*GHLL**2
       ELSE
        HHH=GF/16D0/DSQRT(2D0)/PI*AMZ**4/AMH*BETA_HDEC(AML**2/AMH**2)
     .      *GHLL**2
       ENDIF
      endif
      ELSE
       IF (AMH.LE.2*AML) THEN
        HHH=0
       ELSE
        HHH=GF/16D0/DSQRT(2D0)/PI*AMZ**4/AMH*BETA_HDEC(AML**2/AMH**2)
     .      *GHLL**2
       ENDIF
      ENDIF
      endif

c MMM changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=0.1d0
            dlu=0.1d0
            xm1 = 2d0*aml-dld
            xm2 = 2d0*aml+dlu
            if (amh.le.aml) then
               hhh = 0d0
            elseif (amh.le.xm1) then
               xh=aml**2/amh**2
               xh1=(xh-1.d0)*(2.d0-.5d0*dlog(xh))+(1.d0-5.d0*xh)
     .              *(datan((2.d0*xh-1.d0)/dsqrt(4.d0*xh-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xh-1.d0)))
     .              /dsqrt(4.d0*xh-1.d0)
               xh2=3*gf**2/32.d0/pi**3*amz**4/amh*ghll**2*glb**2*amb**2
               hhh=xh1*xh2
            elseif (amh.le.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               xh=aml**2/xx(1)**2
               xh1=(xh-1.d0)*(2.d0-.5d0*dlog(xh))+(1.d0-5.d0*xh)
     .              *(datan((2.d0*xh-1.d0)/dsqrt(4.d0*xh-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xh-1.d0)))
     .              /dsqrt(4.d0*xh-1.d0)
               xh2=3*gf**2/32.d0/pi**3*amz**4/xx(1)*glb**2*amb**2
               yy(1)=xh1*xh2
               xh=aml**2/xx(2)**2
               xh1=(xh-1.d0)*(2.d0-.5d0*dlog(xh))+(1.d0-5.d0*xh)
     .              *(datan((2.d0*xh-1.d0)/dsqrt(4.d0*xh-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xh-1.d0)))
     .              /dsqrt(4.d0*xh-1.d0)
               xh2=3*gf**2/32.d0/pi**3*amz**4/xx(2)*glb**2*amb**2
               yy(2)=xh1*xh2
               yy(3)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(3)
     .              *beta_hdec(aml**2/xx(3)**2)
               yy(4)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(4)
     .              *beta_hdec(aml**2/xx(4)**2)
               hhh = fint_hdec(amh,xx,yy)*ghll**2
            else
               hhh=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*
     .              beta_hdec(aml**2/amh**2)*ghll**2
            endif
         else
            if (amh.le.2*aml) then
               hhh=0
            else
               hhh=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*
     .              beta_hdec(aml**2/amh**2)*ghll**2
            endif
         endif
      endif
c end MMM changed 22/8/2013

c      print*,'H -> hh',HHH,ghll

C  H ---> A A
      if(i2hdm.eq.0) then
      IF(IONSH.EQ.0)THEN
      if(islhai.eq.0)then
       DLD = 0.3D0*(TGBET-1.3D0)
       DLD = DMAX1(0.1D0,DLD)
       DLU=DLD
       ALD = DLD/2
       ALU = DLU/2
       XM1 = 2*AMA-DLD
       XM2 = 2*AMA+DLU
       IF (AMH.LE.AMA) THEN
        HAA=0
       ELSEIF (AMH.LT.XM1) THEN
        XA=AMA**2/AMH**2
        XA1=(XA-1.D0)*(2.D0-.5D0*DLOG(XA))+(1.D0-5.D0*XA)
     .    *(DATAN((2.D0*XA-1.D0)/DSQRT(4.D0*XA-1.D0))
     .     -DATAN(1.D0/DSQRT(4.D0*XA-1.D0)))/DSQRT(4.D0*XA-1.D0)
        XA2=3*GF**2/32.D0/PI**3*AMZ**4/AMH*GHAA**2*GAB**2*AMB**2
        HAA=XA1*XA2
       ELSEIF (AMH.LT.XM2) THEN
        ZZMA=AMAR
        AMACRIT = AMAR
        AMA0 = 10.D0
        AMA1 = AMAR + 50.D0
        AMA = AMA0
        CALL twohdmcp_hdec(TGBET)
        IF(AMH.LT.2*AMA) THEN
         IFLON0 = -1
        ELSEIF(AMH.EQ.2*AMA) THEN
         IFLON0 = 0
         AMACRIT = AMAR
        ELSE
         IFLON0 = 1
        ENDIF
        AMA = AMA1
        CALL twohdmcp_hdec(TGBET)
        IF(AMH.LT.2*AMA) THEN
         IFLON1 = -1
        ELSEIF(AMH.EQ.2*AMA) THEN
         IFLON1 = 0
         AMACRIT = AMAR
        ELSE
         IFLON1 = 1
        ENDIF
        IF(IFLON0*IFLON1.EQ.0)THEN
         IFLON0 = 0
         IFLON1 = 0
        ENDIF
        IF(IFLON0.NE.IFLON1)THEN
502      AMA = (AMA0+AMA1)/2
         CALL twohdmcp_hdec(TGBET)
         IF(AMH.LT.2*AMA) THEN
          IF(IFLON0.EQ.-1) THEN
           AMA0 = AMAR
          ELSE
           AMA1 = AMAR
          ENDIF
         ELSEIF(AMH.EQ.2*AMA) THEN
          IFLON0 = 0
          IFLON1 = 0
          AMACRIT = AMAR
         ELSE
          IF(IFLON0.EQ.-1) THEN
           AMA1 = AMAR
          ELSE
           AMA0 = AMAR
          ENDIF
         ENDIF
         IF(IFLON0.NE.0)THEN
          AMACRIT = (AMA0+AMA1)/2
          DEL = 1.D-8
          AMDEL = 2*DABS(AMA1-AMA0)/(AMA1+AMA0)
          IF(AMDEL.GT.DEL) GOTO 502
         ENDIF
        ENDIF
        DEL = 1.D-4
        XX(1) = AMACRIT - ALD - DEL
        XX(2) = AMACRIT - ALD
        XX(3) = AMACRIT + ALU
        XX(4) = AMACRIT + ALU + DEL
        DO J=1,4
         AMA = XX(J)
         CALL twohdmcp_hdec(TGBET)
         XX(J) = AMH
         IF(AMH.GE.2*AMA)THEN
          YY(J)=GF/16D0/DSQRT(2D0)/PI*AMZ**4/XX(J)
     .          *BETA_HDEC(AMA**2/XX(J)**2)
         ELSEIF(AMH.LE.AMA)THEN
          YY(J) = 0
         ELSE
          XA=AMA**2/XX(J)**2
          XA1=(XA-1.D0)*(2.D0-.5D0*DLOG(XA))+(1.D0-5.D0*XA)
     .    *(DATAN((2.D0*XA-1.D0)/DSQRT(4.D0*XA-1.D0))
     .     -DATAN(1.D0/DSQRT(4.D0*XA-1.D0)))/DSQRT(4.D0*XA-1.D0)
          XA2=3*GF**2/32.D0/PI**3*AMZ**4/XX(J)*GAB**2*AMB**2
          YY(J)=XA1*XA2
         ENDIF
        ENDDO
        AMA = ZZMA
        CALL twohdmcp_hdec(TGBET)
        HAA = FINT_HDEC(AMH,XX,YY)*GHAA**2
       ELSE
        HAA=GF/16D0/DSQRT(2D0)/PI*AMZ**4/AMH*BETA_HDEC(AMA**2/AMH**2)
     .       *GHAA**2
       ENDIF
      else
       DLD=0.1D0
       DLU=0.1D0
       XM1 = 2D0*AMA-DLD
       XM2 = 2D0*AMA+DLU
       IF (AMH.LE.AMA) THEN
        HAA = 0D0
       ELSEIF (AMH.LE.XM1) THEN
        XA=AMA**2/AMH**2
        XA1=(XA-1.D0)*(2.D0-.5D0*DLOG(XA))+(1.D0-5.D0*XA)
     .    *(DATAN((2.D0*XA-1.D0)/DSQRT(4.D0*XA-1.D0))
     .     -DATAN(1.D0/DSQRT(4.D0*XA-1.D0)))/DSQRT(4.D0*XA-1.D0)
        XA2=3*GF**2/32.D0/PI**3*AMZ**4/AMH*GHAA**2*GAB**2*AMB**2
        HAA=XA1*XA2
       ELSEIF (AMH.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        XA=AMA**2/XX(1)**2
        XA1=(XA-1.D0)*(2.D0-.5D0*DLOG(XA))+(1.D0-5.D0*XA)
     .  *(DATAN((2.D0*XA-1.D0)/DSQRT(4.D0*XA-1.D0))
     .   -DATAN(1.D0/DSQRT(4.D0*XA-1.D0)))/DSQRT(4.D0*XA-1.D0)
        XA2=3*GF**2/32.D0/PI**3*AMZ**4/XX(1)*GAB**2*AMB**2
        YY(1)=XA1*XA2
        XA=AMA**2/XX(2)**2
        XA1=(XA-1.D0)*(2.D0-.5D0*DLOG(XA))+(1.D0-5.D0*XA)
     .  *(DATAN((2.D0*XA-1.D0)/DSQRT(4.D0*XA-1.D0))
     .   -DATAN(1.D0/DSQRT(4.D0*XA-1.D0)))/DSQRT(4.D0*XA-1.D0)
        XA2=3*GF**2/32.D0/PI**3*AMZ**4/XX(2)*GAB**2*AMB**2
        YY(2)=XA1*XA2
        YY(3)=GF/16D0/DSQRT(2D0)/PI*AMZ**4/XX(3)
     .        *BETA_HDEC(AMA**2/XX(3)**2)
        YY(4)=GF/16D0/DSQRT(2D0)/PI*AMZ**4/XX(4)
     .        *BETA_HDEC(AMA**2/XX(4)**2)
        HAA = FINT_HDEC(AMH,XX,YY)*GHAA**2
       ELSE
        HAA=GF/16D0/DSQRT(2D0)/PI*AMZ**4/AMH*BETA_HDEC(AMA**2/AMH**2)
     .       *GHAA**2
       ENDIF
      endif
      ELSE
       IF (AMH.LE.2*AMA) THEN
        HAA=0
       ELSE
        HAA=GF/16D0/DSQRT(2D0)/PI*AMZ**4/AMH*BETA_HDEC(AMA**2/AMH**2)
     .       *GHAA**2
       ENDIF
      ENDIF
      endif

c MMM changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=0.1d0
            dlu=0.1d0
            xm1 = 2d0*ama-dld
            xm2 = 2d0*ama+dlu
            if (amh.le.ama) then
               haa = 0d0
            elseif (amh.le.xm1) then
               xa=ama**2/amh**2
               xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .              *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xa-1.d0)))
     .              /dsqrt(4.d0*xa-1.d0)
               xa2=3*gf**2/32.d0/pi**3*amz**4/amh*ghaa**2*gab**2*amb**2
               haa=xa1*xa2
            elseif (amh.le.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               xa=ama**2/xx(1)**2
               xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .              *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xa-1.d0)))
     .              /dsqrt(4.d0*xa-1.d0)
               xa2=3*gf**2/32.d0/pi**3*amz**4/xx(1)*gab**2*amb**2
               yy(1)=xa1*xa2
               xa=ama**2/xx(2)**2
               xa1=(xa-1.d0)*(2.d0-.5d0*dlog(xa))+(1.d0-5.d0*xa)
     .              *(datan((2.d0*xa-1.d0)/dsqrt(4.d0*xa-1.d0))
     .              -datan(1.d0/dsqrt(4.d0*xa-1.d0)))
     .              /dsqrt(4.d0*xa-1.d0)
               xa2=3*gf**2/32.d0/pi**3*amz**4/xx(2)*gab**2*amb**2
               yy(2)=xa1*xa2
               yy(3)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(3)
     .              *beta_hdec(ama**2/xx(3)**2)
               yy(4)=gf/16d0/dsqrt(2d0)/pi*amz**4/xx(4)
     .              *beta_hdec(ama**2/xx(4)**2)
               haa = fint_hdec(amh,xx,yy)*ghaa**2
            else
               haa=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*
     .              beta_hdec(ama**2/amh**2)*ghaa**2
            endif
         else
            if (amh.le.2*ama) then
               haa=0
            else
               haa=gf/16d0/dsqrt(2d0)/pi*amz**4/amh*
     .              beta_hdec(ama**2/amh**2)*ghaa**2
            endif
         endif
      endif
c end MMM changed 22/8/2013

c      print*,'H -> AA',haa,ghaa

C  H ---> H+ H- 

      if(i2hdm.eq.1) then
         if (amh.le.2*amch) then
            hchch=0.D0
         else
            hchch=gf/8d0/dsqrt(2d0)/pi*amz**4/amh*
     .           beta_hdec(amch**2/amh**2)*ghpm**2
         endif
      elseif(i2hdm.eq.0) then
         hchch=0.D0
      endif

c      print*,'H -> H+H-',hchch,ghpm

C  H ---> A Z
      if(i2hdm.eq.0)then
      IF(IONSH.EQ.0)THEN
      if(islhai.eq.0)then
       DLD=1D0
       DLU=8D0
       XM1 = AMA+AMZ-DLD
       XM2 = AMA+AMZ+DLU
       IF (AMH.LT.AMA) THEN
        HAZ=0
       ELSEIF (AMH.LT.XM1) THEN
        IF(AMH.LE.DABS(AMZ-AMA))THEN
         HAZ=0
        ELSE
        HAZ=9.D0*GF**2/8.D0/PI**3*AMZ**4*AMH*GZAH**2*
     .      (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .      *HVH((AMA/AMH)**2,(AMZ/AMH)**2)
        ENDIF
       ELSEIF (AMH.LT.XM2) THEN
        ZZMA=AMAR
165     AMA = AMAR - 1.D0
        CALL twohdmcp_hdec(TGBET)
        IF(AMH.LT.AMA+AMZ+DLU.AND.AMH.GT.AMA+AMZ-DLD) GOTO 165
        XX(1) = AMAR-1D0
        XX(2) = AMAR
        AMA = ZZMA
        CALL twohdmcp_hdec(TGBET)
166     AMA = AMAR + 1.D0
        CALL twohdmcp_hdec(TGBET)
        IF(AMH.LT.AMA+AMZ+DLU.AND.AMH.GT.AMA+AMZ-DLD) GOTO 166
        XX(3) = AMAR
        XX(4) = AMAR+1D0
        DO IJ=1,4
         AMA = XX(IJ)
         CALL twohdmcp_hdec(TGBET)
         XX(IJ) = AMH
         IF(AMH.LE.AMA+AMZ) THEN
          YY(IJ)=9.D0*GF**2/8.D0/PI**3*AMZ**4*XX(IJ)*
     .          (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .          *HVH((AMA/XX(IJ))**2,(AMZ/XX(IJ))**2)
         ELSE
          CAZ=LAMB_HDEC(AMA**2/XX(IJ)**2,AMZ**2/XX(IJ)**2)
     .       *LAMB_HDEC(XX(IJ)**2/AMZ**2,AMA**2/AMZ**2)**2
          YY(IJ)=GF/8.D0/DSQRT(2D0)/PI*AMZ**4/XX(IJ)*CAZ
         ENDIF
        ENDDO
        AMA = ZZMA
        CALL twohdmcp_hdec(TGBET)
        HAZ = FINT_HDEC(AMH,XX,YY)*GZAH**2
       ELSE
        CAZ=LAMB_HDEC(AMA**2/AMH**2,AMZ**2/AMH**2)
     .     *LAMB_HDEC(AMH**2/AMZ**2,AMA**2/AMZ**2)**2
        HAZ=GF/8.D0/DSQRT(2D0)/PI*AMZ**4/AMH*CAZ*GZAH**2
       ENDIF
      else
       DLD=1D0
       DLU=8D0
       XM1 = AMA+AMZ-DLD
       XM2 = AMA+AMZ+DLU
       IF (AMH.LT.AMA) THEN
        HAZ=0
       ELSEIF (AMH.LT.XM1) THEN
        IF(AMH.LE.DABS(AMZ-AMA))THEN
         HAZ=0
        ELSE
        HAZ=9.D0*GF**2/8.D0/PI**3*AMZ**4*AMH*GZAH**2*
     .      (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .      *HVH((AMA/AMH)**2,(AMZ/AMH)**2)
        ENDIF
       ELSEIF (AMH.LT.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        YY(1)=9.D0*GF**2/8.D0/PI**3*AMZ**4*XX(1)*
     .        (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .        *HVH((AMA/XX(1))**2,(AMZ/XX(1))**2)
        YY(2)=9.D0*GF**2/8.D0/PI**3*AMZ**4*XX(2)*
     .        (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .        *HVH((AMA/XX(2))**2,(AMZ/XX(2))**2)
        CAZ=LAMB_HDEC(AMA**2/XX(3)**2,AMZ**2/XX(3)**2)
     .     *LAMB_HDEC(XX(3)**2/AMZ**2,AMA**2/AMZ**2)**2
        YY(3)=GF/8.D0/DSQRT(2D0)/PI*AMZ**4/XX(3)*CAZ
        CAZ=LAMB_HDEC(AMA**2/XX(4)**2,AMZ**2/XX(4)**2)
     .     *LAMB_HDEC(XX(4)**2/AMZ**2,AMA**2/AMZ**2)**2
        YY(4)=GF/8.D0/DSQRT(2D0)/PI*AMZ**4/XX(4)*CAZ
        HAZ = FINT_HDEC(AMH,XX,YY)*GZAH**2
       ELSE
        CAZ=LAMB_HDEC(AMA**2/AMH**2,AMZ**2/AMH**2)
     .     *LAMB_HDEC(AMH**2/AMZ**2,AMA**2/AMZ**2)**2
        HAZ=GF/8.D0/DSQRT(2D0)/PI*AMZ**4/AMH*CAZ*GZAH**2
       ENDIF
      endif
      ELSE
       IF (AMH.LT.AMZ+AMA) THEN
        HAZ=0
       ELSE
        CAZ=LAMB_HDEC(AMA**2/AMH**2,AMZ**2/AMH**2)
     .     *LAMB_HDEC(AMH**2/AMZ**2,AMA**2/AMZ**2)**2
        HAZ=GF/8.D0/DSQRT(2D0)/PI*AMZ**4/AMH*CAZ*GZAH**2
       ENDIF
      ENDIF
      endif

c MMM changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=1d0
            dlu=8d0
            xm1 = ama+amz-dld
            xm2 = ama+amz+dlu
            if (amh.lt.ama) then
               haz=0
            elseif (amh.lt.xm1) then
               if(amh.le.dabs(amz-ama))then
                  haz=0
               else
                  haz=9.d0*gf**2/8.d0/pi**3*amz**4*amh*gzah**2*
     .                 (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .                 *hvh((ama/amh)**2,(amz/amh)**2)
               endif
            elseif (amh.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(1)*
     .              (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .              *hvh((ama/xx(1))**2,(amz/xx(1))**2)
               yy(2)=9.d0*gf**2/8.d0/pi**3*amz**4*xx(2)*
     .              (7.d0/12.d0-10.d0/9.d0*ss+40.d0/27.d0*ss**2)
     .              *hvh((ama/xx(2))**2,(amz/xx(2))**2)
               caz=lamb_hdec(ama**2/xx(3)**2,amz**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amz**2,ama**2/amz**2)**2
               yy(3)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(3)*caz
               caz=lamb_hdec(ama**2/xx(4)**2,amz**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amz**2,ama**2/amz**2)**2
               yy(4)=gf/8.d0/dsqrt(2d0)/pi*amz**4/xx(4)*caz
               haz = fint_hdec(amh,xx,yy)*gzah**2
            else
               caz=lamb_hdec(ama**2/amh**2,amz**2/amh**2)
     .              *lamb_hdec(amh**2/amz**2,ama**2/amz**2)**2
               haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/amh*caz*gzah**2
            endif
         else
            if (amh.lt.amz+ama) then
               haz=0
            else
               caz=lamb_hdec(ama**2/amh**2,amz**2/amh**2)
     .              *lamb_hdec(amh**2/amz**2,ama**2/amz**2)**2
               haz=gf/8.d0/dsqrt(2d0)/pi*amz**4/amh*caz*gzah**2
            endif
         endif
      endif
c end MMM changed 22/8/2013

c      print*,'H -> AZ',haz

C  H ---> H+ W-
      if(i2hdm.eq.0)then
      IF(IONSH.EQ.0)THEN
      if(islhai.eq.0)then
       DLD=3D0
       DLU=9D0
       XM1 = AMCH+AMW-DLD
       XM2 = AMCH+AMW+DLU
       IF (AMH.LT.AMCH) THEN
        HHW=0.D0
       ELSEIF (AMH.LT.XM1) THEN
        IF(AMH.LE.DABS(AMW-AMCH))THEN
         HHW=0
        ELSE
        HHW=9.D0*GF**2/16.D0/PI**3*AMW**4*AMH*GLVV**2*2
     .      *HVH((AMCH/AMH)**2,(AMW/AMH)**2)
        ENDIF
       ELSEIF (AMH.LT.XM2) THEN
        ZZMA=AMAR
167     AMA = AMAR - 1.D0
        CALL twohdmcp_hdec(TGBET)
        IF(AMH.LT.AMCH+AMW+DLU) GOTO 167
        XX(1) = AMAR-1D0
        XX(2) = AMAR
        AMA = ZZMA
        CALL twohdmcp_hdec(TGBET)
168     AMA = AMAR + 1.D0
        CALL twohdmcp_hdec(TGBET)
        IF(AMH.GT.AMCH+AMW-DLD) GOTO 168
        XX(3) = AMAR
        XX(4) = AMAR+1D0
        AMA = XX(1)
        CALL twohdmcp_hdec(TGBET)
        XX(1) = AMH
        CHW=LAMB_HDEC(AMCH**2/XX(1)**2,AMW**2/XX(1)**2)
     .     *LAMB_HDEC(XX(1)**2/AMW**2,AMCH**2/AMW**2)**2
        YY(1)=2*GF/8.D0/DSQRT(2D0)/PI*AMW**4/XX(1)*CHW
        AMA = XX(2)
        CALL twohdmcp_hdec(TGBET)
        XX(2) = AMH
        CHW=LAMB_HDEC(AMCH**2/XX(2)**2,AMW**2/XX(2)**2)
     .     *LAMB_HDEC(XX(2)**2/AMW**2,AMCH**2/AMW**2)**2
        YY(2)=2*GF/8.D0/DSQRT(2D0)/PI*AMW**4/XX(2)*CHW
        AMA = XX(3)
        CALL twohdmcp_hdec(TGBET)
        XX(3) = AMH
        YY(3)=9.D0*GF**2/16.D0/PI**3*AMW**4*XX(3)*2
     .       *HVH((AMCH/XX(3))**2,(AMW/XX(3))**2)
        AMA = XX(4)
        CALL twohdmcp_hdec(TGBET)
        XX(4) = AMH
        YY(4)=9.D0*GF**2/16.D0/PI**3*AMW**4*XX(4)*2
     .       *HVH((AMCH/XX(4))**2,(AMW/XX(4))**2)
        AMA = ZZMA
        CALL twohdmcp_hdec(TGBET)
        HHW=FINT_HDEC(AMH,XX,YY)*GLVV**2
       ELSE
        CHW=LAMB_HDEC(AMCH**2/AMH**2,AMW**2/AMH**2)
     .     *LAMB_HDEC(AMH**2/AMW**2,AMCH**2/AMW**2)**2
        HHW=2*GF/8.D0/DSQRT(2D0)/PI*AMW**4/AMH*CHW*GLVV**2
       ENDIF
      else
       DLD=3D0
       DLU=9D0
       XM1 = AMCH+AMW-DLD
       XM2 = AMCH+AMW+DLU
       IF (AMH.LT.AMCH) THEN
        HHW=0.D0
       ELSEIF (AMH.LT.XM1) THEN
        IF(AMH.LE.DABS(AMW-AMCH))THEN
         HHW=0
        ELSE
        HHW=9.D0*GF**2/16.D0/PI**3*AMW**4*AMH*GLVV**2*2
     .      *HVH((AMCH/AMH)**2,(AMW/AMH)**2)
        ENDIF
       ELSEIF (AMH.LT.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        YY(1)=9.D0*GF**2/16.D0/PI**3*AMW**4*XX(1)*2
     .       *HVH((AMCH/XX(1))**2,(AMW/XX(1))**2)
        YY(2)=9.D0*GF**2/16.D0/PI**3*AMW**4*XX(2)*2
     .       *HVH((AMCH/XX(2))**2,(AMW/XX(2))**2)
        CHW=LAMB_HDEC(AMCH**2/XX(3)**2,AMW**2/XX(3)**2)
     .     *LAMB_HDEC(XX(3)**2/AMW**2,AMCH**2/AMW**2)**2
        YY(3)=2*GF/8.D0/DSQRT(2D0)/PI*AMW**4/XX(3)*CHW
        CHW=LAMB_HDEC(AMCH**2/XX(4)**2,AMW**2/XX(4)**2)
     .     *LAMB_HDEC(XX(4)**2/AMW**2,AMCH**2/AMW**2)**2
        YY(4)=2*GF/8.D0/DSQRT(2D0)/PI*AMW**4/XX(4)*CHW
        HHW=FINT_HDEC(AMH,XX,YY)*GLVV**2
       ELSE
        CHW=LAMB_HDEC(AMCH**2/AMH**2,AMW**2/AMH**2)
     .     *LAMB_HDEC(AMH**2/AMW**2,AMCH**2/AMW**2)**2
        HHW=2*GF/8.D0/DSQRT(2D0)/PI*AMW**4/AMH*CHW*GLVV**2
       ENDIF
      endif
      ELSE
       IF (AMH.LT.AMW+AMCH) THEN
        HHW=0.D0
       ELSE
        CHW=LAMB_HDEC(AMCH**2/AMH**2,AMW**2/AMH**2)
     .     *LAMB_HDEC(AMH**2/AMW**2,AMCH**2/AMW**2)**2
        HHW=2*GF/8.D0/DSQRT(2D0)/PI*AMW**4/AMH*CHW*GLVV**2
       ENDIF
      ENDIF
      endif

c MMM changed 22/8/2013
      if(i2hdm.eq.1) then
         if(ionsh.eq.0) then
            dld=3d0
            dlu=9d0
            xm1 = amch+amw-dld
            xm2 = amch+amw+dlu
            if (amh.lt.amch) then
               hhw=0.d0
            elseif (amh.lt.xm1) then
               if(amh.le.dabs(amw-amch))then
                  hhw=0
               else
                  hhw=9.d0*gf**2/16.d0/pi**3*amw**4*amh*glvv**2*2
     .                 *hvh((amch/amh)**2,(amw/amh)**2)
               endif
            elseif (amh.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)*2
     .              *hvh((amch/xx(1))**2,(amw/xx(1))**2)
               yy(2)=9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)*2
     .              *hvh((amch/xx(2))**2,(amw/xx(2))**2)
               chw=lamb_hdec(amch**2/xx(3)**2,amw**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amw**2,amch**2/amw**2)**2
               yy(3)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*chw
               chw=lamb_hdec(amch**2/xx(4)**2,amw**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amw**2,amch**2/amw**2)**2
               yy(4)=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*chw
               hhw=fint_hdec(amh,xx,yy)*glvv**2
            else
               chw=lamb_hdec(amch**2/amh**2,amw**2/amh**2)
     .              *lamb_hdec(amh**2/amw**2,amch**2/amw**2)**2
               hhw=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/amh*chw*glvv**2
            endif
         else
            if (amh.lt.amw+amch) then
               hhw=0.d0
            else
               chw=lamb_hdec(amch**2/amh**2,amw**2/amh**2)
     .              *lamb_hdec(amh**2/amw**2,amch**2/amw**2)**2
               hhw=2*gf/8.d0/dsqrt(2d0)/pi*amw**4/amh*chw*glvv**2
            endif
         endif
      endif
c end MMM changed 22/8/2013

c      print*,'H -> H+W-',hhw,hhw/2.D0

C ========================== SUSY DECAYS 
C
      IF(IOFSUSY.EQ.0) THEN
C  HH ----> CHARGINOS
      DO 741 I=1,2
      DO 741 J=1,2
      IF (AMH.GT.AMCHAR(I)+AMCHAR(J)) THEN
      WHHCH(I,J)=GF*AMW**2/(2*PI*DSQRT(2.D0))/AMH 
     .     *LAMB_HDEC(AMCHAR(I)**2/AMH**2,AMCHAR(J)**2/AMH**2)
     .     *( (AC1(I,J)**2+AC1(J,I)**2)*(AMH**2-AMCHAR(I)
     .         **2-AMCHAR(J)**2)-4.D0*AC1(I,J)*AC1(J,I)* 
     .         XMCHAR(I)*XMCHAR(J) ) 
      ELSE
      WHHCH(I,J)=0.D0
      ENDIF
 741  CONTINUE
      WHHCHT=WHHCH(1,1)+WHHCH(1,2)+WHHCH(2,1)+WHHCH(2,2)
C
C  HH ----> NEUTRALINOS 
      DO 742 I=1,4
      DO 742 J=1,4
      IF (AMH.GT.AMNEUT(I)+AMNEUT(J)) THEN
      WHHNE(I,J)=GF*AMW**2/(2*PI*DSQRT(2.D0))/AMH
     .         *AN1(I,J)**2*(AMH**2-(XMNEUT(I)+XMNEUT(J))**2)
     .         *LAMB_HDEC(AMNEUT(I)**2/AMH**2,AMNEUT(J)**2/AMH**2)
      ELSE 
      WHHNE(I,J)=0.D0
      ENDIF
 742  CONTINUE
      WHHNET= WHHNE(1,1)+WHHNE(1,2)+WHHNE(1,3)+WHHNE(1,4)
     .       +WHHNE(2,1)+WHHNE(2,2)+WHHNE(2,3)+WHHNE(2,4)
     .       +WHHNE(3,1)+WHHNE(3,2)+WHHNE(3,3)+WHHNE(3,4)
     .       +WHHNE(4,1)+WHHNE(4,2)+WHHNE(4,3)+WHHNE(4,4)
C
C  HH ----> SLEPTONS 
C
      IF (AMH.GT.2.D0*AMSE(1)) THEN
      WHHSLEL=2*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AMH*DCOS(B+A)**2
     .      *BETA_HDEC(AMSE(1)**2/AMH**2)*(-0.5D0+SS)**2
      ELSE
      WHHSLEL=0.D0
      ENDIF

      IF (AMH.GT.2.D0*AMSE(2)) THEN
      WHHSLER=2*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AMH*DCOS(B+A)**2
     .      *BETA_HDEC(AMSE(2)**2/AMH**2)*SS**2
      ELSE
      WHHSLER=0.D0
      ENDIF

      WHHSLNL=0.D0
      IF (AMH.GT.2.D0*AMSN1(1)) THEN
      WHHSLNL=2*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AMH*DCOS(B+A)**2
     .      *BETA_HDEC(AMSN1(1)**2/AMH**2)*0.5D0**2
      ENDIF
      IF (AMH.GT.2.D0*AMSN(1)) THEN
      WHHSLNL=WHHSLNL + GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AMH*DCOS(B+A)**2
     .      *BETA_HDEC(AMSN(1)**2/AMH**2)*0.5D0**2
      ENDIF

      DO 748 I=1,2
      DO 748 J=1,2
      IF(AMH.GT.AMSL(I)+AMSL(J)) THEN
      WHHSTAU(I,J)=GF*AMZ**4/2.D0/DSQRT(2.D0)/PI*GHEE(I,J)**2*
     .      LAMB_HDEC(AMSL(I)**2/AMH**2,AMSL(J)**2/AMH**2)/AMH
      ELSE
      WHHSTAU(I,J)=0.D0
      ENDIF
 748  CONTINUE

      WHHSLT=WHHSTAU(1,1)+WHHSTAU(1,2)+WHHSTAU(2,1)+WHHSTAU(2,2) 
     .       +WHHSLEL+WHHSLER+WHHSLNL
C
C  HH ----> SQUARKS 
C
      IF (AMH.GT.2.D0*AMSU(1)) THEN
      WHHSQUL=6*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AMH*DCOS(B+A)**2
     .      *BETA_HDEC(AMSU(1)**2/AMH**2)*(0.5D0-2.D0/3.D0*SS)**2
      ELSE
      WHHSQUL=0.D0
      ENDIF

      IF (AMH.GT.2.D0*AMSU(2)) THEN
      WHHSQUR=6*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AMH*DCOS(B+A)**2
     .      *BETA_HDEC(AMSU(2)**2/AMH**2)*(-2.D0/3.D0*SS)**2
      ELSE
      WHHSQUR=0.D0
      ENDIF

      IF (AMH.GT.2.D0*AMSD(1)) THEN
      WHHSQDL=6*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AMH*DCOS(B+A)**2
     .      *BETA_HDEC(AMSD(1)**2/AMH**2)*(-0.5D0+1.D0/3.D0*SS)**2
      ELSE
      WHHSQDL=0.D0
      ENDIF

      IF (AMH.GT.2.D0*AMSD(2)) THEN
      WHHSQDR=6*GF/2.D0/DSQRT(2D0)/PI*AMZ**4/AMH*DCOS(B+A)**2
     .      *BETA_HDEC(AMSD(2)**2/AMH**2)*(+1.D0/3.D0*SS)**2
      ELSE
      WHHSQDR=0.D0
      ENDIF

      WHHSQ=WHHSQUL+WHHSQUR+WHHSQDL+WHHSQDR
C
C  HH ----> STOPS 
      SUSY = 1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     DO K=-10,10
c     DO K=-1,1
c     DO I=1,2
c     DO J=1,2
c     QSQ = AMH*10.D0**(K/10.D0)
c     QSQ = AMH*2.D0**(K)
c     IF(AMH.GT.YMST(I)+YMST(J)) THEN
c      CALL SQMBAPP_HDEC(QSQ)
c      SUSY = 1+SQSUSY_HDEC(2,1,I,J,QSQ,0,1)
c      WHHST0(I,J)=3*GF*AMZ**4/2.D0/DSQRT(2.D0)/PI*YHTT(I,J)**2*
c    .       LAMB_HDEC(YMST(I)**2/AMH**2,YMST(J)**2/AMH**2)/AMH
c      WHHST(I,J)=3*GF*AMZ**4/2.D0/DSQRT(2.D0)/PI*YHTT(I,J)**2*
c    .       LAMB_HDEC(YMST(I)**2/AMH**2,YMST(J)**2/AMH**2)/AMH
c    .      *SUSY
c     ELSE
c     WHHST(I,J)=0.D0
c     ENDIF
c     ENDDO
c     ENDDO
c     write(9,*)'H -> t1 t1: ',QSQ/AMH,WHHST0(1,1),WHHST(1,1)
c     write(9,*)'numbers: ',3*GF/2.D0/DSQRT(2.D0)/PI/AMH,
c    .YHTT(1,1)**2*AMZ**4,LAMB_HDEC(YMST(1)**2/AMH**2,YMST(1)**2/AMH**2)
c    .,YHTT(1,1)*AMZ**2
c    .,3*GF/2.D0/DSQRT(2.D0)/PI/AMH*
c    .YHTT(1,1)**2*AMZ**4*LAMB_HDEC(YMST(1)**2/AMH**2,YMST(1)**2/AMH**2)
c     write(9,*)'H -> t1 t2: ',QSQ/AMH,WHHST0(1,2),WHHST(1,2)
c     write(9,*)'H -> t2 t2: ',QSQ/AMH,WHHST0(2,2),WHHST(2,2)
c     write(901,('1X,G10.4,6(1X,G10.4)'))QSQ/AMH,WHHST0(1,1),WHHST(1,1),
c    .                    WHHST0(1,2),WHHST(1,2),WHHST0(2,2),WHHST(2,2)
c     ENDDO
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      DO 743 I=1,2
      DO 743 J=1,2
c     QSQ = (YMST(I)+YMST(J))/2
      QSQ = AMH
      IF(AMH.GT.YMST(I)+YMST(J)) THEN
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CALL SQMBAPP_HDEC(QSQ)
       SUSY = 1+SQSUSY_HDEC(2,1,I,J,QSQ,0,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       WHHST(I,J)=3*GF*AMZ**4/2.D0/DSQRT(2.D0)/PI*YHTT(I,J)**2*
     .       LAMB_HDEC(YMST(I)**2/AMH**2,YMST(J)**2/AMH**2)/AMH
     .      *SUSY
c     write(6,*)'H -> stop: ',I,J,AMH,YMST(I),YMST(J),100*(SUSY-1),'% ',
c    .          WHHST(I,J)/SUSY,WHHST(I,J)
c      if(i.eq.1.and.j.eq.1)write(511,*)AMH,WHHST(I,J),WHHST(I,J)/SUSY
c      if(i.eq.1.and.j.eq.2)write(512,*)AMH,WHHST(I,J),WHHST(I,J)/SUSY
c      if(i.eq.2.and.j.eq.1)write(521,*)AMH,WHHST(I,J),WHHST(I,J)/SUSY
c      if(i.eq.2.and.j.eq.2)write(522,*)AMH,WHHST(I,J),WHHST(I,J)/SUSY
c      write(6,*)'H -> stop: ',I,J,AMH,YMST(I),YMST(J),SUSY-1
      ELSE
      WHHST(I,J)=0.D0
      ENDIF
 743  CONTINUE
C
C  HH ----> SBOTTOMS 
      SUSY = 1
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     DO K=-10,10
c     DO K=-1,1
c     DO I=1,2
c     DO J=1,2
c     QSQ = AMH*10.D0**(K/10.D0)
c     QSQ = AMH*2.D0**(K)
c     IF(AMH.GT.YMSB(I)+YMSB(J)) THEN
c      CALL SQMBAPP_HDEC(QSQ)
c      SUSY = 1+SQSUSY_HDEC(2,2,I,J,QSQ,0,1)
c      WHHSB0(I,J)=3*GF*AMZ**4/2.D0/DSQRT(2.D0)/PI*YHBB(I,J)**2*
c    .       LAMB_HDEC(YMSB(I)**2/AMH**2,YMSB(J)**2/AMH**2)/AMH
c      WHHSB(I,J)=3*GF*AMZ**4/2.D0/DSQRT(2.D0)/PI*YHBB(I,J)**2*
c    .       LAMB_HDEC(YMSB(I)**2/AMH**2,YMSB(J)**2/AMH**2)/AMH
c    .      *SUSY
c     ELSE
c      WHHSB(I,J)=0.D0
c     ENDIF
c     ENDDO
c     ENDDO
c     write(9,*)'H -> b1 b1: ',QSQ/AMH,WHHSB0(1,1),WHHSB(1,1)
c     write(9,*)'H -> b1 b2: ',QSQ/AMH,WHHSB0(1,2),WHHSB(1,2)
c     write(9,*)'H -> b2 b2: ',QSQ/AMH,WHHSB0(2,2),WHHSB(2,2)
c     write(902,('1X,G10.4,6(1X,G10.4)'))QSQ/AMH,WHHSB0(1,1),WHHSB(1,1),
c    .        WHHSB0(1,2),WHHSB(1,2),WHHSB0(2,2),WHHSB(2,2)
c     ENDDO
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     dummy0 = 0
c     dummy1 = 0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      DO 744 I=1,2
      DO 744 J=1,2
c     QSQ = (YMSB(I)+YMSB(J))/2
      QSQ = AMH
      IF(AMH.GT.YMSB(I)+YMSB(J)) THEN
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CALL SQMBAPP_HDEC(QSQ)
       SUSY = 1+SQSUSY_HDEC(2,2,I,J,QSQ,0,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       WHHSB(I,J)=3*GF*AMZ**4/2.D0/DSQRT(2.D0)/PI*YHBB(I,J)**2*
     .       LAMB_HDEC(YMSB(I)**2/AMH**2,YMSB(J)**2/AMH**2)/AMH
     .      *SUSY
c     write(6,*)'H -> sbot: ',I,J,AMH,YMSB(I),YMSB(J),100*(SUSY-1),'% ',
c    .          WHHSB(I,J)/SUSY,WHHSB(I,J)
c      if(i.eq.1.and.j.eq.1)write(611,*)AMH,WHHSB(I,J),WHHSB(I,J)/SUSY
c      if(i.eq.1.and.j.eq.2)write(612,*)AMH,WHHSB(I,J),WHHSB(I,J)/SUSY
c      if(i.eq.2.and.j.eq.1)write(621,*)AMH,WHHSB(I,J),WHHSB(I,J)/SUSY
c      if(i.eq.2.and.j.eq.2)write(622,*)AMH,WHHSB(I,J),WHHSB(I,J)/SUSY
c      write(6,*)'H -> sbot: ',I,J,AMH,YMSB(I),YMSB(J),SUSY-1
c      dummy0 = dummy0 + WHHSB(I,J)/SUSY
c      dummy1 = dummy1 + WHHSB(I,J)
      ELSE
      WHHSB(I,J)=0.D0
      ENDIF
 744  CONTINUE
c     write(6,*)'H -> sbot0: ',YMSB(1),YMSB(2),dummy0,dummy1,
c    .                        100*(dummy1/dummy0-1),'%'
C
      WHHSTT=WHHST(1,1)+WHHST(1,2)+WHHST(2,1)+WHHST(2,2) 
      WHHSBB=WHHSB(1,1)+WHHSB(1,2)+WHHSB(2,1)+WHHSB(2,2) 
      WHHSQT=WHHSTT+WHHSBB+WHHSQ
C
      ELSE 
      WHHCHT=0.D0
      WHHNET=0.D0
      WHHSLT=0.D0
      WHHSQT=0.D0
C--Change thanks to Elzbieta Richter-Was
      DO I=1,2
       DO J=1,2
        WHHCH(I,J)=0.D0
        WHHST(I,J)=0.D0
        WHHSB(I,J)=0.D0
        WHHSTAU(I,J)=0.D0
       ENDDO
      ENDDO
      DO I=1,4
       DO J=1,4
        WHHNE(I,J)=0.D0
       ENDDO
      ENDDO
      ENDIF

      IF(IGOLD.NE.0)THEN
C   HH ---> GOLDSTINOS
       DO 740 I=1,4
       IF (AMH.GT.AMNEUT(I)) THEN
        WHHGD(I)=AMH**5/AXMPL**2/AXMGD**2/48.D0/PI*
     .           (1.D0-AMNEUT(I)**2/AMH**2)**4*AGDH(I)**2
       ELSE
        WHHGD(I)=0.D0
       ENDIF
 740   CONTINUE
       WHHGDT=WHHGD(1)+WHHGD(2)+WHHGD(3)+WHHGD(4)
      ELSE
       WHHGDT=0
      ENDIF
C
C    ==========  TOTAL WIDTH AND BRANCHING RATIOS 
      WTOT=HLL+HMM+HSS+HCC+HBB+HTT+HGG+HGA+HZGA+HWW+HZZ+HHH+HAA+HAZ
     .    +HHW+WHHCHT+WHHNET+WHHSLT+WHHSQT + WHHGDT

c     write(6,*)'H: ',WTOT
c     write(6,*)'H: ',HLL,HMM,HSS
c     write(6,*)'H: ',HCC,HBB,HTT
c     write(6,*)'H: ',HGG,HGA,HZGA
c     write(6,*)'H: ',HWW,HZZ,HHH
c     write(6,*)'H: ',HAA,HAZ,HHW
c     write(6,*)'H: ',WHHCHT,WHHNET,WHHSLT
c     write(6,*)'H: ',WHHSQT , WHHGDT
c     write(6,*)

c     print*,'wtot',wtot

c maggie changed 21/10/2013
      WTOT=WTOT+HCHCH
      HHBRCHCH=HCHCH/WTOT
c end maggie changed 21/10/2013

c     print*,'wtot',wtot

      HHBRT=HTT/WTOT
      HHBRB=HBB/WTOT
      HHBRL=HLL/WTOT
      HHBRM=HMM/WTOT
      HHBRS=HSS/WTOT
      HHBRC=HCC/WTOT
      HHBRG=HGG/WTOT
      HHBRGA=HGA/WTOT
      HHBRZGA=HZGA/WTOT
      HHBRW=HWW/WTOT
      HHBRZ=HZZ/WTOT
      HHBRH=HHH/WTOT
      HHBRA=HAA/WTOT
      HHBRAZ=HAZ/WTOT
      HHBRHW=HHW/WTOT
      DO 841 I=1,2
      DO 841 J=1,2
      HHBRSC(I,J)=WHHCH(I,J)/WTOT
841   CONTINUE
      DO 842 I=1,4
      DO 842 J=1,4
      HHBRSN(I,J)=WHHNE(I,J)/WTOT
842   CONTINUE
      HHBRCHT=WHHCHT/WTOT 
      HHBRNET=WHHNET/WTOT 
      HHBRSL=WHHSLT/WTOT
      HHBRSQ=WHHSQ/WTOT
      HHBRSQT=WHHSQT/WTOT
      DO 843 I=1,2
      DO 843 J=1,2
      HHBRST(I,J)=WHHST(I,J)/WTOT
843   CONTINUE
      DO 844 I=1,2
      DO 844 J=1,2
      HHBRSB(I,J)=WHHSB(I,J)/WTOT
844   CONTINUE
      HHBRGD =WHHGDT/WTOT
      HHWDTH=WTOT

      BHHSLNL = WHHSLNL/WTOT
      BHHSLEL = WHHSLEL/WTOT
      BHHSLER = WHHSLER/WTOT
      BHHSQUL = WHHSQUL/WTOT
      BHHSQUR = WHHSQUR/WTOT
      BHHSQDL = WHHSQDL/WTOT
      BHHSQDR = WHHSQDR/WTOT
      DO I = 1,2
       DO J = 1,2
        BHHST(I,J) = WHHST(I,J)/WTOT
        BHHSB(I,J) = WHHSB(I,J)/WTOT
        BHHSTAU(I,J) = WHHSTAU( I,J)/WTOT
       ENDDO
      ENDDO

      ENDIF

      IF(IHIGGS.EQ.3.OR.IHIGGS.EQ.5)THEN 
C
C        =========================================================
C                       CP ODD  HIGGS DECAYS
C        =========================================================
C     =============  RUNNING MASSES 
      RMS = RUNM_HDEC(AMA,3)
      RMC = RUNM_HDEC(AMA,4)
      RMB = RUNM_HDEC(AMA,5)
      RMT = RUNM_HDEC(AMA,6)
      RATCOUP = GAT/GAB
      HIGTOP = AMA**2/AMT**2

      ASH=ALPHAS_HDEC(AMA,3)
      AMC0=1.D8
      AMB0=2.D8
C     AMT0=3.D8
      AS3=ALPHAS_HDEC(AMA,3)
      AMC0=AMC
      AS4=ALPHAS_HDEC(AMA,3)
      AMB0=AMB
C     AMT0=AMT

C     =============== PARTIAL WIDTHS
C  A ---> G G
       EPS=1.D-8
       NFEXT = 3
       ASG = AS3
       CTT = 4*AMT**2/AMA**2*DCMPLX(1D0,-EPS)
       CTB = 4*AMB**2/AMA**2*DCMPLX(1D0,-EPS)
       CAT = CTT*CF(CTT)*GAT
       CAB = CTB*CF(CTB)*GAB
       CTC = 4*AMC**2/AMA**2*DCMPLX(1D0,-EPS)
       CAC = CTC*CF(CTC)*GAT
       FQCD=AGGQCD(ASG,NFEXT)
       XFAC = CDABS(CAT+CAB+CAC)**2*FQCD
       HGG=GF/(16.D0*PI*DSQRT(2.D0))*AMA**3*(ASG/PI)**2*XFAC

c      print*
c      print*,'A decay widths'
c      print*,'hgg_NLO',hgg

C  A ---> G G* ---> G CC   TO BE ADDED TO A ---> CC
       NFEXT = 4
       ASG = AS4
       FQCD=AGGQCD(ASG,NFEXT)
       XFAC = CDABS(CAT+CAB+CAC)**2*FQCD
       DCC=GF/(16.D0*PI*DSQRT(2.D0))*AMA**3*(ASG/PI)**2*XFAC
     .     - HGG

C  A ---> G G* ---> G BB   TO BE ADDED TO A ---> BB
       NFEXT = 5
       ASG = ASH
       FQCD=AGGQCD(ASG,NFEXT)
       XFAC = CDABS(CAT+CAB+CAC)**2*FQCD
       DBB=GF/(16.D0*PI*DSQRT(2.D0))*AMA**3*(ASG/PI)**2*XFAC
     .     - HGG - DCC
       HGG=GF/(16.D0*PI*DSQRT(2.D0))*AMA**3*(ASG/PI)**2*XFAC

C  A ---> G G: FULL NNLO CORRECTIONS TO TOP LOOPS FOR NF=5
       FQCD0=AGGQCD(ASG,5)
       FQCD=AGGQCD2(ASG,5,AMA,AMT)
       XFAC = CDABS(CAT+CAB+CAC)**2*(FQCD-FQCD0)
       HGG=HGG+GF/(16.D0*PI*DSQRT(2.D0))*AMA**3*(ASG/PI)**2*XFAC

      IF(NFGG.EQ.3)THEN
       HGG = HGG - DBB - DCC
      ELSEIF(NFGG.EQ.4)THEN
       HGG = HGG - DBB
       DCC = 0
      ELSE
       DCC = 0
       DBB = 0
      ENDIF

c     write(6,*)AMA,FQCD0-1,FQCD-FQCD0
c     print*,'hgg_NNLO',hgg

C  A ---> MU MU
      XGLM = GLB
      XGHM = GHB
      XGAM = GAB
      if(i2hdm.eq.1) then
         xgam = galep
      endif
      IF(IOFSUSY.EQ.0) THEN
       CALL STAUSUSY_HDEC(GLB,GHB,GAB,XGLM,XGHM,XGAM,QSUSY,0)
      ENDIF
      IF(AMA.LE.2*AMMUON) THEN
       HMM = 0
      ELSE
      HMM=AFF(AMA,(AMMUON/AMA)**2)*XGAM**2
      ENDIF
      
c     print*,'A -> mumu',hmm

C  A ---> LL
      XGLT = GLB
      XGHT = GHB
      XGAT = GAB
      if(i2hdm.eq.1) then
         xgat = galep
      endif
      IF(IOFSUSY.EQ.0) THEN
       CALL STAUSUSY_HDEC(GLB,GHB,GAB,XGLT,XGHT,XGAT,QSUSY,1)
      ENDIF
      IF(AMA.LE.2*AMTAU) THEN
       HLL = 0
      ELSE
      HLL=AFF(AMA,(AMTAU/AMA)**2)*XGAT**2
      ENDIF

c     write(6,*)'A: tau/mu: ',HLL/HMM*AMMUON**2/AMTAU**2,XGAT**2/XGAM**2
c     print*,'A -> tautau',hll
C  A --> SS
      XGLS = GLB
      XGHS = GHB
      XGAS = GAB
      SSUSY = (AMSD(1)+AMSD(2)+AMGLU)/3*QSUSY
      IF(IOFSUSY.EQ.0) THEN
       CALL STRSUSY_HDEC(GLB,GHB,GAB,XGLS,XGHS,XGAS,SSUSY,LOOP)
      ENDIF
      IF(AMA.LE.2*AMS) THEN
       HSS = 0
      ELSE
       HS1=3.D0*AFF(AMA,(AMS/AMA)**2)
     .    *XGAS**2
     .    *TQCDA(AMS**2/AMA**2)
       HS2=3.D0*AFF(AMA,(RMS/AMA)**2)
     .    *XGAS**2
     .    *QCDA(RMS**2/AMA**2)
       IF(HS2.LT.0.D0) HS2 = 0
       RAT = 2*AMS/AMA
       HSS = QQINT_HDEC(RAT,HS1,HS2)
      ENDIF

c     print*,'A -> ss',hs1,hs2
C  A --> CC
      RATCOUP = 1
      IF(AMA.LE.2*AMC) THEN
       HCC = 0
      ELSE
       HC1=3.D0*AFF(AMA,(AMC/AMA)**2)
     .    *GAT**2
     .    *TQCDA(AMC**2/AMA**2)
       HC2=3.D0*AFF(AMA,(RMC/AMA)**2)
     .    *GAT**2
     .    *QCDA(RMC**2/AMA**2)
     .   + DCC
       IF(HC2.LT.0.D0) HC2 = 0
       RAT = 2*AMC/AMA
       HCC = QQINT_HDEC(RAT,HC1,HC2)
      ENDIF

c     print*,'A -> cc',hcc

C  A --> BB :
      QQ = AMB
      SUSY = 0
      XGAB = GAB
      SSUSY = (AMSB(1)+AMSB(2)+AMGLU)/3*QSUSY
      FSUSY = SUSYSCALE
      AS0 = ALPHAS_HDEC(FSUSY,3)
      IF(IOFSUSY.EQ.0) THEN
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       I0 = 0
       CALL DMBAPP_HDEC(I0,DGLB,DGHB,DGAB,FSUSY,LOOP)
       DELB1 = -DGAB/(1+1/TGBET**2)
       DELB0 = DELB1/(1-DELB1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       I0 = 1
       CALL DMBAPP_HDEC(I0,DGLB,DGHB,DGAB,FSUSY,LOOP)
       I0 = 3
       BSC = (AMSQ+AMUR+AMDR)/3
       XMB = RUNM_HDEC(FSUSY,5)/(1+DELB0)
C       XMB = AMB
c1357
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       if(islhai.ne.0) XMB = AMB
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       SUSY = COFSUSY_HDEC(I0,AMB,XMB,QQ)*AS0/PI - 2*DGAB
       CALL BOTSUSY_HDEC(GLB,GHB,GAB,XGLB,XGHB,XGAB,SSUSY,LOOP)
      ENDIF
      RATCOUP = GAT/XGAB
      IF(AMA.LE.2*AMB) THEN
       HBB = 0
      ELSE
       HB1=3.D0*AFF(AMA,(AMB/AMA)**2)
     .    *(XGAB**2+XGAB*GAB*SUSY)
     .    *TQCDA(AMB**2/AMA**2)
       HB2=3.D0*AFF(AMA,(RMB/AMA)**2)
     .    *(XGAB**2+XGAB*GAB*SUSY)
     .    *QCDA(RMB**2/AMA**2)
     .   + DBB
       IF(HB2.LT.0.D0) HB2 = 0
       RAT = 2*AMB/AMA
       HBB = QQINT_HDEC(RAT,HB1,HB2)

c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      XXX = COFSUSY_HDEC(I0,AMB,XMB,QQ)
c      write(1,*)'A -> bb: ',XXX*AS0/PI
c      write(1,*)'A -> bb: ',2*DGAB
c      write(1,*)'A -> bb: ',GAB,XGAB,SUSY
c      write(6,('A3,4(1X,G15.8)'))'A: ',AMA,AMA,SUSY+2*DGAB,
c    .                             SUSY/(SUSY+2*DGAB)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      ENDIF
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     write(6,*)'A -> bb: ',XGAB**2,XGAB*GAB*SUSY/XGAB**2,
c    .                      (XGAB**2+XGAB*GAB*SUSY)/XGAB**2
c     write(6,*)'approx:  ',SUSY+2*DGAB,2*DGAB,SUSY
c     FAC = AS0/PI
c     write(6,*)'approx2: ',(SUSY+2*DGAB)/FAC,2*DGAB/FAC,SUSY/FAC
c     write(52,*)AMA,HBB
c     write(6,*)'A -> bb: ',AMA,HBB,QSUSY1,LOOP
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

c     print*,'A -> bb',hbb
c      print*
c      print*,'A -> bb:',hbb,gab,xgab,gab*susy,2*dgab,susy,2*dgab+susy
C  A --> TT :
      RATCOUP = 0
      CALL TOPSUSY_HDEC(GLT,GHT,GAT,XGLTOP,XGHTOP,XGATOP,SCALE,1)
c     write(6,*)xgltop,xghtop,xgatop
      IF(IONSH.EQ.0)THEN
       DLD=3D0
       DLU=4D0
       XM1 = 2D0*AMT-DLD
       XM2 = 2D0*AMT+DLU
       IF (AMA.LE.AMT+AMW+AMB) THEN
        HTT=0.D0
       ELSEIF (AMA.LE.XM1) THEN
        FACTT=6.D0*GF**2*AMA**3*AMT**2/2.D0/128.D0/PI**3*GAT**2
        call ATOTT_HDEC(ama,amt,amb,amw,amch,att0,gab,gat)
        HTT=FACTT*ATT0
       ELSEIF (AMA.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        FACTT=6.D0*GF**2*XX(1)**3*AMT**2/2.D0/128.D0/PI**3
        call ATOTT_HDEC(xx(1),amt,amb,amw,amch,att0,gab,gat)
        YY(1)=FACTT*ATT0
        FACTT=6.D0*GF**2*XX(2)**3*AMT**2/2.D0/128.D0/PI**3
        call ATOTT_HDEC(xx(2),amt,amb,amw,amch,att0,gab,gat)
        YY(2)=FACTT*ATT0
        XMT = RUNM_HDEC(XX(3),6)
        XYZ1 =3.D0*AFF(XX(3),(AMT/XX(3))**2)
     .    *TQCDA(AMT**2/XX(3)**2)
        XYZ2 =3.D0*AFF(XX(3),(XMT/XX(3))**2)
     .    *QCDA(XMT**2/XX(3)**2)
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        RAT = 2*AMT/XX(3)
        YY(3) = QQINT_HDEC(RAT,XYZ1,XYZ2)
        XMT = RUNM_HDEC(XX(4),6)
        XYZ1 =3.D0*AFF(XX(4),(AMT/XX(4))**2)
     .    *TQCDA(AMT**2/XX(4)**2)
        XYZ2 =3.D0*AFF(XX(4),(XMT/XX(4))**2)
     .    *QCDA(XMT**2/XX(4)**2)
        IF(XYZ2.LT.0.D0) XYZ2 = 0
        RAT = 2*AMT/XX(4)
        YY(4) = QQINT_HDEC(RAT,XYZ1,XYZ2)
        HTT = FINT_HDEC(AMA,XX,YY)*GAT**2
       ELSE
        HT1=3.D0*AFF(AMA,(AMT/AMA)**2)*GAT**2
     .    *TQCDA(AMT**2/AMA**2)
        HT2=3.D0*AFF(AMA,(RMT/AMA)**2)*GAT**2
     .    *QCDA(RMT**2/AMA**2)
        IF(HT2.LT.0.D0) HT2 = 0
        RAT = 2*AMT/AMA
        HTT = QQINT_HDEC(RAT,HT1,HT2)
       ENDIF
      ELSE
       IF (AMA.LE.2.D0*AMT) THEN
        HTT=0.D0
       ELSE
        HT1=3.D0*AFF(AMA,(AMT/AMA)**2)*GAT**2
     .    *TQCDA(AMT**2/AMA**2)
        HT2=3.D0*AFF(AMA,(RMT/AMA)**2)*GAT**2
     .    *QCDA(RMT**2/AMA**2)
        IF(HT2.LT.0.D0) HT2 = 0
        RAT = 2*AMT/AMA
        HTT = QQINT_HDEC(RAT,HT1,HT2)
       ENDIF
      ENDIF

c     print*,'A -> tt',htt

C  A ---> GAMMA GAMMA
       EPS=1.D-8
       XRMC = RUNM_HDEC(AMA/2,4)*AMC/RUNM_HDEC(AMC,4)
       XRMB = RUNM_HDEC(AMA/2,5)*AMB/RUNM_HDEC(AMB,5)
       XRMT = RUNM_HDEC(AMA/2,6)*AMT/RUNM_HDEC(AMT,6)
       CTT = 4*XRMT**2/AMA**2*DCMPLX(1D0,-EPS)
       CTB = 4*XRMB**2/AMA**2*DCMPLX(1D0,-EPS)
       CAT = 4/3D0 * CTT*CF(CTT)*GAT
     .     * CFACQ_HDEC(1,AMA,XRMT)
       CAB = 1/3D0 * CTB*CF(CTB)*GAB
     .     * CFACQ_HDEC(1,AMA,XRMB)
       CTC = 4*XRMC**2/AMA**2*DCMPLX(1D0,-EPS)
       CAC = 4/3D0 * CTC*CF(CTC)*GAT
     .     * CFACQ_HDEC(1,AMA,XRMC)
       CTL = 4*AMTAU**2/AMA**2*DCMPLX(1D0,-EPS)
       CAL = 1.D0  * CTL*CF(CTL)*GAB
       if(i2hdm.eq.1) then
          CAL = 1.D0  * CTL*CF(CTL)*galep
       endif
       IF(IOFSUSY.EQ.0) THEN 
        CX1 = 4*AMCHAR(1)**2/AMA**2*DCMPLX(1D0,-EPS)
        CX2 = 4*AMCHAR(2)**2/AMA**2*DCMPLX(1D0,-EPS)
        CAX1= AMW/XMCHAR(1) * CX1*CF(CX1) * 2*AC3(1,1) 
        CAX2= AMW/XMCHAR(2) * CX2*CF(CX2) * 2*AC3(2,2) 
        XFAC = CDABS(CAT+CAB+CAC+CAL+CAX1+CAX2)**2
       ELSE 
        XFAC = CDABS(CAT+CAB+CAC+CAL)**2
       ENDIF
       HGA=GF/(32.D0*PI*DSQRT(2.D0))*AMA**3*(ALPH/PI)**2*XFAC

c      print*,'A -> gamgam',hga
C  A ---> Z GAMMA
      XRMC = RUNM_HDEC(AMA/2,4)*AMC/RUNM_HDEC(AMC,4)
      XRMB = RUNM_HDEC(AMA/2,5)*AMB/RUNM_HDEC(AMB,5)
      XRMT = RUNM_HDEC(AMA/2,6)*AMT/RUNM_HDEC(AMT,6)
c     print*,'xrmc,xrmb,xrmt ',xrmc,xrmb,xrmt
      IF(AMA.LE.AMZ)THEN
       HZGA=0
      ELSE
       TS = SS/CS
       FT = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS)*GAT
       FB = 3*1D0/3*(-1+4*1D0/3*SS)/DSQRT(SS*CS)*GAB
       FC = -3*2D0/3*(1-4*2D0/3*SS)/DSQRT(SS*CS)*GAT
       FL = (-1+4*SS)/DSQRT(SS*CS)*GAB
       if(i2hdm.eq.1) then
          FL = (-1+4*SS)/DSQRT(SS*CS)*galep
       endif
       EPS=1.D-8
c      CTT = 4*XRMT**2/AMA**2*DCMPLX(1D0,-EPS)
c      CTB = 4*XRMB**2/AMA**2*DCMPLX(1D0,-EPS)
c      CTC = 4*XRMC**2/AMA**2*DCMPLX(1D0,-EPS)
       CTT = 4*AMT**2/AMA**2*DCMPLX(1D0,-EPS)
       CTB = 4*AMB**2/AMA**2*DCMPLX(1D0,-EPS)
       CTC = 4*AMC**2/AMA**2*DCMPLX(1D0,-EPS)
       CTL = 4*AMTAU**2/AMA**2*DCMPLX(1D0,-EPS)
c      CLT = 4*XRMT**2/AMZ**2*DCMPLX(1D0,-EPS)
c      CLB = 4*XRMB**2/AMZ**2*DCMPLX(1D0,-EPS)
c      CLC = 4*XRMC**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLT = 4*AMT**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLB = 4*AMB**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLC = 4*AMC**2/AMZ**2*DCMPLX(1D0,-EPS)
       CLE = 4*AMTAU**2/AMZ**2*DCMPLX(1D0,-EPS)
       CAT = FT*(- CI2(CTT,CLT))
       CAB = FB*(- CI2(CTB,CLB))
       CAC = FC*(- CI2(CTC,CLC))
       CAL = FL*(- CI2(CTL,CLE))
c
c       CTC = 4*AMC**2/AMA**2*DCMPLX(1D0,-EPS)
c       CLC = 4*AMC**2/AMZ**2*DCMPLX(1D0,-EPS)
c       CAC = FT*(- CI2(CTC,CLC))
c
       XFAC = CDABS(CAT+CAB+CAC+CAL)**2
       ACOUP = DSQRT(2D0)*GF*AMZ**2*SS*CS/PI**2
       HZGA = GF/(4.D0*PI*DSQRT(2.D0))*AMA**3*(ALPH/PI)*ACOUP/16.D0
     .        *XFAC*(1-AMZ**2/AMA**2)**3
      ENDIF

c     print*,'A -> Zgam',hzga
C  A ---> h Z* ---> HFF
      IF(IONSH.EQ.0)THEN
       DLD=3D0
       DLU=5D0
       XM1 = AML+AMZ-DLD
       XM2 = AML+AMZ+DLU
       IF (AMA.LE.AML) THEN
        HAZ=0
       ELSEIF (AMA.LE.XM1) THEN
        IF (AMA.LE.DABS(AMZ-AML)) THEN
         HAZ=0
        ELSE
         HAZ=9.D0*GF**2/8.D0/PI**3*AMZ**4*AMA*GZAL**2*
     .      (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .      *HVH((AML/AMA)**2,(AMZ/AMA)**2)
        ENDIF
       ELSEIF (AMA.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        YY(1)=9.D0*GF**2/8.D0/PI**3*AMZ**4*XX(1)*
     .      (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .      *HVH((AML/XX(1))**2,(AMZ/XX(1))**2)
        YY(2)=9.D0*GF**2/8.D0/PI**3*AMZ**4*XX(2)*
     .      (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .      *HVH((AML/XX(2))**2,(AMZ/XX(2))**2)
        CAZ=LAMB_HDEC(AML**2/XX(3)**2,AMZ**2/XX(3)**2)
     .     *LAMB_HDEC(XX(3)**2/AMZ**2,AML**2/AMZ**2)**2
        YY(3)=GF/8D0/DSQRT(2D0)/PI*AMZ**4/XX(3)*CAZ
        CAZ=LAMB_HDEC(AML**2/XX(4)**2,AMZ**2/XX(4)**2)
     .     *LAMB_HDEC(XX(4)**2/AMZ**2,AML**2/AMZ**2)**2
        YY(4)=GF/8D0/DSQRT(2D0)/PI*AMZ**4/XX(4)*CAZ
        HAZ = FINT_HDEC(AMA,XX,YY)*GZAL**2
       ELSE
        CAZ=LAMB_HDEC(AML**2/AMA**2,AMZ**2/AMA**2)
     .     *LAMB_HDEC(AMA**2/AMZ**2,AML**2/AMZ**2)**2
        HAZ=GF/8D0/DSQRT(2D0)/PI*AMZ**4/AMA*GZAL**2*CAZ
       ENDIF
      ELSE
       IF (AMA.LE.AMZ+AML) THEN
        HAZ=0
       ELSE
        CAZ=LAMB_HDEC(AML**2/AMA**2,AMZ**2/AMA**2)
     .     *LAMB_HDEC(AMA**2/AMZ**2,AML**2/AMZ**2)**2
        HAZ=GF/8D0/DSQRT(2D0)/PI*AMZ**4/AMA*GZAL**2*CAZ
       ENDIF
      ENDIF

c     print*,'A -> Zh',haz

c MMM changed 23/8/2013
C  A ---> H Z* ---> HFF
      if(i2hdm.eq.1) then
      IF(IONSH.EQ.0)THEN
       DLD=3D0
       DLU=5D0
       XM1 = AMH+AMZ-DLD
       XM2 = AMH+AMZ+DLU
       IF (AMA.LE.AMH) THEN
        HHAZ=0
       ELSEIF (AMA.LE.XM1) THEN
        IF (AMA.LE.DABS(AMZ-AMH)) THEN
         HHAZ=0
        ELSE
         HHAZ=9.D0*GF**2/8.D0/PI**3*AMZ**4*AMA*GZAH**2*
     .      (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .      *HVH((AMH/AMA)**2,(AMZ/AMA)**2)
        ENDIF
       ELSEIF (AMA.LE.XM2) THEN
        XX(1) = XM1-1D0
        XX(2) = XM1
        XX(3) = XM2
        XX(4) = XM2+1D0
        YY(1)=9.D0*GF**2/8.D0/PI**3*AMZ**4*XX(1)*
     .      (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .      *HVH((AMH/XX(1))**2,(AMZ/XX(1))**2)
        YY(2)=9.D0*GF**2/8.D0/PI**3*AMZ**4*XX(2)*
     .      (7.D0/12.D0-10.D0/9.D0*SS+40.D0/27.D0*SS**2)
     .      *HVH((AMH/XX(2))**2,(AMZ/XX(2))**2)
        CAZ=LAMB_HDEC(AMH**2/XX(3)**2,AMZ**2/XX(3)**2)
     .     *LAMB_HDEC(XX(3)**2/AMZ**2,AMH**2/AMZ**2)**2
        YY(3)=GF/8D0/DSQRT(2D0)/PI*AMZ**4/XX(3)*CAZ
        CAZ=LAMB_HDEC(AMH**2/XX(4)**2,AMZ**2/XX(4)**2)
     .     *LAMB_HDEC(XX(4)**2/AMZ**2,AMH**2/AMZ**2)**2
        YY(4)=GF/8D0/DSQRT(2D0)/PI*AMZ**4/XX(4)*CAZ
        HHAZ = FINT_HDEC(AMA,XX,YY)*GZAH**2
       ELSE
        CAZ=LAMB_HDEC(AMH**2/AMA**2,AMZ**2/AMA**2)
     .     *LAMB_HDEC(AMA**2/AMZ**2,AMH**2/AMZ**2)**2
        HHAZ=GF/8D0/DSQRT(2D0)/PI*AMZ**4/AMA*GZAH**2*CAZ
       ENDIF
      ELSE
       IF (AMA.LE.AMZ+AMH) THEN
        HHAZ=0
       ELSE
        CAZ=LAMB_HDEC(AMH**2/AMA**2,AMZ**2/AMA**2)
     .     *LAMB_HDEC(AMA**2/AMZ**2,AMH**2/AMZ**2)**2
        HHAZ=GF/8D0/DSQRT(2D0)/PI*AMZ**4/AMA*GZAH**2*CAZ
       ENDIF
      ENDIF
      endif

      if(i2hdm.eq.0) then
         HHAZ=0.D0
      endif

c     print*,'A -> ZH',hhaz

C  A ---> W+ H-
      if(i2hdm.eq.1) then
         if(ionsh.eq.0)then
            dld=3d0
            dlu=5d0
            xm1 = amw+amch-dld
            xm2 = amw+amch+dlu
            if (ama.lt.amch) then
               hawphm=0
            elseif (ama.le.xm1) then
               if(ama.le.dabs(amw-amch))then
                  hawphm=0
               else
                  hawphm=9.d0*gf**2/16.d0/pi**3*amw**4*ama
     .                 *hvh((amch/ama)**2,(amw/ama)**2)
               endif
            elseif (ama.lt.xm2) then
               xx(1) = xm1-1d0
               xx(2) = xm1
               xx(3) = xm2
               xx(4) = xm2+1d0
               yy(1) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(1)
     .              *hvh((amch/xx(1))**2,(amw/xx(1))**2)
               yy(2) = 9.d0*gf**2/16.d0/pi**3*amw**4*xx(2)
     .              *hvh((amch/xx(2))**2,(amw/xx(2))**2)
               cwh=lamb_hdec(amch**2/xx(3)**2,amw**2/xx(3)**2)
     .              *lamb_hdec(xx(3)**2/amw**2,amch**2/amw**2)**2
               yy(3)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(3)*cwh
               cwh=lamb_hdec(amch**2/xx(4)**2,amw**2/xx(4)**2)
     .              *lamb_hdec(xx(4)**2/amw**2,amch**2/amw**2)**2
               yy(4)=gf/8.d0/dsqrt(2d0)/pi*amw**4/xx(4)*cwh
               hawphm = fint_hdec(ama,xx,yy)
            else
               cwh=lamb_hdec(amch**2/ama**2,amw**2/ama**2)
     .              *lamb_hdec(ama**2/amw**2,amch**2/amw**2)**2
               hawphm=gf/8.d0/dsqrt(2d0)/pi*amw**4/ama*cwh
            endif
         else
            if (ama.lt.amw+amch) then
               hawphm=0
            else
               cwh=lamb_hdec(amch**2/ama**2,amw**2/ama**2)
     .              *lamb_hdec(ama**2/amw**2,amch**2/amw**2)**2
               hawphm=gf/8.d0/dsqrt(2d0)/pi*amw**4/ama*cwh
            endif
         endif
      endif

      if(i2hdm.eq.0) then
         hawphm = 0.D0
      endif

      hawphm = 2.D0*hawphm

c     print*,'A -> W+H- + W-H+',hawphm,hawphm/2.D0
c end MMM changed 23/8/2013

C
C ========================== SUSY DECAYS  
C
      IF(IOFSUSY.EQ.0) THEN 
C  A ----> CHARGINOS
      DO 731 I=1,2
      DO 731 J=1,2
      IF (AMA.GT.AMCHAR(I)+AMCHAR(J)) THEN
      WHACH(I,J)=GF*AMW**2/(2*PI*DSQRT(2.D0))/AMA
     .     *LAMB_HDEC(AMCHAR(I)**2/AMA**2,AMCHAR(J)**2/AMA**2)
     .     *( (AC3(I,J)**2+AC3(J,I)**2)*(AMA**2-AMCHAR(I)
     .         **2-AMCHAR(J)**2)+4.D0*AC3(I,J)*AC3(J,I)* 
     .         XMCHAR(I)*XMCHAR(J) ) 
      ELSE 
      WHACH(I,J)=0.D0
      ENDIF
 731  CONTINUE
      WHACHT=WHACH(1,1)+WHACH(1,2)+WHACH(2,1)+WHACH(2,2)
C  A ----> NEUTRALINOS 
      DO 732 I=1,4
      DO 732 J=1,4
      IF (AMA.GT.AMNEUT(I)+AMNEUT(J)) THEN
      WHANE(I,J)=GF*AMW**2/(2*PI*DSQRT(2.D0))/AMA
     .         *AN3(I,J)**2*(AMA**2-(XMNEUT(I)-XMNEUT(J))**2)
     .         *LAMB_HDEC(AMNEUT(I)**2/AMA**2,AMNEUT(J)**2/AMA**2)
      ELSE 
      WHANE(I,J)=0.D0
      ENDIF
 732  CONTINUE
      WHANET= WHANE(1,1)+WHANE(1,2)+WHANE(1,3)+WHANE(1,4)
     .       +WHANE(2,1)+WHANE(2,2)+WHANE(2,3)+WHANE(2,4)
     .       +WHANE(3,1)+WHANE(3,2)+WHANE(3,3)+WHANE(3,4)
     .       +WHANE(4,1)+WHANE(4,2)+WHANE(4,3)+WHANE(4,4)

C  A ----> STAU'S 
C
      IF(AMA.GT.AMSL(1)+AMSL(2)) THEN
      WHASL=GF*AMZ**4/DSQRT(2.D0)/PI*GAEE**2*
     .      LAMB_HDEC(AMSL(1)**2/AMA**2,AMSL(2)**2/AMA**2)/AMA
      ELSE
      WHASL=0.D0
      ENDIF
C
C  A ----> STOPS 
C
      SUSY = 1
c     QSQ = (YMST(1)+YMST(2))/2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     DO K=-10,10
c     QSQ = AMA*10.D0**(K/10.D0)
c     DO K=-1,1
c     QSQ = AMA*2.D0**(K)
c     IF(AMA.GT.YMST(1)+YMST(2)) THEN
c      CALL SQMBAPP_HDEC(QSQ)
c      SUSY = 1+SQSUSY_HDEC(3,1,1,2,QSQ,0,1)
c      WHAST0=3*GF*AMZ**4/DSQRT(2.D0)/PI*YATT**2*
c    .       LAMB_HDEC(YMST(1)**2/AMA**2,YMST(2)**2/AMA**2)/AMA
c      WHAST=3*GF*AMZ**4/DSQRT(2.D0)/PI*YATT**2*
c    .       LAMB_HDEC(YMST(1)**2/AMA**2,YMST(2)**2/AMA**2)/AMA
c    .      *SUSY
c     ELSE
c      WHAST=0.D0
c     ENDIF
c     write(9,*)'A -> t1 t2: ',QSQ/AMA,WHAST0,WHAST
c     write(903,('1X,G10.4,2(1X,G10.4)'))QSQ/AMA,WHAST0,WHAST
c     ENDDO
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      QSQ = AMA
      IF(AMA.GT.YMST(1)+YMST(2)) THEN
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CALL SQMBAPP_HDEC(QSQ)
       SUSY = 1+SQSUSY_HDEC(3,1,1,2,QSQ,0,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       WHAST=3*GF*AMZ**4/DSQRT(2.D0)/PI*YATT**2*
     .       LAMB_HDEC(YMST(1)**2/AMA**2,YMST(2)**2/AMA**2)/AMA
     .      *SUSY
c      write(6,*)'A -> stop: ',AMA,AMST(1),AMST(2),100*(SUSY-1),'% ',
c    .           WHAST/SUSY,WHAST
c      write(712,*)AMA,WHAST/2,WHAST/SUSY/2
c      write(721,*)AMA,WHAST/2,WHAST/SUSY/2
c      write(6,*)'A -> stop: ',AMA,AMST(1),AMST(2),SUSY-1
      ELSE
      WHAST=0.D0
      ENDIF
C
C  A ----> SBOTTOMS 
C
      SUSY = 1
c     QSQ = (YMSB(1)+YMSB(2))/2
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     DO K=-10,10
c     QSQ = AMA*10.D0**(K/10.D0)
c     DO K=-1,1
c     QSQ = AMA*2.D0**(K)
c     IF(AMA.GT.YMSB(1)+YMSB(2)) THEN
c      CALL SQMBAPP_HDEC(QSQ)
c      SUSY = 1+SQSUSY_HDEC(3,2,1,2,QSQ,0,1)
c      WHASB0=3*GF*AMZ**4/DSQRT(2.D0)/PI*YABB**2*
c    .       LAMB_HDEC(YMSB(1)**2/AMA**2,YMSB(2)**2/AMA**2)/AMA
c      WHASB=3*GF*AMZ**4/DSQRT(2.D0)/PI*YABB**2*
c    .       LAMB_HDEC(YMSB(1)**2/AMA**2,YMSB(2)**2/AMA**2)/AMA
c    .      *SUSY
c     ELSE
c      WHASB=0.D0
c     ENDIF
c     write(9,*)'A -> b1 b2: ',QSQ/AMA,WHASB0,WHASB
c     write(904,('1X,G10.4,2(1X,G10.4)'))QSQ/AMA,WHASB0,WHASB
c     ENDDO
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      QSQ = AMA
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c     dummy0 = 0
c     dummy1 = 0
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      IF(AMA.GT.YMSB(1)+YMSB(2)) THEN
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       CALL SQMBAPP_HDEC(QSQ)
       SUSY = 1+SQSUSY_HDEC(3,2,1,2,QSQ,0,1)
c>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
       WHASB=3*GF*AMZ**4/DSQRT(2.D0)/PI*YABB**2*
     .       LAMB_HDEC(YMSB(1)**2/AMA**2,YMSB(2)**2/AMA**2)/AMA
     .      *SUSY
c      write(6,*)'A -> sbot: ',AMA,YMSB(1),YMSB(2),100*(SUSY-1),'% ',
c    .           WHASB/SUSY,WHASB
c      write(812,*)AMA,WHASB/2,WHASB/SUSY/2
c      write(821,*)AMA,WHASB/2,WHASB/SUSY/2
c      write(6,*)'A -> sbot: ',AMA,AMSB(1),AMSB(2),SUSY-1
c      dummy0 = dummy0 + WHASB/SUSY
c      dummy1 = dummy1 + WHASB
      ELSE
      WHASB=0.D0
      ENDIF
c     write(6,*)'A -> sbot0: ',YMSB(1),YMSB(2),dummy0,dummy1,
c    .                        100*(dummy1/dummy0-1),'%'
C
      ELSE 
      WHACHT=0.D0
      WHANET=0.D0
      WHASL=0.D0
      WHAST=0.D0
      WHASB=0.D0
C--Change thanks to Elzbieta Richter-Was
      DO I=1,2
       DO J=1,2
        WHACH(I,J)=0.D0
       ENDDO
      ENDDO
      DO I=1,4
       DO J=1,4
        WHANE(I,J)=0.D0
       ENDDO
      ENDDO
      ENDIF

      IF(IGOLD.NE.0)THEN
C   HA ---> GOLDSTINOS
       DO 730 I=1,4
       IF (AMA.GT.AMNEUT(I)) THEN
        WHAGD(I)=AMA**5/AXMPL**2/AXMGD**2/48.D0/PI*
     .           (1.D0-AMNEUT(I)**2/AMA**2)**4*AGDA(I)**2
       ELSE
        WHAGD(I)=0.D0
       ENDIF
 730   CONTINUE
       WHAGDT=WHAGD(1)+WHAGD(2)+WHAGD(3)+WHAGD(4)
      ELSE
       WHAGDT=0
      ENDIF
C
C    ==========  TOTAL WIDTH AND BRANCHING RATIOS 
      WTOT=HLL+HMM+HSS+HCC+HBB+HGG+HGA+HZGA+HAZ+HTT
     .    +WHACHT+WHANET+WHASL+WHAST+WHASB + WHAGDT

c MMM changed 23/8/2013
      if(i2hdm.eq.1) then
         wtot = wtot + hhaz + hawphm
         abrhhaz = hhaz/wtot
         abrhawphm = hawphm/wtot
      endif
      if(i2hdm.eq.0) then
         abrhhaz = 0.D0
         abrhawphm = 0.D0
      endif
c end MMM changed 23/8/2013

c     print*,'wtot',wtot

      ABRT=HTT/WTOT
      ABRB=HBB/WTOT
      ABRL=HLL/WTOT
      ABRM=HMM/WTOT
      ABRS=HSS/WTOT
      ABRC=HCC/WTOT
      ABRG=HGG/WTOT
      ABRGA=HGA/WTOT
      ABRZGA=HZGA/WTOT
      ABRZ=HAZ/WTOT
      DO 831 I=1,2
      DO 831 J=1,2
      HABRSC(I,J)=WHACH(I,J)/WTOT
831   CONTINUE
      DO 832 I=1,4
      DO 832 J=1,4
      HABRSN(I,J)=WHANE(I,J)/WTOT
832   CONTINUE
      HABRCHT=WHACHT/WTOT      
      HABRNET=WHANET/WTOT      
      HABRSL=WHASL/WTOT 
      HABRST=WHAST/WTOT 
      HABRSB=WHASB/WTOT 
      HABRGD=WHAGDT/WTOT
C 
      AWDTH=WTOT

      BHASTAU = WHASL/WTOT
      BHASB = WHASB/WTOT
      BHAST = WHAST/WTOT

C    ==============================================================
      ENDIF

      RETURN
      END