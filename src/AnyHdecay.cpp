#include "AnyHdecay.hpp"

#include "wrappers/hdecay_wrap.h"
#include <cstddef>

namespace {
template <size_t size>
std::map<std::string_view, double>
zipToMap(const std::array<std::string_view, size> &keys,
         const std::array<double, size> &values) {
    auto result = std::map<std::string_view, double>{};
    for (size_t i = 0; i < size; ++i) {
        result.emplace(keys[i], values[i]);
    }
    return result;
}
} // namespace

namespace AnyHdecay {

SMParameters Hdecay::smParameters() const noexcept {
    return {constants_[0],  constants_[1],  constants_[2],  constants_[3],
            constants_[4],  constants_[5],  constants_[6],  constants_[7],
            constants_[8],  constants_[9],  constants_[10], constants_[11],
            constants_[12], constants_[13], constants_[14], constants_[15],
            constants_[16], constants_[17], constants_[18], constants_[19],
            constants_[20], constants_[21]};
}

std::map<std::string_view, double> Hdecay::sm(HMass mh) const {
    auto result = std::array<double, smKeys.size()>{};
    hdecay_SM(mh, constants_.data(), result.data());
    return zipToMap(smKeys, result);
}

std::map<std::string_view, double>
Hdecay::r2hdm(Yukawa type, TanBeta tbeta, SquaredMassPar m12sq, AMass mA,
              HcMass mHc, HMass mHh, HMass mHl, MixAngle alpha) const {
    if (mHh < mHl)
        throw(InvalidMassHierarchy("mHl", mHl, "mHh", mHh));
    auto result = std::array<double, r2hdmKeys.size()>{};
    hdecay_2HDM(static_cast<int>(type), tbeta, m12sq, mA, mHc, mHh, mHl, alpha,
                constants_.data(), result.data());
    return zipToMap(r2hdmKeys, result);
}

std::map<std::string_view, double>
Hdecay::r2hdm_eft(Yukawa type, TanBeta tbeta, SquaredMassPar m12sq, AMass mA,
              HcMass mHc, HMass mHh, HMass mHl, MixAngle alpha, std::array<double,5> lambdas, std::array<double,9> yukawas_mod) const {
    if (mHh < mHl)
        throw(InvalidMassHierarchy("mHl", mHl, "mHh", mHh));
    auto result = std::array<double, r2hdmKeys.size()>{};
    hdecay_2HDM_eft(static_cast<int>(type), tbeta, m12sq, mA, mHc, mHh, mHl, alpha, lambdas.data(), yukawas_mod.data(),
                constants_.data(), result.data());
    return zipToMap(r2hdmKeys, result);
}

std::map<std::string_view, double>
Hdecay::n2hdmBroken(Yukawa type, TanBeta tbeta, SquaredMassPar m12sq, AMass mA,
                    HcMass mHc, HMass mH1, HMass mH2, HMass mH3, MixAngle a1,
                    MixAngle a2, MixAngle a3, SingletVev vs) const {
    if (mH1 > mH2)
        throw(InvalidMassHierarchy("mH1", mH1, "mH2", mH2));
    if (mH2 > mH3)
        throw(InvalidMassHierarchy("mH2", mH2, "mH3", mH3));
    auto result = std::array<double, n2hdmBrokenKeys.size()>{};
    hdecay_N2HDM_broken(static_cast<int>(type), tbeta, m12sq, mA, mHc, mH1, mH2,
                        mH3, a1, a2, a3, vs, constants_.data(), result.data());
    return zipToMap(n2hdmBrokenKeys, result);
}

std::map<std::string_view, double>
Hdecay::n2hdmDarkSinglet(Yukawa type, TanBeta tbeta, SquaredMassPar m12sq,
                         AMass mA, HcMass mHc, DarkHMass mHD, HMass mH1,
                         HMass mH2, MixAngle alpha, Lambda L7,
                         Lambda L8) const {
    if (mH1 > mH2)
        throw(InvalidMassHierarchy("mH1", mH1, "mH2", mH2));
    auto result = std::array<double, n2hdmDarkSingletKeys.size()>{};
    hdecay_N2HDM_darks(static_cast<int>(type), tbeta, m12sq, mA, mHc, mHD, mH1,
                       mH2, alpha, L7, L8, constants_.data(), result.data());
    return zipToMap(n2hdmDarkSingletKeys, result);
}

std::map<std::string_view, double>
Hdecay::n2hdmDarkDoublet(DarkAMass mAD, DarkHcMass mHDc, DarkHMass mHD,
                         HMass mH1, HMass mH2, MixAngle alpha, Lambda L8,
                         SquaredMassPar m22sq, SingletVev vs) const {
    if (mH1 > mH2)
        throw(InvalidMassHierarchy("mH1", mH1, "mH2", mH2));
    auto result = std::array<double, n2hdmDarkDoubletKeys.size()>{};
    hdecay_N2HDM_darkd(mAD, mHDc, mHD, mH1, mH2, alpha, L8, m22sq, vs,
                       constants_.data(), result.data());
    return zipToMap(n2hdmDarkDoubletKeys, result);
}

std::map<std::string_view, double> Hdecay::n2hdmDarkSingletDoublet(
    DarkAMass mAD, DarkHcMass mHDc, DarkHMass mHDD, HMass mHsm, DarkHMass mHDS,
    SquaredMassPar m22sq, SquaredMassPar mssq) const {
    auto result = std::array<double, n2hdmDarkSingletDoubletKeys.size()>{};
    hdecay_N2HDM_darksd(mAD, mHDc, mHsm, mHDD, mHDS, m22sq, mssq,
                        constants_.data(), result.data());
    return zipToMap(n2hdmDarkSingletDoubletKeys, result);
}

std::map<std::string_view, double>
Hdecay::rxsmBroken(MixAngle alpha, HMass mH1, HMass mH2, SingletVev vs) const {
    auto result = std::array<double, rxsmBrokenKeys.size()>{};
    hdecay_RxSM_broken(alpha, mH1, mH2, vs, constants_.data(), result.data());
    return zipToMap(rxsmBrokenKeys, result);
}

std::map<std::string_view, double> Hdecay::rxsmDark(HMass mh, DarkHMass mHD,
                                                    SquaredMassPar mSsq) const {
    auto result = std::array<double, rxsmDarkKeys.size()>{};
    hdecay_RxSM_dark(mh, mHD, mSsq, constants_.data(), result.data());
    return zipToMap(rxsmDarkKeys, result);
}

std::map<std::string_view, double> Hdecay::cxsmBroken(MixAngle a1, MixAngle a2,
                                                      MixAngle a3, HMass mH1,
                                                      HMass mH3,
                                                      SingletVev vs) const {
    auto result = std::array<double, cxsmBrokenKeys.size()>{};
    hdecay_CxSM_broken(a1, a2, a3, mH1, mH3, vs, constants_.data(),
                       result.data());
    return zipToMap(cxsmBrokenKeys, result);
}

std::map<std::string_view, double> Hdecay::cxsmDark(MixAngle alpha, HMass mH1,
                                                    HMass mH2, DarkAMass mAD,
                                                    SingletVev vs,
                                                    PotentialPar a1) const {
    auto result = std::array<double, cxsmDarkKeys.size()>{};
    hdecay_CxSM_dark(alpha, mH1, mH2, mAD, vs, a1, constants_.data(),
                     result.data());
    return zipToMap(cxsmDarkKeys, result);
}

std::map<std::string_view, double>
Hdecay::c2hdm(Yukawa type, TanBeta tbeta, SquaredMassPar re_m12sq, HcMass mHc,
              HMassCPM mH1, HMassCPM mH2, MixAngle a1, MixAngle a2,
              MixAngle a3) const {
    if (mH1 > mH2)
        throw(InvalidMassHierarchy("mH1", mH1, "mH2", mH2));
    auto result = std::array<double, c2hdmKeys.size()>{};
    hdecay_C2HDM(static_cast<int>(type), tbeta, re_m12sq, mHc, mH1, mH2, a1, a2,
                 a3, constants_.data(), result.data());
    return zipToMap(c2hdmKeys, result);
}

InvalidMassHierarchy::InvalidMassHierarchy(const std::string &lightName,
                                           double lightMass,
                                           const std::string &heavyName,
                                           double heavyMass) noexcept
    : std::runtime_error{lightName + " = " + std::to_string(lightMass) +
                         " has to be lighter than " + heavyName + " = " +
                         std::to_string(heavyMass)} {}

} // namespace AnyHdecay
