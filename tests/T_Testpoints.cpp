#include "AnyHdecay.hpp"
#include "catch.hpp"
#include <algorithm>
#include <array>
#include <cstddef>
#include <fstream>
#include <iterator>
#include <map>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

namespace {
template <size_t size, class Iterator>
auto zipToMap(const std::array<std::string_view, size> &keys,
              Iterator valuesBegin) {
    auto result = std::map<std::string_view, double>{};
    for (size_t i = 0; i < size; ++i) {
        result.emplace(keys[i], *(valuesBegin++));
    }
    return result;
}

auto parseToDoubles(const std::string &line) {
    auto iss = std::istringstream(line);
    return std::vector<double>(std::istream_iterator<double>(iss),
                               std::istream_iterator<double>());
}

std::ifstream openReferenceFile(const std::string &filename) {
    return std::ifstream(ANYHDECAY_TEST_DIR "/references/" + filename + ".tsv");
}

template <class HdecFunction, class NameArray>
void checkReferencePoints(const std::string filename, HdecFunction hdec,
                          std::size_t nParameters,
                          const NameArray &resultKeys) {
    auto testpoints = openReferenceFile(filename);
    REQUIRE(testpoints.good());
    for (auto line = std::string(); std::getline(testpoints, line);) {
        auto point = parseToDoubles(line);
        const auto result = hdec(point);
        const auto reference =
            zipToMap(resultKeys, begin(point) + 1 + nParameters);
        auto compareToReference = [n = &point[0],
                                   &reference](const auto &resultElement) {
            auto [key, value] = resultElement;
            INFO(key << " for point " << *n);
            CHECK(value ==
                  Approx{reference.at(key)}.epsilon(1e-3).margin(1e-6));
        };
        std::for_each(begin(result), end(result), compareToReference);
    }
}
} // namespace

using namespace AnyHdecay;
using namespace std::string_literals;

TEST_CASE("SM testpoints", "[SM][functional]") {
    const auto nPar = 1;
    auto hdec = [hdecay = Hdecay()](const auto &point) {
        return hdecay.sm(HMass{point[1]});
    };
    checkReferencePoints("sm", hdec, nPar, Hdecay::smKeys);
};

TEST_CASE("R2HDM testpoints", "[r2hdm][functional]") {
    const auto nPar = 8;
    auto hdec = [hdecay = Hdecay()](const auto &point) {
        return hdecay.r2hdm(static_cast<Yukawa>(point[1]), TanBeta(point[2]),
                            SquaredMassPar(point[3]), AMass(point[4]),
                            HcMass(point[5]), HMass(point[6]), HMass(point[7]),
                            MixAngle(point[8]));
    };
    checkReferencePoints("r2hdm", hdec, nPar, Hdecay::r2hdmKeys);
}

TEST_CASE("C2HDM testpoints", "[c2hdm][functional]") {
    const auto nPar = 9;
    auto hdec = [hdecay = Hdecay()](const auto &point) {
        return hdecay.c2hdm(static_cast<Yukawa>(point[1]), TanBeta(point[2]),
                            SquaredMassPar(point[3]), HcMass(point[4]),
                            HMassCPM(point[5]), HMassCPM(point[6]),
                            MixAngle(point[7]), MixAngle(point[8]),
                            MixAngle(point[9]));
    };
    checkReferencePoints("c2hdm", hdec, nPar, Hdecay::c2hdmKeys);
}

TEST_CASE("RxSM Dark testpoints", "[rxsm][functional]") {
    const auto nPar = 3;
    auto hdec = [hdecay = Hdecay()](const auto &point) {
        return hdecay.rxsmDark(HMass(point[1]), DarkHMass(point[2]),
                               SquaredMassPar(point[3]));
    };
    checkReferencePoints("rxsmDark", hdec, nPar, Hdecay::rxsmDarkKeys);
}

TEST_CASE("RxSM Broken testpoints", "[rxsm][functional]") {
    const auto nPar = 4;
    auto hdec = [hdecay = Hdecay()](const auto &point) {
        return hdecay.rxsmBroken(MixAngle(point[1]), HMass(point[2]),
                                 HMass(point[3]), SingletVev(point[4]));
    };
    checkReferencePoints("rxsmBroken", hdec, nPar, Hdecay::rxsmBrokenKeys);
}

TEST_CASE("CxSM Dark testpoints", "[cxsm][functional]") {
    const auto nPar = 6;
    auto hdec = [hdecay = Hdecay()](const auto &point) {
        return hdecay.cxsmDark(MixAngle(point[1]), HMass(point[2]),
                               HMass(point[3]), DarkAMass(point[4]),
                               SingletVev(point[5]), PotentialPar(point[6]));
    };
    checkReferencePoints("cxsmDark", hdec, nPar, Hdecay::cxsmDarkKeys);
}

TEST_CASE("CxSM Broken testpoints", "[cxsm][functional]") {
    const auto nPar = 6;
    auto hdec = [hdecay = Hdecay()](const auto &point) {
        return hdecay.cxsmBroken(MixAngle(point[1]), MixAngle(point[2]),
                                 MixAngle(point[3]), HMass(point[4]),
                                 HMass(point[5]), SingletVev(point[6]));
    };
    checkReferencePoints("cxsmBroken", hdec, nPar, Hdecay::cxsmBrokenKeys);
}

TEST_CASE("N2HDM Broken testpoints", "[n2hdm][functional]") {
    const auto nPar = 12;
    auto hdec = [hdecay = Hdecay()](const auto &point) {
        return hdecay.n2hdmBroken(
            static_cast<Yukawa>(point[1]), TanBeta(point[2]),
            SquaredMassPar(point[3]), AMass(point[4]), HcMass(point[5]),
            HMass(point[6]), HMass(point[7]), HMass(point[8]),
            MixAngle(point[9]), MixAngle(point[10]), MixAngle(point[11]),
            SingletVev(point[12]));
    };
    checkReferencePoints("n2hdmBroken", hdec, nPar, Hdecay::n2hdmBrokenKeys);
}

TEST_CASE("N2HDM Dark Singlet testpoints", "[n2hdm][functional]") {
    const auto nPar = 11;
    auto hdec = [hdecay = Hdecay()](const auto &point) {
        return hdecay.n2hdmDarkSinglet(
            static_cast<Yukawa>(point[1]), TanBeta(point[2]),
            SquaredMassPar(point[3]), AMass(point[4]), HcMass(point[5]),
            DarkHMass(point[6]), HMass(point[7]), HMass(point[8]),
            MixAngle(point[9]), Lambda(point[10]), Lambda(point[11]));
    };
    checkReferencePoints("n2hdmDarkSinglet", hdec, nPar,
                         Hdecay::n2hdmDarkSingletKeys);
}

TEST_CASE("N2HDM Dark Doublet testpoints", "[n2hdm][functional]") {
    const auto nPar = 9;
    auto hdec = [hdecay = Hdecay()](const auto &point) {
        return hdecay.n2hdmDarkDoublet(
            DarkAMass(point[1]), DarkHcMass(point[2]), DarkHMass(point[3]),
            HMass(point[4]), HMass(point[5]), MixAngle(point[6]),
            Lambda(point[7]), SquaredMassPar(point[8]), SingletVev(point[9]));
    };
    checkReferencePoints("n2hdmDarkDoublet", hdec, nPar,
                         Hdecay::n2hdmDarkDoubletKeys);
}

TEST_CASE("N2HDM Dark Singlet and Doublet testpoints", "[n2hdm][functional]") {
    const auto nPar = 7;
    auto hdec = [hdecay = Hdecay()](const auto &point) {
        return hdecay.n2hdmDarkSingletDoublet(
            DarkAMass(point[1]), DarkHcMass(point[2]), DarkHMass(point[3]),
            HMass(point[4]), DarkHMass(point[5]), SquaredMassPar(point[6]),
            SquaredMassPar(point[7]));
    };
    checkReferencePoints("n2hdmDarkSingletDoublet", hdec, nPar,
                         Hdecay::n2hdmDarkSingletDoubletKeys);
}
