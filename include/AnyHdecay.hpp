/**
 * @file AnyHdecay.hpp
 * @brief Main functionality through the Hdecay class.
 *
 * This file is part of AnyHdecay, a modern C++ interface to HDECAY and its
 * variants.
 *
 * @author Jonas Wittbrodt (Jonas.Wittbrodt@desy.de)
 * @copyright Copyright (c) 2019 Jonas Wittbrodt, released under GNU GPLv3
 */
#pragma once

#include <array>
#include <map>
#include <stdexcept>
#include <string>
#include <string_view>

//! The AnyHdecay namespace
namespace AnyHdecay {

//! Yukawa types
enum class Yukawa {
    typeI = 1,          //!< type I
    typeII = 2,         //!< type II
    leptonSpecific = 3, //!< lepton specific (type X)
    flipped = 4         //!< flipped (type Y)
};

/**
 * @brief A class providing strong types wrapping double.
 *
 * This class isinspired by https://github.com/joboccara/NamedType.
 *
 * @tparam Parameter a unique tag to differentiate the types
 */
template <typename Parameter> class NamedDouble {
  public:
    //! Construct a NamedDouble from a double.
    explicit constexpr NamedDouble(double value) : value_{value} {}
    //! Implicit conversion to double
    constexpr operator double() const { return value_; }
    //! Implicit conversion to double&
    constexpr operator double &() { return value_; }

  private:
    double value_;
};

//! A neutral CP-even Higgs mass
using HMass = NamedDouble<struct CPEvenHiggsMassTag>;
//! A neutral CP-odd Higgs mass
using AMass = NamedDouble<struct CPOddHiggsMassTag>;
//! A neutral CP-mixed Higgs mass
using HMassCPM = NamedDouble<struct CPMixedHiggsMassTag>;
//! A charget Higgs mass
using HcMass = NamedDouble<struct ChargedHiggsMassTag>;
//! A dark sector neutral CP-even Higgs mass
using DarkHMass = NamedDouble<struct DarkCPEvenHiggsMassTag>;
//! A dark sector neutral CP-odd Higgs mass
using DarkAMass = NamedDouble<struct DarkCPOddHiggsMassTag>;
//! A dark sector charged Higgs mass
using DarkHcMass = NamedDouble<struct DarkChargedHiggsMassTag>;
//! A mixing angle
using MixAngle = NamedDouble<struct MixAngleTag>;
//! A vev ratio, \f$ \tan\beta \f$
using TanBeta = NamedDouble<struct TanBetaTag>;
//! Potential parameters of mass dimension 2, e.g. \f$ m_{12}^2 \f$
using SquaredMassPar = NamedDouble<struct SquaredMassParTag>;
//! Dimensionless potential parameters
using Lambda = NamedDouble<struct LambdaTag>;
//! Other parameters of the scalar potential
using PotentialPar = NamedDouble<struct PotentialParTag>;
//! A singlet vacuum expectation value
using SingletVev = NamedDouble<struct SingletVevTag>;

//! Default SM parameters for Hdecay.
struct SMParameters {
    double alphaS = 0.118;           //!< \f$ \alpha_s(m_Z) \f$
    double mS = 0.095;               //!< \f$ m_s(\overline{MS}) \f$ in GeV
    double mC = 0.986;               //!< \f$ m_c(\overline{MS}) \f$ in GeV
    double mB = 4.180;               //!< \f$ m_b(\overline{MS}) \f$ in GeV
    double mT = 1.725e2;             //!< \f$ m_t \f$ in GeV
    double mTau = 1.77682;           //!< \f$ m_\tau \f$ in GeV
    double mMu = 0.1056583715;       //!< \f$ m_\mu \f$ in GeV
    double invAlpha = 1.370359997e2; //!< \f$ 1/\alpha \f$
    double GF = 1.1663787e-5;        //!< \f$ G_F \f$
    double widthW = 2.08430;         //!< \f$ \Gamma_W \f$
    double widthZ = 2.49427;         //!< \f$ \Gamma_Z \f$
    double mZ = 9.115348e1;          //!< \f$ m_Z \f$
    double mW = 8.035797e1;          //!< \f$ m_W \f$
    double ckmTB = 0.9991;           //!< \f$ V_{tb} \f$ CKM matrix element
    double ckmTS = 0.0404;           //!< \f$ V_{ts} \f$ CKM matrix element
    double ckmTD = 0.00867;          //!< \f$ V_{td} \f$ CKM matrix element
    double ckmCB = 0.0412;           //!< \f$ V_{cb} \f$ CKM matrix element
    double ckmCS = 0.97344;          //!< \f$ V_{cs} \f$ CKM matrix element
    double ckmCD = 0.22520;          //!< \f$ V_{cd} \f$ CKM matrix element
    double ckmUB = 0.00351;          //!< \f$ V_{ub} \f$ CKM matrix element
    double ckmUS = 0.22534;          //!< \f$ V_{us} \f$ CKM matrix element
    double ckmUD = 0.97427;          //!< \f$ V_{ud} \f$ CKM matrix element
};

/**
 * @brief The main class providing the AnyHdecay functionality.
 *
 * This class is instantiated with customizable SM parameter values. The
 * branching ratios and total widths in the different supported models can then
 * be obtained by calling the corresponding member functions. These functions
 * take strongly typed parameters to make accidental mixups less likely. For
 * example call the r2hdm() as
 * ```
 * using namespace AnyHdecay;
 * auto hdec = Hdecay();
 * auto result = test.r2hdm(Yukawa::typeI, TanBeta{2.5}, SquaredMassPar{1000},
 AMass{250}, HcMass{250}, HMass{200}, HMass{125}, MixAngle{-0.3});
 * ```
 *
 * The keys that index the returned maps can be found in the corresponding
 * `...Keys` variable (e.g. Hdecay::r2hdmKeys).
 *
 *
 */
class Hdecay {
    std::array<double, 22> constants_;

  public:
    /**
     * @brief Construct a new Hdecay object
     *
     * Works with any object that has the required members. See SMParameters for
     * the required parameters and their default values.
     *
     * @tparam Constants a type providing the values of the required SM
     * constants
     * @param constants object containing values for the constants
     */
    template <class Constants = SMParameters>
    explicit Hdecay(const Constants &constants = SMParameters{}) noexcept
        : constants_{{constants.alphaS, constants.mS,       constants.mC,
                      constants.mB,     constants.mT,       constants.mTau,
                      constants.mMu,    constants.invAlpha, constants.GF,
                      constants.widthW, constants.widthZ,   constants.mZ,
                      constants.mW,     constants.ckmTB,    constants.ckmTS,
                      constants.ckmTD,  constants.ckmCB,    constants.ckmCS,
                      constants.ckmCD,  constants.ckmUB,    constants.ckmUS,
                      constants.ckmUD}} {}

    //! Retrieve the SMParameters
    SMParameters smParameters() const noexcept;

    //! Names of the branching ratios and total widths in sm()
    static constexpr std::array<std::string_view, 12> smKeys{
        "w_H",    "b_H_bb", "b_H_tautau", "b_H_mumu", "b_H_ss", "b_H_cc",
        "b_H_tt", "b_H_gg", "b_H_gamgam", "b_H_Zgam", "b_H_WW", "b_H_ZZ"};

    /**
     * @brief Run HDECAY in the SM
     *
     * @param mh \f$ m_h \f$ in GeV
     * @return std::map<std::string_view, double> branching ratios and total
     * widths indexed by #smKeys
     */
    std::map<std::string_view, double> sm(HMass mh) const;

    //! Names of the branching ratios and total widths in r2hdm()
    static constexpr std::array<std::string_view, 63> r2hdmKeys{
        "w_Hl",       "b_Hl_bb",    "b_Hl_tautau", "b_Hl_mumu",   "b_Hl_ss",
        "b_Hl_cc",    "b_Hl_tt",    "b_Hl_gg",     "b_Hl_gamgam", "b_Hl_Zgam",
        "b_Hl_WW",    "b_Hl_ZZ",    "b_Hl_ZA",     "b_Hl_WpHm",   "b_Hl_AA",
        "b_Hl_HpHm",  "w_Hh",       "b_Hh_bb",     "b_Hh_tautau", "b_Hh_mumu",
        "b_Hh_ss",    "b_Hh_cc",    "b_Hh_tt",     "b_Hh_gg",     "b_Hh_gamgam",
        "b_Hh_Zgam",  "b_Hh_WW",    "b_Hh_ZZ",     "b_Hh_ZA",     "b_Hh_WpHm",
        "b_Hh_HlHl",  "b_Hh_AA",    "b_Hh_HpHm",   "w_A",         "b_A_bb",
        "b_A_tautau", "b_A_mumu",   "b_A_ss",      "b_A_cc",      "b_A_tt",
        "b_A_gg",     "b_A_gamgam", "b_A_Zgam",    "b_A_ZHl",     "b_A_ZHh",
        "b_A_WpHm",   "w_Hp",       "b_Hp_cb",     "b_Hp_taunu",  "b_Hp_munu",
        "b_Hp_us",    "b_Hp_cs",    "b_Hp_tb",     "b_Hp_cd",     "b_Hp_ub",
        "b_Hp_ts",    "b_Hp_td",    "b_Hp_WHl",    "b_Hp_WHh",    "b_Hp_WA",
        "w_t",        "b_t_Wb",     "b_t_Hpb"};

    /**
     * @brief Run HDECAY in the CP-conserving (real) 2HDM
     *
     * @param type Yukawa type
     * @param tbeta \f$ \tan\beta \f$
     * @param m12sq  \f$ m_{12}^2 \f$ in GeV\f$^2\f$
     * @param mA \f$ m_A \f$ in GeV
     * @param mHp \f$ m_{H^\pm} \f$ in GeV
     * @param mHh \f$ m_{H} \f$ in GeV, with \f$ m_H > m_h \f$
     * @param mHl \f$ m_{h} \f$ in GeV, with \f$ m_H > m_h \f$
     * @param alpha \f$ \alpha \f$
     * @return std::map<std::string_view, double> branching ratios and total
     * widths indexed by #r2hdmKeys
     */
    std::map<std::string_view, double> r2hdm(Yukawa type, TanBeta tbeta,
                                             SquaredMassPar m12sq, AMass mA,
                                             HcMass mHp, HMass mHh, HMass mHl,
                                             MixAngle alpha) const;

    /**
     * @brief Run HDECAY in the CP-conserving (real) 2HDM with EFT operators
     * All DIM6 operator modifications are captured in the lambdas and yukawa modifiers
     *
     * @param type Yukawa type
     * @param tbeta \f$ \tan\beta \f$
     * @param m12sq  \f$ m_{12}^2 \f$ in GeV\f$^2\f$
     * @param mA \f$ m_A \f$ in GeV
     * @param mHp \f$ m_{H^\pm} \f$ in GeV
     * @param mHh \f$ m_{H} \f$ in GeV, with \f$ m_H > m_h \f$
     * @param mHl \f$ m_{h} \f$ in GeV, with \f$ m_H > m_h \f$
     * @param alpha \f$ \alpha \f$
     * @return std::map<std::string_view, double> branching ratios and total
     * widths indexed by #r2hdmKeys
     */
    std::map<std::string_view, double> r2hdm_eft(Yukawa type, TanBeta tbeta,
                                             SquaredMassPar m12sq, AMass mA,
                                             HcMass mHp, HMass mHh, HMass mHl,
                                             MixAngle alpha, std::array<double,5> lambdas, std::array<double,9> yukawas_mod) const;

    //! Names of the branching ratios and total widths in n2hdmBroken()
    static constexpr std::array<std::string_view, 84> n2hdmBrokenKeys{
        "w_H1",        "b_H1_bb",     "b_H1_tautau", "b_H1_mumu",
        "b_H1_ss",     "b_H1_cc",     "b_H1_tt",     "b_H1_gg",
        "b_H1_gamgam", "b_H1_Zgam",   "b_H1_WW",     "b_H1_ZZ",
        "b_H1_ZA",     "b_H1_WpHm",   "b_H1_AA",     "b_H1_HpHm",
        "w_H2",        "b_H2_bb",     "b_H2_tautau", "b_H2_mumu",
        "b_H2_ss",     "b_H2_cc",     "b_H2_tt",     "b_H2_gg",
        "b_H2_gamgam", "b_H2_Zgam",   "b_H2_WW",     "b_H2_ZZ",
        "b_H2_ZA",     "b_H2_WpHm",   "b_H2_H1H1",   "b_H2_AA",
        "b_H2_HpHm",   "w_H3",        "b_H3_bb",     "b_H3_tautau",
        "b_H3_mumu",   "b_H3_ss",     "b_H3_cc",     "b_H3_tt",
        "b_H3_gg",     "b_H3_gamgam", "b_H3_Zgam",   "b_H3_WW",
        "b_H3_ZZ",     "b_H3_ZA",     "b_H3_WpHm",   "b_H3_H1H1",
        "b_H3_H1H2",   "b_H3_H2H2",   "b_H3_AA",     "b_H3_HpHm",
        "w_A",         "b_A_bb",      "b_A_tautau",  "b_A_mumu",
        "b_A_ss",      "b_A_cc",      "b_A_tt",      "b_A_gg",
        "b_A_gamgam",  "b_A_Zgam",    "b_A_ZH1",     "b_A_ZH2",
        "b_A_ZH3",     "b_A_WpHm",    "w_Hp",        "b_Hp_cb",
        "b_Hp_taunu",  "b_Hp_munu",   "b_Hp_us",     "b_Hp_cs",
        "b_Hp_tb",     "b_Hp_cd",     "b_Hp_ub",     "b_Hp_ts",
        "b_Hp_td",     "b_Hp_WH1",    "b_Hp_WH2",    "b_Hp_WH3",
        "b_Hp_WA",     "w_t",         "b_t_Wb",      "b_t_Hpb"};

    /**
     * @brief Run N2HDECAY in the broken phase of the N2HDM
     *
     * Parameters as defined in 1805.00966, Eq. (13)
     *
     * @param type Yukawa type
     * @param tbeta \f$ \tan\beta \f$
     * @param m12sq  \f$ m_{12}^2 \f$ in GeV\f$^2\f$
     * @param mA \f$ m_A \f$ in GeV
     * @param mHp \f$ m_{H^\pm} \f$ in GeV
     * @param mH1 \f$ m_{H_1} \f$ in GeV, with \f$m_{H_1} < m_{H_2}\f$
     * @param mH2 \f$ m_{H_2} \f$ in GeV, with \f$m_{H_1} < m_{H_2} < m_{H_3}\f$
     * @param mH3 \f$ m_{H_3} \f$ in GeV, with \f$m_{H_2} < m_{H_3}\f$
     * @param a1 \f$ \alpha_1 \f$
     * @param a2 \f$ \alpha_2 \f$
     * @param a3 \f$ \alpha_3 \f$
     * @param vs \f$ v_s \f$ in GeV
     * @return std::map<std::string_view, double> branching ratios and total
     * widths indexed by #n2hdmBrokenKeys
     */
    std::map<std::string_view, double>
    n2hdmBroken(Yukawa type, TanBeta tbeta, SquaredMassPar m12sq, AMass mA,
                HcMass mHp, HMass mH1, HMass mH2, HMass mH3, MixAngle a1,
                MixAngle a2, MixAngle a3, SingletVev vs) const;

    //! Names of the branching ratios and total widths in n2hdmDarkSinglet()
    static constexpr std::array<std::string_view, 65> n2hdmDarkSingletKeys{
        "w_H1",        "b_H1_bb",   "b_H1_tautau", "b_H1_mumu",   "b_H1_ss",
        "b_H1_cc",     "b_H1_tt",   "b_H1_gg",     "b_H1_gamgam", "b_H1_Zgam",
        "b_H1_WW",     "b_H1_ZZ",   "b_H1_ZA",     "b_H1_WpHm",   "b_H1_HDHD",
        "b_H1_AA",     "b_H1_HpHm", "w_H2",        "b_H2_bb",     "b_H2_tautau",
        "b_H2_mumu",   "b_H2_ss",   "b_H2_cc",     "b_H2_tt",     "b_H2_gg",
        "b_H2_gamgam", "b_H2_Zgam", "b_H2_WW",     "b_H2_ZZ",     "b_H2_ZA",
        "b_H2_WpHm",   "b_H2_HDHD", "b_H2_H1H1",   "b_H2_AA",     "b_H2_HpHm",
        "w_A",         "b_A_bb",    "b_A_tautau",  "b_A_mumu",    "b_A_ss",
        "b_A_cc",      "b_A_tt",    "b_A_gg",      "b_A_gamgam",  "b_A_Zgam",
        "b_A_ZH1",     "b_A_ZH2",   "b_A_WpHm",    "w_Hp",        "b_Hp_cb",
        "b_Hp_taunu",  "b_Hp_munu", "b_Hp_us",     "b_Hp_cs",     "b_Hp_tb",
        "b_Hp_cd",     "b_Hp_ub",   "b_Hp_ts",     "b_Hp_td",     "b_Hp_WH1",
        "b_Hp_WH2",    "b_Hp_WA",   "w_t",         "b_t_Wb",      "b_t_Hpb"};

    /**
     * @brief Run N2HDECAY in the dark singlet phase of the N2HDM
     *
     * Parameters as defined in 1805.00966, Eq. (17)
     *
     * @param type Yukawa type
     * @param tbeta \f$ \tan\beta \f$
     * @param m12sq  \f$ m_{12}^2 \f$ in GeV\f$^2\f$
     * @param mA \f$ m_A \f$ in GeV
     * @param mHp \f$ m_{H^\pm} \f$ in GeV
     * @param mHD \f$ m_{H_D} \f$ in GeV
     * @param mH1 \f$ m_{H_1} \f$ in GeV, with \f$ m_{H_1} < m_{H_2} \f$
     * @param mH2 \f$ m_{H_2} \f$ in GeV, with \f$ m_{H_1} < m_{H_2} \f$
     * @param alpha \f$ \alpha \f$
     * @param L7 \f$ \lambda_7 \f$
     * @param L8 \f$ \lambda_8 \f$
     * @return std::map<std::string_view, double>  branching ratios and total
     * widths indexed by #n2hdmDarkSingletKeys
     */
    std::map<std::string_view, double>
    n2hdmDarkSinglet(Yukawa type, TanBeta tbeta, SquaredMassPar m12sq, AMass mA,
                     HcMass mHp, DarkHMass mHD, HMass mH1, HMass mH2,
                     MixAngle alpha, Lambda L7, Lambda L8) const;

    //! Names of the branching ratios and total widths in n2hdmDarkDoublet()
    static constexpr std::array<std::string_view, 40> n2hdmDarkDoubletKeys{
        "w_H1",        "b_H1_bb",     "b_H1_tautau", "b_H1_mumu",
        "b_H1_ss",     "b_H1_cc",     "b_H1_tt",     "b_H1_gg",
        "b_H1_gamgam", "b_H1_Zgam",   "b_H1_WW",     "b_H1_ZZ",
        "b_H1_HDHD",   "b_H1_ADAD",   "b_H1_HDpHDm", "w_H2",
        "b_H2_bb",     "b_H2_tautau", "b_H2_mumu",   "b_H2_ss",
        "b_H2_cc",     "b_H2_tt",     "b_H2_gg",     "b_H2_gamgam",
        "b_H2_Zgam",   "b_H2_WW",     "b_H2_ZZ",     "b_H2_HDHD",
        "b_H2_H1H1",   "b_H2_ADAD",   "b_H2_HDpHDm", "w_HD",
        "b_HD_ZAD",    "b_HD_WpHDm",  "w_AD",        "b_AD_ZHD",
        "b_AD_WpHDm",  "w_HDp",       "b_HDp_WHD",   "b_HDp_WAD"};

    /**
     * @brief Run N2HDECAY in the inert doublet phase of the N2HDM
     *
     * Parameters as defined in 1805.00966, Eq. (21)
     *
     * @param mAD \f$ m_{A_D} \f$ in GeV
     * @param mHDp \f$ m_{H^\pm_D} \f$ in GeV
     * @param mHD \f$ m_{H_D} \f$ in GeV
     * @param mH1 \f$ m_{H_1} \f$ in GeV, with \f$ m_{H_1} < m_{H_2} \f$
     * @param mH2 \f$ m_{H_2} \f$ in GeV, with \f$ m_{H_1} < m_{H_2} \f$
     * @param alpha \f$ \alpha \f$
     * @param L8 \f$ \lambda_8 \f$
     * @param m22sq \f$ m_{22}^2 \f$ in GeV\f$^2\f$
     * @param vs \f$ v_s \f$
     * @return std::map<std::string_view, double>  branching ratios and total
     * widths indexed by #n2hdmDarkDoubletKeys
     */
    std::map<std::string_view, double>
    n2hdmDarkDoublet(DarkAMass mAD, DarkHcMass mHDp, DarkHMass mHD, HMass mH1,
                     HMass mH2, MixAngle alpha, Lambda L8, SquaredMassPar m22sq,
                     SingletVev vs) const;

    //! Names of the branching ratios and total widths in n2hdmDarkDoublet()
    static constexpr std::array<std::string_view, 25>
        n2hdmDarkSingletDoubletKeys{
            "w_Hsm",        "b_Hsm_bb",   "b_Hsm_tautau", "b_Hsm_mumu",
            "b_Hsm_ss",     "b_Hsm_cc",   "b_Hsm_tt",     "b_Hsm_gg",
            "b_Hsm_gamgam", "b_Hsm_Zgam", "b_Hsm_WW",     "b_Hsm_ZZ",
            "b_Hsm_HDDHDD", "b_Hsm_ADAD", "b_Hsm_HDpHDm", "b_Hsm_HDSHDS",
            "w_HDD",        "b_HDD_ZAD",  "b_HDD_WpHDm",  "w_AD",
            "b_AD_ZHDD",    "b_AD_WpHDm", "w_HDp",        "b_HDp_WHDD",
            "b_HDp_WAD"};

    /**
     * @brief Run N2HDECAY in the dark singlet and doublet phase of the N2HDM
     *
     * Parameters as defined in the N2HDECAY manual.
     *
     * @param mAD \f$ m_{A_D} \f$ in GeV
     * @param mHDp \f$ m_{H^\pm_D} \f$ in GeV
     * @param mHDD \f$ m_{H_D^D} \f$ in GeV
     * @param mHsm \f$ m_{H_\mathrm{SM}} \f$ in GeV
     * @param mHDS \f$ m_{H_D^S} \f$ in GeV, with \f$ m_{H_1} < m_{H_2} \f$
     * @param m22sq \f$ m_{22}^2 \f$ in GeV\f$^2\f$
     * @param mssq \f$ m_S^2 \f$ in GeV\f$^2\f$
     * @return std::map<std::string_view, double>  branching ratios and total
     * widths indexed by #n2hdmDarkSingletDoubletKeys
     */
    std::map<std::string_view, double>
    n2hdmDarkSingletDoublet(DarkAMass mAD, DarkHcMass mHDp, DarkHMass mHDD,
                            HMass mHsm, DarkHMass mHDS, SquaredMassPar m22sq,
                            SquaredMassPar mssq) const;

    //! Names of the branching ratios and total widths in rxsmBroken()
    static constexpr std::array<std::string_view, 26> rxsmBrokenKeys{
        "w_H1",        "b_H1_bb",     "b_H1_tautau", "b_H1_mumu",   "b_H1_ss",
        "b_H1_cc",     "b_H1_tt",     "b_H1_gg",     "b_H1_gamgam", "b_H1_Zgam",
        "b_H1_WW",     "b_H1_ZZ",     "b_H1_H2H2",   "w_H2",        "b_H2_bb",
        "b_H2_tautau", "b_H2_mumu",   "b_H2_ss",     "b_H2_cc",     "b_H2_tt",
        "b_H2_gg",     "b_H2_gamgam", "b_H2_Zgam",   "b_H2_WW",     "b_H2_ZZ",
        "b_H2_H1H1"};

    /**
     * @brief Run sHDECAY in the broken phase of the RxSM
     *
     * Parameters as defined in 1512.05355, Table 3
     *
     * @param alpha \f$ \alpha \f$
     * @param mH1 \f$ m_{H_1} \f$ in GeV, no mass order required
     * @param mH2 \f$ m_{H_2} \f$ in GeV, no mass order required
     * @param vs \f$ v_s \f$ in GeV
     * @return std::map<std::string_view, double> branching ratios and total
     * widths indexed by #rxsmBrokenKeys
     */
    std::map<std::string_view, double>
    rxsmBroken(MixAngle alpha, HMass mH1, HMass mH2, SingletVev vs) const;

    //! Names of the branching ratios and total widths in rxsmDark()
    static constexpr std::array<std::string_view, 13> rxsmDarkKeys{
        "w_H1",    "b_H1_bb", "b_H1_tautau", "b_H1_mumu",   "b_H1_ss",
        "b_H1_cc", "b_H1_tt", "b_H1_gg",     "b_H1_gamgam", "b_H1_Zgam",
        "b_H1_WW", "b_H1_ZZ", "b_H1_HDHD"};

    /**
     * @brief Run sHDECAY in the dark phase of the RxSM
     *
     * Parameters as defined in 1512.05355, end of Sec. 2.2. The parameter
     * \f$\lambda_S\f$ is irrelevant and not required.
     *
     * @param mh \f$ m_h \f$ in GeV
     * @param mHD \f$ m_{D} \f$ in GeV
     * @param mSsq \f$ m_S^2 \f$ in GeV\f$^2\f$
     * @return std::map<std::string_view, double> branching ratios and total
     * widths indexed by #rxsmDarkKeys
     */
    std::map<std::string_view, double> rxsmDark(HMass mh, DarkHMass mHD,
                                                SquaredMassPar mSsq) const;

    //! Names of the branching ratios and total widths in cxsmBroken()
    static constexpr std::array<std::string_view, 45> cxsmBrokenKeys{
        "w_H1",    "b_H1_bb", "b_H1_tautau", "b_H1_mumu",   "b_H1_ss",
        "b_H1_cc", "b_H1_tt", "b_H1_gg",     "b_H1_gamgam", "b_H1_Zgam",
        "b_H1_WW", "b_H1_ZZ", "b_H1_H2H2",   "b_H1_H2H3",   "b_H1_H3H3",
        "w_H2",    "b_H2_bb", "b_H2_tautau", "b_H2_mumu",   "b_H2_ss",
        "b_H2_cc", "b_H2_tt", "b_H2_gg",     "b_H2_gamgam", "b_H2_Zgam",
        "b_H2_WW", "b_H2_ZZ", "b_H2_H1H1",   "b_H2_H1H3",   "b_H2_H3H3",
        "w_H3",    "b_H3_bb", "b_H3_tautau", "b_H3_mumu",   "b_H3_ss",
        "b_H3_cc", "b_H3_tt", "b_H3_gg",     "b_H3_gamgam", "b_H3_Zgam",
        "b_H3_WW", "b_H3_ZZ", "b_H3_H1H1",   "b_H3_H1H2",   "b_H3_H2H2"};

    /**
     * @brief Run sHDECAY in the broken phase of the CxSM
     *
     * Parameters as defined in 1512.05355, Table 2
     *
     * @param a1 \f$ \alpha_1 \f$
     * @param a2 \f$ \alpha_2 \f$
     * @param a3 \f$ \alpha_3 \f$
     * @param mH1 \f$ m_{H_1} \f$ in GeV, no mass order required
     * @param mH3 \f$ m_{H_3} \f$ in GeV, no mass order required
     * @param vs \f$ v_s \f$ in GeV
     * @return std::map<std::string_view, double> branching ratios and total
     * widths indexed by #cxsmBrokenKeys
     */
    std::map<std::string_view, double> cxsmBroken(MixAngle a1, MixAngle a2,
                                                  MixAngle a3, HMass mH1,
                                                  HMass mH3,
                                                  SingletVev vs) const;

    //! Names of the branching ratios and total widths in cxsmDark()
    static constexpr std::array<std::string_view, 30> cxsmDarkKeys{
        "w_H1",    "b_H1_bb", "b_H1_tautau", "b_H1_mumu",   "b_H1_ss",
        "b_H1_cc", "b_H1_tt", "b_H1_gg",     "b_H1_gamgam", "b_H1_Zgam",
        "b_H1_WW", "b_H1_ZZ", "b_H1_H2H2",   "b_H1_H2HD",   "b_H1_HDHD",
        "w_H2",    "b_H2_bb", "b_H2_tautau", "b_H2_mumu",   "b_H2_ss",
        "b_H2_cc", "b_H2_tt", "b_H2_gg",     "b_H2_gamgam", "b_H2_Zgam",
        "b_H2_WW", "b_H2_ZZ", "b_H2_H1H1",   "b_H2_H1HD",   "b_H2_HDHD"};

    /**
     * @brief Run sHDECAY in the dark phase of the CxSM
     *
     * Parameters as defined in 1512.05355, Table 2
     *
     * @param alpha \f$ \alpha_1 \f$
     * @param mH1 \f$ m_{H_1} \f$ in GeV, no mass order required
     * @param mH2 \f$ m_{H_2} \f$ in GeV, no mass order required
     * @param mAD \f$ m_A \f$ in GeV
     * @param vs \f$ v_s \f$ in GeV
     * @param a1 \f$ a_1 \f$ in GeV\f$^3\f$
     * @return std::map<std::string_view, double> branching ratios and total
     * widths indexed by #cxsmDarkKeys
     */
    std::map<std::string_view, double> cxsmDark(MixAngle alpha, HMass mH1,
                                                HMass mH2, DarkAMass mAD,
                                                SingletVev vs,
                                                PotentialPar a1) const;

    //! Names of the branching ratios and total widths in c2hdm()
    static constexpr std::array<std::string_view, 66> c2hdmKeys{
        "w_H1",      "b_H1_bb",     "b_H1_tautau", "b_H1_mumu",   "b_H1_ss",
        "b_H1_cc",   "b_H1_tt",     "b_H1_gg",     "b_H1_gamgam", "b_H1_Zgam",
        "b_H1_WW",   "b_H1_ZZ",     "b_H1_WpHm",   "b_H1_HpHm",   "w_H2",
        "b_H2_bb",   "b_H2_tautau", "b_H2_mumu",   "b_H2_ss",     "b_H2_cc",
        "b_H2_tt",   "b_H2_gg",     "b_H2_gamgam", "b_H2_Zgam",   "b_H2_WW",
        "b_H2_ZZ",   "b_H2_ZH1",    "b_H2_WpHm",   "b_H2_H1H1",   "b_H2_HpHm",
        "w_H3",      "b_H3_bb",     "b_H3_tautau", "b_H3_mumu",   "b_H3_ss",
        "b_H3_cc",   "b_H3_tt",     "b_H3_gg",     "b_H3_gamgam", "b_H3_Zgam",
        "b_H3_WW",   "b_H3_ZZ",     "b_H3_ZH1",    "b_H3_ZH2",    "b_H3_WpHm",
        "b_H3_H1H1", "b_H3_H2H2",   "b_H3_H1H2",   "b_H3_HpHm",   "w_Hp",
        "b_Hp_cb",   "b_Hp_taunu",  "b_Hp_munu",   "b_Hp_us",     "b_Hp_cs",
        "b_Hp_tb",   "b_Hp_cd",     "b_Hp_ub",     "b_Hp_ts",     "b_Hp_td",
        "b_Hp_WH1",  "b_Hp_WH2",    "b_Hp_WH3",    "w_t",         "b_t_Wb",
        "b_t_Hpb"};

    /**
     * @brief Run c2hdm_HDECAY in the CP-violating (complex) 2HDM
     *
     * Parameters as defined in 1711.09419
     *
     * @param type Yukawa type
     * @param tbeta \f$ \tan\beta \f$
     * @param re_m12sq \f$ \Re{m_{12}^2} \f$
     * @param mHp \f$ m_{H^\pm} \f$ in GeV
     * @param mH1 \f$ m_{H_1} \f$ in GeV, with \f$ m_{H_1} < m_{H_2} <
     * m_{H_3}\f$ (which is calculated internally from the input)
     * @param mH2 \f$ m_{H_2} \f$ in GeV, with \f$ m_{H_1} < m_{H_2} <
     * m_{H_3}\f$ (which is calculated internally from the input)
     * @param a1 \f$ \alpha_1 \f$
     * @param a2 \f$ \alpha_2 \f$
     * @param a3 \f$ \alpha_3 \f$
     * @return std::map<std::string_view, double> branching ratios and total
     * widths indexed by #c2hdmKeys
     */
    std::map<std::string_view, double> c2hdm(Yukawa type, TanBeta tbeta,
                                             SquaredMassPar re_m12sq,
                                             HcMass mHp, HMassCPM mH1,
                                             HMassCPM mH2, MixAngle a1,
                                             MixAngle a2, MixAngle a3) const;
};

//! Thrown when an assumed mass hierarchy is violated.
class InvalidMassHierarchy : public std::runtime_error {
  public:
    /**
     * @brief Construct a new Invalid Mass Hierarchy object
     *
     * @param lightName name of the supposedly light mass
     * @param lightMass value of the supposedly light mass
     * @param heavyName name of the supposedly heavy mass
     * @param heavyMass value of the supposedly heavy mass
     */
    InvalidMassHierarchy(const std::string &lightName, double lightMass,
                         const std::string &heavyName,
                         double heavyMass) noexcept;
};

} // namespace AnyHdecay
